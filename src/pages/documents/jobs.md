import Layout from '../../components/layout'
import {dates, dates_offset, yyyymmdd} from '../../components/dates'

export const date = '2000'
export const title = "Job Descriptions"

<Layout title={title}>
<p>
	<b>
		32<sup>nd</sup> Legislative District Democratic Organization
	</b>
</p>
<h2>
	{date}
</h2>
<h1>
	{title}
</h1>

Duties & Responsibilities of District Officers

<ul className="menu">
<li><a href="#chair">01 – Chair</a></li>
<li><a href="#vice-chair">02 – Vice Chair</a></li>
<li><a href="#state">03 – State Committee Member</a></li>
<li><a href="#state-2">04 – State Committee Member 2</a></li>
<li><a href="#county">05 – County Democrats Executive-Board Delegate 1</a></li>
<li><a href="#county-2">06 – County Democrats Executive-Board Delegate 2</a></li>
<li><a href="#county-alt">07 – County Democrats Executive-Board Alternate 1</a></li>
<li><a href="#county-alt-2">08 – County Democrats Executive-Board Alternate 2</a></li>
<li><a href="#secretary">09 – Recording Secretary</a></li>
<li><a href="#treasurer">10 – Treasurer</a></li>
<li><a href="#newsletter">11 – Newsletter Editor</a></li>
<li><a href="#election-board">12 – Election-Board Coordinator</a></li>
<li><a href="#fundraiser">13 – Fundraiser</a></li>
<li><a href="#membership">14 – Membership Officer</a></li>
<li><a href="#campaigns">15 – District Campaigns Coordinator</a></li>
<li><a href="#sergeant-at-arms">16 – Sergeant-at-Arms</a></li>
<li><a href="#parliamentarian">17 – Parliamentarian</a></li>
</ul>
<pre className='article'>
<h1 id='chair'><a href='#chair'>1. Chair</a></h1>

The Chair shall be the 32nd Legislative District Democratic Organization and carry out those duties assigned by the Executive Board or the membership. They shall chair all district General-membership and Executive Board meetings and be responsible for setting the proposed agendas for these meetings. In addition, the Chair shall:
a. Be a voting member of the Executive Board for the 32nd Le District Democratic Organization;
b. Serve on the County Democrats Executive Board and attend all of their meetings;
c. Attend all meetings of the Washington State Organization of Chairs and Vice Chairs and all meetings of the King County District Chairs Mce Chairs/Officers;
d. Attend any special meetings for the First Congressional District and the Seventh Congressional District called by the State Party, county parties, district organizations, and/or elected Democratic federal officials;
e. Ensure attendance/representation at all required meetings by giving sufficient notice to their alternate(s) if they are unable to attend a specific meeting or event:
f. Be a member of the Budget Committee;
g. Be a member of the Members Committee, helping to develop a strategic plan for recruiting PCOs;
h. Be a member of the Legislative Liaison Committee;
I. Review all meeting Minutes prior to publication and distribution;
j. Act as a signer on all district accounts but only to disburse funds authorized by the membership;
k. Appoint all committee chairs, subject to ratification by the Executive Board, and establish special committees as
needed;
l. Supervise the 32nd Legislative District's Democratic precinct caucuses and the Legislative District caucus by working with the fourteen area teams and the King County Caucus Coordinator, and by serving as the Temporary Chair of the 32nd Legislative District Democratic Caucus and appointing the chairs for Affirmative- Action, Credentials, Platform, and Rules committees, subject to LD Caucus ratification;
m. Perform no tasks not specifically referenced in the organization's Bylaws, these Duties & Responsibilities or
authorized by either the membership or by the Executive Board.
<h1 id='vice-chair'><a href='#vice-chair'>2. Vice Chair</a></h1>

The Vice Chair shall serve in the absence of the Chair and shall carry out those duties assigned by the Chair, the Executive Board, or the membership. In addition, the Vice Chair shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve as alternate for the Chair (and as third alternate for the 2 Delegates) to the County Democrats Executive Board end should attend one of their meetings every three months:
c. Attend all meetings of the Washington State Organization of Chairs and Vice Chairs and all meetings of the King County District Chairs Mce Chairs/Officers;
d. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine which caucuses can and will be held in either a PCO's or a neighboring home, and (3) book the Area Caucus site;
e. Be the Calendar Coordinator for the 32nd Legislative District Democratic Organization:
f. Be a member of the: Program Committee, Budget Committee, and Legislative Liaison Committee;
g. Be a member of the Fundraising Committee, helping to develop strategic plans to improve and to enhance fundraising for the District's Campaign Fund;
h. Serve as Acting Chair for all newly formed committees and for standing committees that do not have a chair until a new chair is designated/appointed
i. Act as signer on all district accounts but only to disbursements authorized by the membership
j. Assist the Chair to coordinate activities with County Democrats and the WSDCC, such as the annual Jefferson-Jackson Day Dinner, district, county, or state Campaign Training School(s); or candidate fundraisers.
<h1 id='state'><a href='#state'>3. State Committee Member</a></h1>

The State Committee Member shall be the District organization voting member of the WSDCC. They shall attend all regular and special meetings of the 3CC and shall always have a signed proxy (per 3CC laws) at the ready should they either be unable to attend a meeting or if they will be missing any portion of a scheduled meeting. In conjunction with State Committee Member 2, they will give an oral and/or written report at all General-membership and Executive Board meetings, along with providing information to the Newsletter Editor for insertion in the District Newsletter. They shall carry out any additional duties assigned by the Executive Board or by the membership. In addition, the State Committee Member shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on the County Democrats Executive Board as a nonvoting member;
c. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to
(1) determine which PCOs will be hosting their precinct caucuses,
(2) determine which caucuses can and will be held in either a PCO's or a neighboring home, and
(3) book the Area Caucus site;
d. Be a member of the Legislative Liaison Committee.
<h1 id='state-2'><a href='#state-2'>4. State Committee Member 2</a></h1>

State Committee Member 2 shall be the District organizations male voting member of the WSDCC. They shall attend all regular and special meetings of the WSDCC and shall always have assigned proxy (per WSDCC Bylaws) at the ready should they either be unable to attend a meeting or if they will be missing any portion of a scheduled meeting. In conjunction with State Committee Member 1, they will give an oral and/or written report at all General-membership and Executive Board meetings, along with providing Information/articles to the Newsletter Editor for insertion in the District Newsletter. They shall carry out any additional duties assigned by the Executive Board or by the membership. In addition, State Committee Member 2 shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on the County Democrats Executive Board as a nonvoting member;
c. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine wt caucuses can and will be held in either a PCO's or a neighboring home, and (3) book the Area Caucus site;
d. Be a member of the Legislative Liaison Committee.
<h1 id='county'><a href='#county'>5. County Democrats Executive-Board Delegate 1</a></h1>

County Democrats Executive-Board Delegate 1 shall be the District organization's female voting member on the County Democrats Executive Board. They shall attend all regular and spec meetings of the County Democrats Executive Board and will arrange for an alternate to take her piece whenever they are unable to attend all or part of a County Democrats Executive Board meeting. In conjunction with the Delegate 2, they will give an oral and/or written report at all General-membership and Executive Board meetings, along with providing information articles to the Newsletter Editor for insertion in the District Newsletter. They shall carry out any additional duties assigned by the Executive Board or the membership. In addition, County Democrats Executive-Board Delegate 1 shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine which caucuses can and will be held in either a PCO's or a neighboring home, and (3) book the Area Caucus site.
<h1 id='county-2'><a href='#county-2'>6. County Democrats Executive-Board Delegate 2</a></h1>

County Democrats Executive-Board Delegate 2 shall be the District organization's male voting member on the County Democrats Executive Board. They shall attend all regular and special meetings of the County Democrats Executive Board and w arrange for an alternate to take his place whenever they are unable to attend all or pert of a County Democrats Executive Board meeting. In conjunction with the Delegate 1, they will give an oral and/or written report at all General membership and Executive Board meetings, along with providing information/articles to the Newsletter Editor for insertion In the District Newsletter. They shall carry out any additional duties assigned by the Executive Board or the membership. In addition, County Democrats Executive-Board Delegate 1 shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine which caucuses can and wilt be held In either a PCO's or a neighboring home, and (3) book the Area Caucus site.
<h1 id='county-alt'><a href='#county-alt'>7. County Democrats Executive-Board Alternate 1</a></h1>

County Democrats Executive-Board Alternate 1, along with the Alternate 2, shall be the District organization's chief ombudsman, serving as a liaison between the County Democrats and District executive boards and the members of the District organization. They serves as not only the alternate to the County Democrats Executive-Board Delegate 1 but as second alternate to County Democrats Executive-Board Delegate 2 and as one of the two second alternates for the Chair; they should attend one County Democrats Executive Board meeting every three months. They shall carry out any additional duties as assigned by the Executive Board or the membership. In addition, County Democrats Executive-Board Alternate 1 shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to
(1) determine which PCOs will be hosting their precinct caucuses,
(2) determine which caucuses can and will be held In either a PCO's or a neighboring home, and
(3) book the Area Caucus site.
<h1 id='county-alt-2'><a href='#county-alt-2'>8. County Democrats Executive-Board Alternate 2</a></h1>

County Democrats Executive-Board Alternate 2, along with the Alternate 1, shall be the District organization's chief ombudsman, serving as a liaison between the County Democrats and District executive boards and the members of the District organization. They serve as not only the alternate to County Democrats Executive-Board Delegate 2 but as second alternate to County Democrats Executive-Board Delegate 1 and as one of the two second alternates for the Chair; they should attend one County Democrats Executive Board meeting every three months. They shall carry out any additional duties as assigned by the Executive Board or the membership. In addition, County Democrats Executive-Board Alternate 2 shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization:
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to
(1) Determine which PCOs will be hosting their precinct caucuses,
(2) Determine which caucuses can and will be held in either a PCO's or a neighboring home, and
(3) Book the Area Caucus site.
<h1 id='secretary'><a href='#secretary'>9. Recording Secretary</a></h1>

The Recording Secretary is responsible for taking Minutes at aft District General membership and Executive Board meetings. They shall provide a draft of the Minutes to the Chair for their concurrence/input and then provide a final copy of General-membership Minutes to the Newsletter Editor by the deadline for inclusion in the next District Newsletter and provide a final copy of the Executive Board Minutes to all Executive Board members for approval at their meeting.
They shall recommend to the Chair additional member(s) for appointment as Assistant Secretary) to assist her/him and to serve in their absence. They shall carry out any additional duties assigned by the Executive Board or the membership.
In addition, the Recording Secretary shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine which caucuses can and with be held in either a PCO's or a neighboring home, and (3) book the Area Caucus site:
c. Circulate a Sign-In Sheet at all General-membership meetings to keep a record of those members who were in
attendance, along with any guests, and forward a copy to the Membership Officer;
d. Sign PCO petitions approved by the membership and, after the Chair has signed them, forward said petitions to
the County Chair for final approval and appointment;
e. Nominate Correspondence-Secretary candidate(s) for appointment by the Chair when the need arises;
f. Receive any recap petitions where the Chew is the subject of the Recall.
<h1 id='treasurer'><a href='#treasurer'>10. Treasurer</a></h1>

The Treasurer shall be responsible for the safekeeping of all funds of the 32nd Legislative District Democratic Organization, including but not limited to its General Operation Fund and Campaign Fund accounts. They shall prepare the Washington Public Disclosure Committee (PDC) forms for both accounts by the tenth day of the following month, twenty-one days before the Primary Election, seven days before the General Election, and at all other deadline dates established by the PDC. The Treasurer shall prepare a report for all Executive Board meetings, and they shall make oral and/or written reports at all General-membership meetings (a copy of wt will appear in the next District Newsletter); these reports shall include ending balances for all accounts and the total number of paid memberships. They shall recommend to the Chair an additional member for appointment as Comptroller (Assistant Treasurer) who shall track income and expenses for the District organization to ensure spending does not exceed the approved Annual Budget (Article XI, Section 2) and prepare a monthly spreadsheet for inclusion in the Treasurer report. The Treasurer shall carry out any additional duties assigned by the Executive Board or the membership. In addition, the Treasurer shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses (2) determine which caucuses can and will be held in either a PCO's or a neighboring home, and (3) book the Area Caucus site;
c. Maintain the District records of donations received and memberships paid;
d. Provide the Membership Officer with the names/addresses/phone numbers and the amount of dues paid at a mutually agreed-upon interval;
e. Be a member of the: Budget Committee and Fundraising Committee.
<h1 id='newsletter'><a href='#newsletter'>11. Newsletter Editor</a></h1>

The Newsletter Editor is responsible for the production and mailing of the monthly District Newsletter: collecting material, organizing it into a newsletter format, having copies printed, and ensuring it is mailed in a timely manner — meeting all Bylaws time requirements that define adequate notice for meetings and specific business Items (such as Bylaws amendments, resolutions, Motions to Recall, etc.). They will establish and make known their production schedule and deadline date for receipt of items to be included In the Newsletter, and they will contact the Vice Chair once a month for all District Calendar information. They w coordinate with the Treasurer to ensure that copying and mailing costs are covered, so as not to hinder the timely receipt of each issue. The Editor will have final editorial control of the Newsletter, but the Executive Board may establish editorial guidelines to be followed, The Editor will provide an advertising rate schedule to the Executive Board for approval. They shall carry out any additional duties assigned by the Executive Board or the membership. In addition, the Newsletter Editor shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine which caucuses can and will be held in either a PCO's or a neighbor's home, and (3) book the Area Caucus site;
c. Recruit and coordinate the Ne Committee;
d. Maintain the District organizations mailing list
e. Be a member of the Budget Committee.
f. Be a member of the: Budget Committee and Fundraising Committee.
<h1 id='election-board'><a href='#election-board'>12. Election-Board Coordinator</a></h1>

The Election-Board Coordinator is responsible for the annual recruitment of Democratic Election Inspectors and Judges assigned to work at the polling places for all precincts of the 32nd Legislative District from July 1 through the following June 30. They receives all nominations submitted by District PCOs. They coordinates with the Chair, the County Democrats Chief Election-Board Officer, and the Democratic Recruiters at King County Records and Elections to ensure that all polling places have qualified Election-Board workers who are Democrats serving in those slots assigned to the Democratic Party.
They will attend all meetings called by the County Democrats Chair, and they are responsible for the District organization meeting the deadline dates established by the County Democrats Chair and King County Records and Elections. They shall carry out any additional duties assigned by the Executive Board or the membership. In addition, the Election-Board Coordinator shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine which caucuses can and will be held in either a PCO's or a neighboring home, and (3) book the Area Caucus site;
c. Recruit and chair the telephoning committee that verifies and recruits Election Board workers;
d. Assist the Democratic Recruiters at King County Records and Elections to till vacancies when they occur during the year.
<h1 id='fundraiser'><a href='#fundraiser'>13. Fundraiser</a></h1>

The Fundraiser is responsible for planning and executing all fundraising efforts for the 32nd Legislative District Democratic Organization. They shall carry out any additional duties assigned by the Executive Board or the membership.
In addition, the Fundraiser shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to
(1) determine which PCOs will be hosting their precinct caucuses,
(2) determine which caucuses can and will be held in either a PCO's or a neighboring home, and
(3) book the Area Caucus site;
c. Serve as Chair of the Fundraising Committee;
d. Coordinate with the Events Committee chair when appropriate;
e. Coordinate with the Membership Officer and their Hospitality Subcommittee chair on the planning of the annual Picnic and Holiday Party as regards raffles, auctions, and other fundraising activities planned these events.

<h1 id='membership'><a href='#membership'>14. Membership Officer</a></h1>

The Membership Officer is responsible for all membership-recruitment efforts of the District organization. As chair of the Membership Committee. They are charged with actively promoting opportunities for Democrats to join the 32nd Legislative District Democratic Organization. The Membership Committee's responsibility is to attract and keep members. The Membership Officers tasks are to:
(1) Work with the Publicity Committee to make Democrats aware that our organization exists, when and where it meets, and what our goals are;
(2) Encourage pride in being a Democrat and work to make our meetings a strong, positive place for Democrats to meet and effect change in local, state, and national political arenas;
(3) Establish and maintain the Hospitality Subcommittee, whose purpose s to ensure all District activities are fun, welcoming activities that promote goodwill and discourage petty arguing;
(4) Provide timely and relevant information regarding the benefits of being a member of the 32nd Legislative Democratic Organization.
They shall carry out any additional duties assigned by the Executive Board or the membership. In addition, the Membership Officer shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine which caucuses can and will be held in either a PCO's or a neighboring home, and (3) book the Area Caucus site;
c. Be a member of the Budget Committee
d. Call regular meetings of the Membership Committee to plan membership-recruitment efforts
e. Update the District organization's Membership Brochure, distribute and maintain the Talent Bank forms, and ensure the membership form is published in the District Newsletter with up-to-date information;
f. Coordinate with the Treasurer in order to maintain a Current Membership list and an up-to-date count of members (PCOs, General Members, and Associate Members) and to ensure that new members:
(1) have a name tag at the next meeting,
(2) appear on the next Credentials list, and
(3) receive the next District Newsletter,
g. Work with the Chair, Recording Secretary, Treasurer, Newsletter Editor, and District Campaigns Coordinator to accurately track and maintain membership growth in the organization;
h. Assist the Chair and the Training Committee with PCO recruitment and training;
i. Provide the Newsletter Editor with the names and addresses of aft new members and all changes of address;
j. Provide the District Campaigns Coordinator with meeting-attendance information that they, in turn, will pass on to the Area Coordinators;
k. Nominate the chat of the Hospitality Subcommittee for appointment by the Chair.

<h1 id='campaigns'><a href='#campaigns'>15. District Campaigns Coordinator</a></h1>

The District Campaigns Coordinator is responsible for the establishment and maintenance of the Area Coordinator system for ‘the distribution of campaign literature, sample-ballot masters, and District Campaign places. They shall develop and maintain the District organization's Telephone Tree. They shall carry out any additional duties assigned by the Executive Board or the membership In addition, the District Campaigns Coordinator shall:
a. Be a voting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to (1) determine which PCOs will be hosting their precinct caucuses, (2) determine which caucuses can and will be held In either a PCO's or a neighboring home, and (3) book the Area Caucus site;
c. Serve as a member of the Legislative Liaison Committee;
d. Notify Area Coordinators about less-active members in order that they may be contacted, informed of upcoming meetings/programs/events, and encouraged to attend.
<h1 id='sergeant-at-arms'><a href='#sergeant-at-arms'>16. Sergeant-at-Arms</a></h1>

The Sergeant-at-Arms shall maintain decorum at all District meetings where they are in attendance. They shall recommend to the Chair additional member(s) for appointment as Corporal(s)-at-Arms to assist her/him and to serve in their absence. They shall carry out any additional duties assigned by the Executive Board or the membership. In addition, the Sergeant-at-Arms shall:
a. Be a nonvoting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Lead the membership in the Flag Salute at all District General-membership meetings;
c. Serve as Timekeeper for alt endorsement meetings;
d. Serve as chair of the Credentials Committee;
e. Be custodian of the membership name tags.
<h1 id='parliamentarian'><a href='#parliamentarian'>17. Parliamentarian</a></h1>

The Parliamentarian shall serve as a resource person for the Chair. They shall be acquainted with Roberts Rules of Order and will provide the Chair with opinions based on that knowledge. They will only respond to requests from the Chair. They shall carry out any additional duties assigned by the Executive Board or the membership. In addition, the Parliamentarian shall:
a. Be a nonvoting member of the Executive Board for the 32nd Legislative District Democratic Organization;
b. Serve as Chair of the Ballot Committee.
Delegates to the County Democrats Legislative Action Committee are expected to:
a. Attend all LAC meetings or notify the district Alternate when they cannot
b. Report to the membership on a regular basis through periodic reports and informational articles in the District Newsletter;
c. Attend all Legislative Liaison Committee meetings.
Area Coordinators are expected to:
a. Assist the District Campaigns Coordinator in keeping the District Telephone Tree up-to-date;
b. Distribute campaign literature and sample ballots (or sample-ballot masters) to the PCOs in their Area;
c. Serve on one of the fourteen two-person Precinct-caucus area teams whose responsibility is to
(1) determine which PCOs will be hosting precinct caucuses,
(2) determine which caucuses can and will be held in either a PCO's or a neighboring home, and 
(3) book the Area Caucus site.
</pre>
</Layout>
