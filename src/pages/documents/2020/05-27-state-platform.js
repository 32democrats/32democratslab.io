import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'
import {dates} from '../../../components/dates'
const date = '2020-05-27'
const {day, month_name, year} = dates(date)
const type = 'State Platform Report'
export default () => (
	<Layout>
		<Helmet>
			<title>{date} {type}</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			{type}, {month_name} {day}, {year}
		</h1>
		<p><em>Gary McCaig, 32nd LD representative to the Washington State Democratic Convention Platform Committee</em></p>
<p>The Platform Committee members accomplished a lot in spite of some major changes and challenges:</p>
<ul>
<li><p>Zoom meetings instead of in person discussion during the “off” hours and dealing with different technological challenges and knowledge</p></li>
<li><p>Merging multiple individual planks into 7 and then 8 planks</p>
<ul>
<li><p>Social Justice</p></li>
<li><p>Labor and Economic Justice</p></li>
<li><p>Environmental Justice</p></li>
<li><p>Education</p></li>
<li><p>Health Care and Human Services</p></li>
<li><p>Government</p></li>
<li><p>Criminal Justice Reform</p></li>
<li><p>Tribal Relations and Sovereignty</p></li>
</ul></li>
<li><p>Usual lack of time to craft each plank equally well due to the diverse perspectives, passions, and knowledge [Saturday’s meeting took over three time the original scheduled time]</p></li>
<li><p>Usual late start by the State, in part due to the virus</p></li>
</ul>
<p>Each platform representative was assigned to primarily work on one plank. I was:</p>
<ul>
<li><p>Initially assigned to Government</p></li>
<li><p>Reassigned to new Criminal Justice Reform plank.</p></li>
</ul>
<p>Throughout the process I have tried to keep the LD members informed. I have posted multiple links to google documents of our <strong>Work-In-Progress</strong> and used FB to announce postings and updates.</p>
<p>As you review the platform please keep in mind that we did our best to follow these guidelines:</p>
<ul>
<li><blockquote>
<p>Recommend that we are concise and to the point to make this a more manageable/readable/useful document.</p>
</blockquote></li>
<li><blockquote>
<p>The goal was also to craft (wordsmith?) each item to maximize its acceptance by State Democrats and to minimize the ability to the weaponization of the platform by opponents.</p>
</blockquote></li>
<li><blockquote>
<p>The Platform should contain:</p>
</blockquote>
<ul>
<li><blockquote>
<p>A One Page Preamble;</p>
</blockquote></li>
<li><blockquote>
<p>A <strong>short</strong> Executive Summary for each plank;</p>
</blockquote></li>
<li><blockquote>
<p>Eight Pillars/Planks at 3 pages each, including:</p>
</blockquote>
<ul>
<li><blockquote>
<p>We Believe statements: 10 to 15;</p>
</blockquote></li>
<li><blockquote>
<p>We Call For statements: 30 for each local, state and federal level column;</p>
</blockquote></li>
<li><blockquote>
<p>We Oppose statements: 10 to 15;</p>
</blockquote></li>
</ul></li>
</ul></li>
<li><blockquote>
<p>We did not meet our goal. [see above]</p>
</blockquote></li>
</ul>
<p><strong>Next Steps:</strong></p>
<ul>
<li><blockquote>
<p>I will post updates as soon as I can after receiving them</p>
</blockquote></li>
<li><blockquote>
<p>From the State:</p>
</blockquote>
<ul>
<li><blockquote>
<p>If any one person has input - that should be worked through their elected convention delegate(s).</p>
</blockquote></li>
<li><blockquote>
<p>Delegates have 4 days to provide proposed revisions to the proposed platform.</p>
</blockquote></li>
<li><blockquote>
<p>We will meet one additional time to decide if provided revisions should be included in the platform as our majority opinion, have sufficient support (35%) for a minority opinion to be written and prepared to be voted on by the full convention, or die in committee by lack of support (&lt;35%). (Date/Time of our meeting TBD)</p>
</blockquote></li>
<li><blockquote>
<p>The final Majority Report (final platform) is distributed to all delegates, along with any minority reports that were passed out of our committee.</p>
</blockquote></li>
<li><blockquote>
<p>Platform goes to state convention, where delegates vote on the platform and any minority reports.</p>
</blockquote></li>
</ul></li>
</ul>
	</Layout>
)
