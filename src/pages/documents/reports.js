import React from 'react'
import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import Layout from '../../components/layout'

const title = 'Reports'
export default () => (
	<Layout>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<h1>{title}</h1>
		<ul className="menu">
			<li>
				<Link to="/endorsements/reports">Endorsement Committee Reports</Link>
			</li>
			<li>
				<Link to="/documents/2020/05-27-state-platform">2020-05 State Platform Report</Link>
			</li>
		</ul>
	</Layout>
)
