import React from 'react'
import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import { base } from '../../config'
import Layout from '../../components/layout'

const title = 'Documents'
export default () => (
	<Layout>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<h1 className="title">{title}</h1>
		<ul className="menu">
			<li><Link to="/documents/reports">Reports</Link></li>
		</ul>
		<hr/>
		<ul className="menu">
			<li><a href="/doc/2020/election-rights-wa-2020.pdf">Know Your Rights, Know the Law</a></li>
			<li><Link to="/documents/big-tent">Big Tent Democratic Party</Link></li>
			<li><Link to="/bylaws">Bylaws</Link></li>
			<li><Link to="/civility-pledge">Civility Pledge</Link></li>
			<li><Link to="/endorsements">Endorsements Info & Archive</Link></li>
			<li><Link to="/documents/jobs">Job Descriptions</Link></li>
			<li><Link to="/minutes">Minutes</Link></li>
			<li><Link to="/mission-values">Mission & Values</Link></li>
			<li><Link to="/platform/2018">Platform</Link></li>
			<li><Link to="/resolutions">Resolutions</Link></li>
			<li><Link to="/resolutions/help">Resolution drafting help</Link></li>
			<li><Link to="/rules">Standing Rules</Link></li>
			<li><a href='/doc/2019/WSDCC-Advocacy.pdf'>WSDCC Advocacy report & 2020 Legislative Priorities FAQ</a></li>
			<li><a href={base+"/documents/"}>...old documents</a></li>
		</ul>
	</Layout>
)
