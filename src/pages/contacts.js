import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
// import img_officers from '../../static/img/2019/officers.jpg'
import Layout from '../components/layout'

// const td = <td></td>
const td2 = [<td></td>,<td></td>]
const email = <span role="img" aria-label="Email" title="Email">📧</span>
const fb = <b className="facebook" role="img" aria-label="Facebook" title="Facebook">f</b>
// const web = <span role="img" aria-label="Website" title="Website" className="fade no-print">🏠</span>
// status
const appointed = <td>Appointed & Ratified</td>
const elected = <td>Elected</td>
// group
const ad_hoc = <td>Ad Hoc Committee</td>
const exec = <td>Executive Board</td>
// Executive Board
const no_vote = <td>No Vote</td>
const vote = <td>Vote</td>

const Alan_Charnley = [<td>Alan Charnley</td>,<td><div><a href="mailto:Alan.Charnley@32democrats.org">{email}</a><a href="https://www.facebook.com/alan.charnley.39">{fb}</a></div></td>]
const Brent_McFarlane = [<td>Brent McFarlane</td>,<td></td>]
const Carin_Chase = [<td>Carin Chase</td>,<td><div><a href="mailto:Carin.Chase@32democrats.org">{email}</a><a href="https://www.facebook.com/carin.chase">{fb}</a></div></td>]
const Cathy_Baylor = [<td>Cathy Baylor</td>,<td><div><a href="mailto:Cathy.Baylor@32democrats.org">{email}</a><a href="https://www.facebook.com/cathy.baylor">{fb}</a></div></td>]
const Chris_Roberts = [<td>Chris Roberts</td>,<td><div><a href="mailto:Chris.Roberts@32democrats.org">{email}</a><a href="https://www.facebook.com/ceroberts">{fb}</a></div></td>]
// const Carol_McMahon = [<td>Carol McMahon</td>,<td><div><a href="mailto:Carol.McMahon@32democrats.org">{email}</a><a href="https://www.facebook.com/carol.mcmahon.750">{fb}</a></div></td>]
const Carolyn_Ahlgreen = [<td>Carolyn Ahlgreen</td>,<td><div><a href="mailto:CAhlgreen@32democrats.org@32democrats.org">{email}</a><a href="https://www.facebook.com/carolyn.ahlgreen">{fb}</a></div></td>]
const Colin_Cole = [<td>Colin Cole</td>,<td><div><a href="mailto:Colin.Cole@32democrats.org">{email}</a></div></td>]
const Dean_Fournier = [<td>Dean Fournier</td>,<td><a href="mailto:demdean48@gmail.com">{email}</a></td>]
const Deborah_Viertel = [<td>Deborah Viertel</td>,<td><div><a href="mailto:deb.viertel@32democrats.org">{email}</a><a href="https://www.facebook.com/Pr1ss">{fb}</a></div></td>]
const Eric_Valpey = [<td>Eric Valpey</td>,<td><div><a href="mailto:Eric.Valpey@32democrats.org">{email}</a><a href="https://www.facebook.com/valpey">{fb}</a></div></td>]
const Gray_Petersen = [<td>Gray Petersen</td>,<td><div><a href="mailto:Gray.Petersen@32democrats.org">{email}</a><a href="https://www.facebook.com/graywpetersen">{fb}</a></div></td>]
const Jenna_Nand = [<td>Jenna Nand</td>,<td><div><a href="mailto:Jenna.Nand@32democrats.org">{email}</a><a href="https://www.facebook.com/jenna.nand">{fb}</a></div></td>]
const Lael_White = [<td>Lael White</td>,<td><div><a href="mailto:Lael.White@32democrats.org">{email}</a><a href="https://www.facebook.com/laelc.white">{fb}</a></div></td>]
const Lillian_Hawkins = [<td>Lillian Hawkins</td>,<td><div><a href="mailto:Lillian.Hawkins@32democrats.org">{email}</a></div></td>]
// const Raphael_Baltuth = [<td>Raphael Baltuth</td>,<td><div><a href="mailto:Raphael.Baltuth@32democrats.org">{email}</a><a href="http://raphael.baltuth.com/">{web}</a></div></td>]
const Robert_Petersen = [<td>Robert Petersen</td>,<td><div></div></td>]
const Rosamaria_Graziani = [<td>Rosamaria Graziani</td>,<td><div><a href="mailto:Rosamaria.Graziani@32democrats.org">{email}</a><a href="https://www.facebook.com/rosamaria.graziani.33">{fb}</a></div></td>]
const Sally_Soriano = [<td>Sally Soriano</td>,<td><div><a href="mailto:sally@sallysoriano.org">{email}</a><a href="https://www.facebook.com/sally.soriano.733">{fb}</a></div></td>]
// const Stephanie_Harris = [<td>Stephanie Harris</td>,<td><div><a href="mailto:Stephanie.Harris@32democrats.org">{email}</a><a href="https://www.facebook.com/profile.php?id=524953357">{fb}</a></div></td>]
const Trudy_Bialic = [<td>Trudy Bialic</td>,<td><a href="mailto:Trudy.Bialic@32democrats.org">{email}</a></td>]
const Winona_Hauge = [<td>Winona Hauge</td>,<td></td>]

export default () => (
	<Layout>
		<Helmet>
			<title>Officers & Contacts</title>
		</Helmet>
		<h1>Officers & Contacts</h1>
		<p>
			2021–2022
			<br />
			32nd District Democrats Executive Board and Committee Chairs
		</p>
		<table className="contacts">
			<tr>
				<th>Office</th>
				<th>Name</th>
				<th>Links</th>
				<th>Process</th>
				<th>Group</th>
				<th>Executive Board</th>
			</tr>
			<tr><td><Link to="/documents/jobs#chair">Chair</Link></td>{Chris_Roberts}{elected}{exec}{vote}</tr>

			<tr><td><Link to="/documents/jobs#vice-chair">1st Vice Chair</Link></td>{Jenna_Nand}{elected}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#vice-chair">2nd Vice Chair</Link></td>{Lillian_Hawkins}{elected}{exec}{vote}</tr>

			<tr><td><Link to="/documents/jobs#state">State Committee 1</Link></td>{Alan_Charnley}{elected}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#state-2">State Committee 2</Link></td>{Carin_Chase}{elected}{exec}{vote}</tr>

			<tr><td><Link to="/documents/jobs#county">KCDCC Committee 1</Link></td>{Dean_Fournier}{elected}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#county-alt">KCDCC Alternate 1</Link></td>{Carolyn_Ahlgreen}{elected}{exec}{vote}</tr>
			
			<tr><td><Link to="/documents/jobs#county">SCDCC Committee 1</Link></td>{Rosamaria_Graziani}{elected}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#county-2">SCDCC Committee 2</Link></td>{Robert_Petersen}{elected}{exec}{vote}</tr>
			
			<tr><td><Link to="/documents/jobs#county-alt">SCDCC Alternate 1</Link></td>{Lael_White}{elected}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#county-alt-2">SCDCC Alternate 2</Link></td>{Colin_Cole}{elected}{exec}{vote}</tr>

			<tr><td>Bylaws and Rules</td>{td2}{appointed}{ad_hoc}{no_vote}</tr>
			<tr><td><Link to="/rules/#S10">Communications 1</Link></td>{Deborah_Viertel}{appointed}{exec}{vote}</tr>
			<tr><td><Link to="/rules/#S10">Communications 2</Link></td>{Sally_Soriano}{appointed}{exec}{vote}</tr>
			<tr><td>Community Service</td>{td2}{appointed}{exec}{vote}</tr>
			<tr><td><Link to="/rules/#S9.3">Correspondence</Link></td>{td2}{appointed}{ad_hoc}{no_vote}</tr>
			<tr><td><Link to="/documents/jobs#campaigns">District Campaigns</Link></td>{td2}{appointed}{exec}{vote}</tr>
			<tr><td>Diversity and Inclusion</td>{Winona_Hauge}{appointed}{exec}{vote}</tr>
			<tr><td><Link to="/rules/#S2.5">Endorsement Committee</Link></td>{td2}{appointed}{ad_hoc}{no_vote}</tr>
			<tr><td>Environmental Caucus</td>{td2}{appointed}{ad_hoc}{no_vote}</tr>
			<tr><td><Link to="/documents/jobs#fundraiser">Fundraising</Link></td>{td2}{appointed}{exec}{vote}</tr>
			<tr><td>Homeless Issues Caucus</td>{td2}{appointed}{ad_hoc}{no_vote}</tr>
			<tr><td>Labor Liaison</td>{Brent_McFarlane}{appointed}{ad_hoc}{no_vote}</tr>
			<tr><td>Legislative Action</td>{Gray_Petersen}{appointed}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#membership">Membership</Link></td>{Lael_White}{appointed}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#parliamentarian">Parliamentarian</Link></td>{td2}{appointed}{exec}{no_vote}</tr>
			<tr><td>Programs Coordinator</td>{td2}{appointed}{exec}{vote}</tr>
			<tr><td>Resolutions</td>{Dean_Fournier}{appointed}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#secretary">Secretary</Link></td>{Sally_Soriano}{elected}{exec}{vote}</tr>
			<tr><td>Seniors Issues Advocate</td>{Trudy_Bialic}{appointed}{ad_hoc}{no_vote}</tr>
			<tr><td><Link to="/documents/jobs#sergeant-at-arms">Sergeant at Arms</Link></td>{td2}{appointed}{exec}{no_vote}</tr>
			<tr><td>Technology</td>{td2}{appointed}{exec}{vote}</tr>
			<tr><td><Link to="/documents/jobs#treasurer">Treasurer</Link></td>{Eric_Valpey}{elected}{exec}{vote}</tr>
			<tr><td><a href="https://www.facebook.com/pg/WashingtonStateWomen/about/">Federation of Democratic Women</a></td>{Cathy_Baylor}{appointed}{exec}{vote}</tr>
			<tr><td>Young Democrats</td>{td2}{appointed}{exec}{vote}</tr>
		</table>
		<h1>Mail</h1>
		<pre>
			32nd Legislative District Democratic Organization<br/>
			PO Box 55622<br/>
			Shoreline WA 98155
		</pre>
		{/* <img
			alt=""
			src={img_officers}
			style={{'max-width':'100%'}}
		/> */}
	</Layout>
)
