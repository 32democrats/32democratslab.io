import Helmet from 'react-helmet'
// import { Link } from 'gatsby'
import React from 'react'
import Layout from '../components/layout'

const title = 'Precinct Committee Officers'
const csv = `Region,Precinct,LD,CD,KCC,Voters,ID,Nickname,First,Middle,Last,Suffix,Status,Approved,Start
SEA 32-2200,2200,32,7,4,411,,,,,,,,,
SEA 32-2201,2201,32,7,4,507,,,,,,,,,
SEA 32-2202,2202,32,7,4,509,3528,,Marianne,,Tatom,,E,,2020-12-01
SEA 32-2203,2203,32,7,4,862,,,,,,,,,
SEA 32-2204,2204,32,7,4,358,623,,Brent,E,McFarlane,,E,,2020-12-01
SEA 32-2206,2206,32,7,4,451,3205,,Lanna,,Ripp,,E,,2020-12-01
SEA 32-2208,2208,32,7,4,365,,,,,,,,,
SEA 32-2209,2209,32,7,4,626,1093,,Justin,,Simmons,,E,,2020-12-01
SEA 32-2210,2210,32,7,4,650,,,,,,,,,
SEA 32-2211,2211,32,7,4,704,,,,,,,,,
SEA 32-2215,2215,32,7,4,885,525,,Amy,,Freedheim,,E,,2020-12-01
SEA 32-2216,2216,32,7,4,428,3251,,Sally,,Soriano,,E,,2020-12-01
SEA 32-2217,2217,32,7,4,909,2635,,Charles,R,Lewis,,E,,2020-12-01
SEA 32-2218,2218,32,7,4,845,3074,,Patrick,Thomas,Walton,,E,,2020-12-01
SEA 32-2224,2224,32,7,4,677,,,,,,,,,
SEA 32-2537,2537,32,7,4,512,3877,,David,A,Johnson,,E,,2020-12-01
SEA 32-2539,2539,32,7,4,926,1094,,Dean,,Fournier,,E,,2020-12-01
SEA 32-3568,3568,32,7,4,535,,,,,,,,,
SEA 32-3656,3656,32,7,4,554,790,,Kiraya,,Kestin-Langberg,,E,,2020-12-01
SEA 32-3746,3746,32,7,4,605,,,,,,,,,
SHL 32-0001,1,32,7,1,557,1500,,Henry,S,Brown,Jr.,E,,2020-12-01
SHL 32-0012,12,32,7,1,528,,,,,,,,,
SHL 32-0024,24,32,7,1,497,1468,,Krista,Lund,Tenney,,E,,2020-12-01
SHL 32-0038,38,32,7,1,580,,,,,,,,,
SHL 32-0088,88,32,7,1,304,,,,,,,,,
SHL 32-0232,232,32,7,1,443,4494,,Dustin,John,McIntyre,,E,,2020-12-01
SHL 32-0241,241,32,7,1,807,,,,,,,,,
SHL 32-0267,267,32,7,1,707,,,,,,,,,
SHL 32-0274,274,32,7,1,311,4464,,Mark,Allen,Enders,,E,,2020-12-01
SHL 32-0277,277,32,7,1,287,,,,,,,,,
SHL 32-0296,296,32,7,1,418,5013,,Bonnie,Jule Maheala,Stieber,,E,,2020-12-01
SHL 32-0319,319,32,7,1,323,,,,,,,,,
SHL 32-0335,335,32,7,1,452,,,,,,,,,
SHL 32-0346,346,32,7,1,291,5011,,Meghan,Alexandra,Jernigan,,E,,2020-12-01
SHL 32-0357,357,32,7,1,555,,,,,,,,,
SHL 32-0368,368,32,7,1,340,,,,,,,,,
SHL 32-0378,378,32,7,1,345,,,,,,,,,
SHL 32-0396,396,32,7,1,953,,,,,,,,,
SHL 32-0401,401,32,7,1,425,,,,,,,,,
SHL 32-0407,407,32,7,1,714,1056,,Jeff,,Sandys,,E,,2020-12-01
SHL 32-0423,423,32,7,1,414,1884,,Janet,M,Way,,E,,2020-12-01
SHL 32-0424,424,32,7,1,349,,,,,,,,,
SHL 32-0444,444,32,7,1,478,,,,,,,,,
SHL 32-0454,454,32,7,1,336,,,,,,,,,
SHL 32-0462,462,32,7,1,504,,,,,,,,,
SHL 32-0470,470,32,7,1,451,4992,,Mary,Ellen,Stone,,E,,2020-12-01
SHL 32-0474,474,32,7,1,416,3044,,Abram,,Bender,,E,,2020-12-01
SHL 32-0486,486,32,7,1,321,,,,,,,,,
SHL 32-0488,488,32,7,1,327,,,,,,,,,
SHL 32-0494,494,32,7,1,517,4988,,Tiffany,,Chang,,E,,2020-12-01
SHL 32-0499,499,32,7,1,547,,,,,,,,,
SHL 32-0510,510,32,7,1,713,,,,,,,,,
SHL 32-0514,514,32,7,1,525,5005,,Marilyn,,Harris,,E,,2020-12-01
SHL 32-0519,519,32,7,1,449,5006,,Joan,E,Herrick,,E,,2020-12-01
SHL 32-0527,527,32,7,1,828,674,,Michael,,Brunson,,E,,2020-12-01
SHL 32-0530,530,32,7,1,297,4465,,Teresa,Rose Costanzo,Catford,,E,,2020-12-01
SHL 32-0537,537,32,7,1,368,5000,,Marcy,Lynn,Bloom,,E,,2020-12-01
SHL 32-0538,538,32,7,1,501,1575,Chris,Christopher,,Roberts,,E,,2020-12-01
SHL 32-0558,558,32,7,1,334,4991,,Madeline,,Hanhardt,,E,,2020-12-01
SHL 32-0559,559,32,7,1,356,,,,,,,,,
SHL 32-0605,605,32,7,1,480,1715,,Robin,S,McClelland,,E,,2020-12-01
SHL 32-0606,606,32,7,1,399,2881,,David,L,Crow,,E,,2020-12-01
SHL 32-0609,609,32,7,1,417,,,,,,,,,
SHL 32-0648,648,32,7,1,531,548,,Angela,,Henry,,E,,2020-12-01
SHL 32-0665,665,32,7,1,474,955,Pat,Patricia,,Weber,,E,,2020-12-01
SHL 32-0692,692,32,7,1,609,678,,Maryn,C,Wynne,,E,,2020-12-01
SHL 32-0711,711,32,7,1,367,4566,,Mark,,Weiss,,E,,2020-12-01
SHL 32-0735,735,32,7,1,308,,,,,,,,,
SHL 32-0754,754,32,7,1,690,,,,,,,,,
SHL 32-0802,802,32,7,1,548,,,,,,,,,
SHL 32-0819,819,32,7,1,453,,,,,,,,,
SHL 32-0832,832,32,7,1,369,,,,,,,,,
SHL 32-0844,844,32,7,1,494,1527,Ron,Ronald,,Leslie,,E,,2020-12-01
SHL 32-0861,861,32,7,1,831,,,,,,,,,
SHL 32-0866,866,32,7,1,494,1440,,Diane,,Hettrick,,E,,2020-12-01
SHL 32-0875,875,32,7,1,633,2654,,Bettelinn,K,Brown,,E,,2020-12-01
SHL 32-0888,888,32,7,1,293,26,,Rose,,Laffoon,,E,,2020-12-01
SHL 32-0895,895,32,7,1,596,,,,,,,,,
SHL 32-0897,897,32,7,1,517,5003,,Betsy,E,Robertson,,E,,2020-12-01
SHL 32-0898,898,32,7,1,311,270,,Cindy,,Ryu,,E,,2020-12-01
SHL 32-0903,903,32,7,1,311,2883,,Carolyn,,Ahlgreen,,E,,2020-12-01
SHL 32-0914,914,32,7,1,527,260,,Doris,F,McConnell,,E,,2020-12-01
SHL 32-0917,917,32,7,1,624,,,,,,,,,
SHL 32-0920,920,32,7,1,343,,,,,,,,,
SHL 32-0921,921,32,7,1,289,271,,Winona,D,Hauge,,E,,2020-12-01
SHL 32-1020,1020,32,7,1,331,745,,Keith,Patrick,Scully,,E,,2020-12-01
SHL 32-1021,1021,32,7,1,628,4989,,Travis,,Windleharth,,E,,2020-12-01
SHL 32-1030,1030,32,7,1,399,4990,,Vivian,,Korneliussen,,E,,2020-12-01
SHL 32-1038,1038,32,7,1,293,,,,,,,,,
SHL 32-1053,1053,32,7,1,388,1528,,Alan,D,Charnley,,E,,2020-12-01
SHL 32-1077,1077,32,7,1,531,2540,,Lillian,,Hawkins,,E,,2020-12-01
SHL 32-1080,1080,32,7,1,271,,,,,,,,,
SHL 32-1101,1101,32,7,1,348,,,Jerry,,Cronk,,,2020-12-09,
SHL 32-1106,1106,32,7,1,643,,,,,,,,,
SHL 32-1140,1140,32,7,1,270,,,,,,,,,
SHL 32-1158,1158,32,7,1,347,,,,,,,,,
SHL 32-1168,1168,32,7,1,523,5012,,David,Yeong-Song,Chen,,E,,2020-12-01
SHL 32-1178,1178,32,7,1,457,4999,,Kayla,,Pennington,,E,,2020-12-01
SHL 32-1197,1197,32,7,1,389,28,Larry,Lawrence,,Owens,,E,,2020-12-01
SHL 32-1209,1209,32,7,1,553,5001,,John,Martin,Ramsdell,,E,,2020-12-01
SHL 32-1210,1210,32,7,1,636,5007,,Eric,L,Scheir,,E,,2020-12-01
SHL 32-1228,1228,32,7,1,397,,,,,,,,,
SHL 32-1236,1236,32,7,1,293,,,,,,,,,
SHL 32-2552,2552,32,7,1,547,,,,,,,,,
SHL 32-2753,2753,32,7,1,571,,,,,,,,,
SHL 32-3363,3363,32,7,1,467,,,,,,,,,
SHL 32-3556,3556,32,7,1,498,,,,,,,,,
ALDERCREST,23234444,32,2,3,,,,,,,,,,
ANN,73234176,32,7,3,,,,Stephanie,,Harris,,,,
ASHFORD,23234761,32,2,3,,,,,,,,,,
CHAPEL,73234182,32,7,3,,,,Chris,,Forgy,,,,
CYPRESS,23234175,32,2,3,,,,Derek,,Fike,,,,
EDMONDS 12,73231389,32,7,3,,,,Barbara,,McGuire,,,,
EDMONDS 14,73231112,32,7,3,,,,Colin,,Cole,,,,
EDMONDS 15,73231113,32,7,3,,,,,,,,,,
EDMONDS 16,73231114,32,7,3,,,,,,,,,,
EDMONDS 18,73231132,32,7,3,,,,Josh,,Thompson,,,,
EDMONDS 19,73231133,32,7,3,,,,Adrienne,,Fraley-Monillas,,,,
EDMONDS 25,73231153,32,7,3,,,,Andrew,W.,Palmer,,,,
EDMONDS 27,73231154,32,7,3,,,,,,,,,,
EDMONDS 40,73231500,32,7,3,,,,,,,,,,
EDMONDS 41,73231511,32,7,3,,,,Elizabethe,(Liz),Brown,,,,
EDMONDS 42,73231512,32,7,3,,,,,,,,,,
EDMONDS 43,73231513,32,7,3,,,,,,,,,,
EDMONDS 44,73231514,32,7,3,,,,,,,,,,
EDMONDS 45,73231523,32,7,3,,,,,,,,,,
EDMONDS 47,73231525,32,7,3,,,,,,,,,,
EDMONDS 48,73231562,32,7,3,,,,,,,,,,
EDMONDS 49,73231563,32,7,3,,,,Trudy,,Bialic,,,,
EDMONDS 50,73231564,32,7,3,,,,,,,,,,
EDMONDS 52,73231577,32,7,3,,,,,,,,,,
EDMONDS  6,73231106,32,7,3,,,,,,,,,,
HILLTOP,23234077,32,2,3,,,,,,,,,,
HOLLY,73234099,32,7,3,,,,Carin,,Chase,,,,
LARCH,23234461,32,2,3,,,,Doris,,Fulton,,,,
LAURA,23234184,32,2,3,,,,,,,,,,
LYNNWOOD  1,23231115,32,2,3,,,,,,,,,,
LYNNWOOD 10,23231124,32,2,3,,,,,,,,,,
LYNNWOOD 11,23231125,32,2,3,,,,,,,,,,
LYNNWOOD 12,23231419,32,2,3,,,,James,M.,Trefry,,,,
LYNNWOOD 13,23231168,32,2,3,,,,Marylou,,Eckart,,,,
LYNNWOOD 14,23231420,32,2,3,,,,,,,,,,
LYNNWOOD 15,23231421,32,2,3,,,,Gray,,Petersen,,,,
LYNNWOOD 16,23231186,32,2,3,,,,Lisa,,Utter,,,,
LYNNWOOD 17,23231201,32,2,3,,,,Deborah,,Kilgore,,,,
LYNNWOOD 18,23231202,32,2,3,,,,,,,,,,
LYNNWOOD  2,23231116,32,2,3,,,,,,,,,,
LYNNWOOD 20,23231203,32,2,3,,,,Shirley,,Sutton,,,,
LYNNWOOD 21,23231423,32,2,3,,,,Deana,,Knutsen,,,,
LYNNWOOD 22,23231204,32,2,3,,,,Gary,,McCaig,,,,
LYNNWOOD 23,23231205,32,2,3,,,,Deborah,,Viertel,,,,
LYNNWOOD 24,23231206,32,2,3,,,,David,C.,Parshall,,,,
LYNNWOOD 25,23231626,32,2,3,,,,Robert,,Jared,,,,
LYNNWOOD 26,23231627,32,2,3,,,,Elizabeth,Ann,Lunsford,,,,
LYNNWOOD 28,23231207,32,2,3,,,,Theodore,R. (Ted),Hikel,,,,
LYNNWOOD 29,23231208,32,2,3,,,,,,,,,,
LYNNWOOD  3,23231117,32,2,3,,,,,,,,,,
LYNNWOOD 30,23231210,32,2,3,,,,Cathy,,Baylor,,,,
LYNNWOOD 31,23231424,32,2,3,,,,,,,,,,
LYNNWOOD 32,23231462,32,2,3,,,,,,,,,,
LYNNWOOD 33,23231516,32,2,3,,,,,,,,,,
LYNNWOOD 34,23231516,32,2,3,,,,Megan,,Atkins,,,2020-12-09,
LYNNWOOD  4,23231118,32,2,3,,,,,,,,,,
LYNNWOOD  5,23231119,32,2,3,,,,Kyle,,Burleigh,,,,
LYNNWOOD  6,23231120,32,2,3,,,,,,,,,,
LYNNWOOD  7,23231121,32,2,3,,,,Carol,,McMahon,,,,
LYNNWOOD  8,23231122,32,2,3,,,,,,,,,,
LYNNWOOD  9,23231123,32,2,3,,,,Stephanie,,Wright,,,,
MAGNOLIA,23234539,32,2,3,,,,,,,,,,
MOUNTLAKE TERRACE 10,23241137,32,2,4,,,,Grace,,Lindsley,,,,
MOUNTLAKE TERRACE  7,23241095,32,2,4,,,,,,,,,,
MOUNTLAKE TERRACE  8,23241096,32,2,4,,,,,,,,,,
MOUNTLAKE TERRACE  9,23241097,32,2,4,,,,,,,,,,
NILE,23244749,32,2,4,,,,,,,,,,
NRV MTLK TERR WELL SITE,73238910,32,7,3,,,,,,,,,,
NRV TANK FARM,73238917,32,7,3,,,,,,,,,,
PACIFIC,23234550,32,2,3,,,,,,,,,,
RIDGEVIEW,23234755,32,2,3,,,,James,Roger,Nelson,,,,
ROB,73234143,32,7,3,,,,,,,,,,
SUMMIT,73234149,32,7,3,,,,,,,,,,
WOODWAY 1,73231173,32,7,3,,,,,,,,,,
WOODWAY 2,73231174,32,7,3,,,,Cindi,,Jay,,,,
`
	const cols = 11
	const rows = csv.split('\n')
	const ths = rows[0].split(',').slice(0,cols)
	const data = rows.slice(1).map(row => row.split(',')).sort(
		(a,b)=> (b[1] > a[1]) ? '-1' : '1'
	)
	const data_fmt = data.map(
		row => <tr>{row
			.slice(0,cols)
			.map(
				cell => <td>{cell}</td>
			)
		}</tr>
)
export default () => (
	<Layout>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<h1>{title}</h1>
<p>
Precinct Committee Officers (PCOs) engage and organize their neighbors, get out the vote to elect and keep good Democrats in office, and help run Democratic organizations.
</p>
<p>
Find your precinct in <a href="http://www.kingcounty.gov/elections/referenceresources/electionmaps/findmydistrict.aspx"
>King County
</a> or <a href="http://gis.snoco.org/maps/voting/index.htm">Snohomish County</a>.
</p>
<p>
PCO appointments are made to fill vacancies. See PCO applications for <a href="https://www.kcdems.org/get-involved/become-a-pco/">King County</a> or	<a href="http://snocodems.org/wp-content/uploads/2018/03/PCO-Application.pdf">Snohomish County</a>.
</p>
<ul className="menu">
<li><a href="https://s.kcdems.org/lists/pco?f=15&s1=&s2=&dx=&df=now&dt=now&a=&l=32">King County Democrats PCOs, LD 32</a></li>
<li><a href="https://www.snohomishcountywa.gov/DocumentCenter/View/75437/Summary-Report-for-2020-august-Primary?bidId=">Snohomish County PCO election results</a></li>
</ul>
		<h2>Coordinators</h2>
		<dl className="links">
			<dt>Edmonds, Mountlake Terrace, Woodway, unincorporated</dt>
			<dd><a href="mailto:carin.chase@32democrats.org">Carin Chase</a></dd>
			<dt>Lynnwood</dt>
			<dd><a href="mailto:carol.mcmahon@32democrats.org">Carol McMahon</a></dd>
			<dd><a href="mailto:m.eckart@32democrats.org">MD Eckart</a></dd>
			<dd><a href="mailto:deb.viertel@32democrats.org">Deb Viertel</a></dd>
			<dt>Shoreline</dt>
			<dd><a href="mailto:carolyn.ahlgreen@32democrats.org">Carolyn Ahlgreen</a></dd>
			<dt>Seattle</dt>
			<dd><a href="mailto:Sally.Soriano@32democrats.org">Sally Soriano</a></dd>
		</dl>
		<h2>2021–2022 {title}</h2>
		<table className="contacts">
			{ths.map(th => <th>{th}</th>)}
			{data_fmt}
		</table>
	</Layout>
)
