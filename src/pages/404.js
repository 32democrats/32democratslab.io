import React from 'react'
import { Link } from 'gatsby'
import Layout from '../components/layout'
export default () => (
	<Layout>
		<p><Link to="/">Page not found.</Link></p>
	</Layout>
)
