import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
<Layout>
	<div className="article">
		<Helmet>
			<title>2018 Platform</title>
		</Helmet>
		<h1>2018 Platform of the 32nd District Democrats</h1>
		<ol>
			<li>
				<strong>
					2018 Platform of the 32<sup>nd </sup>District Democrats - ADOPTED 3/24/18 *
					FLAGGED BULLETED ITEMS ARE NOT ADOPTED - FLAGGED  FOR DISCUSSION ON
					APRIL 11, 2018
				</strong>
			</li>
		</ol>
		<table>
			<tbody>
				<tr>
					<td>
						<p>
							<strong>2.</strong>
						</p>
					</td>
					<td />
					<td />
				</tr>
				<tr>
					<td>
						<p>
							<strong>3.</strong>
						</p>
					</td>
					<td>
						<p>
							<strong>Plank</strong>
						</p>
					</td>
					<td>
						<p>
							<strong>Lines</strong>
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>4.</strong>
						</p>
					</td>
					<td>
						<p>Preamble</p>
					</td>
					<td>
						<p>24 – 42</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>5.</strong>
						</p>
					</td>
					<td>
						<p>Agriculture</p>
					</td>
					<td>
						<p>43 – 105</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>6.</strong>
						</p>
					</td>
					<td>
						<p>Civil Rights and Human Rights</p>
					</td>
					<td>
						<p>107 – 175   </p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>7.</strong>
						</p>
					</td>
					<td>
						<p>Corporate Power</p>
					</td>
					<td>
						<p>177 – 207</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>8.</strong>
						</p>
					</td>
					<td>
						<p>Economic Justice, Jobs and Tax Fairness</p>
					</td>
					<td>
						<p>209 – 280</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>9.</strong>
						</p>
					</td>
					<td>
						<p>Education</p>
					</td>
					<td>
						<p>282 – 327</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>10.</strong>
						</p>
					</td>
					<td>
						<p>Environmental Justice, Climate Change and Energy</p>
					</td>
					<td>
						<p>329 – 412</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>11.</strong>
						</p>
					</td>
					<td>
						<p>Foreign Policy</p>
					</td>
					<td>
						<p>414 – 458</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>12.</strong>
						</p>
					</td>
					<td>
						<p>Government and Political Reform</p>
					</td>
					<td>
						<p>460 – 507</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>13.</strong>
						</p>
					</td>
					<td>
						<p>Gun Safety and Reform</p>
					</td>
					<td>
						<p>509 – 534</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>14.</strong>
						</p>
					</td>
					<td>
						<p>Health Care</p>
					</td>
					<td>
						<p>536 – 575 </p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>15.</strong>
						</p>
					</td>
					<td>
						<p>Housing Justice</p>
					</td>
					<td>
						<p>577 – 616</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>16.</strong>
						</p>
					</td>
					<td>
						<p>Human Services</p>
					</td>
					<td>
						<p>618 – 655   </p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>17.</strong>
						</p>
					</td>
					<td>
						<p>Immigration</p>
					</td>
					<td>
						<p>657 – 691</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>18.</strong>
						</p>
					</td>
					<td>
						<p>Labor</p>
					</td>
					<td>
						<p>693 – 756</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>19.</strong>
						</p>
					</td>
					<td>
						<p>Law and the Justice System</p>
					</td>
					<td>
						<p>758 – 812</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>20.</strong>
						</p>
					</td>
					<td>
						<p>Media Reform</p>
					</td>
					<td>
						<p>814 – 850</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>21.</strong>
						</p>
					</td>
					<td>
						<p>Military</p>
					</td>
					<td>
						<p>852 – 886</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>22.</strong>
						</p>
					</td>
					<td>
						<p>Transportation</p>
					</td>
					<td>
						<p>888 – 922</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>23.</strong>
						</p>
					</td>
					<td>
						<p>Tribal Relations and Sovereignty</p>
					</td>
					<td>
						<p>924 – 971</p>
					</td>
				</tr>
			</tbody>
		</table>

		<ol start="24">
			<li>
				<strong>Preamble</strong>
			</li>
			<li />
			<li>
				<em>
					“Without equality there can be no democracy.” - Eleanor Roosevelt
				</em>
			</li>
			<li />
			<li>
				In the current era of political upheaval and intensifying ideological
				animosities, what sharply
			</li>
			<li>
				distinguishes us is that we proclaim our unshakable loyalty and
				dedication to creating the best
			</li>
			<li>
				government we can achieve in the service of all, not just the few.
			</li>
			<li />
			<li>
				<em>
					“The legitimate object of government is to do for a community of
					people whatever they need to
				</em>
			</li>
			<li>
				<em>
					have done but cannot do at all, or cannot do as well, for themselves.”
					- Abraham Lincoln{" "}
				</em>
			</li>
			<li />
			<li>
				A just society is based on the principles of equality and solidarity,
				values human rights, and
			</li>
			<li>recognizes the dignity of every human being.</li>
			<li />
			<li>
				We reaffirm our determination to pursue equal opportunity for all in
				every realm of life, and to
			</li>
			<li>
				expand our dreams of excellence with each step forward, aiming ever
				higher toward that grand
			</li>
			<li>
				goal set out in the Preamble to our Constitution: “to form a more
				perfect Union.”
			</li>
			<li />
			<li>To that end, we offer this Platform.</li>
		</ol>

		<h1>43.        Agriculture</h1>
		<ol start="44">
			<li>
				<em>
					Food is essential to human survival, and forest products for survival
					in reasonable comfort;
				</em>
			</li>
			<li>
				<em>
					accordingly, agriculture must remain of vital importance in
					Washington, and Washington in our
				</em>
			</li>
			<li>
				<em>nation’s agricultural production. </em>
			</li>
			<li />
		</ol>
		<h2>48.        We support:</h2>
		<ol start="49">
			<li>
				Programs that ensure the availability of high-quality and organic food,
				strengthen rural
			</li>
			<li>
				communities, preserve family farms and maintain the viability of the
				land and soil into the
			</li>
			<li />
			<li>
				Equitable distribution of water rights that respects senior appropriator
				water rights and
			</li>
			<li />
			<li>
				Quantifying our surface and ground water resources and characterizing
				their hydraulic
			</li>
			<li />
			<li>
				Expansion of SNAP benefits to increase purchasing power and access to
				fresh fruits and
			</li>
			<li />
			<li>
				Local food and beverage producers, and that government should develop
			</li>
			<li>strategies for promoting local products.</li>
			<li>
				Requiring all food to be labeled to disclose whether or not it includes
				genetically modified
			</li>
			<li>organisms (GMOs).</li>
			<li>
				Explicit food labeling, including date harvested or packaged, nation of
				origin, irradiation,
			</li>
			<li>and organic certification.</li>
			<li>
				Increased funding for and inspection of domestic and imported foods and
				livestock to
			</li>
			<li>ensure safe food.</li>
			<li>Measures that promote and sustain family farms.</li>
			<li>
				Measures that sustain farming when calamities occur and that ensure
				farmers and farm
			</li>
			<li>
				workers receive a fair return on their efforts, safety in doing that
				work, and are able to
			</li>
			<li>produce food that is safe to eat.</li>
			<li>
				Bringing sustainable farming to our urban communities, including
				community and
			</li>
			<li>backyard gardens and local farmers’ markets.</li>
			<li>
				Encouraging the use of locally grown food in school meal programs
				through local
			</li>
			<li>farmers’ markets.</li>
			<li>
				Policies consistent with the Washington State Growth Management Act that
				preserve
			</li>
			<li>
				agricultural land and natural resources critical to the viability of
				food, fuel, and forest
			</li>
			<li>production into the future.</li>
			<li>
				Protecting waterways and habitat by reducing the use of herbicides,
				pesticides, and other
			</li>
			<li>hazardous materials.</li>
			<li>
				Decisions about water resources based on sound, credible scientific and
				economic
			</li>
			<li>information, including local concerns.</li>
			<li>
				Ensuring that farm subsidy programs benefit family and organic farms.
			</li>
			<li>
				Incentives to encourage the agricultural skills, careers, and lifestyles
				that are essential to
			</li>
			<li>our country’s survival.</li>
			<li>
				Protection of productive farmland and wetlands from residential,
				industrial, and other
			</li>
			<li>types of development.</li>
			<li>
				Farming and forest management practices that encourage in soil
				conservation and soil
			</li>
			<li>carbon sequestration.</li>
			<li>
				Concentrated Animal Feeding Operations (CAFOS) to mitigate ground and
				surface water
			</li>
			<li>
				and drinking water pollution caused by contaminants emanating from
				manure lagoons and
			</li>
			<li>
				spray fields as well as strict monitoring of water quality and
				enforcement of compliance
			</li>
			<li />
			<li />
		</ol>
		<h2>93.         We oppose:</h2>
		<ol start="94">
			<li>
				Experimenting with genetically modified organisms (GMOs) by
				field-testing.
			</li>
			<li>
				Intentional infringement of existing senior appropriator certified water
				rights.
			</li>
			<li>
				Cutting of funding to agricultural programs that support closely held
				family farms.
			</li>
			<li>
				Toxic chemical sprays and the devastating effects on public health and
				safety.
			</li>
			<li>
				Reduction of SNAP benefits and forcing families into a one-size-fits-all
				option that removes
			</li>
			<li>consumer choice.</li>
			<li>
				Subsidies for large conglomerates – such as Monsanto – which chokes out
				small farms and
			</li>
			<li>devastates local economies.</li>
			<li>
				The exclusion of agriculture workers from overtime pay and minimum wage
				laws.
			</li>
			<li>
				Intentional infringement of existing senior appropriator certified water
				rights.
			</li>
			<li>
				“Ag Gag” laws that criminalize the exposure of inhumane and unhealthy
				animal production
			</li>
			<li>
				practices, such as Concentrated Animal Feeding Operations (CAFOs).
			</li>
			<li />
		</ol>

		<h1>107.     Civil Rights and Human Rights</h1>
		<ol start="108">
			<li>
				<em>
					We must remain at the forefront of the struggle to extend social,
					political, economic and legal
				</em>
			</li>
			<li>
				<em>
					rights to all persons, and we oppose policies that would tend to
					reverse or impede those gains in
				</em>
			</li>
			<li>
				<em>
					human dignity. Food security, housing, medical care, education, and
					equal employment are basic
				</em>
			</li>
			<li>
				<em>human rights.</em>
			</li>
			<li />
		</ol>
		<h2>113.     We support:</h2>
		<ol start="114">
			<li>
				The right of all women to have autonomy over their own bodies, to be
				free of government,
			</li>
			<li>
				corporate, or religious interference in their reproductive decisions,
				including birth control
			</li>
			<li>
				and abortion, and to have safe, legal, protected, affordable and
				accessible health care that
			</li>
			<li>enables them to make those choices according to their own wishes.</li>
			<li>
				Privacy as a basic human right that the government and the private
				sector at all levels
			</li>
			<li>must acknowledge in their rules and regulations.</li>
			<li>
				The right of every adult to marry another person without regard to
				gender, and to enjoy the
			</li>
			<li>same civil and legal rights accorded to all married persons.</li>
			<li>
				Our Constitutionally guaranteed religious freedom that prevents the
				government from
			</li>
			<li>
				imposing or suppressing religious beliefs, or favoring or funding any
				set of beliefs.
			</li>
			<li>
				Anti-discrimination laws that sustain human dignity in all aspects of
				life.
			</li>
			<li>Full employment at a livable wage.</li>
			<li>Providing universal access to safe and affordable housing.</li>
			<li>Freedom from hunger.</li>
			<li>
				Restorative justice to provide a more constructive, humane, and less
				punitive correctional
			</li>
			<li />
			<li>Equal pay for equal work.</li>
			<li>
				Upholding and defending every U.S. working person’s right to organize,
				negotiate
			</li>
			<li>collectively, protest, and strike.</li>
			<li>
				Enacting an immigration policy based on human, civil, and labor rights,
				acknowledging that
			</li>
			<li>
				the U.S. imposed trade agreements compel migration into this country.
			</li>
			<li>Universal health care for all, ensuring no one is excluded.</li>
			<li>Enriching and equitable education system.</li>
			<li>Ratification of the Equal Rights Amendment.</li>
			<li>Strict separation of church and state.</li>
			<li>
				Adding enforcement provisions to the Americans with Disabilities Act of
				1990, as amended
			</li>
			<li>
				An immediate end to human trafficking for the sex trade, and all other
				forms of involuntary
			</li>
			<li />
			<li>
				Strong legislation, treatment programs and education designed to reduce
				harassment,
			</li>
			<li>
				intimidation, domestic or sexual violence, gun violence, and bullying.
			</li>
			<li>
				Honoring the rich diversity of society, and efforts to reflect that
				diversity in our Party,
			</li>
			<li>
				including special consideration for the rights of such currently
				and historically marginalized
			</li>
			<li>
				communities as people with disabilities, immigrants, peoples of color,
				indigenous peoples,
			</li>
			<li>
				poor people, LGBTQ+ people, and religious minorities and
				atheists. People subjected
			</li>
			<li>
				to discrimination must be afforded the legal means and economic
				opportunities to overcome
			</li>
			<li>such injustices.</li>
			<li>
				Policies and actions that will strengthen our country by affirming the
				value of all
			</li>
			<li>
				individuals, and by eliminating systemic conditions that perpetuate
				inequality, oppression,
			</li>
			<li>and lack of equitable access to opportunities.</li>
			<li>
				Defending our constitutionally guaranteed rights to free speech and free
				association,
			</li>
			<li>
				peaceable assembly, and protest, which supersede all state and local
				jurisdiction
			</li>
			<li>
				Legislation defining “Conversion Therapy” to alter sexual orientation as
				a destructive and
			</li>
			<li>damaging act of fraud.</li>
			<li />
		</ol>
		<h2>158.     We oppose:</h2>
		<ol start="159">
			<li>
				Every law, regulation, or practice that tends to diminish, by intent or
				effect, the
			</li>
			<li>Constitutional rights of citizens.</li>
			<li>
				Discrimination in voting, employment, housing, public accommodations,
				healthcare,
			</li>
			<li>
				military service and veterans’ status, insurance, licensing, or
				education based on race,
			</li>
			<li>
				ethnicity, religion, age, sex, sexual orientation, gender identity,
				disability, size,
			</li>
			<li>
				socioeconomic status, political affiliation, and national origin or
				immigration status.
			</li>
			<li>
				Any “bathroom bill” that attacks the rights of Trans-Queer-Gender
				non-binary peoples.
			</li>
			<li>
				Any claim of religious liberty to justify or protect discriminatory
				practices.
			</li>
			<li>
				Discrimination against LGBTQ+ people on parenting rights or
				opportunities.
			</li>
			<li>
				Warrantless wiretapping and searches that violate the 4th Amendment of
				the U.S.
			</li>
			<li>
				Constitution or Article 1, Section 7 of the Washington State
				Constitution.
			</li>
			<li>
				Arbitrary limits, such as “Free Speech Zones”, which inhibit the right
				to disrupt business as
			</li>
			<li>
				usual to express grievances and demand redress from our government.
			</li>
			<li>
				Extreme and unconstitutional provisions of the PATRIOT Act which allow
				the government
			</li>
			<li>
				and corporate surveillance of individuals, activist groups, and media.
			</li>
			<li>
				Denial of women’s health services by governmental, insurance, or
				healthcare corporations,
			</li>
			<li>local pharmacies, or military benefits programs.</li>
			<li />
		</ol>

		<h1>177.     Corporate Power</h1>
		<ol start="178">
			<li>
				<em>
					Our government was created of, by and for its people, not
					corporations. As artificial entities,
				</em>
			</li>
			<li>
				<em>
					corporations are not entitled to the political rights of human beings,
					yet have gained undue
				</em>
			</li>
			<li>
				<em>influence over our government and political process.</em>
			</li>
			<li />
		</ol>
		<h2>182.     We support:</h2>
		<ol start="183">
			<li>
				A Constitutional amendment to establish that corporations shall not be
				considered as
			</li>
			<li>
				“persons” for purposes of political activity, and to reverse the
				pernicious notion that
			</li>
			<li>
				money equals speech as is purported in the Citizens United decision.
			</li>
			<li>
				As an interim step until Citizens United is reversed, institution of
				stringent
			</li>
			<li>
				corporate campaign-contribution reporting requirements and prohibitions
				on such
			</li>
			<li>
				contributions without specific advance approval by stockholders who are
				U.S. citizens.
			</li>
			<li>Rigorous enforcement of anti-trust and consumer protection laws.</li>
			<li>
				Effective penalties for corporations and the persons who control them,
				including prison
			</li>
			<li>terms as warranted, when those corporations violate the law.</li>
			<li>Transparency in all corporate accounting.</li>
			<li>
				Separation of investment banking from retail banking, and subjecting all
				banking to
			</li>
			<li>tighter regulation, transparency, and accountability.</li>
			<li>
				Corporations receiving bailouts with taxpayer funds becoming publicly
				owned entities
			</li>
			<li>operated for the public good.</li>
			<li>
				Corporate officers being held personally liable and aggressively
				prosecuted for corporate
			</li>
			<li>crimes and fraud.</li>
			<li>
				Salary caps for corporate executives at 20 times the average wage of
				employees.
			</li>
			<li />
		</ol>
		<h2>201.      We oppose:</h2>
		<ol start="202">
			<li>
				Transferring performance of the government’s customary functions into
				private hands.
			</li>
			<li>
				Direct or indirect subsidies, whether through the federal tax code or by
				other means, to
			</li>
			<li>any corporation that moves American jobs offshore.</li>
			<li>
				U.S. corporations going offshore in order to evade U.S. taxes and other
				laws.
			</li>
			<li>Unlimited and undisclosed campaign contributions.</li>
			<li>
				Handing over of public service to the private sector called
				“outsourcing”.
			</li>
			<li />
		</ol>

		<h1>209.        Economic Justice, Jobs, and Tax Fairness</h1>
		<ol start="210">
			<li>
				<em>
					Justice in economic affairs is essential for the people to be able to
					fulfill their highest
				</em>
			</li>
			<li>
				<em>
					{" "}
					Government must so align its policies and finances, and so conduct its
					affairs, that
				</em>
			</li>
			<li>
				<em>
					its economy is strong and its entire population has a fair opportunity
					to prosper. Working people{" "}
				</em>
			</li>
			<li>
				<em>
					have a right to a good standard of living, collective bargaining in
					the workplace, reliable pensions{" "}
				</em>
			</li>
			<li>
				<em>
					and social security benefits, and an equitable tax system. “Taxes are
					the dues we pay for the{" "}
				</em>
			</li>
			<li>
				<em>
					privilege of membership in an organized society.” - President Franklin
					Roosevelt.{" "}
				</em>
			</li>
			<li />
		</ol>
		<h2>217.          We support:</h2>
		<ol start="218">
			<li>
				Institution of a progressive tax system in our State, relying on a
				personal income tax
			</li>
			<li>
				(including capital gains), reduced sales and property taxes, and
				replacement of the B&amp;O
			</li>
			<li>tax with a tax on business net income.</li>
			<li>
				Making the federal tax system more progressive, including higher taxes
				on capital gains
			</li>
			<li>and other investment income.</li>
			<li>
				Imposing a federal tax on every stock exchange sale or purchase, based
				upon a percentage
			</li>
			<li>of the price of the sale or purchase.</li>
			<li>
				Tax cuts for the middle class and tax credits for those who live in
				poverty, to stimulate
			</li>
			<li>the economy and create jobs.</li>
			<li>
				Preserving Social Security as a Trust Fund, and removing the cap on
				income subject to
			</li>
			<li>Social Security tax.</li>
			<li>Establishment of a publicly-owned State bank.</li>
			<li>
				Targeted economic-development incentives that provide verifiable,
				immediate, and
			</li>
			<li>
				lasting benefits to our communities, subject to their being reviewed at
				least biannually
			</li>
			<li>
				and promptly terminated if not fulfilling their intended purposes.
			</li>
			<li>
				Increasing accountability for tax exemptions and preferences by
				requiring each recipient
			</li>
			<li>
				to demonstrate annually that the state is realizing the promised
				benefit, and by treating
			</li>
			<li>each such preference as a budgeted expenditure.</li>
			<li>
				Creation of living-wage jobs and initiation of significant improvements
				to our
			</li>
			<li />
			<li>
				Multilateral trade agreements if they (1) are conditioned on strict
				health, safety and
			</li>
			<li>
				environmental standards, human rights and workers’ rights, and (2)
				support transparent
			</li>
			<li>
				democratic processes, including federal, state, and local laws, and (3)
				do not give special
			</li>
			<li>
				privileges to foreign corporations over domestic companies, and (4) do
				not disadvantage
			</li>
			<li>
				American workers, and (5) do not include an investor-state-dispute
				process which favors
			</li>
			<li>corporations over sovereign national governments.</li>
			<li>
				Aid for small businesses, including tax credits, low interest loans, and
				nonprofit micro-
			</li>
			<li />
			<li>
				Enforcement of usury laws, including capping payday loans at an 18% APR.
			</li>
			<li>
				Increased transparency of federal and state financial institutions and
				their policies.
			</li>
			<li>
				A windfall-profits tax on businesses that take unreasonable or excessive
				profit.
			</li>
			<li>
				Withholding government contracts from corporations that establish a
				corporate presence
			</li>
			<li>in a different jurisdiction for tax-avoidance purposes.</li>
			<li>
				Preparation for a just post-automation economy by taking steps to study
				and then
			</li>
			<li>
				implement a universal basic income funded via progressive revenue
				sources.
			</li>
			<li>
				Reversing the causes for the gross disparity in wealth and income.
			</li>
			<li>
				Fair-trade reconfiguration of global free trade agreements by publicly
				and transparently
			</li>
			<li>
				renegotiating globalization rules to benefit working people and
				ecosystems of all countries.
			</li>
			<li>
				Expand and improve Social Security by raising the cap on payroll taxes.
			</li>
			<li>
				Promoting an economy that prioritizes real goods and services over
				high-risk financial
			</li>
			<li>instruments and derivatives trading.</li>
			<li>Reinstatement of the Glass-Steagall Act. </li>
			<li>Breaking up banks considered “too-big-to-fail".</li>
			<li>
				A diverse banking environment including public banks, community banks,
				and credit
			</li>
			<li />
			<li />
		</ol>
		<h2>264.         We oppose:</h2>
		<ol start="265">
			<li>
				Predatory lending and misuse of private data by financial institutions.
			</li>
			<li>Exploitation of migrant, temporary, and contract workers.</li>
			<li>
				The Trade Promotion Authority (TPA) because it prevents our elected
				Congressional
			</li>
			<li>
				members from debating and/or amending the content of international trade
				agreements.
			</li>
			<li>
				International trade agreements, such as WTO, NAFTA, CAFTA, and the TPP,
				that contain
			</li>
			<li>
				provisions such as the Investment-State Dispute Settlement (ISDS),
				Section B of the
			</li>
			<li>
				Investment Chapter of the TPP, which abrogate our judicial sovereignty
				over resolution of
			</li>
			<li>disputes between foreign corporations concerning our own laws.</li>
			<li>All efforts to undermine minimum wage laws.</li>
			<li>A tax system that rewards investment over hard work.</li>
			<li>The use of credit scores in hiring or insurance rating.</li>
			<li>
				Privatization of workers’ compensation or Social Security, or reduction
				of Social Security
			</li>
			<li>
				 benefits by any means – including subjection of cost of living
				adjustments to the “Chained”
			</li>
			<li>Consumer Price Index.</li>
			<li>Hiring policies that discriminate against unemployed applicants.</li>
			<li>
				Austerity measures that transfer wealth from the 99% to enrich the 1%.
			</li>
			<li />
		</ol>

		<ol start="282">
			<li>
				<strong>Education</strong>
			</li>
			<li>
				<em>
					Educational opportunity is a basic right of all Americans. An
					excellent, quality public education,
				</em>
			</li>
			<li>
				<em>
					preschool through post-secondary, with equal access for all, is
					fundamental to maintaining a
				</em>
			</li>
			<li>
				<em>
					healthy democracy. It is the paramount duty of our State to make ample
					provision for the
				</em>
			</li>
			<li>
				<em>education of all children. </em>
			</li>
			<li />
			<li>
				<strong>We support:</strong>
			</li>
			<li>
				A rigorous, comprehensive, scientifically and historically accurate
				curriculum.
			</li>
			<li>Smaller class sizes based on effective student-teacher ratios.</li>
			<li>
				Head Start and Early Childhood Education, special education, meal
				support, and other
			</li>
			<li>assistive programs.</li>
			<li>
				Inclusion of music, fine arts, civics and physical education in the
				curriculum.
			</li>
			<li>
				Full federal and State funding of all basic, gifted, vocational,
				technical, alternative,
			</li>
			<li>
				special education, English Language Learners, and other educational
				mandates, including
			</li>
			<li>the Individuals with Disabilities Education Act.</li>
			<li>
				Adequate financial resources and infrastructure for the education of
				Fircrest Residential
			</li>
			<li>
				Habilitation Center children and other individuals with disabilities.
			</li>
			<li>
				Educator salaries, cost of living increases and retirement and health
				care benefits equal to
			</li>
			<li>
				those of other professionals of similar experience and qualifications,
				in order to attract
			</li>
			<li>and retain quality public-school employees.</li>
			<li>Teacher-led reforms for better outcomes in the classroom.</li>
			<li>
				Increased public funding of higher education and more full-time
				instructional faculty, and
			</li>
			<li>
				improved salary, benefits, and professional opportunity for part-time or
				non-tenured
			</li>
			<li />
			<li>Overturning state funding of charter schools.</li>
			<li>
				Medically accurate and comprehensive sex education in schools, including
				healthy
			</li>
			<li>relationships and models of consent.</li>
			<li>
				Full staffing of non-classroom support such as counselors, librarians,
				nurses, psychologists,
			</li>
			<li>
				to meet the social, emotional, health, safety and educational needs of
				all students.
			</li>
			<li>
				Equitable and comprehensive services for students including those who
				have special needs,
			</li>
			<li>
				English language learners, foster or homeless youth, highly capable,
				low-income, LGBTQ
			</li>
			<li>and students of color.</li>
			<li>
				Increased opportunities in diverse and low-income communities to earn
				post-secondary
			</li>
			<li>
				credit in high school through exam classes, career &amp; technical
				education, apprenticeships,
			</li>
			<li>and internships.</li>
			<li>
				Tuition-free public colleges, universities, and vocational schools.
			</li>
			<li>
				State-funded universal pre-school, as well as affordable before and
				after-school programs.
			</li>
			<li />
		</ol>
		<h2>320.         We oppose:</h2>
		<ol start="321">
			<li>Standardized testing as the primary means of accountability.</li>
			<li>
				Government funding of charter schools not subject to the governance of a
				local public
			</li>
			<li>school board, at all levels of government.</li>
			<li>
				Profit-driven education reform characterized by standardized curriculum
				and testing,
			</li>
			<li>vouchers and charter schools.</li>
			<li>Commercial marketing or military recruiting in public schools.</li>
			<li>Predatory student loans that cannot be discharged in bankruptcy.</li>
			<li />
		</ol>

		<h1>329.        Environmental Justice, Climate Change and Energy</h1>
		<ol start="330">
			<li>
				<em>
					We depend on clean air, clean water, and a healthy natural
					environment. Nothing in environmental{" "}
				</em>
			</li>
			<li>
				<em>
					resources management makes sense except in the light of scarcity.
					Significant and devastating{" "}
				</em>
			</li>
			<li>
				<em>
					effects of climate change are already impacting ecosystems, economies
					and communities. The well-
				</em>
			</li>
			<li>
				<em>
					being of every nation requires intelligent management of growth,
					limiting of urban sprawl, and{" "}
				</em>
			</li>
			<li>
				<em>
					preservation of farmland, wildlife habitat, and natural resources.
					Accordingly, protective laws and{" "}
				</em>
			</li>
			<li>
				<em>
					regulations must be vigorously enforced, and strengthened where
					needed.{" "}
				</em>
			</li>
			<li />
		</ol>
		<h2>337.         We support:</h2>
		<ol start="338">
			<li>
				Aggressive action now to reduce the emission of greenhouse gases in
				order to immediately
			</li>
			<li>
				retard, and ultimately prevent, further global climate change and ocean
				acidification.
			</li>
			<li>
				The development and greater use of clean and sustainable alternatives to
				fossil fuels, by
			</li>
			<li>
				such strategies as: (1) Government-subsidized research into wind, solar,
				ocean and
			</li>
			<li>
				geothermal power, along with biomass and other innovative technologies;
				(2) Higher fuel-
			</li>
			<li>
				efficiency and anti-pollution standards for all vehicles, and increasing
				the percentage of
			</li>
			<li>
				Alternative-fuel vehicles in use; (3) Tax incentives, including
				imposition of a carbon tax and
			</li>
			<li>
				repeal of tax breaks for oil companies; (4) Prohibition of new or
				expanded nuclear power
			</li>
			<li>
				plants without proof that their operation will be “clean” and waste
				dealt with responsibly.
			</li>
			<li>
				Intensified international efforts to prevent the release of chlorine
				gases, in order to protect
			</li>
			<li>the ozone layer.</li>
			<li>
				Creating incentives for conservation of energy, water and other
				resources, and for reuse and
			</li>
			<li>
				recycling in order to reduce the waste stream while assuring safe
				disposition of hazardous
			</li>
			<li />
			<li>Strengthening the State’s Growth Management Act.</li>
			<li>Reforming the State’s excessively liberal land-use vesting rules.</li>
			<li>
				Protecting environmentally sensitive areas, including the continental
				shelf, from exploration
			</li>
			<li>
				and/or extraction of oil, gas, and other substances, and from the
				disposal or treatment of
			</li>
			<li />
			<li>
				Production of renewable energy on farms, including biofuels, solar and
				wind power, to
			</li>
			<li>
				reduce greenhouse gases in ways that are sustainable and do not compete
				with food crops.
			</li>
			<li>
				Management and preservation of Wilderness areas and other public lands
				as national
			</li>
			<li>
				treasures, and according state and national parks the funding needed for
				their preservation
			</li>
			<li>and public enjoyment.</li>
			<li>Increased federal and state protection of wild and scenic rivers.</li>
			<li>
				A fair system of paying for growth, including assessment of impact fees
				on developers.
			</li>
			<li>
				Preserving green and open spaces in urban environments even as density
				increases so as to
			</li>
			<li>
				protect the quality of life for urban residents in terms of their
				physical, emotional and mental
			</li>
			<li>
				health, as scientific research has demonstrated this need (see, e.g.,
			</li>
			<li>https://depts.washington.edu/hhwb/Thm_Mental.html).</li>
			<li>
				Revising state laws to require greater participation by and authority of
				residents to approve
			</li>
			<li>major changes in zoning that affect more than one property.</li>
			<li>Producer responsibility for packaging and waste products.</li>
			<li>
				ASARCO funds and funds from the Model Toxics Control Act being used only
				for toxic
			</li>
			<li>
				site cleanup, including the 20,000 developed and undeveloped parcels
				slated for cleanup.
			</li>
			<li>
				Implementation of storm water reduction to address polluted runoff,
				including
			</li>
			<li>daylighting streams and creeks where possible.</li>
			<li>
				Restoration of endangered species and their habitats, which support
				biodiversity and
			</li>
			<li>
				sustainability, and incorporating the recovery management agreements
				into the endangered
			</li>
			<li>species act.</li>
			<li>
				Comprehensively quantifying Washington State water resources and
				managing them with a
			</li>
			<li>policy that is science-based, transparent, and effective.</li>
			<li>Transitioning to 100% renewables.</li>
			<li>
				Ensuring that 80% of the remaining global fossil fuel reserves must
				remain in the ground.
			</li>
			<li>Net metering in residential solar.</li>
			<li>
				Requiring that Environmental Impact Statements include climate change
				impacts.
			</li>
			<li>
				Adopting and sharing best practices in energy conservation and
				production to improve
			</li>
			<li>global innovation towards climate mitigation and solutions.</li>
			<li>
				Enforcing requirements that all corporations pay for the true costs of
				carbon emissions,
			</li>
			<li>chemical pollution, and waste-stream management in production.</li>
			<li>
				Making containment of existing and future nuclear waste a top priority
				and require this
			</li>
			<li>industry to pay for clean-up.</li>
			<li>
				Enhancing air, water, and soil quality protections by strengthening the
				Environmental
			</li>
			<li>Protection Agency (EPA).</li>
			<li>
				Protecting public lands and the public commons from destructive
				extraction industries and
			</li>
			<li>corporate exploitation.</li>
			<li>
				Requiring companies to increase safe use of their waste and byproducts
				for energy
			</li>
			<li>production and as raw materials for new products.</li>
			<li>
				Enforcing and strengthening our state's Growth Management Act to protect
				against further
			</li>
			<li>
				urban sprawl that consumes farmland, forests, wildlife habitat and
				natural resources.
			</li>
			<li />
		</ol>
		<h2>399.          We oppose:</h2>
		<ol start="400">
			<li>
				Coal power generating plants anywhere, and coal transportation by any
				means.
			</li>
			<li>
				Increased shipment of oil by train without increased safety measures.
			</li>
			<li>
				The construction of potentially hazardous pipelines through
				environmentally
			</li>
			<li>sensitive areas, especially areas containing aquifers.</li>
			<li>Undermining Initiative 937’s renewable-energy goals.</li>
			<li>
				Mountaintop removal for extraction of natural resources<strong>
					.{" "}
				</strong>
			</li>
			<li>
				Hydraulic fracturing for natural gas and mountaintop removal for
				extraction of coal and
			</li>
			<li>other natural resources.</li>
			<li>
				Spraying known toxins and carcinogens into public parks and greenspaces,
				including but
			</li>
			<li>not limited to: glyphosate and neonicotinoid.</li>
			<li>
				Low-income and Native American communities bearing a disproportionate
				load of
			</li>
			<li>polluting industries and otherwise dangerous facilities.</li>
			<li>Canada-to-Mexico tar sands pipeline (Keystone XL).</li>
			<li />
		</ol>

		<ol start="414">
			<li>
				<strong>Foreign Policy</strong>
			</li>
			<li>
				<em>
					Recognizing that our security ultimately depends on peace throughout
					the world, the United
				</em>
			</li>
			<li>
				<em>
					States will best protect its interests by working within the
					international community, using the
				</em>
			</li>
			<li>
				<em>
					tools of development, diplomacy and defense, in a spirit of mutual
					respect and
				</em>
			</li>
			<li />
			<li />
		</ol>
		<h2>420.         We support:</h2>
		<ol start="421">
			<li>Fully cooperative participation in the United Nations.</li>
			<li>
				Resolving international conflicts through diplomacy and international
				institutions, not by
			</li>
			<li>force or threat of force.</li>
			<li>
				Reduction of nuclear arsenals, coupled with international control of
				fissile material.
			</li>
			<li>
				Providing our fair share of aid to reduce world poverty and improve
				health, education,
			</li>
			<li>and access to safe water and food.</li>
			<li>
				Limiting military and other aid to only those nations that have
				demonstrated their respect
			</li>
			<li>for human and civil rights for men and women alike.</li>
			<li>
				Working closely and persistently with other countries to prevent
				genocide.
			</li>
			<li>
				Immediate signing and ratification of the U.N. agreement forming an
				International
			</li>
			<li>Criminal Court, and recognition of its jurisdiction.</li>
			<li>
				All nations’ strict adherence to the Nuclear Nonproliferation Treaty,
				the Geneva
			</li>
			<li>
				Conventions, the Biological and Chemical Weapons Conventions, and other
				international
			</li>
			<li>agreements protecting civilian populations.</li>
			<li>
				Improvement of international financial systems to prevent economic
				disruptions.
			</li>
			<li>
				Prompt use of the full influence of the United States, through serious,
				constructive and
			</li>
			<li>
				persistent engagement, to promote negotiations and other actions that
				lead to a successful
			</li>
			<li>
				and sustainable    resolution    of   the    Israeli–Palestinian
				conflict,    including    mutual
			</li>
			<li>
				recognition, that ensures security, economic growth, and quality of life
				for the peoples of
			</li>
			<li>the sovereign state of Israel and a sovereign state of Palestine.</li>
			<li>
				Greater participation with our neighboring states and provinces through
				the Pacific
			</li>
			<li>Northwest Economic Region.</li>
			<li>
				A secure peace in Palestine and Israel and throughout the Middle East;
				and oppose the
			</li>
			<li>
				creation of new settlements in Palestinian territory and demolition of
				existing settlements.
			</li>
			<li>
				The President obeying the War Powers Clause of the U.S. Constitution,
				which provides that
			</li>
			<li>
				only Congress has the non-delegable prerogative to decide to go to war.
			</li>
			<li />
		</ol>
		<h2>448.         We oppose:</h2>
		<ol start="449">
			<li>Preemptive war.</li>
			<li>
				Overt or covert efforts to destabilize other nations’ governments.
			</li>
			<li>
				The marginalization of women in any way, in any sphere of human
				activity, in every part
			</li>
			<li>of the world, whether by law, custom, or culture.</li>
			<li>
				Foreign trade agreements that put the interests of corporations above
				the rights and
			</li>
			<li>interests of governments or workers and the environment.</li>
			<li>
				The sale of weapons to nations that are utilized in human rights
				atrocities.
			</li>
			<li>
				Agents of foreign nations that work to misinform the populace through
				hoaxes and false
			</li>
			<li />
			<li>Any and all foreign interference in U.S. election efforts.</li>
			<li />
		</ol>

		<h1>460.        Government and Political Reform</h1>
		<ol start="461">
			<li>
				<em>
					Our government derives its legitimacy from, and is answerable to, the
					people. We are committed
				</em>
			</li>
			<li>
				<em>
					to a representative democracy that encourages maximum active
					participation, a voting process
				</em>
			</li>
			<li>
				<em>
					that is legitimate, fair, transparent, and open to all citizens, and a
					requirement of responsibility
				</em>
			</li>
			<li>
				<em>
					from voters and accountability from those they elect. No American
					citizen should be removed
				</em>
			</li>
			<li>
				<em>
					from the voting rolls or otherwise constrained from participating in
					an election for which they
				</em>
			</li>
			<li>
				<em>are eligible. </em>
			</li>
			<li />
		</ol>
		<h2>468.         We support:</h2>
		<ol start="469">
			<li>
				Vote-by-mail and other electoral procedures that increase voter
				participation, including
			</li>
			<li>
				same-day voter registration and an end to felon disenfranchisement.
			</li>
			<li>Instant Runoff elections.</li>
			<li>
				A full and thorough 2020 census count that ensures all residents,
				regardless of residency,
			</li>
			<li>
				citizenship status, sexual orientation or gender identity, are counted.
			</li>
			<li>Full representation of all United States territories in Congress.</li>
			<li>
				Voting rights to all Washington residents regardless of incarceration
				status, felony status,
			</li>
			<li>LFO repayment status, or community supervision status.</li>
			<li>
				Gender neutralization of the state constitution using they/them
				pronouns.
			</li>
			<li>
				Redistricting commissions that are fair and independent of political
				influence.
			</li>
			<li>Public financing for political campaigns.</li>
			<li>
				Adherence to open public meeting laws, with genuine opportunities for
				public comment
			</li>
			<li>on policy proposals, and to public disclosure laws.</li>
			<li>
				Accounting for tax exemptions as expenditures in the State budget.
			</li>
			<li>
				Prompt production of documents requested under the federal Freedom of
				Information Act
			</li>
			<li>and its state and local counterparts.</li>
			<li>
				Encouraging women, minorities, and other traditionally underrepresented
				persons to seek
			</li>
			<li>political office.</li>
			<li>
				Electing the U.S. President by popular vote, not a so-called "electoral
				college”.
			</li>
			<li>
				Redistricting in a fair and rational manner, independent of political
				influence and conducted
			</li>
			<li>by nonpartisan bodies.</li>
			<li>
				Making voting easier for members of the military and other citizens
				located overseas.
			</li>
			<li>
				Adherence to open-meeting laws at all levels, with genuine opportunities
				for public
			</li>
			<li>comment on policy proposals.</li>
			<li>
				Exclusion of lobbyists from any meeting or session of a federal or state
				legislative or
			</li>
			<li>regulatory body or committee, from which the public is excluded.</li>
			<li>
				Drastic reduction of the exemptions to our state's Public Records Act.
			</li>
			<li>
				Enforcing the Whistleblower Protection Act, which guarantees that
				government and
			</li>
			<li>
				corporate whistleblowers shall be protected from job loss or
				retaliation.
			</li>
			<li />
		</ol>
		<h2>499.          We oppose:</h2>
		<ol start="500">
			<li>The top-two “winnowing” primary.</li>
			<li>
				Onerous voter ID requirements and other techniques of voter suppression.
			</li>
			<li>Laws that require a supermajority vote to raise revenue.</li>
			<li>
				Expansion of Presidential powers, including the use of “signing
				statements” that purport
			</li>
			<li>to invalidate a portion of legislation being signed into law.</li>
			<li>
				The privatization or outsourcing of any essential governmental function.
			</li>
			<li>
				Government officials and employees becoming corporate lobbyists through
				the “revolving
			</li>
			<li>
				door” without a "cooling-off period" between government service and
				private employment.
			</li>
			<li />
		</ol>
		<h1>509.        Gun Safety and Reform</h1>
		<ol start="510">
			<li>
				<em>
					We believe that the right to own firearms is subject to reasonable
					regulation. Too many families in{" "}
				</em>
			</li>
			<li>
				<em>
					America have suffered – and continue to suffer – from gun violence.
					Over 30,000{" "}
				</em>
			</li>
			<li>
				<em>
					Americans die every year from gun violence, including over 10,000
					homicides. The time to{" "}
				</em>
			</li>
			<li>
				<em>act is now to address this public health crisis.</em>
			</li>
			<li />
		</ol>
		<h2>518.         We support:</h2>
		<ol start="519">
			<li>
				Mandatory registration, licensing, and certification of all firearms.
			</li>
			<li>
				Mandatory periodic safety training and certification for all gun owners,
				as well as
			</li>
			<li>safety locks and secure storage for all guns.</li>
			<li>
				Maintaining the total ban on automatic weapons in Washington State.
			</li>
			<li>A total ban on semi-automatic rifles.</li>
			<li>
				Background checks and waiting periods enforced for all gun and
				ammunition purchases.
			</li>
			<li>
				Keeping firearms out of the hands of those who present a danger to
				themselves or others.
			</li>
			<li>
				Holding firearm owners liable when crimes are committed with their
				unsecured gun(s).
			</li>
			<li>
				Banning firearm accessories that enable sustained high rates of fire,
				including bump stocks
			</li>
			<li>and magazines holding more than ten cartridges.</li>
			<li />
		</ol>
		<h2>530.         We oppose:</h2>
		<ol start="531">
			<li>
				Arming school teachers, administration, and faculty members, and other
				attempts to
			</li>
			<li>introduce or increase the presence of firearms in the classroom.</li>
			<li>Resale of guns seized by law enforcement.</li>
			<li>Illegal trafficking of guns.</li>
			<li />
		</ol>

		<h1>536.          Health Care</h1>
		<ol start="537">
			<li>
				<em>
					Health care is a basic human right. Our government should assure, and
					guarantee by law,
				</em>
			</li>
			<li>
				<em>
					accessible and affordable health care for all, making healthcare in
					Washington State a right for
				</em>
			</li>
			<li>
				<em>
					every person, and securing a patient’s right to choice of licensed
					provider type.{" "}
				</em>
			</li>
			<li />
			<li>
				<strong>We support:</strong>
			</li>
		</ol>
		<table>
			<tbody>
				<tr>
					<td>
						<p>
							<strong>542.</strong>
						</p>
					</td>
					<td>
						<p>
							●     Establishment of a comprehensive, publicly funded
							single-payer national
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>543.</strong>
						</p>
					</td>
					<td>
						<p>
							health care system for all, emphasizing preventive and primary
							care and chronic disease
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>544.</strong>
						</p>
					</td>
					<td>
						<p>
							management and relying on evidence-based guidelines, at a cost
							individuals and society
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>545.</strong>
						</p>
					</td>
					<td>
						<p>
							can afford, with specific benefits that address all the patient’s
							health needs without being
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>546.</strong>
						</p>
					</td>
					<td>
						<p>subject to arbitrary exclusions or termination.</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>547.</strong>
						</p>
					</td>
					<td>
						<p>●      </p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>548.</strong>
						</p>
					</td>
					<td />
				</tr>
				<tr>
					<td>
						<p>
							<strong>549.</strong>
						</p>
					</td>
					<td>
						<p>
							●     Use of government purchasing power to negotiate reduced
							prices for prescriptions that
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>550.</strong>
						</p>
					</td>
					<td>
						<p>include drugs, medical supplies and equipment.</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>551.</strong>
						</p>
					</td>
					<td>
						<p>
							●     Increasing the supply of primary-care providers, especially
							those who will work with
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>552.</strong>
						</p>
					</td>
					<td>
						<p>
							underserved communities and populations. In the 2015 Washington
							State legislative
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>553.</strong>
						</p>
					</td>
					<td>
						<p>
							session, funding was restored to the Washington State Health
							Professionals Loan
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>554.</strong>
						</p>
					</td>
					<td>
						<p>
							Repayment Program (HPLRP). We recommend continued funding for this
							program to
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>555.</strong>
						</p>
					</td>
					<td>
						<p>
							support underserved communities in recruiting and retaining
							primary care providers.
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>556.</strong>
						</p>
					</td>
					<td>
						<p>
							●     Ample funding of medical research under the National
							Institutes of Health, based on
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong> 557.</strong>
						</p>
					</td>
					<td>
						<p>
							scientific merit, and basing Public Health policy on sound
							objective scientific guidelines.
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>558.</strong>
						</p>
					</td>
					<td>
						<p>
							●     A unified electronic medical record system to reduce errors
							and enable a seamless
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>559.</strong>
						</p>
					</td>
					<td>
						<p>
							transfer of information among institutions, while providing a more
							accurate way to assess
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>560.</strong>
						</p>
					</td>
					<td>
						<p>
							clinical outcomes and compare treatment approaches, consistent
							with safeguarding of
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>561.</strong>
						</p>
					</td>
					<td>
						<p>patient privacy.</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>562.</strong>
						</p>
					</td>
					<td>
						<p>
							●     A Patient’s Bill of Rights guaranteeing (1) respect for
							people’s wishes regarding their
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>563.</strong>
						</p>
					</td>
					<td>
						<p>
							medical care, especially at the end of life, and for a patient’s
							right to refuse life-
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>564.</strong>
						</p>
					</td>
					<td>
						<p>
							sustaining interventions when it is the will of the patient or
							parent/guardian to choose that
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>565.</strong>
						</p>
					</td>
					<td>
						<p>
							approach, (2) the right of patients and loved ones to voice
							grievances to health care
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>566.</strong>
						</p>
					</td>
					<td>
						<p>
							institutions without fear of retaliation, and (3) a patient’s
							right to die in a non-institutional
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>567.</strong>
						</p>
					</td>
					<td>
						<p>setting.</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>568.</strong>
						</p>
					</td>
					<td>
						<p>
							●     Increased funding for global family planning, including
							comprehensive sex education and
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>569.</strong>
						</p>
					</td>
					<td>
						<p>contraception.</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>570.</strong>
						</p>
					</td>
					<td>
						<p>
							●     Availability of reproductive health and abortion services at
							medical institutions, with
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>571.</strong>
						</p>
					</td>
					<td>
						<p>guaranteed insurance coverage.</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>572.</strong>
						</p>
					</td>
					<td>
						<p>
							●     The use by all health care entities that receive any
							federal, state or city funds – including
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>573.</strong>
						</p>
					</td>
					<td>
						<p>
							but not limited to clinics, emergency rooms, pharmacies,
							hospitals, nursing homes – of the
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>574.</strong>
						</p>
					</td>
					<td>
						<p>
							scientific best practice principles for care and treatment
							regardless of religious affiliation
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>575.</strong>
						</p>
					</td>
					<td>
						<p>
							or belief of the providers, and honoring advance directives of all
							patients in their care.
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<p>
							<strong>576.</strong>
						</p>
					</td>
					<td />
				</tr>
			</tbody>
		</table>

		<ol start="577">
			<li>
				<strong>Housing Justice</strong>
			</li>
			<li>
				<em>
					Housing is a human right. We believe that the housing crisis must be
					addressed with real world{" "}
				</em>
			</li>
			<li>
				<em>
					solutions that provide for the needs of people. We realize that the
					private housing market has failed{" "}
				</em>
			</li>
			<li>
				<em>
					to adequately provide for those at low to middle income ranges. It is
					the right and proper role of{" "}
				</em>
			</li>
			<li>
				<em>
					government to provide permanent stable housing to people experiencing
					homelessness, to expand{" "}
				</em>
			</li>
			<li>
				<em>
					the stock of supportive, low income, and affordable housing, and to
					use all tools available to{" "}
				</em>
			</li>
			<li>
				<em>control the astronomic explosion of the cost of housing.</em>
			</li>
			<li />
			<li>
				<strong>We support:</strong>
			</li>
			<li>
				Immediate construction of publicly funded and owned housing in an
				adequate amount to
			</li>
			<li>provide housing for all.</li>
			<li>
				Providing enough clean, livable, temporary shelter to ensure people
				experiencing
			</li>
			<li>
				homelessness do not go unsheltered until stably housed in permanent
				housing.
			</li>
			<li>
				Housing first and low-barrier housing strategies that require as few
				barriers or restrictions as
			</li>
			<li>
				possible, recognizing that being stably housed is the first step to
				stabilization.
			</li>
			<li>
				Ensuring that both private commercial housing and public housing are
				maintained in a safe,
			</li>
			<li>livable condition.</li>
			<li>
				Protections against discrimination in housing on the basis of race,
				country of origin,
			</li>
			<li>
				religious or non-religious status, gender or gender identity, sexual
				orientation, veteran status,
			</li>
			<li>source of income, political beliefs, or relationship status.</li>
			<li>
				Aggressive state and local action funded with progressive revenue
				sources to secure housing
			</li>
			<li>
				as a human right for all in Washington with or without federal
				cooperation to these ends.
			</li>
			<li>
				Aggressive federal action funded by progressive revenue sources to
				secure housing as a
			</li>
			<li>human right for all nationally.</li>
			<li>Repeal of Washington State’s prohibition on rent control.</li>
			<li>
				Requiring private developers to set aside 25% of units for affordable
				housing on a
			</li>
			<li>permanent basis in up-zoned developments.</li>
			<li>
				Relaxation of zoning which prevents construction of tiny houses,
				duplexes, and accessory
			</li>
			<li>
				dwelling units on single lots as well as restrictions on co-housing.
			</li>
			<li>
				Halting and reversing the growth of regressive residential property
				taxes in favor of more
			</li>
			<li>progressive forms of revenue collection.</li>
			<li />
			<li>
				<strong>We oppose:</strong>
			</li>
			<li>
				Dehumanization and victim blaming of our neighbors experiencing
				homelessness.
			</li>
			<li>Any policy of “sweeps” aimed at our unsheltered neighbors.</li>
			<li>
				Hostile urban architecture that serves no purpose other than to keep
				people in extreme
			</li>
			<li>poverty removed from certain areas.</li>
			<li>Criminalization of people who are experiencing homelessness.</li>
			<li>
				Anti-nuisance ordinances which force people to choose between their
				safety and being
			</li>
			<li />
			<li />
		</ol>

		<ol start="618">
			<li>
				<strong>Human Services</strong>
			</li>
			<li>
				<em>
					One of the highest priorities of government is to provide a safety net
					of social services that meets{" "}
				</em>
			</li>
			<li>
				<em>
					the basic needs of people who may be on the margins of society,
					whether they be elderly,{" "}
				</em>
			</li>
			<li>
				<em>
					impoverished, homeless, mentally ill, disabled, or have chemical
					dependency, to enable them to{" "}
				</em>
			</li>
			<li>
				<em>
					attain their full potential. The only genuine welfare reform is one
					that reduces poverty, not just the{" "}
				</em>
			</li>
			<li>
				<em>welfare rolls. </em>
			</li>
			<li />
		</ol>
		<h2>625.         We support:</h2>
		<ol start="626">
			<li>
				Quality, affordable childcare, education, training, medical care and
				substance-abuse
			</li>
			<li>
				treatment as cost-effective methods of helping working parents and
				others get off welfare
			</li>
			<li>and escape poverty.</li>
			<li>
				Public assistance to safeguard those unable to provide for themselves.
			</li>
			<li>
				Maintaining Social Security benefits for dependent survivors and the
				disabled.
			</li>
			<li>
				Alternatives to nursing homes for people who do not need full-time
				skilled nursing care.
			</li>
			<li>
				Increased funding for and access to fully-funded mental health and
				substance-abuse
			</li>
			<li />
			<li>
				Making more public facilities accessible under the Americans with
				Disabilities Act.
			</li>
			<li>
				Investigating all reports of abuse and neglect in adult family homes,
				boarding homes,
			</li>
			<li>group homes and other assisted living facilities.</li>
			<li>
				Providing appropriate housing and care for the mentally ill and the
				developmentally
			</li>
			<li />
			<li>
				Maintaining state mental hospitals and residential habitation centers,
				including Fircrest
			</li>
			<li>and Rainier School.</li>
			<li>
				Adequate training for all staff who work with vulnerable populations.
			</li>
			<li>
				Community Health Engagement Locations (CHELs), also referred to as safe
				consumption
			</li>
			<li>
				sites, and requiring first responders to be trained and equipped for
				overdose response.
			</li>
			<li>
				Reinvesting a significant portion of the military budget into family
				support, living-wage job
			</li>
			<li>development, and work training programs.</li>
			<li />
		</ol>
		<h2>647.         We oppose:</h2>
		<ol start="648">
			<li>Erosion of the social safety net.</li>
			<li>
				Discharging hospital patients or residents from state-operated
				residential facilities to
			</li>
			<li>
				homeless shelters or group homes without adequately trained staff,
				medicines and
			</li>
			<li>
				services, including safety oversight to make sure vulnerable people are
				not at risk of
			</li>
			<li />
			<li>
				Using the criminal justice system to incarcerate the mentally ill and
				the developmentally
			</li>
			<li />
			<li>Privatization of Social Security.</li>
			<li />
		</ol>

		<h1>657.        Immigration</h1>
		<ol start="658">
			<li>
				<em>
					America is a nation of immigrants. Immigrants strengthen America. All
					immigrants should be
				</em>
			</li>
			<li>
				<em>
					afforded full human rights and a fair, safe, and timely path to legal
					status. Children of
				</em>
			</li>
			<li>
				<em>
					immigrants should be accorded the same educational opportunities as
					other children. We
				</em>
			</li>
			<li>
				<em>
					encourage state and local government to continuously respect and honor
					diversity in our
				</em>
			</li>
			<li />
			<li />
		</ol>
		<h2>664.         We support:</h2>
		<ol start="665">
			<li>
				Comprehensive immigration reform that protects the integrity of our
				borders while
			</li>
			<li>
				recognizing the basic human rights of immigrants and providing them a
				clear pathway to
			</li>
			<li>legal status and citizenship if desired.</li>
			<li>
				Ensuring that immigrants obtain due process in all proceedings and have
				access to bail
			</li>
			<li />
			<li>
				Policies that integrate and support immigrants as members of our
				communities, including
			</li>
			<li>
				preparation for citizenship, and establishing statute of limitations to
				barriers for citizenship.
			</li>
			<li>
				Requiring state and local government to provide interpretation services
				or translation of
			</li>
			<li>government documents.</li>
			<li>Education of all children regardless of immigration status.</li>
			<li>Sanctuary jurisdictions adopting sanctuary policies.</li>
			<li>
				Legal residents having the same rights as American citizens concerning
				waiting time for
			</li>
			<li>their legal spouses to join them in the U.S.</li>
			<li>
				Giving ID documents and driver’s licenses to immigrants regardless of
				status.
			</li>
			<li />
		</ol>
		<h2>680.      We oppose:</h2>
		<ol start="681">
			<li>
				    Demands for immediate production of documents regarding one’s status
				in the United
			</li>
			<li>States, other than as part of federal immigration enforcement.</li>
		</ol>
		<ol start="683">
			<li>Removal of DACA until a clean DREAM Act is passed.</li>
			<li>The construction of a wall on the Mexican border.</li>
			<li>
				Giving ICE arresting powers without a warrant within 100 miles of the
				border.
			</li>
			<li>Companies that import workers and do not pay them minimum wage.</li>
			<li>
				Separating children from their parents while in immigration proceedings.
			</li>
			<li>
				Cooperation by local or state governments with federal efforts to detain
				and/or deport
			</li>
			<li>undocumented community members.</li>
			<li>
				Detention of immigrants for crimes which citizens would not be detained
				for.
			</li>
			<li>The use of private facilities for immigration detention.</li>
			<li />
		</ol>

		<h1>693.        Labor</h1>
		<ol start="694">
			<li>
				<em>
					Organized labor is essential to the social, economic, and political
					health of our democracy. The
				</em>
			</li>
			<li>
				<em>
					tremendous improvement in the overall standard of living that occurred
					in the years after World
				</em>
			</li>
			<li>
				<em>
					War II was due to the preceding and ongoing struggles of unions and
					working people who fought,
				</em>
			</li>
			<li>
				<em>
					sacrificed, and died to gain the right to organize and bargain
					collectively for better working
				</em>
			</li>
			<li>
				<em>
					conditions and a share in our prosperity. The decline of real wages
					over the past three
				</em>
			</li>
			<li>
				<em>
					decades, accompanied by powerful anti-union political attacks, has
					intensified the need for a
				</em>
			</li>
			<li>
				<em>strong union movement.</em>
			</li>
			<li />
		</ol>
		<h2>702.         We support:</h2>
		<ol start="703">
			<li>
				The legal right of public and private sector employees to organize into
				unions, to bargain
			</li>
			<li>
				collectively on all matters pertaining to their working conditions and
				compensation, and
			</li>
			<li>the right to strike without fear of reprisal or replacement.</li>
			<li>
				The automatic recognition of a union, based on the signatures of a
				majority of those
			</li>
			<li>
				represented, and punishment of corporations that engage in anti-union
				efforts.
			</li>
			<li>
				Enforcement of prevailing wage laws for work under government contracts,
				and of area
			</li>
			<li>standards for wages and benefits.</li>
			<li>
				A minimum wage that is a living wage, equal pay for equal work, and
				protection of
			</li>
			<li>overtime pay.</li>
			<li>
				A workers’ Bill of Rights that assures rights of assembly, association
				and free speech,
			</li>
			<li>
				due process, freedom from discrimination, and democracy within unions.
			</li>
			<li>
				Equal participation between labor and employers in the management of
				workers’
			</li>
			<li />
			<li>
				Apprenticeship set-asides, and full funding for enforcement of child
				labor standards that
			</li>
			<li>
				limit the hours youths are allowed to be employed during the school
				year.
			</li>
			<li>Respecting picket lines by not crossing them.</li>
			<li>
				“Best Value Contractor” policies and “Fair Contracting” with
				“Responsible and
			</li>
			<li>
				Responsive” bidder language in all public works jobs that include
				contractor
			</li>
			<li>
				responsibilities for prevailing wages, health insurance, retirement and
				training.
			</li>
			<li>
				Preference for Washington State residents and contractors on public
				works projects in our
			</li>
			<li />
			<li>Provision of medical and other benefits to all employees.</li>
			<li>
				Creation and rigorous enforcement of anti-wage theft laws and
				regulations.
			</li>
			<li>Paid family and medical leave.</li>
			<li>
				A closing of sub-minimum wage loopholes in state, county, and local
				levels and ending of
			</li>
			<li> 4C certifications under the minimum wage.</li>
			<li>
				Passage of an employee non-discrimination act to protect queer, trans,
				and gender non-
			</li>
			<li>confirming/non-binary people in the workplace.</li>
			<li>
				We support a minimum wage of at least 15 dollars per hour, in 2016
				dollars adjusted for
			</li>
			<li>
				inflation, by 2021 increasing with the rate of inflation annually
				thereafter.
			</li>
			<li>
				Full workers compensation and medical treatment for on the job injuries.
			</li>
			<li>
				Strong whistleblower laws that ensure the protection of workers who
				report safety hazards,
			</li>
			<li>
				executive malfeasance, harassment, or other violations of law or
				regulations in the
			</li>
			<li />
			<li>
				Requiring corporations to allocate a percentage of corporate board seats
				to labor.
			</li>
			<li>
				The Employee Free-Choice Act, which streamlines union certification for
				bargaining with
			</li>
			<li />
			<li />
			<li>
				<strong>We oppose:</strong>
			</li>
			<li>
				So-called “right-to-work” laws and other anti-union legislation,
				including the Taft-Hartley
			</li>
			<li />
			<li>Government officials’ attacks on unions and government employees.</li>
			<li>The privatization or out-sourcing of public services.</li>
			<li>
				Under-funding of workers’ pensions by either the private or public
				sector.
			</li>
			<li>
				The unilateral redefining of workers as independent contractors in order
				to reduce labor
			</li>
			<li>
				costs, render workers ineligible for benefits, or weaken safety
				standards and other
			</li>
			<li />
			<li>
				Foreign trade agreements that put the interests of corporations above
				the rights of
			</li>
			<li>workers and protection of the environment.</li>
			<li>
				Weakening of either State or federal Family and Medical Leave laws.
			</li>
			<li>
				Training wages, counting tips towards wages, and any other exceptions to
				the minimum
			</li>
			<li />
			<li>
				Efforts to limit the autonomy of local jurisdictions to enact a minimum
				wage higher than the
			</li>
			<li>state minimum wage.</li>
			<li />
		</ol>

		<p>
			<em> </em>
		</p>
		<h1>758.          Law and the Justice System</h1>
		<ol start="759">
			<li>
				<em>
					The rule of law is prerequisite to a civil society. A responsible
					government provides safety,
				</em>
			</li>
			<li>
				<em>
					security, and even-handed justice for all, with fairness and respect
					for the individual.{" "}
				</em>
			</li>
			<li />
		</ol>
		<h2>762.         We support:</h2>
		<ol start="763">
			<li>
				Refocusing the criminal justice system to emphasize prevention,
				diversion, and
			</li>
			<li>
				rehabilitation over incarceration, and on preparing prisoners for
				re-entry into society.
			</li>
			<li>
				Working to eliminate the conditions that lead to crime, by government
				investment in
			</li>
			<li>
				education and social services (including mental health and youth
				programs) and
			</li>
			<li>
				promotion of living-wage jobs so as to reduce poverty and homelessness.
			</li>
			<li>Abolition of the death penalty. </li>
			<li>Abolition of youth solitary confinement.</li>
			<li>
				Keeping nonviolent crimes off the “Three Strikes” list, with immediate
				transfer
			</li>
			<li>of inmates to rehabilitation programs.</li>
			<li>An end to the “war on drugs”.</li>
			<li>
				The creation and funding of specialized drug courts, and of specialized
				mental health
			</li>
			<li>
				courts to arrange for the humane treatment and rehabilitation of
				mentally ill people
			</li>
			<li>caught up in the criminal justice system.</li>
			<li>
				Vigorous prosecution of white-collar crime, especially that occurring in
				the financial
			</li>
			<li />
			<li>
				Removing Cannabis from Schedule I of the Controlled Substances Act.
			</li>
			<li>
				The end of the prison industrial complex, including the school to prison
				pipeline.
			</li>
			<li>Ensuring racial and socio-economic diversity on juries.</li>
			<li>
				Application of policing and prosecution that is race-neutral in intent
				and effect.
			</li>
			<li>
				Protecting our state prerogative to maintain a safe and legal market for
				the manufacture,
			</li>
			<li>sale, and consumption of cannabis.</li>
			<li>
				Ending sentences currently being served related to the manufacture,
				sale, and consumption
			</li>
			<li>
				of small amounts of cannabis, and expunging said sentences from
				residents’ criminal
			</li>
			<li> </li>
			<li>
				Increasing the funding of legal aid programs to ensure access to the
				courts for all people,
			</li>
			<li>regardless of wealth.</li>
			<li>
				Increasing the funding of public defender programs to ensure all people,
				regardless of
			</li>
			<li>
				wealth, have access to adequate representation in criminal cases, as
				guaranteed by the
			</li>
			<li />
			<li>
				Implementing safeguards to ensure access to courts for all people,
				regardless of citizenship
			</li>
			<li />
			<li>
				The abolishment of money bail for those charged with crimes, the use of
				statutory-based
			</li>
			<li>
				detention for those who pose an unacceptable risk to the community, and
				to expand the use
			</li>
			<li>of pre-trial services.</li>
			<li>
				Mandate charge or disposal of a complaint within 24 hours of arrest by
				prosecutors.
			</li>
			<li>
				Prohibition of arrest or detention on behalf of judgements for private
				debt collection
			</li>
			<li>corporations, also referred to as debtor examination warrants.</li>
			<li>
				Elimination of civil asset forfeiture without criminal conviction
				outside of state mandated
			</li>
			<li />
			<li>
				Improved law enforcement de-escalation training to prevent killing of
				suspects and
			</li>
			<li>excessive use of force.</li>
			<li>
				Accountability in law enforcement, with effective civilian review.
			</li>
			<li>
				Providing adequate health care, education, vocational training, and
				rehabilitation for all
			</li>
			<li>incarcerated persons.</li>
			<li />
			<li>
				<strong>We oppose:</strong>
			</li>
			<li>The utilization of public records to hunt undocumented persons.</li>
			<li>Private prisons.</li>
			<li>
				Economic incentives which encourage imprisonment and detainment, such as
				sub-minimum
			</li>
			<li />
			<li />
		</ol>

		<ol start="814">
			<li>
				<strong>Media Reform</strong>
			</li>
			<li>
				<em>
					Essential for democracy to flourish are an informed citizenry,
					persistently inquisitive media, and
				</em>
			</li>
			<li>
				<em>
					the free flow of information, including Internet neutrality. The
					public owns the broadcast
				</em>
			</li>
			<li>
				<em>
					airwaves and the Internet, which should be managed to serve the public
					interest.
				</em>
			</li>
			<li />
		</ol>
		<h2>819.         We support:</h2>
		<ol start="820">
			<li>
				Diversity of ownership as a central principle of broadcast licensing.
			</li>
			<li>
				Strengthening media ownership regulations to avoid corporate domination
				of our
			</li>
			<li />
			<li>Encouraging minority and community media ownership.</li>
			<li>
				Ensuring that media license holders provide diverse programming daily.
			</li>
			<li>
				Increased    funding    for    public    broadcasting,    including
				documentary    films    and
			</li>
			<li>non-commercial news programs.</li>
			<li>
				Adequate, stable public funding, free of political pressure, for public
				radio and public
			</li>
			<li />
			<li>
				Establishing a system for community-level, non-profit and non-commercial
				radio and TV
			</li>
			<li />
			<li>
				Ensuring that rural Americans have access to a modern communications
				infrastructure.
			</li>
			<li>
				Reinstatement of the Fairness Doctrine for broadcast media during
				election campaigns
			</li>
			<li>
				and making it applicable to cable networks to promote greater balance in
				coverage.
			</li>
			<li>Increasing affordable access to high-speed Internet statewide.</li>
			<li>
				Enhancing content diversity and press freedom by retaining and
				strengthening
			</li>
			<li>
				community media voices and messages, in order to foster greater
				participation in our
			</li>
			<li>community’s shared social, cultural and political life.</li>
			<li>
				Aggressive application of antitrust laws to prevent the formation of
				monopolies in the
			</li>
			<li>wireless and broadband service industries.</li>
			<li>
				Local governments placing more public information and documents online.
			</li>
			<li>
				Public access to the Internet and public ownership of Internet
				infrastructure, emphasizing
			</li>
			<li>development of broadband in rural areas.</li>
			<li>
				Internet neutrality, in which all users and content providers have equal
				access to Internet
			</li>
			<li>
				service without discrimination or prioritization based on content
				transmitted.
			</li>
			<li>
				Free radio and TV access for candidates and ballot issues before each
				election.
			</li>
			<li />
		</ol>
		<h2>847.        We oppose:</h2>
		<ol start="848">
			<li>
				    False claims of “national security” to suppress investigative
				journalism.
			</li>
			<li>
				    Media consolidation, which has a negative impact on communities
				across the United
			</li>
			<li />
			<li />
		</ol>

		<h1>852.        Military</h1>
		<ol start="853">
			<li>
				<em>
					Those who have answered our country’s call to military service must be
					accorded the quality
				</em>
			</li>
			<li>
				<em>
					medical care and family/veterans’ benefits promised at the time of
					their recruitment and
				</em>
			</li>
			<li>
				<em>enlistment</em>.
			</li>
			<li />
		</ol>
		<h2>857.         We support:</h2>
		<ol start="858">
			<li>
				Assuring that our military personnel and veterans receive the very best
				in physical and
			</li>
			<li>mental rehabilitation wherever needed and without cost.</li>
			<li>
				Military service on our behalf being performed only by men and women
				accountable to
			</li>
			<li>
				the public, the law, and the Uniform Code of Military Justice, and not
				by contractors.
			</li>
			<li>
				Assuring that all American military personnel are instructed in their
				rights and
			</li>
			<li>
				responsibilities, including those under the Geneva Conventions, and that
				they are
			</li>
			<li>
				subjected to vigorous prosecution for violations thereof or for other
				criminal conduct
			</li>
			<li>including sexual assault or harassment.</li>
			<li>
				Ending restrictions on providing abortions or reproductive health
				services as part of
			</li>
			<li>humanitarian aid.</li>
			<li>
				Independent military prosecutors, not commanding officers, investigating
				and
			</li>
			<li>prosecuting all allegations of sexual misconduct.</li>
			<li>Full funding for the Department of Veterans Affairs.</li>
			<li>
				The expeditious and orderly draw-down of U.S. forces from conflicts
				abroad.
			</li>
			<li>
				Maintaining a well-trained and well-equipped military, sufficient for
				the defense of the
			</li>
			<li>
				American people, our vital interests, and our treaty partners when all
				other means of
			</li>
			<li>diplomacy and protection are exhausted.</li>
			<li>
				Congress asserting its constitutional powers to declare war, regulate
				and govern the
			</li>
			<li />
			<li />
			<li>
				<strong>We oppose:</strong>
			</li>
			<li>
				Activation of National Guard personnel for service outside the United
				States, other than
			</li>
			<li>pursuant to a declaration of war.</li>
			<li>Preemptive use of military force.</li>
			<li>
				Nuclear, chemical, or biological weapons and genocidal biological
				warfare.
			</li>
			<li>Excessive or unnecessary military spending.</li>
			<li>
				The undue influence of the military-industrial complex over national
				policy.
			</li>
			<li>
				U.S. training of foreign military or police forces that suppress human
				rights in their own
			</li>
			<li />
			<li />
		</ol>

		<h1>888.      Transportation</h1>
		<ol start="889">
			<li>
				<em>
					We believe that an efficient, well-planned, multimodal transportation
					system promotes a healthy
				</em>
			</li>
			<li>
				<em>economy, environment and community. </em>
			</li>
			<li />
		</ol>
		<h2>892.     We support:</h2>
		<ol start="893">
			<li>
				Increased investment in the expansion and maintenance of the State’s
				transportation
			</li>
			<li>
				infrastructure, including accessible transportation for all, with safe,
				affordable, and efficient
			</li>
			<li>bus and rail services.</li>
			<li>
				Land-use planning around existing and planned transportation
				infrastructure, with the
			</li>
			<li>
				goal of decreasing the need to drive, while recognizing that there will
				remain a need for
			</li>
			<li>the personal automobile.</li>
			<li>
				A transportation system safe for all users, motorized and non-motorized.
			</li>
			<li>
				The creation of pedestrian friendly and cycling master plans as separate
				pieces of local
			</li>
			<li>government transportation planning.</li>
			<li>
				The construction and maintenance of sidewalks, bike trails, and bike
				corridors, including
			</li>
			<li>the Burke-Gilman and Interurban Trails.</li>
			<li>
				Increased investment by the federal and State government in public
				infrastructure that
			</li>
			<li>broadens individuals’ multimodal transportation choices.</li>
			<li>
				Institutions, including employers, providing free or low-cost mass
				transit passes to their
			</li>
			<li>employees or other constituents.</li>
			<li>
				The development of a high-speed rail system from Seattle to Vancouver
				and Seattle to
			</li>
			<li />
			<li>Free and reduced transit fare for people with low incomes.</li>
			<li>
				Reducing barriers for transit ridership, including lower fares and other
				incentives.
			</li>
			<li>
				Ecologically sound forms of transportation that minimize pollution and
				maximize
			</li>
			<li />
			<li>
				Design streets to promote safe speeds and safe interaction with
				pedestrians.
			</li>
			<li>
				Regularly increase Corporate Average Fuel Economy (CAFE) standards to
				levels which
			</li>
			<li>challenge automakers to improve fuel efficiency.</li>
			<li />
		</ol>
		<h2>918.     We oppose:</h2>
		<ol start="919">
			<li>
				Increased privatization of public infrastructure and transportation.
			</li>
			<li>
				Regressive funding of transit, such as flat car tabs and property taxes
				and fares.
			</li>
			<li>
				The continuation of massive subsidies to the auto, nuclear power plant,
				and fossil fuel
			</li>
			<li />
			<li />
		</ol>

		<ol start="924">
			<li>
				<strong>Tribal Relations and Sovereignty </strong>
			</li>
			<li>
				<em>
					Local, state, and federal governments must respect Native American
					nations on a sovereign
				</em>
			</li>
			<li>
				<em>
					government-to-government basis and must respect the decisions of
					Native American Nations,
				</em>
			</li>
			<li>
				<em>
					affirm their rights derived from treaties and state compacts, and
					oppose attempts to diminish
				</em>
			</li>
			<li>
				<em>
					their sovereignty and cultures. We call on local, state, and federal
					governments to help improve
				</em>
			</li>
			<li>
				<em>
					the social, economic, and health status of all Native American people
					to a level equivalent to that
				</em>
			</li>
			<li>
				<em>of other Americans. </em>
			</li>
			<li />
		</ol>
		<h2>932.         We support:</h2>
		<ol start="933">
			<li>Upholding treaties and tribal agreements by all parties.</li>
			<li>
				Restoration of federal recognition for formerly recognized Tribes,
				including the
			</li>
			<li>Duwamish and Chinook.</li>
			<li>
				Continued efforts to maintain and restore salmon runs and protect
				shellfish resources
			</li>
			<li>critical to Native American cultures and economies.</li>
			<li>
				The preservation and protection of sites of historic, cultural and
				religious significance.
			</li>
			<li>
				Educating the American public about the inherent and treaty-based rights
				of Indian tribes,
			</li>
			<li>
				a step that is vital to respectful and civil relations between Indian
				tribes and local
			</li>
			<li>communities, the state, and the nation.</li>
			<li>
				Educating students in the local school districts by incorporating more
				of the history and
			</li>
			<li>
				culture of Native American Peoples, especially those residing in
				Washington.
			</li>
			<li>
				Continued funding of programs that combat social and economic problems{" "}
				<span style={{"text-decoration": "line-through"}}>s</span>uch as
			</li>
			<li>poverty and alcoholism.</li>
			<li>Fully cooperative relations with Indian Country regarding water.</li>
			<li>
				Recognize that Urban Natives in Seattle make up a higher percentage per
				capita of the
			</li>
			<li>
				homeless population and are seven more times likely to be homeless than
				any other race in
			</li>
			<li>the Seattle area according to the 2016 one-night count.</li>
			<li>
				Providing Police Officer training on cross-cultural Urban Native
				communication to avoid
			</li>
			<li>discriminatory policing.</li>
			<li>
				Providing Judicial training on the federal Indian Child Welfare Act of
				1978 and the
			</li>
			<li>
				Washington State Indian Child Welfare Act of 2013 to prevent the loss of
				children’s Native
			</li>
			<li>ties and identity.</li>
			<li>
				Increased governmental efforts, including data collection done in
				consultation with Urban
			</li>
			<li>
				Natives, to identify American Indian and Alaska Native students who are
				multi-racial, in
			</li>
			<li>
				order to address necessary education funding purposes and programs.
			</li>
			<li>
				Inclusiveness in any education summits put on by state, county, regional
				and local
			</li>
			<li>
				governments where Urban Natives have been systematically left out.
			</li>
			<li>
				Aggressive reduction of carbon emissions at levels that will protect the
				indigenous/human
			</li>
			<li>
				rights of future generations, including those dependent on traditional
				foods.
			</li>
			<li>
				Support a declaration of the second Monday in October as “Indigenous
				Peoples Day”.
			</li>
			<li>Fully cooperative relations with Indian Country regarding water.</li>
			<li />
		</ol>
		<h2>965.        We oppose:</h2>
		<ol start="966">
			<li>
				All attempts to diminish Tribal sovereignty or Tribal Treaty Rights of
				the 29
			</li>
			<li>Federally Recognized Tribes in the State.</li>
			<li>
				All attempts of governments to continue to treat Urban Natives as
				invisible
			</li>
			<li>
				due to forced assimilation through prior discriminatory Acts of Congress
				and
			</li>
			<li>racist policies throughout U.S. history.</li>
			<li>
				The harmful legacy of the use of “Indian mascots” in all sports in the
				State.
			</li>
		</ol>
		<p>So, we got to line 769 -</p>
	</div>
</Layout>
)