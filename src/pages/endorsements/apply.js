import Helmet from 'react-helmet'
import React from 'react'
import { Link } from 'gatsby'
import Layout from '../../components/layout'

const title = 'Endorsing candidates'
export default () => (
<Layout>
	<div className="article">
	<Helmet>
		<title>{title}</title>
	</Helmet>
	<h1>{title}</h1>
	<p>
		Our endorsement committee vets candidates for public office.
		The bylaws state that only self-identified Democratic candidates may be endorsed.
		(Judicial candidates exempt.)
	</p>
	<h2>
		Candidates seeking endorsement must:
	</h2>
	<ol>
		<li>
			Declare candidacy. (See{' '}
			<a href="https://www.kingcounty.gov/depts/elections/for-candidates.aspx">King County</a> or{' '}
			<a href="https://snohomishcountywa.gov/1989/Candidates-Political-Parties">Snohomish County</a>.)
		</li>
		<li>
			<a href="mailto:endorsements@32democrats.org">Email the Endorsement Committee</a> the following:
			<ol className="alpha">
				<li>
					<Link to="/civility-pledge">Signed civility pledge</Link>
				</li>
				<li>
					Completed questionnaire, as appropriate from the <a href="https://www.kcdems.org/our-party/endorsements/2020-endorsements/">King County Democrats</a> or <a href="http://snocodems.org/endorsements/">Snohomish County Democrats</a> (
					<a href="/doc/2020/SnoCoDems-Questionnaire-Judicial.pdf">judicial</a>
					).
				</li>
			</ol>
		</li>
		<li>
			Interview at the Endorsement Committee meeting.
			<ul>
				<li>2020-04-15 from 6:30 p.m. to 8:30 p.m.</li>
				<li>2020-05-09 from 10:00 a.m. to 2:30 p.m.</li>
				<li>2020-05-23 from 10:00 a.m. to 2:30 p.m.</li>
			</ul>
		</li>
	</ol>
	<p>
		Rules governing candidate endorsements are specified in the <Link to="/bylaws#S9">bylaws</Link> and <Link to="/rules#S2">standing rules 2</Link> and <Link to="/rules#S3">3</Link>.
	</p>
	<p>
		Work with us to keep the 32nd District a Democratic Party stronghold!
	</p>
	</div>
</Layout>
)
