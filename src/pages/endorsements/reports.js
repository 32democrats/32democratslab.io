import React from 'react'
import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import Layout from '../../components/layout'

const title = 'Endorsement Committee Reports'
export default () => (
	<Layout>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<h1>{title}</h1>
		<ul className="menu">
			<li>
				<Link to="/endorsements/2020/concerns-about-King-County-Amendment-2">Concerns about King County Amendment #2</Link>
			</li>
			<li>
				<Link to="/endorsements/2020/05">2020-05-27</Link>
			</li>
		</ul>
	</Layout>
)
