import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'

const year = '2020'
const email = <span role="img" aria-label="Email" title="Email" className="fade no-print">📧</span>
const info = <span role="img" aria-label="Info" title="Info" className="info-icon fade no-print">ℹ</span>
const web = <span role="img" aria-label="Website" title="Website" className="fade no-print">🏠</span>
const vote_info = [
	<b>Vote by Tuesday, November 3, {year}.</b>,
	<br/>,
	`Mail your ballot (no stamp needed) or use a drop box.`,
	<br/>,
	`Drop boxes are open all hours until 8 p.m. on election day.`,
]

const render = () => (
<Layout>
<Helmet>
	<title>{year} Endorsements</title>
</Helmet>
<p style={{marginBottom:0}}>
	{vote_info}
</p>
<ul className="menu">
	<li>
		<a href="https://www.kingcounty.gov/depts/elections/how-to-vote/ballots/returning-my-ballot/ballot-drop-boxes.aspx">
			King County ballot drop boxes
		</a>
	</li>
	<li>
		<a href="https://snohomishcountywa.gov/225/Ballot-Drop-Box-Locations">
			Snohomish County ballot drop boxes
		</a>
	</li>
</ul>
<br/>
<h1>
	<Link to="/endorsements">Endorsements</Link>: {year}
</h1>
<ul className="endorsements">
<li><Link to="/resolutions/2020/R-90">Referendum Measure 90</Link>: <b>Approved</b></li>
<li>Federal
		<ul>
			<li>President / Vice President: <b>Joseph R. Biden / Kamala D. Harris</b>
				{' '}<a href="https://joebiden.com/#">{web}</a>
			</li>
			<li>Congressional District 2 U.S. Representative: <b>Rick Larsen</b>
				{' '}<a href="https://www.ricklarsen.org/">{web}</a>
			</li>
			<li>Congressional District 7 U.S. Representative: <b>Pramila Jayapal</b>
				{' '}<a href="https://www.pramilaforcongress.com/">{web}</a>
			</li>
		</ul>
	</li>
	<li>State
		<ul>
			<li>Governor: <b>Jay Inslee</b>
				{' '}<a href="https://jayinslee.com">{web}</a>
			</li>
			<li>Lieutenant Governor: <b>Denny Heck</b>
				{' '}<a href="https://dennyheck.com/">{web}</a>
			</li>
			<li>Secretary of State: <b>Gael Tarleton</b>
				{' '}<a href="https://www.voteforgael.org/">{web}</a>
			</li>
			<li>Treasurer: <b>Mike Pellicciotti</b>
				{' '}<a href="https://www.electmikep.com/">{web}</a>
			</li>
			<li>Auditor: <b>Pat (Patrice) McCarthy</b>
				{' '}<a href="https://www.patmccarthyauditor.com/">{web}</a>
			</li>
			<li>Attorney General: <b>Bob Ferguson</b>
				{' '}<a href="https://www.electbobferguson.com/">{web}</a>
			</li>
			<li>Commissioner of Public Lands: <b>Hilary Franz</b>
				{' '}<a href="https://www.hilaryfranz.com/">{web}</a>
			</li>
			<li>Superintendent of Public Instruction: <b>Chris Reykdal</b>
				{' '}<a href="https://www.chrisreykdal.org/">{web}</a>
			</li>
			<li>Insurance Commissioner: <b>Mike Kreidler</b>
				{' '}<a href="https://mikekreidler.com/">{web}</a>
			</li>
			<li>State Supreme Court, Justice Position 6: <b>G. Helen Whitener</b>
				{' '}<a href="https://www.judgehelenwhitener.com/">{web}</a>
			</li>
		</ul>
	</li>
	<li>Legislative District 32
		<ul>
			<li>State Representative Pos. 1: <b>Shirley Sutton</b>
				{' '}<a href="http://voteshirleysutton.com/">{web}</a>
			</li>
			<li>State Representative Pos. 2: <b>Lauren Davis</b>
				{' '}<a href="https://electlaurendavis.com/">{web}</a>
			</li>
		</ul>
	</li>
	<li>King County
		<ul>
			<li>Charter Amendment 1: <b>Yes</b></li>
			<li>Charter Amendment 2: <i>A position was not taken.</i></li>
			<li>Charter Amendment 3: <b>Yes</b></li>
			<li>Charter Amendment 4: <b>Yes</b></li>
			<li>Charter Amendment 5: <b>Yes</b></li>
			<li>Charter Amendment 6: <b>Yes</b></li>
			<li>Charter Amendment 7: <b>Yes</b></li>
			<li>Proposition 1: <b>Approved</b></li>
			<li>King Superior Court
				<ul>
					<li>Judge Position 13: <b>Hillary Madsen</b>{' '}
						<a href="https://www.hillaryforjudge.com/">{web}</a>
						<a href="https://31stdistrictdemocrats.org/king-county-superior-court-hillary-madsen/">{info}</a>
					</li>
					<li>Judge Position 19: <b>Nelson Kao Hwa Lee</b>{' '}
						<a href="https://www.judgenelsonlee.com/">{web}</a>
						<a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-19-judge-nelson-lee/">{info}</a>
					</li>
					<li>Judge Position 26: <b>David Keenan</b>{' '}
						<a href="http://www.judgekeenan.com/">{web}</a>
						<a href="https://31stdistrictdemocrats.org/king-county-superior-court-position-26-david-keenan/">{info}</a>
					</li>
					<li>Judge Position 30: <b>Douglass North</b>{' '}
						<a href="https://www.reelectjudgedougnorth.com/">{web}</a>
						<a href="https://31stdistrictdemocrats.org/king-county-superior-court-position-30-douglass-north/">{info}</a>
					</li>
					<li>Judge Position 51: <b>Cindi Port</b>{' '}
						<a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-51-incumbent-cindi-port/">{info}</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>Snohomish County
		<ul>
			<li>Snohomish County Council, District 4: <b>Jared Mead</b>
						{' '}<a href="https://www.jaredmead.org/">{web}</a>
				{/*<!--did not advance to General--><ul>
					<li><b>Amber King</b>
						{' '}<a href="https://voteamberking.com/">{web}</a>
					</li>
				</ul> */}
			</li>
			<li>Court of Appeals, Division 1, District 2, Judge Position 2: <b>Linda Coburn</b>
				{' '}<a href="https://www.judgecoburn.com/">{web}</a>
			</li>
			<li>Snohomish Superior Court
				<ul>
					<li>Judge Position 4: <b>Edirin Okoloko</b>
						{' '}<a href="https://www.judgeokoloko.com/">{web}</a>
					</li>
					<li>Judge Position 8: <b>Robert Grant</b>
						{' '}<a href="https://www.robertkgrantforjudge.com/">{web}</a>
					</li>
					<li>Judge Position 13: <b>Jennifer Langbehn</b>
						{' '}<a href="mailto:JUDGEJENNIFERLANGBEHN@GMAIL.COM ">{email}</a>
					</li>
					<li>Judge Position 14: <b>Paul W. Thompson</b>
						{' '}<a href="https://www.judgepaulthompson.com/">{web}</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
</ul>
</Layout>
)
export {
	render as default,
	vote_info,
}