import Helmet from 'react-helmet'
import {createElement as h} from 'react'
import logo from '../../../../../components/logo.js'

const county = 'King'
const election = 'Primary'
const year = '2019'
const month = 'August'
const day = '6'
const title = `${county} ${year} ${election} Endorsements`

const color = '#0000A0'
const highlight = '#E8E8F8'
const highlight_logo = '#8080C0'
const size = 14
const fontSize = size
const padding = `3px ${size}px`

const group = {style:{
	background: highlight,
	fontSize: '1.5em',
	marginTop: size,
	padding,
}}
const office = {style:{
	fontWeight: 'bold',
	padding,
}}
const names = {style:{
	display:'flex',
}}
const name = {
	style: {
		display: 'list-item',
		listStyleType: 'square',
		margin: `3px 0 3px ${size*3}px`,
		whiteSpace:'nowrap',
	},
}
const Page = () => (
	h('div', {style:{
		display: 'flex',
		fontSize,
		lineHeight:1,
	}},
		h(Helmet, {},
			h('title', {}, title),
		),
		h(Card),
		h(Card),
	)
)

const Card = () => (
h('div', {style:{
	borderColor: color,
	borderStyle: 'solid',
	borderWidth: 4,
	boxSizing:'border-box',
	color,
	fontFamily:'Roboto, sans-serif',
	fontWeight: 'normal',
	margin: '0.25in',
	height: '10.5in',
	width: '3.75in',
}},
h('div', {style:{
	display: 'flex',
	margin: '.5em',
}},
	h('div', {style: {
		background:color,
		marginRight:'.5em',
		}},
		h('div', {
		style: {height:'5em', width:'5em', },
		},
			logo({fill:highlight_logo})
		),
	),
	h('div', {style:{
		alignSelf:'flex-end',
		fontFamily:'Roboto Slab, serif',
		fontSize:'1.25em',
		fontWeight:'bold',
		letterSpacing:'3',
		}},
		h('div', {},
			h('span', {style:{fontSize:'2em'}},'32'),
			h('span', {style:{textTransform:'uppercase'}},'nd'),
		),
		h('div', {style: {textTransform:'uppercase'}}, 'District'),
		h('div', {style: {textTransform:'uppercase'}}, 'Democrats'),
	),
),
h('div', {style:{
	background: color,
	color: 'white',
	padding: '.5em 0',
}},
	h('div', {style:{
		fontSize:'2em',
		padding,
	}},
		`${month} ${day}, ${year}`
	),
	h('div', {style:{padding}}, `${election} Election Endorsements`),
),
h('div', {style:{
		background: highlight,
		fontSize:'2em',
		marginTop: size,
		padding,
	}},
	`${county} County`
	),
	h('div', group, 'Port of Seattle'),
	h('div', office, 'Port of Seattle, Commissioner Position 2'),
	h('div', name, 'Sam Cho'),
	h('div', office, 'Port of Seattle, Commissioner Position 5'),
	h('div', name, 'Fred Felleman'),
	h('div', group, 'City of Seattle'),
	h('div', office, 'Seattle Council, District 5'),
	h('div', names,
		h('div', name, 'Debora Juarez'),
		h('div', name, 'John Lombard'),
	),
	h('div', office, 'Seattle School District Director, District 1'),
	h('div', name, 'Eric Blumhagen'),

	h('div', group, 'City of Shoreline'),
	h('div', office, 'Shoreline Council, Position 4'),
	h('div', name, 'Doris McConnell'),
	h('div', office, 'Shoreline School District 412, Director, District 5'),
	h('div', names,
		h('div', name, 'Joe Cunningham'),
		h('div', name, 'Sara Betnel'),
	),
)
)

export default Page
