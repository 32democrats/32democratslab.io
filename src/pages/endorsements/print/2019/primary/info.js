import Helmet from 'react-helmet'
import {createElement as h} from 'react'
import logo from '../../../../../components/logo.js'

const title = `Primary Endorsements Info`
const month = 'August'
const day_ordinal = '6th'

const color = '#0000A0'
const highlight_logo = '#8080C0'
const size = 15
const fontSize = size
const padding = `0 ${size}px`

const p = {style: {
	lineHeight:'1.5',
	marginBottom:size,
	padding,
}}
const Page = () => (
	h('div', {style: {
		display:'flex',
		fontSize,
		lineHeight:1,
	}},
		h(Helmet, {},
			h('title', {}, title),
		),
		h(Card),
		h(Card),
	)
)

const Card = () => (
h('div', {style: {
	borderColor:color,
	borderStyle:'solid',
	borderWidth:4,
	boxSizing:'border-box',
	color,
	display:'flex',
	flexDirection:'column',
	justifyContent:'space-between',
	fontFamily:'Roboto, sans-serif',
	fontWeight:'normal',
	margin:'0.25in',
	height:'10.5in',
	width:'3.75in',
}},
h('div', {style: {
	display:'flex',
	margin:'.5em',
}},
	h('div', {style: {
		background:color,
		marginRight:'.5em',
		}},
		h('div', {
		style: {height:'5em', width:'5em', },
		},
			logo({fill:highlight_logo})
		),
	),
	h('div', {style: {
		alignSelf:'flex-end',
		fontFamily:'Roboto Slab, serif',
		fontSize:'1.25em',
		fontWeight:'bold',
		letterSpacing:'3',
		}},
		h('div', {},
			h('span', {style: {fontSize:'2em'}}, '32'),
			h('span', {style: {textTransform:'uppercase'}}, 'nd'),
		),
		h('div', {style: {textTransform:'uppercase'}}, 'District'),
		h('div', {style: {textTransform:'uppercase'}}, 'Democrats'),
	),
),
h('div', {style:{
		fontFamily: 'Roboto Mono, monospace',
		padding,
	}},
	'See reverse side for endorsements.',
),
h('div', {},
	h('div', p, 'Hi Neighbors,'),
	h('div', p, `We are the 32nd District Democrats, and we fight for progressive issues. We hope this endorsement list will help you with your voting decisions for the ${month} ${day_ordinal} ballot.`),
	h('div', p, 'Our meetings are held on the 2nd Wednesday of the each month at 7pm — please join us!'),
	h('div', p, 'Check out our website for meeting location, agenda, and current issues: https://32democrats.org/'),
),
h('img', {src:'/img/map-screenshot.png', style:{
	maxHeight:'4.75in',
	maxWidth:`calc(3.75in - ${2*size}px)`,
	objectFit: 'contain',
	margin:'0 auto',
}}),
h('div', p,
	'Paid for by: 32nd District Democrats',
	h('br'),
	'PO Box 55622, Shoreline, WA 98155',
	h('br'),
	'Alan Charnley, Chair',
),
)
)

export default Page
