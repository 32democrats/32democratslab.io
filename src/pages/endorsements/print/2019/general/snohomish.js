import Helmet from 'react-helmet'
import {createElement as h} from 'react'
import logo from '../../../../../components/logo.js'

const county = 'Snohomish'
const election = 'General'
const year = '2019'
const month = 'November'
const day = '5'
const title = `${county} ${year} ${election} Endorsements`

const color = '#0000A0'
const highlight = '#E8E8F8'
const highlight_logo = '#8080C0'
const size = 12
const marginTop = 6
const fontSize = size
const padding = `1px ${size}px`

const group = {style: {
	background:highlight,
	fontSize:'1.5em',
	marginTop,
	padding,
}}
const office = {style: {
	fontWeight:'bold',
	padding,
}}
const name = {
	style: {
		display:'list-item',
		listStyleType:'square',
		margin:`3px 0 3px ${size * 3}px`,
		whiteSpace:'nowrap',
	},
}

const Page = () => (
	h('div', {style: {
		display:'flex',
		fontSize,
		lineHeight:1,
	}},
		h(Helmet, {},
			h('title', {}, title),
		),
		h(Card),
		h(Card),
	)
)

const Card = () => (
h('div', {style: {
	borderColor:color,
	borderStyle:'solid',
	borderWidth:4,
	boxSizing:'border-box',
	color,
	fontFamily:'Roboto, sans-serif',
	fontWeight:'normal',
	margin:'0.25in',
	height:'10.5in',
	width:'3.75in',
}},
h('div', {style: {
	display:'flex',
	margin:'.5em',
}},
	h('div', {style: {
		background:color,
		marginRight:'.5em',
		}},
		h('div', {
		style: {height:'5em', width:'5em', },
		},
			logo({fill:highlight_logo})
		),
	),
	h('div', {style: {
		alignSelf:'flex-end',
		fontFamily:'Roboto Slab, serif',
		fontSize:'1.25em',
		fontWeight:'bold',
		letterSpacing:'3',
		}},
		h('div', {},
			h('span', {style: {fontSize:'2em'}}, '32'),
			h('span', {style: {textTransform:'uppercase'}}, 'nd'),
		),
		h('div', {style: {textTransform:'uppercase'}}, 'District'),
		h('div', {style: {textTransform:'uppercase'}}, 'Democrats'),
	),
),
h('div', {style: {
	background:color,
	color:'white',
	padding:'.5em 0',
}},
	h('div', {style: {
		fontSize:'2em',
		padding,
	}},
		`${month} ${day}, ${year}`
	),
	h('div', {style:{padding}}, `${election} Election Endorsements`),
	),
	h('div', {style:{
		background: highlight,
		fontSize:'2em',
		padding,
	}},
		`State of Washington`
	),
	h('div', office, 'Referendum Measure 88'),
	h('div', name, 'Approved'),
	h('div', office, 'Initiative Measure 976'),
	h('div', name, 'No'),
	h('div', {style: {
		background:highlight,
		fontSize:'2em',
		marginTop,
		padding,
	}},
	`${county} County`
	),
	h('div', office, 'Snohomish County Executive'),
	h('div', name, 'Dave Somers'),
	h('div', office, 'Snohomish County Sheriff'),
	h('div', name, 'Ty Trenary'),
	h('div', office, 'Snohomish County Council, District 3'),
	h('div', name, 'Stephanie Wright'),
	h('div', office, 'Snohomish County Auditor'),
	h('div', name, 'Cindy Gobel'),
	h('div', office, 'Snohomish County Treasurer'),
	h('div', name, 'Brian Sullivan'),
	h('div', office, 'Superior Court Judge Position 7'),
	h('div', name, 'Edirin Okoloko'),
	h('div', office, 'Superior Court Judge Position 13'),
	h('div', name, 'Jennifer Langbehn'),
	h('div', office, 'Superior Court Judge Position 14'),
	h('div', name, 'Paul W. Thompson'),
	h('div', office, 'South Sno. Co. Fire & Rescue RFA Commissioner, District 1'),
	h('div', name, 'Ted Hikel'),
	h('div', group, 'City of Edmonds'),
	h('div', office, 'Edmonds Mayor'),
	h('div', name, 'Mike Nelson'),
	h('div', office, 'Edmonds Council, Position 4'),
	h('div', name, 'Jenna Nand'),
	h('div', office, 'Edmonds Council, Position 5'),
	h('div', name, 'Alicia Crank'),
	h('div', office, 'Edmonds Council, Position 6'),
	h('div', name, 'Susan Paine'),
	h('div', office, 'Edmonds Council, Position 7'),
	h('div', name, 'Laura Johnson'),
	
	h('div', {style:{
		background:highlight,
		marginTop,
		padding,
		}},
		h('div', {style:{fontSize:'1.5em',}}, 'Edmonds School District 15'),
		h('div', {style:{lineHeight:1.5}}, 'also includes Brier, Lynnwood, Mountlake Terrace'),
	),
	h('div', office, 'Edmonds School District 15 Director, District 1'),
	h('div', name, 'Carin Chase'),
	h('div', office, 'Edmonds School District 15 Director, District 3'),
	h('div', name, 'Rory Graves'),
	h('div', office, 'Edmonds School District 15 Director, District 5'),
	h('div', name, 'Nancy Katims'),
	h('div', group, 'City of Lynnwood'),
	h('div', office, 'Lynnwood Council, Position 4'),
	h('div', name, 'Van AuBuchon'),
	h('div', office, 'Lynnwood Council, Position 6'),
	h('div', name, 'George Hurst'),
	h('div', office, 'Lynnwood Council, Position 7'),
	h('div', name, 'Shirley Sutton'),
	)
)

export default Page
