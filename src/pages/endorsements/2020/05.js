import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'
import {dates} from '../../../components/dates'
const date = '2020-05-27'
const {day, month_name, year} = dates(date)
const type = 'Endorsement Committee Report'
export default () => (
	<Layout>
		<Helmet>
			<title>{date} {type}</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			{type}, {month_name} {day}, {year}
		</h1>
<p>Since the Committee's last report, we have had three more interview events, speaking with 13 additional candidates for a variety of positions.</p>
<p>In total since the 'endorsement season' has begun, we have spent 30 hours in interviews and discussion, additional to the chair's time spent contacting and scheduling the interviews, arranging for Zoom meetings with several hosts.  We are very grateful to SnoCo Dems for allowing us their Zoom platform for the majority of these events. </p>
<p> As the chair, I would like to acknowledge the great work done by committee members for the many extra hours they have spent researching candidates’ claims made on their questionnaires, following the donation money on the PDC, researching the 'back story' of the individual and group donations, newspaper accounts if there were any.  This extraordinary work is to bring to the body the clearest rationale of why we as a committee are recommending certain candidates and not others.  I commend this year's committee as the hardest working and most fair group I have worked with since I began this task fifteen years ago!  It is, after all, the Legislative District's mission to help elect the best and brightest to public service. </p>
<p><strong>Washington State Supreme Ct, position 6:  G. Helen Whitener.</strong> yeas 9, nays 0; </p>
<p><strong><u>Justice Whitener is overwhelmingly recommended  for endorsement.</u></strong></p>
<p>She has served on the Supreme Ct since being appointed to the position by Governor Inslee, and is now seeking re-election.  She has served many communities in Washington as a Superior Ct. judge, as well as the WA State Industrial Insurance Appeals, served as a Pro-Tem judge in Pierce County, and worked for many legal firms in her role as attorney.  Favorite quote "Justice Whitener believes in just treatment because just treatment is justice".</p>
<p><strong>Washington State Lt. Governor:  Denny Heck</strong> vs. Marko Liias: Congressman Heck: yea 7, nay 1;  St Senator Marko Liias yea 0; nay 6, abstention 2</p>
<p><strong><u>Congressman Heck is recommended for endorsement</u></strong>.  "Denny Heck is about more than applying the rules of the Senate and the Constitution to legislative deliberations.  He brings a legislative philosophy that honors both the people of our state and the constitution that provides the network for holding our public sector together.  He has demonstrated over the years a commitment to principled centered politics and to transparency and accountability that strengthens our Democracy.  That is why we support his candidacy for Lt. Governor."</p>
<p> <strong>King County Superior Ct, Dept. 13:  Hillary Madsen</strong>.  vs. Andrea Roberson The committee votes:  Ms. Robertson 4 yeas, 6 nays.Ms. Madsen: 8 yeas, 0 nays. </p>
<p><strong><u>Ms. Madsen is recommended for endorsement.</u></strong></p>
<p>Rationale:  committee members were extremely impressed with Ms. Madsen's demeanor; her references to research done on issues, her excellent work with Columbia Legal Services, and her work with many youth and youth groups.  </p>
<p><strong>King County Superior Ct. Dep't. 30:  Judge Douglass North</strong> vs Carolyn Ladd :  Ms. Ladd 0 yeas and 8 nays.  Doug North:  yea 8, nay</p>
<p><strong><u>Judge North is recommended for endorsement</u></strong>.  "Judge North presents extensive knowledge and experience, a long positive record on the bench, and displays the progressive values to which we aspire and expect from candidates.  He is an excellent role model for less experienced judges.</p>
<p><strong>Snohomish County Council District 4:  Amber King</strong> vs Jared Mead.  Ms. King yea 5; nay 3  Mr. Mead: yea 2, nay 3, abstention 3. </p>
<p><strong><u>Ms. King is recommended for endorsement. </u></strong></p>
<p>"Amber King lacks political experience, but has extensive life experience in many areas that would promote the kind of progressive decision making that we expect from candidates.  She demonstrates tenacity and ability to quickly learn in a variety of situations, both of which would be conducive to performance on the council that meets our expectations. </p>
<p><strong>32nd Legislative District Representative, position 1:  Gray Petersen</strong> vs Lauren Davis .  Ms. Davis -  yea 4, nay 4, abstention 1;  Mr. Petersen - yea 6, nay 0 abstention 2. </p>
<p><strong><u>Mr. <em>Petersen</em> is recommended for endorsement.</u></strong></p>
<p>"The Committee endorsed Gray Petersen due to his passionate convictions around the urgency for social, economic, and environmental justice. He has sufficient working knowledge of local and state political processes, is engaged and productive, and we believe he will serve our communities well as our 32<sup>nd</sup> LD Representative to the Legislature."</p>
<p>32nd Legislative District Representative, position 2:  Cindy Ryu vs Shirley Sutton vs. Keith Smith.. Ms. Ryu - no votes either yea or nay.  Ms.  Sutton yea 6, nay 0 , abstention 2; Mr. Smith - no yeas or nays; "Bright, good ideas, not ready for prime time'.</p>
<p><strong><u>Ms. Sutton is recommended for endorsement. </u></strong></p>
<p>"Shirley Sutton will seek equitable solutions to financial burdens for working families and small businesses: fair taxation; close tax loopholes for big business; and establish a state bank to provide low interest loans for community. As Lynnwood City Council Member, Shirley found alternative funding to protect Whispering Pines residents. Shirley earned her MA in education and continues to vigorously support funding for public schools, community colleges and jobs training. Her management and labor experience at Burlington Northern informs her commitment to public works that reduce carbon pollution, promote ideals of fairness, diversity and inclusion. Shirley will not accept corporate PAC or charter school donations."</p>
<hr/>
<p>The Washington State Platform is very clear than the human rights of our citizens should not become profit-centers for private or corporate interests. We do not want to privatize our Article 9 public schools, our Article 13 social services, including public housing. The thousands of homeless people, including 44,000 school children in our state, and people living in tents, are ample evidence of market-failure of the private housing market.</p>
<p>The right to privacy in our personal affairs is violated by the many state programs. If the same violations were visited on the Department of Revenue, the protests would be heard in Olympia. There is a lack of transparency and therefore accountability in these pubic policies.</p>
<p>This report represents the work of your Endorsement Committee and contains our recommendations to you for endorsement of the 32<sup>nd</sup> Legislative District.  It is fair that we lay out for you the standards we used in our evaluation of the candidates.</p>
<p> </p>
<p><strong>Our overall position is that the endorsed candidates represent the values and policy positions of the 32<sup>nd</sup> Legislative District as identified in our platform.  This platform has been crafted over many years and represents the principles we expect to be represented by our elected officials.  It is a broad, sweeping document encompassing both local, state, national and international policy positions. </strong></p>
<p> </p>
<p>First of all, the district platform is our guiding document.  The articles of the platform set the policy standards we use when evaluating whether or not a candidate adheres to policy   positions we expect from our elected representatives.  It is important to recognize that our recommendations are grounded In public policy positions, not in our personal likes or dislikes.  Which policies do our elected representatives and candidates support or not? </p>
<p> </p>
<p>Secondly, What public policy accomplishments or relevant volunteering around any public policy proposals has the candidate supported or not?</p>
<p> </p>
<p>Thirdly, what are the candidate’s legislative priorities and do they fit with our district’s platform?  What significant areas or issues of our platform are not addressed?</p>
<p> </p>
<p>Finally, is the candidate electable?  What is the candidate’s electability?</p>
<p> </p>
<blockquote>
<p>Has the aspiring candidate or previously elected candidate, signed and adhered to our Civility Pledge?</p>
</blockquote>
<p> </p>
<p>            Previous elected/candidate experience in the District?  Name recognition?</p>
<p> </p>
<p>            Campaign strategy?</p>
<p> </p>
<p>            Funds raised, source of funds, fund-raising goals?</p>
<p> </p>
<p>            Other endorsing organizations?</p>
<p> </p>
<p>            Constituent groups that present challenges – how these will be addressed.</p>
<p> </p>
<p> </p>
<p> Respectfully Submitted</p>
<p>Endorsement Committee, 32nd LD Democratic Organization:  Steph Harris, Chair, Lael White, Sally Soriano, Brendt McFarland, Ted Hikel, Tom White, Pat Weber, Maralyn Chase, Kyle Burleigh, Joe Cunningham (absent).  </p>
	</Layout>
)
