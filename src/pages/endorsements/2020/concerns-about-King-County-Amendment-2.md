import Helmet from 'react-helmet'
import Layout from '../../../components/layout'
import {dates} from '../../../components/dates'

export const date = '2020-10-14'
export const {day, month_name, year} = dates(date)
export const type = 'Endorsement Committee Chair'
export const title = "Concerns about King County Amendment #2"

<Layout>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h2>
			{type}, {month_name} {day}, {year}
		</h2>
		<h1>
			{title}
		</h1>

by Stephanie Harris, Chair, 32<sup>nd</sup> LD Endorsement Committee

\_

At our last LD meeting on Wed. September 9<sup>th</sup>, Councilmember Rod
Dembowski asked if he might be added to the agenda so he could present
the proposed [Charter
Amendments](https://kingcounty.gov/council/mainnews/2020/July/7-24-charter-amendments-release.aspx)
from the King County Council. Every 10 years, the council may put forth
amendments to its charter and must get approval by voters.

CM Dembowski was seeking the endorsement of the 32<sup>nd</sup> LD for the seven
amendments. Until his presentation on these amendments, the 32<sup>nd</sup> LD
had not known that they would be on the ballot. After discussion, it was
decided that, except for one amendment, the 32<sup>nd</sup> would endorse six of
the seven amendments.

The one amendment that was pulled from the slate for further discussion
at the next LD meeting calls for selling public property in King County
at below market rates \-- to private developers under the guise of
building low-income housing. There were questions raised during Q&A
about the state constitutionality of giving public lands to private
developers. One person asked specifically about article 16, which
stipulates:

**Article 16, School and Granted lands**

**Section 1***.  DISPOSITION OF.  All the public lands granted to the
state are held in trust for all the people and none of such lands, nor
any estate or interest therein, shall ever be disposed of unless the
full market value of the estate or interest disposed of, to be
ascertained in such manner as may be provided by law, be paid or safely
secured to the state, nor shall any lands which the state hold by grant
from the United States.*

In response to the question regarding article 16, CM Dembowski replied,
"historically we have not been able to do this, but a bill on that was
passed \[[HB
2382](http://lawfilesext.leg.wa.gov/biennium/2017-18/Pdf/Bills/House%20Passed%20Legislature/2382-S3.PL.pdf?q=20201013205201)\]
in the last \[state\] legislative session. King County could not give it
\[public property\] to a private developer who would not provide
affordable housing."

Besides the several questions about state constitutionality, an
important detail laid out in the state constitution, is that any
amendment or changes to it, must be approved by a majority vote of the
people. This is analogous to the requirement that any changes to the
King County Charter also go to a vote for approval by the people.

Both the state legislature and the county council are operating without
any standards here. It's just like giving $13 billion to Boeing and
getting jilted anyway because there was not a binding contract or
clawback. King County Amendment \#2 overreaches, meaning that there's a
large space between saying the legislature can use public property for
the public good and a giveaway to private developers.

We need to recognize that if the poor were benefitting from our current
policy of funding private development, we would see a reduction in
homelessness and people barely keeping a roof over their heads. This is
a policy failure. Every year, King County homelessness continues to
rise. And, developers only make small number of affordable units
available. Many of those are at 80% of median income when the hardest
hit people less than 30% of median income. There's plenty of luxury
housing, but not enough low-income housing.

Jerry Conk, 32<sup>nd</sup> PCO, said courts have gone along with decisions to
give public land to developers when poor people are benefiting. But, he
concluded, as an aside, that taxpayers gave Boeing billions without a
return on their agreement not to leave town.

I believe that the state legislature and county council have engaged in
overreach, which means\_stretching the definition and
moving the goal post. To go from 'we can allocate public funds for the
public good' to: 'we can give private developers public land and
public property for free,' is the definition of overreach. The outcome
is that the public loses its land in perpetuity and private development
is free to name the terms.

In any contract, reasonable terms are explicitly determined and
negotiated in contractual proceedings. The public is the stakeholder
here and has been excluded from these legislative and county council
discussions. The public has not been properly informed about the
provisions of this amendment. Where were the public hearings and the
discussions in media for both the legislature's bill and the county
council's decision to place Amendment \#2 on the ballot?

A responsible negotiation on legislation investigates alternatives
instead of settling on just one avenue. In business, this process is
called "due diligence". An example of due diligence is to hold public
hearings, which would reveal that we can and should keep public property
public. Due diligence would also reveal that the constitution empowers
the legislature to build public housing and to finance that housing.

Public-private partnerships have not benefited the public for 30 years,
throwing more money at it won't change that. Private development is not
providing affordable housing commensurate with our public investments.
The lack of oversight is staggering, when the majority of development
produces market rate housing and is allowed to define 'affordable
housing' at 80% of market rate, with maybe a token number of truly low
income units involved.

Developers may receive even thirty years of tax exemptions, and the
public is in the dark about how long buildings or units will be at less
than full market rate. People should know that a lot of the low-income
developments from the 1960s and 1970s have aged out and have been
upgraded to market rate, so privatization has removed that affordable
housing stock. The public is simply not receiving a fair return on its
investment.

The public needs to look at what options are available to the state. The
constitution gives the legislature the power to simply build public
housing. The constitution also provides for financial mechanisms to pay
for public housing. We cannot proceed down the road of giving away the
public treasure without examining alternatives that would cost less, be
transparent, and maintain our public properties as well as build public
collateral that can be used in the future.

What is being proposed by King County is to relinquish precious public
lands in perpetuity when we know that the value of these public lands
will continue to rise. We simply cannot, in good conscience, waste our
public wealth and miss the opportunity to increase it in order to better
serve the public.

I think this issue may explain why developers and real estate interests
are pouring campaign funding into legislative races.

We need to slow this down and take time for ample public consideration.
We need time for public hearings, to take public comments and to look at
other options. We need to vote "no" and not support Amendment \#2 as
part of the slate for King County Charter Amendments. We need to have a
fully informed public discussion, starting with a vetting in our 32nd LD
Endorsement Committee.

We absolutely need to address this housing crisis like any other
disaster. If we had 200K people unhoused because of an earthquake, we
would mobilize public funds for emergency housing, social housing,
medical care, food, etc., without prioritizing a giveaway to private
development. Nobody should profit off of a humanitarian disaster.

In my view, we need to look to the state constitution for public housing
and public financing. We can provide for the public good without
siphoning public funds to for-profit developers.
</Layout>
