import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'

const year = '2018'

export default () => (
<Layout>
<Helmet>
	<title>{year} Endorsements</title>
</Helmet>
<p style={{marginBottom:0}}>
	<b>Vote by Tuesday, November 6, {year}.</b>
	<br/>
	Mail your ballot (no stamp needed) or use a drop box.
	<br/>
	Drop boxes are open 24/7 until 8 p.m. on election day.
</p>
<ul className="menu">
	<li>
		<a href="https://www.kingcounty.gov/depts/elections/how-to-vote/ballots/returning-my-ballot/ballot-drop-boxes.aspx">
			King County ballot drop boxes
		</a>
	</li>
	<li>
		<a href="https://snohomishcountywa.gov/225/Ballot-Drop-Box-Locations">
			Snohomish County ballot drop boxes
		</a>
	</li>
</ul>
<br/>
<h1>
	<Link to="/endorsements">Endorsements</Link>: {year}
</h1>
<ul className="endorsements">
	<li>Federal
		<ul>
			<li>Senator from Washington State: <b>Maria Cantwell</b></li>
			<li>U.S. Representative from Congressional District 2: <b>Rick Larsen</b></li>
			<li>U.S. Representative from Congressional District 7: <b>Pramila Jayapal</b></li>
		</ul>
	</li>
	<li>State Legislature
		<ul>
			<li>Senator: <b>Maralyn Chase</b></li>
			<li>Representative: <b>Chris Roberts</b></li>
		</ul>
	</li>
	<li>State Judiciary
		<ul>
			<li>Supreme Court Justice Position 8: <b>Steve Gonzalez</b></li>
			<li>Supreme Court Justice Position 9: <b>Sheryl Gordon McCloud</b></li>
		</ul>
	</li>
	<li>Snohomish County
		<ul>
			<li>County Offices Prosecuting Attorney: <b>Adam Cornell</b></li>
			<li>Public Utilities District No 1 Commissioner: <b>Rebecca Wolfe</b></li>
			<li>South District Court Judge Position 2: <b>Jeffrey D. Goodwin</b></li>
		</ul>
	</li>
	<li>King County
		<ul>
			<li>Superior Court Position 22: <b>Karen Donohue</b></li>
			<li>Seattle District Court
				<ul>
					<li>Judge Position 1: <b>Lisa Paglisotti</b></li>
					<li>Judge Position 5: <b>Anne C. Harper</b></li>
				</ul>
			</li>
			<li>Shoreline District Court
				<ul>
					<li>Judge Position 1: <b>Joe Campagna</b></li>
					<li>Judge Position 2: <b>Marcine Anderson</b></li>
				</ul>
			</li>
		</ul>
	</li>
	<li>Seattle Municipal Court
		<ul>
			<li>Judge Position 1: <b>Ed McKenna</b></li>
			<li>Judge Position 2: <b>Andrea Chin</b></li>
			<li>Judge Position 3: <b>Adam Eisenberg</b></li>
		</ul>
	</li>
	<li>Initiatives
		<ul>
			<li>I-940: Law enforcement violence de-escalation training and good faith standard
				<br/><strong><a href="https://www.deescalatewa.org/">Yes</a></strong>
			</li>
			<li>I-1631: Carbon fee on polluters
				<br/><strong><a href="https://yeson1631.org/">Yes</a></strong>
			</li>
			<li>I-1634: Prohibiting new taxes on food items and beverages
				<br/><strong><a href="https://www.no1634.org/">No</a></strong>
			</li>
			<li>I-1639: Firearms safety and responsibility
				<br/><strong><a href="https://gunresponsibility.org/reduce-assault-weapon-violence-initiative/">Yes</a></strong>
			</li>
			<li>Advisory Vote 19
				<br/><strong>Maintain</strong>
			</li>
			<li>City of Seattle Proposition 1: Families, education, preschool, and promise levy
				<br/><strong>No</strong>
			</li>
		</ul>
	</li>
</ul>
</Layout>
)
