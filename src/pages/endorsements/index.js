import React from 'react'
import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import Layout from '../../components/layout'

const title = 'Endorsements'
export default () => (
	<Layout>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<ul className="menu">
			<li>
				<Link to="/endorsements/apply">Apply for endorsement</Link>
			</li>
			<li>
				<Link to="/civility-pledge">Civility Pledge</Link>
			</li>
			<li>
				<Link to="/endorsements/reports">Committee Reports</Link>
			</li>
		</ul>
		<h1>{title}</h1>
		<ul className="menu">
			<li>
				<Link to="/endorsements/2020">2020</Link>
			</li>
			<li>
				<Link to="/endorsements/2019">2019</Link>
			</li>
			<li>
				<Link to="/endorsements/2018">2018</Link>
			</li>
		</ul>
	</Layout>
)
