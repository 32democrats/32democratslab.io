import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'

const year = '2019'
const email = <span role="img" aria-label="Email" title="Email" className="fade no-print">📧</span>
const info = <span role="img" aria-label="Info" title="Info" className="info-icon fade no-print">ℹ</span>
const web = <span role="img" aria-label="Website" title="Website" className="fade no-print">🏠</span>

export default () => (
<Layout>
<Helmet>
	<title>{year} Endorsements</title>
</Helmet>
<dl className="links">
	<dt><a href="https://results.vote.wa.gov/results/20191105/Measures.html">2019 General Election Results</a></dt>
	<dd><ul className="menu">
		<li><a href="https://results.vote.wa.gov/results/20191105/king/">King County</a></li>
		<li><a href="https://results.vote.wa.gov/results/20191105/snohomish/">Snohomish County</a></li>
	</ul></dd>
</dl>
<p style={{marginBottom:0}}>
	<b>
		Vote by
		Tuesday, November 5, {year}.
	</b>
	<br/>
	Mail your ballot (no stamp needed) or use a drop box.
	<br/>
	Drop boxes are open 24/7 until 8 p.m. on election day.
</p>
<ul className="menu no-print">
	<li>
		<a href="https://www.kingCounty.gov/depts/elections/how-to-vote/ballots/returning-my-ballot/ballot-drop-boxes.aspx">
			King County ballot drop boxes
		</a>
	</li>
	<li>
		<a href="https://snohomishCountywa.gov/225/Ballot-Drop-Box-Locations">
			Snohomish County ballot drop boxes
		</a>
	</li>
</ul>
<h1>
	<Link to="/endorsements">Endorsements</Link>: {year}
</h1>
<ul className="endorsements">
	<li>
		<a href="https://ballotpedia.org/Washington_Referendum_88,_Vote_on_I-1000_Affirmative_Action_Measure_(2019)">
			State Measures - Referendum Measure 88
		</a>: <b>Approved</b>
	</li>
	<li>
		<a href="https://32democrats.gitlab.io/resolutions/2019/No-I-976/">
			State Measures - Initiative Measure 976
		</a>: <b>No</b>
	</li>
	<li>King County
		<ul>
			<li>Council, District 4: <b>Jeanne Kohl-Welles</b>{' '}
				<a href="mailto:JEANNE@JEANNEKOHLWELLES.COM">{email}</a>
				<a href="https://jeannekohlwelles.com/">{web}</a>
			</li>
			<li>Port of Seattle: Commissioner
				<ul>
					<li>Position 2: <b>Sam Cho</b>{' '}</li>
					<li>Position 5: <b>Fred Felleman</b>{' '}
						<a href="mailto:FREDFORPORT@GMAIL.COM">{email}</a>
					</li>
				</ul>
			</li>
			<li>Superior Court Judge
				<ul>
					<li>Position 5: <b>Maureen McKee</b>{' '}
						<a href="mailto:JUDGEMAUREENMCKEE@GMAIL.COM">{email}</a>
						<a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-5-judge-maureen-mckee/">{info}</a>
					</li>
					<li>Position 16: <b>Averil Rothrock</b>{' '}
						<a href="mailto:JUDGEROTHROCK@GMAIL.COM">{email}</a>
					</li>
					<li>Position 31: <b>
						Marshall Ferguson
						</b>{' '}
						<a href="http://judgemarshallferguson.com/">{web}</a>
						<a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-31-judge-marshall-ferguson/">{info}</a>
					</li>
					<li>Position 37: <b>Michael Ryan</b>{' '}
						<a href="mailto:FRIENDSOFJUDGERYAN@GMAIL.COM">{email}</a>
					</li>
					<li>Position 49: <b>Aimee Sutton</b>{' '}
						<a href="mailto:JUDGEAIMEESUTTON@GMAIL.COM">{email}</a>
						<a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-49-judge-aimee-sutton/">{info}</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>Snohomish County
		<ul>
			<li>Auditor: <b>Cindy Gobel</b>{' '}
				<a href="mailto:GOBEL4AUDITOR@COMCAST.NET">{email}</a>
			</li>
			<li>Council, District 3: <b>Stephanie Wright</b>{' '}
				<a href="mailto:STEPHANIE@STEPHWRIGHT.COM">{email}</a>
				<a href="https://stephwright.com/">{web}</a>
			</li>
			<li>Treasurer: <b>Brian Sullivan</b>{' '}
				<a href="mailto:brianjamessullivan@outlook.com">{email}</a>
				<a href="https://www.sullivan4treasurer.com/">{web}</a>
			</li>
			<li>
				Edmonds School District Director
				<ul>
					<li>
						District 1: <b>Carin Chase</b>{' '}
						<a href="mailto:INFO@CARIN4SCHOOLS.COM">{email}</a>
					</li>
					<li>District 3: <b>Rory Graves</b>{' '}
						{/* <li><b>Jennifer Cail</b>{' '}</li> */}
					</li>
					<li>District 5: <b>Nancy Katims</b>{' '}</li>
				</ul>
			</li>
			<li>Executive: <b>Dave Somers</b>{' '}
				<a href="mailto:SOMERSDAVE@COMCAST.NET">{email}</a>
			</li>
			<li>Sheriff: <b>Ty Trenary</b>{' '}
				<a href="mailto:TY.TRENARY@YAHOO.COM">{email}</a>
			</li>
			<li>South Sno. Co. Fire & Rescue RFA Commissioner, District 1: <b>Ted Hikel</b>{' '}
				<a href="mailto:councilmantedhikel@hotmail.com">{email}</a>
			</li>
			<li>Superior Court Judge
				<ul>
					<li>Position 7: <b>Edirin Okoloko</b>{' '}
						<a href="mailto:JUDGEOKOLOKO@GMAIL.COM">{email}</a>
					</li>
					<li>Position 13: <b>Jennifer Langbehn</b>{' '}
						<a href="mailto:JUDGEJENNIFERLANGBEHN@GMAIL.COM">{email}</a>
					</li>
					<li>Position 14: <b>Paul W. Thompson</b>{' '}
						<a href="mailto:JUDGEPAULTHOMPSON@GMAIL.COM">{email}</a>
					</li>
				</ul>
			</li>
		</ul>
	</li>
	<li>Edmonds
		<ul>
			<li>City Council
				<ul>
					<li>Position 4: <b>Jenna Nand</b>{' '}
						<a href="mailto:jenna@voteforjenna.com">{email}</a>
						<a href="http://voteforjenna.com/">{web}</a>
					</li>
					<li>Position 5: <b>Alicia Crank</b>{' '}
						<a href="https://alicia4edmonds.com/">{web}</a>
					</li>
					<li>Position 6: <b>Susan Paine</b>{' '}
						<a href="https://susanpaine.com/">{web}</a>
					</li>
					<li>Position 7: <b>Laura Johnson</b>{' '}
						<a href="mailto:INFO@VOTELAURAJOHNSON.COM">{email}</a>
						<a href="https://www.votelaurajohnson.com/">{web}</a>
					</li>
				</ul>
			</li>
			<li>Mayor: <b>Mike Nelson</b>{' '}
				<a href="https://www.votenelson.org/">{web}</a>
			</li>
		</ul>
	</li>
	<li>Lynnwood City Council
		<ul>
			<li>Position 4: <b>Van AuBuchon</b>{' '}
				<a href="mailto:VAN@VOTEVAN.NET">{email}</a>
			</li>
			{/* <li>Position 5: <b>Rosamaria Graziani</b>{' '}
				<a href="mailto:rosamaria4lynnwood@gmail.com">{email}</a>
			</li> */}
			<li>Position 6: <b>George Hurst</b>{' '}
				<a href="mailto:Hurst4Lynnwood@outlook.com">{email}</a>
			</li>
			<li>Position 7: <b>Shirley Sutton</b>{' '}
				<a href="mailto:NATC_2@YVN.COM">{email}</a>
			</li>
		</ul>
	</li>
	<li>Seattle City Council, District 5:{' '}
				<b>Debora Juarez</b>{' '}
				<a href="mailto:INFO@DEBORAJUAREZ.COM">{email}</a>
				<a href="https://deborajuarez.com/">{web}</a>
			{/* <b>John Lombard</b>{' '}
				<a href="mailto:info@votejohnlombard.com">{email}</a>
				<a href="https://votejohnlombard.com/">{web}</a> */}
	</li>
	<li>Seattle School District Director
		<ul>
		<li>District 1: <b>Eric Blumhagen</b>{' '}
			<a href="mailto:info@EricBlumhagen.com">{email}</a>
			<a href="https://www.ericblumhagen.com/">{web}</a>
		</li>
		<li>District 3
		<ul>
			<li><b>Chandra Hampson</b>{' '}
				<a href="https://www.electchandra.org">{web}</a>
			</li>
			<li><b>Rebeca Muñiz</b>{' '}
				<a href="https://www.electmuniz.com/">{web}</a>
			</li>
		</ul>
	</li>
		</ul>
	</li>
	<li>
		<a href="http://shorelinewa.gov/prop1">Shoreline Proposition 1</a>:{' '}
		<b>Yes</b>
		<br/>General Obligation Bonds for Aquatic, Recreation and Community Center and Parks and Recreation Improvements
	</li>
	<li>Shoreline City Council
		<ul>
			<li>Position 2: <b>Keith Patrick Scully</b>{' '}
				<a href="mailto:INFO@KEITHSCULLY.ORG">{email}</a>
			</li>
			<li>Position 4: <b>Doris McConnell</b>{' '}
				<a href="mailto:Doris@ReElectDorisMcConnell.com">{email}</a>
				<a href="https://dorismcconnell.com/">{web}</a>
			</li>
			<li>Position 6: <b>Betsy Robertson</b>{' '}
				<a href="mailto:FOBETSYROBERTSON@GMAIL.COM">{email}</a>
			</li>
		</ul>
	</li>
	<li>Shoreline School District Director
		<ul>
			<li>District 1: <b>Meghan Jernigan</b>{' '}</li>
			<li>District 4: <b>Rebeca Rivera</b>{' '}</li>
			<li>District 5
				<ul>
					<li><b>Joe Cunningham</b>{' '}</li>
					<li><b>Sara Betnel</b>{' '}</li>
				</ul>
			</li>
		</ul>
	</li>
</ul>
<hr/>
<h2>Printable Endorsements</h2>
<ul>
	<li>Primary
		<ul>
			<li>King
				{' '}<Link to="/endorsements/print/2019/primary/king">HTML</Link>
				{' '}<a href='/doc/2019/endorsements/primary/king.pdf'>PDF</a>
			</li>
			<li>Snohomish
				{' '}<Link to="/endorsements/print/2019/primary/snohomish">HTML</Link>
				{' '}<a href='/doc/2019/endorsements/primary/snohomish.pdf'>PDF</a>
			</li>
			<li>Info
				{' '}<Link to="/endorsements/print/2019/primary/info">HTML</Link>
				{' '}<a href='/doc/2019/endorsements/primary/info.pdf'>PDF</a>
			</li>
		</ul>
	</li>
	<li>General
		<ul>
			<li>King
				{' '}<Link to="/endorsements/print/2019/general/king">HTML</Link>
				{' '}<a href='/doc/2019/endorsements/general/king.pdf'>PDF</a>
			</li>
			<li>Snohomish
				{' '}<Link to="/endorsements/print/2019/general/snohomish">HTML</Link>
				{' '}<a href='/doc/2019/endorsements/general/snohomish.pdf'>PDF</a>
			</li>
			<li>Info
				{' '}<Link to="/endorsements/print/2019/general/info">HTML</Link>
				{' '}<a href='/doc/2019/endorsements/general/info.pdf'>PDF</a>
			</li>
		</ul>
	</li>
</ul>
</Layout>
)
