import Helmet from 'react-helmet'
import React from 'react'
import A from '../../components/ynq'
import Input from '../../components/input'
import Print from '../../components/print'
import Layout from '../../components/layout'

const demo = {
	// on: true,
}
const email = 'endorsements@32democrats.org'
const title = 'Candidate Questionnaire'
const data = {
	'amend-money-politics': '',
	'appeal': 'Through name recognition and sound bites.',
	'assist': '',
	'budget': '1000',
	'consultants': 'L. Elk, et al.',
	'democrat': '',
	'doorbell': 'Hit precincts without PCOs in the weeks before ballots arrive.',
	'email': 'jdoe@example.com',
	'endorsements-org': 'Unobtanium Workers Union, et al.',
	'endorsements-seek': 'Widget Workers Union, et al.',
	'endorsements': 'M. Moose, et al.',
	'fund-goal': '1000',
	'growth': '',
	'legal-entity': 'Acme LLC',
	'mail': '123 Fake St., Seattle WA 98125',
	'mailings': 'Mail gloss color cards to 1000 likely voters the day before ballots arrive.',
	'manager': 'A. Deer',
	'name': 'J. Doe',
	'office': 'Mayor',
	'opponents': 'O. Ox, et al.',
	'pay-hazmat': '',
	'phone': '555-555-5555',
	'plan': 'Post photos and phrases online.\nHold a rally.',
	'platform': '',
	'printed-name': 'J. Doe',
	'record': 'Majored in basket-weaving.\nFilled forms for 30 years.',
	'save-climate': '',
	'sick-leave': '',
	'signature': 'X',
	'staff': '10',
	'strike': '',
	'support-nominee': '',
	'theme': 'Real change',
	'transit': '',
	'wage': '',
	'website': 'https://example.com/',
	'win': 'I will achieve better name recognition while attacking my opponent.\nAlso, my campaign is centered around saving the whales, which many regard as their top issue.',
}

const data_blank = Object.fromEntries(
	Object.entries(data).map(([k, v])=>[k, ''])
)
const today = (new Date()).toISOString().substring(0,10)

const Page = () => (
	<Layout>
		<Questionnaire data={demo.on && data}/>
	</Layout>
)

export class Questionnaire extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			...data_blank,
			...props.data,
		}
		this.onChange = this.onChange.bind(this)
	}
	onChange(ev){
		const {target} = ev
		const value = (target.type === 'checkbox') ? target.checked : target.value
		this.setState({[ev.target.name]: value})
	}
	onSubmit = (ev) => {
		ev.preventDefault()
	}
	render(){
		const {onChange, state} = this
		return (
	<div className='form32'>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<h1>{title}</h1>
		<form
			action={`mailto:${email}?subject=${title}`}
			onSubmit={this.onSubmit}
		>
		<Input name='name' label='Candidate name' required={true} value={state.name} onChange={onChange}/>
		<Input name='office' label='Elective office sought' required={true} value={state.office} onChange={onChange}/>
		<Input name='legal-entity' label='Campaign legal entity' value={state['legal-entity']} onChange={onChange}/>
		<Input name='email' label='Email' required={true} value={state.email} onChange={onChange}/>
		<Input name='website' label='Website' value={state.website} onChange={onChange}/>
		<Input name='mail' label='Mailing address' value={state.mail} onChange={onChange}/>
		<Input name='phone' label='Phone' value={state.phone} onChange={onChange}/>
		<Input name='manager' label='Manager' value={state.manager} onChange={onChange}/>
		<Input name='consultants' label='Consultant(s)' value={state.consultants} onChange={onChange}/>
		<Input
			label='Individual endorsements'
			name='endorsements'
			value={state.endorsements}
			onChange={onChange}
		/>
		<Input
			label='Organization endorsements'
			name='endorsements-org'
			onChange={onChange}
			value={state['endorsements-org']}
		/>
		<Input
			label='Seeking endorsements of'
			name='endorsements-seek'
			value={state['endorsements-org']}
			onChange={onChange}
		/>
		<Input
			label='Campaign theme'
			name='theme'
			value={state.theme}
			onChange={onChange}
		/>
		<Input
			label='Staff count expected'
			name='staff'
			value={state.staff}
			onChange={onChange}
		/>
		<Input
			label='Budget estimate'
			name='budget'
			value={state.budget}
			onChange={onChange}
		/>
		<Input
			label='Fundraising goal'
			name='fund-goal'
			value={state['fund-goal']}
			onChange={onChange}
		/>
{/* <h2>Questions</h2> */}
<label className='block'>
	What record of performance, ability, personal qualities, and positions on issues distinguish you as a candidate?
	<textarea name='record' value={state.record} onChange={onChange}/>
</label>
<Input
	label='How will your campaign appeal to voters?'
	name='appeal'
	value={state.appeal}
	onChange={onChange}
/>
<Input
	label='Opponents'
	name='opponents'
	value={state.opponents}
	onChange={onChange}
/>
<A name='support-nominee'>
	Will you support a Democratic candidate in the general election
	from the top 2 of the primary if you do not advance?
</A>
<Input
	label='Doorbelling plan'
	name='doorbell'
	value={state.doorbell}
	onChange={onChange}
/>
<Input
	label='Mail campaign plan'
	name='mailings'
	value={state.mailings}
	onChange={onChange}
/>
<label className='block'>
	Other campaign plans
	<textarea name='plan' value={state.plan} onChange={onChange}/>
</label>
<label className='block'>
	Why will you win the election?
	<textarea name='win' value={state.win} onChange={onChange}/>
</label>
<p className='segment'>
Please answer the following questions: 'Yes', 'No', or 'Qualified'.
<br/>
Explain each 'No' and 'Qualified'.
</p>
<A name='democrat'>
Are you known as a Democrat?
</A>
<A name='platform'>
Do you support the 32nd LD platform, and the planks regarding
civil & human rights,
corporate power,
economic justice,
the environment,
government reform,
human services,
immigration,
and media reform?
</A>
<A name='assist'>
Should government assist individuals, and families who are without sufficient food, shelter, or basic necessities through no fault of their own?
</A>
<A name='strike'>
Do you support the right of workers, including public workers, to bargain and strike?
</A>
<A name='wage'>
Should the wages paid to workers in Washington State be raised incrementally towards the goal of living wages?
</A>
<A name='sick-leave'>
Should all employers in Washington State provide their employees paid sick leave?
</A>
<A name='transit'>
Should transportation policy discourage the use of private automobiles, and encourage shorter commutes, and use of public-transit?
</A>
<A name='save-climate'>
Should our government seek to alter the human activities that are creating the conditions contributing to disastrous climate change?
</A>
<A name='growth'>
Will you support locally the Washington State <a
href='https://www.commerce.wa.gov/about-us/rulemaking/gma-laws-rules/'
target='_blank'
rel='noopener noreferrer'
>
Growth Management Act
</a>
?
</A>
<A name='pay-hazmat'>
Should railroad companies be responsible for damages from transporting hazardous materials?
</A>
<A name='amend_money_politics'>
Do you support amending the U.S. Constitution to lessen the influence of money in politics and declare that corporations do not have the rights of people?
</A>
	<div className='segment'>
		<p><b>
			I have read this questionnaire and understand and approve the content and all provided information.
		</b></p>
		<Input label='Printed Name' name='printed-name' value={state['printed-name']} required={true} onChange={onChange}/>
		<div className='flex wrap'>
			<label className='label'>
				Date{' '}
				<input
					defaultValue={today}
					name='date'
					pattern='[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])'
					placeholder='YYYY-MM-DD'
					required={true}
					size='11'
					onChange={onChange}
				/>
			</label>
			<label className='label grow'>
				Signature <input name='signature' value={state.signature} className='grow' onChange={onChange}/>
			</label>
		</div>
	</div>
	<div className='flex wrap no-print'>
		{/* <button type='submit' className='grow'>
			Email Client
		</button> */}
		<Print className='grow block'/>
	</div>
	</form>
</div>
		)
	}
}

export default Page
