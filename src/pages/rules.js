import Helmet from 'react-helmet'
import React from 'react'
import {createHeader} from '../components/header'
import Layout from '../components/layout'
const title = 'Standing Rules'
const d = '.'
const H = createHeader({
	prefixes: [
		'Standing Rule ',
		'Section ',
	],
	suffixes: [':',d,d,d],
})

export default () => (
<Layout>
	<div className='legal'>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<h1 className='title'>{title}</h1>
<p>
32nd Legislative District Democratic Organization
<br/>
Amended March 11, 2015
<br/>
Formatted 2019
</p>
<H id='1'>
Election Procedures
</H>
<H id='1.1'>
Election of District Chair
</H>
<H id='1.1.A'/>
<p>
Each candidate for District Chair will control a total of five (5) minutes for the 
purpose of nominating, seconding, and nominee speeches.
</p>
<H id='1.1.B'/>
<p>
A question-and-answer (Q&A) period of ten (10) minutes will then follow; the length 
of this Q&A period may be extended by majority vote of the PCOs present and voting. 
Questions may not exceed twenty (20) seconds; each nominee's answer may not 
exceed one (1) minute.
</p>
<H id='1.1.C'/>
<p>
A closing statement by or for each nominee, in the order of nomination may then be 
made, not to exceed two (2) minutes.
</p>
<H id='1.1.D'/>
<p>
As per the Bylaws, voting shall be by signed, written ballot of the PCOs eligible and 
present to vote, unless a majority of voting members present vote to elect by 
acclamation.
</p>
<H id='1.2'>
Election of Other Officers
</H>
<H id='1.2.A'/>
<p>
Each candidate for office will control a total of three (3) minutes for the purpose of 
nominating, seconding, and nominee speeches.
</p>
<H id='1.2.B'/>
<p>
A question-and-answer (Q&A) period of five (5) minutes will then follow; the length 
of the Q&A period may be extended by a majority vote of those present who are 
eligible to vote on the office. Questions should not exceed twenty (20) seconds; each 
nominee's answer should not exceed one (1) minute.
</p>
<H id='1.2.C'/>
<p>
A closing statement by or for each nominee, in the order of nomination may then be 
made, not to exceed one (1) minute.
</p>
<H id='1.2.D'/>
<p>
For those offices listed in Article 4 § 1 of the bylaws, voting shall be by signed, written 
ballot of the PCOs eligible and present to vote, except in instances where a majority of 
voting members present vote to elect by acclamation. All other offices shall be by 
raised credential vote, unless by acclamation, or a signed, written ballot is requested.
</p>
<H id='1.3'>
Ratification of Appointed Officers
</H>
<H id='1.3.A'/>
<p>
The District Chair will announce the name of the candidate for the appointed office 
and request a motion for ratification.
</p>
<H id='1.3.B'/>
<p>
If present, the nominee may have two (2) minutes to introduce her/himself, followed 
by a question-and-answer (Q&A) period not to exceed three (3) minutes. Questions 
should not exceed twenty (20) seconds.
</p>
<H id='1.3.C'/>
<p>
Debate on the motion may follow.
</p>
<H id='1.3.D'/>
<p>
A raised-credential vote will be taken, unless a member requests a signed, written 
ballot.
</p>
<H id='1.3.E'/>
<p>
When making an appointment at any meeting except the reorganization meeting, the 
District Chair shall place an announcement of the appointment in the District 
Newsletter or via email at least 10 days in advance of the meeting where the 
appointment would be ratified.
</p>
<H id='2'>
Endorsement Policy and Procedures
</H>
<H id='2.1'/>
<p>
Candidates may be endorsed for elective office, and positions may be taken on 
ballot issues, at any regular meeting in which written notice has been given as an agenda item 
on the District organization's Newsletter, whether mailed or e-mailed a minimum of ten (10) 
days prior to the meeting.
</p>
<H id='2.2'/>
<H id='2.2.A'/>
<p>
Only self-identified Democratic candidates may be endorsed by the District 
organization.
</p>
<H id='2.2.B'/>
<p>
Judicial candidates, who are not allowed to make such identifications, are the only 
exception to this requirement.
</p>
<H id='2.3'/>
<p>
Only candidates, or ballot issues, that will appear on a ballot in the 32nd Legislative 
District can be endorsed or a position taken.
</p>
<H id='2.4'/>
<p>
In races in which no self-identified Democratic candidate will be on the ballot, a 
preference for one of the candidates on the ballot may be decided using the same procedures 
as used for endorsements.
</p>
<H id='2.5'/>
<p>
The Executive Board shall establish an Endorsement Committee by March of each 
year and the Board may make recommendations to the membership regarding endorsement, 
of candidates and incumbents and issues.
</p>
<H id='2.6'>
Voting procedures shall be as follows:
</H>
<H id='2.6.A'/>
<p>
Endorsements shall require a sixty-percent (60%) vote of the members present and 
voting.
</p>
<H id='2.6.B'/>
<p>
More than one candidate may be endorsed for the same office.
</p>
<H id='2.6.C'/>
<p>
Voting will be by raised credential. Written ballots may be requested by fifteen percent 
(15%) of the eligible voters present and voting.
</p>
<H id='2.7'>
Early Endorsement
</H>
<H id='2.7.A'/>
<p>
No endorsement of a candidate shall occur before the close of candidate filing except 
when notice is provided in the District Newsletter before the General-membership 
meeting or by first-class mail or e-mail postmarked at least ten (10) days before the 
meeting when such an endorsement is proposed. 
</p>
<H id='2.7.B'/>
<p>
If the meeting is to be held before the close of filing, a two-thirds (2/3) vote of the 
members present and voting (excluding abstentions) shall be required for 
endorsement. 
</p>
<H id='2.8'/>
<p>
All motions to endorse shall require a second except motions from the Executive 
Board.
</p>
<H id='2.8.A'/>
<p>
After all motions to endorse individuals for a given race have been made there will be a 
question-and-answer (Q&A) period for a panel composed of all potential endorsees in 
the race, (including substitute spokespeople authorized as such in writing), each of 
whom will have an opportunity to respond to each question. The maximum Q&A time 
will be three (3) minutes times the number of potential endorsees for that race. Each 
question may last up to fifteen (15) seconds, and each answer may last up to thirty (30) 
seconds.
</p>
<H id='2.8.B'/>
<p>
The motions will then be debated and voted upon in the order in which they were 
made, starting with the Executive Board motion(s). Up to four (4) speakers, alternating 
pro (two) and con (two), will be allowed. Each speaker may take up to thirty (30) 
seconds. If no one requests recognition to speak against the motion after the first pro 
speaker has spoken, then debate will be closed, and the vote taken. This sequence of 
“debate and vote” will be followed for each of the motions, in turn.
</p>
<H id='2.8.C'/>
<p>
If more than one individual receives a three-fifths (3/5) vote, the result is a multiple 
endorsement. If only one individual is endorsed, it is a sole endorsement. If no 
individual is endorsed, motions for multiple endorsements will then be in order, and 
the same procedures for “debate and vote” (Sec. 6B above) will be followed.
</p>
<H id='2.9'/>
<p>
Only the Executive Board or a member of the 32nd Legislative District Democratic 
Organization may make a motion for endorsement or a motion for a position on a ballot issue.
</p>
<H id='2.10'/>
<p>
Members may debate a motion for positions on ballot measures, alternating pro 
and con. Questions and answers shall be allowed. Debate may be closed by a vote of two-thirds 
(2/3) of the members present and voting.
</p>
<H id='2.11'/>
<p>
Sections 1 through 8 of this Standing Rule also apply to motions for “revocation of 
endorsement“ and “rejection of endorsement,” substituting “revocation of endorsement” as 
“rejection of endorsement” as applicable.
</p>
<H id='2.12'/>
<p>
The organization shall support and publicize its endorsements and positions in 
ways that it deems appropriate. The District organization's literature-distribution system will 
be available only for endorsed candidates and endorsed ballot issues.
</p>
<H id='3'>
Endorsement Committee
</H>
<H id='3.1'>
Establishment and Composition
</H>
<p>
The Executive Board shall appoint an Endorsement Committee from among volunteers from 
the general membership. Executive Board members may serve as Endorsement Committee 
members, but no more than one-half of the committee members can also be Executive Board
members. Appointees must be ratified by majority vote at the next regularly scheduled 
General Membership meeting following their appointment. The Endorsement Committee shall 
consist of a minimum of five (5) members but a larger number is preferred.
</p>
<H id='3.2'>
Invitation to Candidates
</H>
<H id='3.2.A'/>
<p>
The Endorsement Committee shall send a letter to Democratic candidates and 
candidates for nonpartisan office who will appear on the ballot in the 32nd Legislative 
District for a given election.
</p>
<H id='3.2.B'/>
<p>
The letter shall ask if the candidate is interested in seeking our endorsement and shall 
include the provisions of Sections 4 and 5 of this Rule, and any other information the 
Committee wishes to furnish. 
</p>
<H id='3.2.C'/>
<p>
The Committee shall keep a record of letters sent and responses received and shall 
140 begin to schedule interview meetings as soon as possible.
</p>
<H id='3.3'>
Endorsement Committee Interviews and Questionnaires
</H>
<H id='3.3.A'/>
<p>
Except as provided in Section 7 of Standing Rule 2, every candidate who seeks the 
endorsement of the 32nd Legislative District Democratic Organization, during the year 
in which endorsement is sought, must complete a questionnaire provided by the 
Endorsement Committee.
</p>
<H id='3.3.B'/>
<p>
Every congressional, legislative, and local candidate who wants the recommendation of 
the 32nd Legislative District Executive Board must, during the year in which 
endorsement is sought, appear in person or through an authorized representative at an 
interview meeting of the Endorsement Committee. 
</p>
<H id='3.3.C'/>
<p>
Prior to the interview meetings, the Executive Board, or the general membership, by 
majority vote (present and voting), may waive the interview or questionnaire 
requirement. 
</p>
<H id='3.3.D'/>
<p>
If a candidate does not interview with the Endorsement Committee, or obtain a waiver 
as described in this Section, neither the Endorsement Committee nor the Executive 
Board may recommend to the membership that the candidate be endorsed.
</p>
<H id='3.4'>
Interview Meetings
</H>
<H id='3.4.A'/>
<p>
At Endorsement Committee interview meetings, Democratic candidates will be given 
time to present reasons for endorsement, to be followed by time for questions from the 
floor, with time for response by the candidates. The amount of time to be allotted for 
each will be set by the committee depending upon the time available.
</p>
<H id='3.4.B'/>
<p>
All deliberations and votes shall be conducted in Executive Session. A written record 
shall be kept of the number of votes cast for or against each candidate. 
</p>
<H id='3.5'>
Number of Interview Meetings
</H>
<p>
The Endorsement Committee shall hold at least one interview meeting with Democratic 
candidates seeking the endorsement of the 32nd Legislative District Democratic Organization, 
and as many more meetings as it deems necessary to accommodate all the candidates 
requesting endorsement, prior to the Endorsement Meeting of each year. All meetings of the 
Endorsement Committee shall be open to the membership. In addition, the Endorsement 
Committee will make every effort to schedule a candidate forum, open to the public, for 
candidates running for open seats.
</p>
<H id='3.6'>
Endorsement Committee Files and Questionnaires
</H>
<H id='3.6.A'/>
<p>
The Endorsement Committee shall develop a list of questions appropriate to each office 
to be asked of candidates during interviews, and may ask candidates to submit copies of 
questionnaires they have filled out for other organizations. It shall keep a file on each 
candidate who appears during the year, their positions on issues, their responses to 
questions, and any other pertinent information they can gather.
</p>
<H id='3.6.B'/>
<p>
General members will be asked to contribute to the files throughout the year; all of the 
information shall be available to the membership prior to endorsements. Individual 
files shall be saved from year to year to the extent practicable and useful.
</p>
<H id='3.7'>
Review by the Executive Board
</H>
<p>
The Executive Board, by a 2/3 vote of members present and voting, may overrule any 
recommendation of the Endorsement Committee. 
</p>
<H id='4'>
Policy for Reduction of Dues for Those with Limited Income
</H>
<H id='4.1'/>
<p>
Any reduction in dues, installment-payment arrangements, and volunteer time or 
in-kind contributions in lieu of dues shall be at the discretion of the Chair or a Vice-Chair.
</p>
<H id='4.2'/>
<p>
The Chair shall be responsible for making the proper notification to the Treasurer 
and Membership Officer for members covered under this Standing Rule.
</p>
<H id='5'>
Policy and Procedures Regarding Membership Lists
</H>
<H id='5.1'/>
<p>
All membership lists/rosters containing the names, precinct, addresses and phone 
numbers of dues-paying members shall be confidential and for sale only to members of the 
32nd Legislative District Democratic Organization. Upon request, lists in the form of one-time204 use labels, containing only the names and addresses, shall be for sale to Candidate and Ballot 
Issue Campaigns that have been endorsed by the membership. Membership lists for members 
are solely for the private use of the members of the 32nd Legislative District Democratic 
Organization. Members of the Executive Board may have access to membership information, 
without any charge, for carrying out the duties of their office. 
</p>
<H id='5.2'/>
<p>
The Executive Board shall set the sale price for any membership lists. The price to 
members shall be nominal, essentially the cost of producing the list. The price to Candidate 
and Ballot Campaigns shall reflect the political value.
</p>
<H id='5.3'/>
<H id='5.3.A'/>
<p>
Members may obtain a copy of the list by contacting the Membership Officer in advance 
and requesting that a copy of the list be available for sale at the next District Meeting. 
Members may also request a copy of the membership list by mail by sending a request 
along with the fee to the Membership Officer, who will verify membership before 
mailing the list.
</p>
<H id='5.3.B'/>
<p>
All membership lists will include the following statement: “This membership list is 
published by the 32nd Legislative District Democratic Organization, State of 
Washington. It is solely for the private use of members of the 32nd Legislative District 
Democratic Organization. It is not to be reproduced, nor is it intended for publication or 
commercial solicitation by any means, public or private.”
</p>
<H id='5.3.C'/>
<p>
Campaign requests for labels must be approved by the Executive Board. When 
approved and upon confirmation of the receipt of the required fee, the Membership 
Officer will provide the labels.
</p>
<H id='5.4'/>
<p>
Opportunity shall be given to non-PCO members to omit their address, email 
address or telephone number from said lists/rosters by marking the appropriate box on the 
membership form or providing a written request to the Membership Officer, at any time, to 
have their address, email address or phone number omitted from the membership list/roster.
</p>
<H id='5.5'/>
<p>
The email addresses or email aliases of all PCOs shall be published on the District’s 
website.
</p>
<H id='6'>
Policy and Procedures Regarding Meeting Programs
</H>
<H id='6.1'/>
<p>
All programs scheduled for any meeting of the District organization will begin no 
later than 7:30 p.m., thirty (30) minutes after the meeting is called to order.
</p>
<H id='7'>
Policy regarding Debate Procedures
</H>
<H id='7.1'/>
<p>
When a motion is properly before the body, the debate may be started
with either a statement in favor of or against the motion. If there is only a favorable or 
negative statement, that member may be allowed to speak. 
</p>
<H id='8'>
Policy and Procedures for the Website
</H>
<H id='8.1'/>
<p>
A website shall be established to disseminate information to the membership, to 
publish the views and to support endorsed candidates and elected 32nd Legislative District 
Democratic officials.
</p>
<H id='8.2'/>
<p>
The goal for website content includes a copy of the Bylaws, Standing Rules, Platform 
and Resolutions, contact information for officers and for elected Democratic Officials, notices 
and agendas for upcoming 32nd LDDO meetings and other events, meeting minutes, archived 
newsletters, links to web pages of elected Democratic Officials and endorsed candidates, 
initiatives and propositions and PCO-contact information..
</p>
<H id='8.3'/>
<p>
The draft meeting minutes and proposed agendas will be published as soon as 
feasible. Every effort will be made to publish meeting minutes and resolutions in some form
that cannot be readily edited and for which the software to access the document is readily and 
freely available.
</p>
<H id='8.4'/>
<p>
At least three (3) officers of the District shall have the password to the District 
website.
</p>
<H id='8.5'/>
<p>
A majority of the Executive Board, or of the membership, may override changes to 
the District website. 
</p>
<H id='9'>
Resolutions
</H>
<H id='9.1'>
Timing
</H>
<H id='9.1.A'/>
<p>
Resolutions may be submitted to the body at any time, but they shall require ten (10) 
days written notice to the members before a vote on final passage. This requirement 
may be waived by two-thirds (2/3) vote of those members present and voting.
</p>
<H id='9.1.B'/>
<p>
Resolutions presented with proper notice by committees or other subgroups of this 
organization shall require neither a motion nor a second prior to debate and vote.
</p>
<H id='9.1.C'/>
<p>
Every effort will be made to post pending resolutions on the District’s website.
</p>
<H id='9.2'>
Expectation that Resolutions should be typed
</H>
<H id='9.2.A'/>
<p>
Resolutions should be typed and electronically submitted to the Secretary and Chair of 
the District fourteen (14) days in advance of a meeting.
</p>
<H id='9.3'>
Correspondence Committee Responsibilities
</H>
<H id='9.3.A'/>
<p>
The Correspondence Committee shall reformat resolutions adopted by the organization 
to meet the specifications and requirements of the Washington State Democratic 
Central Committee or the Washington State Democratic Convention. 
</p>
<H id='9.3.B'/>
<p>
All resolutions shall be sent within ten (10) days of adoption.
</p>
<H id='10'>
Public Communications
</H>
<H id='10.1'>
Newsletter
</H>
<H id='10.1.A' className='block'>
Publication Schedule and Review
</H>
<H id='10.1.A.a'/>
<p>
The District should distribute a regular newsletter a minimum of twice a month, 
through electronic means or through the United States Postal Service. One 
newsletter shall be distributed a minimum of 10 days before a scheduled 
meeting of the District. The other newsletter should be distributed a minimum of 
10 days after a general meeting of the District. 
</p>
<H id='10.1.A.b'/>
<p>
The newsletter published before a scheduled meeting of the District should
contain an agenda, program information, officer reports, links to any proposed 
resolutions, and other information as determined by the Executive Board. 
</p>
<H id='10.1.A.c'/>
<p>
The newsletter published after a general meeting of the District should contain 
highlights from the business portion of the meeting, a link to the complete 
minutes of the general meeting, and other information as determined by the 
Executive Board. 
</p>
<H id='10.1.A.d'/>
<p>
The Chair shall review the regular newsletter before publication. 
</p>
<H id='10.1.A.e'/>
<p>
The District may distribute additional newsletters throughout each month, and 
may distribute special newsletters relating to campaigns and elections, or to 
specific subpopulations (i.e. city-specific newsletter, or Young Democrats 
newsletter, etc.). 
</p>
<H id='10.1.B' className='block'>
Newsletter and Website Content
</H>
<H id='10.1.B.a'/>
<p>
The District may use its website and/or newsletter to distribute information 
from any elected official about public affairs, subscriptions to official newsletter 
and websites, and public meetings. 
</p>
<H id='10.1.B.b'/>
<p>
An endorsed candidate may have any campaign event publicized by the 32nd 
LDDO on the District's website and in the newsletter. The LDDO may provide 
links to campaign websites, newsletters maintained by an endorsed candidate, 
and may otherwise promote an endorsed candidate on the website or in the 
newsletter. Campaign events sponsored by an endorsed candidate shall be 
highlighted or otherwise distinguished from events sponsored by other 
Democratic candidates.
</p>
<H id='10.1.B.c'/>
<p>
Any independent organization, whose mission and goals are compatible with 
those of the District, may have events publicized by the District in its regular 
newsletter and on the District's website. In order for an independent 
organization to have events publicized by the District, a majority of the 
Executive Board must vote to recognize the organization. A list of these 
recognized organizations shall be maintained.
</p>
<H id='10.1.B.i'/>
<p>
This section does not apply to other organizations recognized in the 
Washington State Democratic Party Charter or by the Washington State 
Democratic Central Committee. Events and information distributed by 
another Democratic Party organization may be included in the newsletter 
and on the website. 
</p>
<H id='10.2'>
Blogs and Social Media
</H>
<H id='10.2.A'/>
<p>
The District may maintain a public Facebook, blog, or other social media pages. At 
least three (3) officers of the District shall have administrative privileges to delete 
1) spam and other offensive posts 2) anonymous comments and 3) other types of 
messages as approved by 2/3rds of the Executive Board.
</p>
</div>
</Layout>
)
