import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../components/layout'
export default () => (
<Layout>
<Helmet>
	<title>
		Mission & Values
	</title>
</Helmet>
<h1>
	Mission
</h1>
<p>
The 32nd Legislative District Democrats work to build vibrant and tolerant societies whose governments are accountable and open to the participation of all people.
</p>
<p>
We seek to strengthen the rule of law; respect for human rights, minorities, and a diversity of opinions; democratically elected governments; and a civil society that helps keep government power in check.
</p>
<p>
We help to shape public policies that assure greater fairness in political, legal, and economic systems and safeguard fundamental rights.
</p>
<p>
We implement initiatives to advance justice, education, public health, and independent media.
</p>
<p>
We build alliances across borders on issues such as corruption and freedom of information.
</p>
<p>
Working in every part of our district, we place a high priority on protecting and improving the lives of people in marginalized communities.
</p>
<h1>
Values
</h1>
<p>
We believe in fundamental human rights, dignity, and the rule of law.
</p>
<p>
We believe in a society where all people are free to participate fully in civic, economic, and cultural life.
</p>
<p>
We believe in addressing inequalities that cut across multiple lines, including race, class, gender, sexual orientation, and citizenship.
</p>
<p>
We believe in holding those in power accountable for their actions and in increasing the power of historically excluded groups.
</p>
<p>
We believe in helping people and communities press for change on their own behalf.
</p>
<p>
We believe in responding quickly and flexibly to the most critical threats to open society.
</p>
<p>
We believe in taking on controversial issues and supporting bold, innovative solutions that address root causes and advance systemic change.
</p>
<p>
We believe in encouraging critical debate and respecting diverse opinions.
</p><p>
* Thanks to the Open Society for sharing.
</p>
<ul className="menu">
	<li>
		<Link to="/civility-pledge">Civility Pledge</Link>
	</li>
</ul>
</Layout>
)
