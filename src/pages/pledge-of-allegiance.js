import React from 'react'
import Helmet from 'react-helmet'
import flag from '../components/flag-US.svg'
import Layout from '../components/layout'

const phrase = {
	display: 'inline-block',
}

export default () => (
	<Layout>
		<Helmet>
			<title>Pledge of Allegiance</title>
		</Helmet>
		<div className="">
			<p style={{
				'font-size':'1.5em',
				// 'white-space':'pre-wrap',
				overflow:'hidden',
			}}>
				<span style={phrase}>I pledge allegiance to the Flag of the United States of America,&nbsp;</span>
				<span style={phrase}>and to the Republic for which it stands,&nbsp;</span>
				<span style={phrase}>one Nation under God,&nbsp;</span>
				<span style={phrase}>indivisible,&nbsp;</span>
				<span style={phrase}>with liberty and justice for all.</span>
				<img src={flag} alt='USA flag' style={{
					display:'block',
					'max-height':'50vh',
					margin: '1.5em 0 0',
				}}/>
			</p>
		</div>
	</Layout>
)
