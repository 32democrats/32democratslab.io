import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../components/layout'
export default () => (
<Layout>
	<div className="article">
		<Helmet>
			<title>Membership</title>
		</Helmet>
		<h1>Membership</h1>
		<h2>32nd Legislative District Democratic Organization Membership</h2>
		<p>
			<em>
				<strong>
					Join us and become a member of the 32nd District Democrats!
				</strong>
			</em>
		</p>
		<p>
			We want you to become a member of the 32nd District. Our dues help our
			organization fund campaign activities, hold regular meetings and special
			events, and maintain a presence in our community.
		</p>
		<p>
			Join us to become an official member of the Democratic Party. Benefits of
			becoming a member include:
		</p>
		<ul>
			<li>Meeting fellow Democrats who share your values</li>
			<li>Our monthly newsletter</li>
			<li>The right to vote on the District's endorsements</li>
			<li>The right to propose and vote on motions and resolutions</li>
			<li>Learn first about special events in your area</li>
		</ul>
		<p>Member dues are:</p>
		<ul>
			<li>Individual: $20</li>
			<li>Family (two or more persons in a household): $32</li>
			<li>Student/Senior: $5</li>
			<li>Silver: add $25</li>
			<li>Gold: add $50</li>
			<li>Diamond: add $75</li>
		</ul>
		<ul className="menu">
			<li>
				<a
					className="money"
					href="https://secure.actblue.com/donate/32nd-legislative-district-democrats-1"
				>
					Pay Online
				</a>
			</li>
		</ul>
		<p>
			<strong>Mail checks to:</strong>
		</p>
		<p>
			32nd Legislative District Democratic Organization (32LDDO)<br />
			PO Box 55622<br />
			Shoreline, WA 98155
		</p>
	</div>
</Layout>
)