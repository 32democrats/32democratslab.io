import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../components/layout'
export default () => (
<Layout>
	<div className="legal">
		<Helmet>
			<title>
				Bylaws
			</title>
		</Helmet>
		<h1 className="title">
			Bylaws
		</h1>
<p>32nd Legislative District Democratic Organization bylaws</p>
<p>Adopted April 12, 2017</p>
<h1>Contents</h1>
<ul className="menu">
<li><a href="#S0">Preamble</a></li>
<li><a href="#S1">Article 1 – Name</a></li>
<li><a href="#S2">Article 2 – Authority and Purpose</a></li>
<li><a href="#S2.1">Section 2.1 – Authority</a></li>
<li><a href="#S2.2">Section 2.2 – Purpose</a></li>
<li><a href="#S3">Article 3 – Membership and Dues</a></li>
<li><a href="#S3.1">Section 3.1 – Precinct Committee Officers</a></li>
<li><a href="#S3.2">Section 3.2 – General Members</a></li>
<li><a href="#S3.3">Section 3.3 – Associate Members</a></li>
<li><a href="#S3.4">Section 3.4 – Dues</a></li>
<li><a href="#S4">Article 4 – Officers, Executive Board, and Committees</a></li>
<li><a href="#S4.1">Section 4.1 – Elected Officers</a></li>
<li><a href="#S4.2">Section 4.2 – Appointed Officers</a></li>
<li><a href="#S4.3">Section 4.3 – Duties and Responsibilities</a></li>
<li><a href="#S4.4">Section 4.4 – Executive Board</a></li>
<li><a href="#S4.5">Section 4.5 – Vacancies</a></li>
<li><a href="#S4.6">Section 4.6 – Committees</a></li>
<li><a href="#S5">Article 5 – Election and Ratification of Officers</a></li>
<li><a href="#S5.1">Section 5.1 – Notice</a></li>
<li><a href="#S5.2">Section 5.2 – Nomination</a></li>
<li><a href="#S5.3">Section 5.3 – Signed Ballot</a></li>
<li><a href="#S5.4">Section 5.4 – Order of Election</a></li>
<li><a href="#S5.5">Section 5.5 – Establishing a Majority</a></li>
<li><a href="#S5.6">Section 5.6 – Ratification</a></li>
<li><a href="#S5.7">Section 5.7 – Additional Procedures</a></li>
<li><a href="#S6">Article 6 – Removal of Officers</a></li>
<li><a href="#S6.1">Section 6.1 – Starting the Process</a></li>
<li><a href="#S6.2">Section 6.2 – Debate and Removal</a></li>
<li><a href="#S7">Article 7 – Meetings</a></li>
<li><a href="#S7.1">Section 7.1 – Regular Meetings</a></li>
<li><a href="#S7.2">Section 7.2 – Special Meetings</a></li>
<li><a href="#S7.3">Section 7.3 – Quorum</a></li>
<li><a href="#S7.4">Section 7.4 – Proxies</a></li>
<li><a href="#S7.5">Section 7.5 – Debatable Motions</a></li>
<li><a href="#S7.6">Section 7.6 – Questions and Feedback</a></li>
<li><a href="#S8">Article 8 – Precinct Committee Officers</a></li>
<li><a href="#S8.1">Section 8.1 – Application</a></li>
<li><a href="#S8.2">Section 8.2 – Approval</a></li>
<li><a href="#S8.3">Section 8.3 – Special Situations</a></li>
<li><a href="#S8.4">Section 8.4 – Public Knowledge</a></li>
<li><a href="#S9">Article 9 – Endorsements</a></li>
<li><a href="#S9.1">Section 9.1 – Endorsement Procedures</a></li>
<li><a href="#S9.2">Section 9.2 – Early Endorsement</a></li>
<li><a href="#S9.3">Section 9.3 – Financial Support</a></li>
<li><a href="#S10">Article 10 – Finances</a></li>
<li><a href="#S10.1">Section 10.1 – Disbursements</a></li>
<li><a href="#S10.2">Section 10.2 – General Operations Fund</a></li>
<li><a href="#S10.3">Section 10.3 – Campaign Fund</a></li>
<li><a href="#S11">Article 11 – Amending the Bylaws</a></li>
<li><a href="#S11.1">Section 11.1 – Notice</a></li>
<li><a href="#S11.2">Section 11.2 – Limitations</a></li>
<li><a href="#S12">Article 12 – Written Notice</a></li>
<li><a href="#S13">Article 13 – Voting</a></li>
<li><a href="#S13.1">Section 13.1 – Method</a></li>
<li><a href="#S13.2">Section 13.2 – Written Ballots</a></li>
<li><a href="#S13.3">Section 13.3 – Secret Votes</a></li>
<li><a href="#S14">Article 14 – Parliamentary Authority</a></li>
</ul>
<h1 id="S0"><a href="#S0">Preamble</a></h1>
<p>This organization is dedicated to the development and promotion of the ideals of the Democratic Party, as expressed in our platform and to increasing the interest and participation of the residents of this district of King and Snohomish counties in their government. We establish these bylaws for the conduct of our business to achieve these goals.</p>
<h1 id="S1"><a href="#S1">Article 1 – Name</a></h1>
<p>The name of this organization shall be the "32nd Legislative District Democratic Organization".</p>
<h1 id="S2"><a href="#S2">Article 2 – Authority and Purpose</a></h1>
<h2 id="S2.1"><a href="#S2.1">Section 2.1 Authority</a></h2>
<p>This organization shall operate under the applicable bylaws and rules of the King County Democratic Central Committee (KCDCC), Snohomish County Democratic Central Committee (SCDCC)<i>, </i>state and national Democratic parties, and state requirements set forth in the Revised Code of Washington (RCW) or Washington Administrative Code (WAC).</p>
<h2 id="S2.2"><a href="#S2.2">Section 2.2 Purpose</a></h2>
<p>It shall be the mission of this organization to contribute to the growth, development, and influence of the Democratic Party by maximizing participation and equal treatment in support of endorsed Democratic issues and Democratic candidates.</p>
<h1 id="S3"><a href="#S3">Article 3 – Membership and Dues</a></h1>
<h2 id="S3.1"><a href="#S3.1">Section 3.1 Precinct Committee Officers (PCOs)</a></h2>
<h3 id="S3.1.A"><a href="#S3.1.A">3.1.A</a></h3><p>All elected and appointed Democratic precinct committee officers who reside in the 32nd legislative district are automatically members of this organization with full voting rights.</p>
<h3 id="S3.1.B"><a href="#S3.1.B">3.1.B</a></h3><p>Acting PCOs do not have the same voting rights as elected or appointed PCOs do.</p>
<h2 id="S3.2"><a href="#S3.2">Section 3.2 General Members</a></h2>
<h3 id="S3.2.A"><a href="#S3.2.A">3.2.A</a></h3><p>Any other person registered to vote in the 32nd legislative district, who publicly identifies as a Democrat, may become a General Member of this organization upon payment of dues, or upon waiver of dues by the District Chair.</p>
<h3 id="S3.2.B"><a href="#S3.2.B">3.2.B</a></h3><p>General Members shall be eligible to vote and run for office as specified by these bylaws 28 days after payment or waiver of dues; provided, however, that those who were members during the previous year shall be eligible to vote immediately upon payment of dues during the first quarter of the current year.</p>
<h3 id="S3.2.C"><a href="#S3.2.C">3.2.C</a></h3><p>All elected officials serving in the Washington State Legislature, or the United States Congress, or in any statewide office in the state of Washington, who reside in the 32nd legislative district and identify as Democrats, shall be General Members of this organization.</p>
<h2 id="S3.3"><a href="#S3.3">Section 3.3 Associate Members</a></h2>
<p>Associate Members are persons who pay dues but are not eligible for General Membership. Upon payment of dues, they shall be given the privilege of participating in debate, serving on committees, and receiving the District Newsletter. They shall not have the right to run for District Office, serve on the Executive Board, make or second motions, nor to vote.</p>
<h2 id="S3.4"><a href="#S3.4">Section 3.4 Dues</a></h2>
<p>Annual dues shall cover a calendar year and shall include a limited-income dues structure to be prescribed by a Standing Rule.</p>
<h1 id="S4"><a href="#S4">Article 4 – Officers, Executive Board, and Committees</a></h1>
<h2 id="S4.1"><a href="#S4.1">Section 4.1 Elected Officers</a></h2>
<h3 id="S4.1.A"><a href="#S4.1.A">4.1.A</a></h3><p>These officers are to be elected by PCOs only. The following officers must either be elected or appointed PCOs residing in their service precinct or have previously been a Democratic PCO for at least the immediately preceding 24 months. These requirements may, however, be waived by a 3/4 vote of the PCOs present and voting.</p>
<ul>
<li>Chair</li>
<li>1st Vice Chair, to reside in a different county from the Chair</li>
<li>2nd Vice Chair</li>
<li>Two Representatives to the KCDCC Executive Board who are of different genders</li>
<li>Two Representatives to the SCDCC Executive Board who are of different genders</li>
<li>Two KCDCC Executive Board Alternates who are of different genders</li>
<li>Two SCDCC Executive Board Alternates who are of different genders</li>
</ul>
<h3 id="S4.1.B"><a href="#S4.1.B">4.1.B</a></h3><p>These officers are to be elected by PCOs only. All members are eligible to be elected to the following District offices:</p>
<p>Two state committee members who are of different genders</p>
<h3 id="S4.1.C"><a href="#S4.1.C">4.1.C</a></h3><p>These officers are to be elected by PCOs and General Members. All members are eligible to be elected to the following District Offices:</p>
<ul>
<li>Secretary</li>
<li>Treasurer</li>
</ul>
<h3 id="S4.1.D"><a href="#S4.1.D">4.1.D</a></h3><p>The following officers are to be elected by an organization:</p>
<p>May Arkwright Hutton Chapter, Washington State Federation of Democratic Women</p>
<h2 id="S4.2"><a href="#S4.2">Section 4.2 Appointed Officers</a></h2>
<p>The District Chair may appoint the following officers, subject to ratification by a majority vote of the members present and voting at a District reorganization or General Membership meeting:</p>
<ul>
<li>Chairs of standing committees, who shall be voting members of the Executive Board</li>
<li>A Sergeant at Arms, who shall be an ex officio member of the Executive Board</li>
<li>A Parliamentarian, who shall be an ex officio member of the Executive Board</li>
</ul>
<h2 id="S4.3"><a href="#S4.3">Section 4.3 Duties and Responsibilities</a></h2>
<h3 id="S4.3.A"><a href="#S4.3.A">4.3.A</a></h3><p>The Chair and representatives to the state and county Democratic Party organizations may be instructed by a 2/3 majority of members present and voting as to how they are to vote at meetings of those bodies, and then must vote accordingly.</p>
<h3 id="S4.3.B"><a href="#S4.3.B">4.3.B</a></h3><p>A list of duties and responsibilities for each of the District Officers listed in Sections 4.1 and 4.2 of this Article shall be adopted and periodically reviewed by the Executive Board. A copy of this list shall be maintained on the District’s website. The Secretary shall make available a current copy of this list of Duties and Responsibilities to any member of the organization, upon request.</p>
<h2 id="S4.4"><a href="#S4.4">Section 4.4 Executive Board</a></h2>
<h3 id="S4.4.A"><a href="#S4.4.A">4.4.A</a></h3><p>The voting members of the Executive Board shall consist of the officers specified in Sections 4.1 and 4.2 of this Article, as well as any KCDCC, SCDCC, Washington State Democratic Central Committee (WSDCC) or Democratic National Committee (DNC) Officer residing in the 32nd legislative district. The principle of “one person – one vote” shall pertain.</p>
<h3 id="S4.4.B"><a href="#S4.4.B">4.4.B</a></h3><p>The Parliamentarian and Sergeant-at-Arms are ex officio members of the Executive Board, with voice but not vote, nor the right to make or second motions during Executive Board meetings.</p>
<h3 id="S4.4.C"><a href="#S4.4.C">4.4.C</a></h3><p>The Executive Board shall meet once a month at the Call of the Chair. Any member of this organization may attend, but participation in discussion will only be with the consent of a majority of the voting members of the Board present. When possible, notice of the meeting shall be published in the District Newsletter.</p>
<h3 id="S4.4.D"><a href="#S4.4.D">4.4.D</a></h3><p>A quorum of the Executive Board shall consist of seven of the officers specified in Sections 4.1 and 4.2 of this Article.</p>
<h3 id="S4.4.E"><a href="#S4.4.E">4.4.E</a></h3><p>A vote of the Executive Board by email or phone may be taken for urgent issues. A majority of all voting members of the Executive Board is required for an email/phone vote to pass. The Secretary will record this vote in the next Executive Board Meeting Minutes.</p>
<h2 id="S4.5"><a href="#S4.5">Section 4.5. Vacancies</a></h2>
<h3 id="S4.5.A"><a href="#S4.5.A">4.5.A</a></h3><p>If any member of the Executive Board fails to attend three consecutive meetings of the Executive Board without previously giving notice of intended absence to the Chair or to the Secretary, that Office may be declared vacant by majority vote of the Executive Board, in which case the Chair shall either call for an election or make a replacement appointment to fill the vacancy, in accordance with the provisions of these Bylaws and Standing Rules.</p>
<h3 id="S4.5.B"><a href="#S4.5.B">4.5.B</a></h3><p>In the event of a vacancy in any elected Executive Board position because of resignation, removal, death, or any other cause, the vacancy shall be filled following the procedures of Article 5.</p>
<h2 id="S4.6"><a href="#S4.6">Section 4.6 Committees</a></h2>
<h3 id="S4.6.A"><a href="#S4.6.A">4.6.A</a></h3><p>The Chair shall serve as an ex officio voting member of all committees, including the following standing committees:</p>
<ul>
<li>Membership</li>
<li>Programs</li>
<li>Legislative Action</li>
<li>District Campaigns</li>
<li>Communications</li>
<li>Community Service</li>
<li>Fundraising</li>
<li>Diversity & Inclusion</li>
<li>Young Democrats</li>
<li>Resolutions</li>
<li>Technology</li>
</ul>
<h3 id="S4.6.B"><a href="#S4.6.B">4.6.B</a></h3><p>The Chair or the Executive Board may establish additional (“Ad hoc”) committees to carry out such duties and have such powers as the Chair or the Executive Board may establish.</p>
<h1 id="S5"><a href="#S5">Article 5 – Election and Ratification of Officers</a></h1>
<h2 id="S5.1"><a href="#S5.1">Section 5.1 Notice</a></h2>
<p>All members shall be notified in writing or electronically by e-mail at least ten days in advance of any meeting at which officers will be elected or ratified.</p>
<h2 id="S5.2"><a href="#S5.2">Section 5.2 Nominations</a></h2>
<p>Nominations and seconds will be made from the floor; only voting members may participate.</p>
<h2 id="S5.3"><a href="#S5.3">Section 5.3 Signed Ballot</a></h2>
<p>Voting for officers specified in Article 4 Section 4.1.A, B, and C, shall be by signed ballot unless by acclamation. The ballots shall be a matter of public record for a minimum of 30 days.</p>
<h2 id="S5.4"><a href="#S5.4">Section 5.4 Order of Election</a></h2>
<p>The nominations and election for each office shall be held in the sequence specified in Article 4, Section 4.1, followed by ratification votes for those offices listed in Article 4, Section 4.2.</p>
<h2 id="S5.5"><a href="#S5.5">Section 5.5 Establishing a Majority</a></h2>
<p>Elections shall be decided by a majority of those eligible to vote who are present and voting. If no candidate has a majority on the first ballot, the candidate with the fewest votes will be dropped. The same rule shall apply on each succeeding ballot until a candidate receives a majority.</p>
<h2 id="S5.6"><a href="#S5.6">Section 5.6 Ratification</a></h2>
<p>Officers subject to ratification shall be ratified by a majority of those eligible to vote who are present and voting.</p>
<h2 id="S5.7"><a href="#S5.7">Section 5.7 Additional Procedures</a></h2>
<p>Additional procedures are provided in “Standing Rule 1 – Election Procedures.”</p>
<h1 id="S6"><a href="#S6">Article 6 – Removal of Officers</a></h1>
<h2 id="S6.1"><a href="#S6.1">Section 6.1 Starting the Process</a></h2>
<h3 id="S6.1.A"><a href="#S6.1.A">6.1.A</a></h3><p>The first step in the process of removing a District Officer is to make a Motion for Removal at a District General-Membership meeting. No debate or action on the motion will take place at that meeting, but at the following regular General-Membership meeting, the Motion for Removal shall be placed third on the agenda, after the Call to Order and the Pledge of Allegiance.</p>
<h3 id="S6.1.B"><a href="#S6.1.B">6.1.B</a></h3><p>The Motion of Removal must include a written Removal Petition, naming the person to be removed and the precise reason(s) for seeking the removal and bearing the signatures of at least 10% of the current PCOs for offices listed in Article 4, Sections 4.1.A and 4.1.B or 10% of the current membership for offices listed in Article 4, Sections 4.1.C and 4.2. The petition must be filed with the Chair or, if the Chair is the subject of the removal, with the 1st Vice Chair or the Secretary.</p>
<h3 id="S6.1.C"><a href="#S6.1.C">6.1.C</a></h3><p>The Chair shall appoint someone to verify the signatures on the petition, unless the Chair is the subject of the removal. If the Chair is the subject, the Chair and the 1st Vice Chair shall jointly appoint someone to do the verification.</p>
<h2 id="S6.2"><a href="#S6.2">Section 6.2 Debate and Removal</a></h2>
<h3 id="S6.2.A"><a href="#S6.2.A">6.2.A</a></h3><p>The District Chair shall preside over the debate unless the Chair is the subject of the motion, in which case the 1st Vice Chair shall preside. The Chair presiding over the meeting during the removal debate and vote shall appoint a credentials chair, a timekeeper, and a tally committee.</p>
<h3 id="S6.2.B"><a href="#S6.2.B">6.2.B</a></h3><p>The pros and cons shall have up to five minutes per side to debate the motion. The time and speakers shall be allocated by the maker of the motion for the pro side and by the subject of the removal on the con side.</p>
<h3 id="S6.2.C"><a href="#S6.2.C">6.2.C</a></h3><p>Questions from the floor will be allowed as provided in the Standing Rules.</p>
<h3 id="S6.2.D"><a href="#S6.2.D">6.2.D</a></h3><p>Before the vote for removal is taken, the credentials chair will report the total number of eligible voting members present. Voting shall be by signed ballot for offices listed in Article 4, Sections 4.1.A and 4.1.B; otherwise unsigned written ballots may be used unless 20% of those present and eligible to vote request a signed ballot. The ballots are to be retained for a minimum of 30 days and are to be open to inspection by the Democratic PCOs of the 32nd legislative district.</p>
<h3 id="S6.2.E"><a href="#S6.2.E">6.2.E</a></h3><p>A vote of 2/3 of the eligible members present and voting for the office is required to pass a Motion for Removal.</p>
<h1 id="S7"><a href="#S7">Article 7 – Meetings</a></h1>
<h2 id="S7.1"><a href="#S7.1">Section 7.1 Regular Meetings</a></h2>
<h3 id="S7.1.A"><a href="#S7.1.A">7.1.A</a></h3><p>The Executive Board shall fix the time and place for a regular monthly District general meeting, which shall be publicized in the District’s newsletter.</p>
<h3 id="S7.1.B"><a href="#S7.1.B">7.1.B</a></h3><p>A scheduled meeting may be cancelled by a majority vote of members present and voting, or by vote of the Executive Board under special circumstances.</p>
<h3 id="S7.1.C"><a href="#S7.1.C">7.1.C</a></h3><p>The agenda of the regular meeting may consist of, but not be limited to, the following:</p>
<p>Call to Order</p>
<p>Pledge of Allegiance</p>
<p>Reports</p>
<p>Unfinished Business</p>
<p>New Business</p>
<p>Good of the Order</p>
<p>Adjournment</p>
<h2 id="S7.2"><a href="#S7.2">Section 7.2 Special Meetings</a></h2>
<p>Special meetings may be called by a majority of the Executive Board present and voting, or by written petition of 10% of all PCOs. Members shall be notified by mail or e-mail, at least ten days prior to the meeting, as to its date, time, place, and purpose.</p>
<h2 id="S7.3"><a href="#S7.3">Section 7.3 Quorum</a></h2>
<p>At all regular or special meetings, 20% of all members, or 20 members, whichever is less, shall constitute a quorum. For votes limited to PCOs, 20% of all PCOs, or 20 PCOs, whichever is less, shall constitute a quorum.</p>
<h2 id="S7.4"><a href="#S7.4">Section 7.4 Proxies</a></h2>
<p>Proxies shall not be valid.</p>
<h2 id="S7.5"><a href="#S7.5">Section 7.5 Debatable Motions</a></h2>
<p>The following motions are debatable to the extent that the maker of the motion and one opposing speaker must be given the opportunity to speak on the motion, with the maker of the motion having the choice of opening or closing debate:</p>
<p>Limit debate</p>
<p>Extend debate</p>
<p>Table (Lay on the Table)</p>
<p>Previous Question</p>
<p>Suspend the Rules</p>
<h2 id="S7.6"><a href="#S7.6">Section 7.6 Questions and Feedback</a></h2>
<h3 id="S7.6.A"><a href="#S7.6.A">7.6.A</a></h3><p>Following each address by an invited speaker, members shall have an opportunity to ask questions and to rebut statements by the invited speaker or other persons present and, to the extent practicable, to express their views. Any speaker or candidate wishing to speak shall be</p>
<p>apprised of this Section by a member of the Executive Board.</p>
<h3 id="S7.6.B"><a href="#S7.6.B">7.6.B</a></h3><p>Members shall have an opportunity to question any candidate for election to public office or Party office who is seeking endorsement or election from our District.</p>
<h1 id="S8"><a href="#S8">Article 8 – Precinct Committee Officers</a></h1>
<h2 id="S8.1"><a href="#S8.1">Section 8.1 Application</a></h2>
<p>Each candidate for an Appointed or Acting PCO position must present to the Chair an application in support of their appointment. When more than one application for the same precinct position is submitted in any District meeting, they shall be voted on at the same time.</p>
<h2 id="S8.2"><a href="#S8.2">Section 8.2 Approval</a></h2>
<p>Following approval, by a majority of the PCOs present and voting at a meeting, of any application for appointment to fill a vacant precinct as an Appointed or Acting PCO, the Chair shall submit the application to the County Chair.</p>
<h2 id="S8.3"><a href="#S8.3">Section 8.3 Special Situations</a></h2>
<p>The organization may, by a 2/3 vote of PCOs present and voting at a regular meeting, temporarily delegate to the Executive Board for the following month its power of approving applications to fill vacant PCO positions. Such delegation may be appropriate for situations subject to precinct-caucus deadlines.</p>
<h2 id="S8.4"><a href="#S8.4">Section 8.4 Public Knowledge</a></h2>
<h3 id="S8.4.A"><a href="#S8.4.A">8.4.A</a></h3><p>The identity and contact information for all PCOs and other officers of the organization shall be public information.</p>
<h3 id="S8.4.B"><a href="#S8.4.B">8.4.B</a></h3><p>The organization shall create an email or email alias for any PCO or officer who requests one.</p>
<h1 id="S9"><a href="#S9">Article 9 – Endorsements</a></h1>
<h2 id="S9.1"><a href="#S9.1">Section 9.1 Endorsement Procedures</a></h2>
<p>Endorsement procedures not specified by Standing Rules shall be approved by a majority vote of Members present and voting, all members having been notified in writing at least ten days in advance of the meeting. An endorsement shall require 60% of Members present and voting.</p>
<h2 id="S9.2"><a href="#S9.2">Section 9.2 Early Endorsement</a></h2>
<p>No endorsement of a candidate shall occur before the close of candidate filing, unless notice has been provided in the District Newsletter before the General-Membership meeting or by first-class mail or e-mail sent at least ten days before the meeting when such an endorsement is proposed. If the meeting is held before the close of filing, a 2/3 vote of the members present and voting shall be required for endorsement.</p>
<h2 id="S9.3"><a href="#S9.3">Section 9.3 Financial Support</a></h2>
<p>This organization may give financial support to candidates, propositions, and ballot issues only if they have been endorsed by the organization.</p>
<h1 id="S10"><a href="#S10">Article 10 – Finances</a></h1>
<h2 id="S10.1"><a href="#S10.1">Section 10.1 Disbursements</a></h2>
<p>All disbursements of funds shall be made by check, debit/credit card, or electronic transfer. The Treasurer must be informed of all disbursements not made under their signature within 24 hours, and the Treasurer may not disburse funds to themselves. The books shall be independently examined prior to reorganization and following a change in the office of Treasurer.</p>
<h2 id="S10.2"><a href="#S10.2">Section 10.2 General Operations</a></h2>
<h3 id="S10.2.A"><a href="#S10.2.A">10.2.A</a></h3><p>The Executive Board shall prepare an annual operating budget in time for its adoption by the membership at the March General-Membership meeting. Any proposed expenditure of $100 or more above the total amount budgeted for a line item in the adopted budget must be presented to the membership before the expenditure is made. All other expenditures from this budget only require approval of a majority of the Executive Board and a report to the membership. Unbudgeted expenditures must be approved by a majority of the District members present and voting at a regular meeting.</p>
<h3 id="S10.2.B"><a href="#S10.2.B">10.2.B</a></h3><p>This is an exempt account, not subject to Public Disclosure Commission spending limits.</p>
<h2 id="S10.3"><a href="#S10.3">Section 10.3 Campaign Fund</a></h2>
<h3 id="S10.3.A"><a href="#S10.3.A">10.3.A</a></h3><p>Each disbursement on behalf of a political campaign must be approved by a majority of the members present and voting at a General-Membership meeting, with the following exception: During the interval between the last General-Membership meeting scheduled before a Primary or General Election and the election itself, the Executive Board can authorize the disbursement of funds by a majority of its members present and voting.</p>
<h3 id="S10.3.B"><a href="#S10.3.B">10.3.B</a></h3><p>Checks for the purposes set forth in Section 9.3 must be signed by two of the persons authorized to sign for this account.</p>
<h3 id="S10.3.C"><a href="#S10.3.C">10.3.C</a></h3><p>This is a non-exempt account, subject to Public Disclosure Commission spending limits.</p>
<h1 id="S11"><a href="#S11">Article 11 – Amending the Bylaws</a></h1>
<h2 id="S11.1"><a href="#S11.1">Section 11.1 Notice</a></h2>
<p>Members must be given written notice of proposed Bylaws changes at least ten days in advance of the meeting at which changes are to be voted upon.</p>
<h2 id="S11.2"><a href="#S11.2">Section 11.2 Limitations</a></h2>
<p>Bylaws not controlled by KCDCC, SCDCC, state or national party charters and/or bylaws, or applicable state laws, may be amended by 2/3 vote of the PCOs present and voting at a meeting designated for considering the proposed changes.</p>
<h1 id="S12"><a href="#S12">Article 12 – Written Notice</a></h1>
<p>An announcement clearly placed in the District Newsletter or via email to those members with email addresses on file, and via USPS to those members without an email address on file, will be considered to be written notice for the purpose of these Bylaws. The date of emailing, or of USPS mailing, counts as the first day of the required advance-notification period.</p>
<h1 id="S13"><a href="#S13">Article 13 – Voting</a></h1>
<h2 id="S13.1"><a href="#S13.1">Section 13.1 Method</a></h2>
<p>All voting – general meetings, special meetings, committee meetings, Executive Board meetings, or any other meeting – shall be by a show of hands or raised credential unless a written ballot is called for.</p>
<h2 id="S13.2"><a href="#S13.2">Section 13.2 Written Ballots</a></h2>
<p>If written ballots are used, each person who votes must sign and print their name legibly on the ballot. Any ballot without this endorsement by the voter will be invalid and will not be counted in the tally.</p>
<h2 id="S13.3"><a href="#S13.3">Section 13.3 Secret Votes</a></h2>
<p>There shall be no secret votes cast by any means, by any person, for any purpose whatsoever.</p>
<h1 id="S14"><a href="#S14">Article 14 – Parliamentary Authority</a></h1>
<p>The affairs and meetings of this organization shall, except as specifically provided by its Bylaws or Standing Rules, or by bylaws or rules of the KCDCC or SCDCC, be governed by the latest edition of Robert’s Rules of Order, Newly Revised.</p>
</div>
</Layout>
)
