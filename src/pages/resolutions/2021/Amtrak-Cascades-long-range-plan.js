import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'Amtrak-Cascades-long-range-plan'
const title = `Fund and Prioritize Amtrak Cascades Long Range Plan`
const title_short = `Amtrak Cascades`
const minutes_date = '2021-01-13'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 1,
	organization: '32LDD',
	title_short,
	wsdcc_action: wsdcc_action.passed,
	wsdcc_issue: wsdcc_issue.transportation,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p><strong>WHEREAS</strong> the United Nations Intergovernmental Panel on Climate Change states that it is imperative that we urgently reduce greenhouse gas emissions by approximately half of 2010 levels by the climate tipping point of 2030 in order to preserve a livable climate, and the Washington State Legislature and Governor have set forth aggressive climate emissions reduction and environmental justice goals, and highway vehicles are the largest contributor of greenhouse gas emissions in Washington State; and</p>
<p><strong>WHEREAS</strong> Inter-regional rail (or conventional, or long-distance rail – herein called “rail”) is a powerful climate solution because rail travel produces two thirds fewer climate emissions than roadway travel, and rail utilizes two thirds less of any kind of fuel than roadway travel, and rail can be immediately expanded and improved incrementally thereby meeting climate goals and environmental justice goals; and</p>
<p><strong>WHEREAS</strong> RCW 47.79 is an unfunded mandate for completion of the Amtrak Cascades Long Range Plan which would expand and improve Amtrak service to be faster and more frequent, and only 10 percent of the plan has been implemented since 1993, in part due to Federal support that has been substantially less than anticipated under the Swift Rail Development Act of 1994; and</p>
<p><strong>WHEREAS</strong> Rail expansion can be implemented immediately and incrementally with improvements to existing infrastructure and service, and each incremental improvement directly reduces emissions from transportation, serves local communities, and addresses environmental justice; and</p>
<p><strong>WHEREAS </strong>proposed Ultra High Speed Ground Transportation (UHSGT) will not be operational for at least a decade past the climate tipping point of 2030, thereby supplying no emissions reduction in the urgent short term due to the long lead time and construction time, and UHSGT in the Cascades corridor will cost several times more than the cost of the completion of the Amtrak Cascades Long Range Plan;</p>
<p><strong>THEREFORE BE IT RESOLVED</strong> That funding be immediately allocated to resume completion of the Amtrak Cascades Long Range Plan in accordance with RCW 47.79, as an urgent transportation priority and in accordance with climate goals and environmental justice goals;</p>
<p><strong>THEREFORE BE IT FINALLY RESOLVED</strong> that this resolution will be sent to Washington State Department of Transportation, Governor Inslee, the Washington Legislature, and the Washington Congressional Delegation.</p>
<hr/>
<p>Presented by <a href="mailto:laelcwhite@gmail.com" target="_blank" rel="noopener noreferrer">Lael White</a> and <a href="mailto:taw@vtd.net" target="_blank" rel="noopener noreferrer">Thomas White</a>.</p>
<p>Submitted by Washington State Democrats Environment and Climate Caucus members, Lael White laelcwhite@gmail.com and Thomas White taw@vtd.net (32<sup>nd</sup> LD ECC) on 12/27/20.</p>
<p>References: Intergovernmental Panel on Climate Change: https://www.ipcc.ch/sr15/https://www.ipcc.ch/sr15/</p>
<p>RCW 47.79: https://apps.leg.wa.gov/Rcw/default.aspx?cite=47.79&amp;full=true</p>
<p>Swift Rail Development Act of 1994: https://www.congress.gov/bill/103rd-congress/house-bill/4867</p>
</Layout>
)
export {render as default}
