import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import {base} from '../../config'
import Layout from '../../components/layout'
import {LinkResolutionDetail, resolution_status} from '../../components/resolution'

import {r as amtrak_cascades} from './2021/Amtrak-Cascades-long-range-plan.js'
import {r as r_90} from './2020/R-90'
import {r as scrap_the_cap} from './2020/scrap-the-cap-social-security-payroll-deductions'
import {r as ahmaud_arbery} from './2020/ahmaud-arbery'
import {r as i_1776} from './2020/I-1776'
import {r as water_affordability} from './2020/water-affordability'
import {r as tax_big} from './2020/tax-big-businesses-fund-green-housing-seattle'
import {r as national_infrastructure_bank} from './2020/national-infrastructure-bank'
import {r as aid_low_income_lynnwood_residents} from './2020/aid-low-income-lynnwood-residents'
import {r as israel_palestine_peace} from './2020/israel-palestine-peace'
import {r as save_shoreline_trees} from './2020/save-shoreline-trees'
import {r as protect_orcas} from './2020/protect-orcas-curtailing-suction-dredge-mining'
import {r as safeguards_SB_5604} from './2020/safeguards-to-guardianship-law-SB-5604'
import {r as wild_wallace_forest_reconveyance} from './2020/wild-wallace-forest-reconveyance'
import {r as defund_the_seattle_police} from './2020/defund-the-seattle-police'
const resolutions = [
	amtrak_cascades,
	scrap_the_cap,
	r_90,
	defund_the_seattle_police,
	wild_wallace_forest_reconveyance,
	ahmaud_arbery,
	i_1776,
	water_affordability,
	tax_big,
	national_infrastructure_bank,
	aid_low_income_lynnwood_residents,
	israel_palestine_peace,
	save_shoreline_trees,
	protect_orcas,
	safeguards_SB_5604,
]

export const proposed = resolutions.filter(r=>r.status === resolution_status.proposed)
const adopted = resolutions.filter(r=>r.status === resolution_status.adopted)

const title = 'Resolutions'
export default () => {
	return (
	<Layout>
	<Helmet>
		<title>{title}</title>
	</Helmet>
	<div className="h">
		<h1 className="inline">{title}</h1>
		<Link to="/resolutions/help" className="status hstatus">Help</Link>
	</div>
	<div className="menu">
	{proposed.length ? <>
		<h2>Proposed ({proposed.length})</h2>
		<ul className="menu">
			{proposed.map(r=>(
				<li><LinkResolutionDetail r={r}/></li>
			))}
		</ul>
	</> : null}
	<h2>
		Adopted
	</h2>
	<ul className="menu">
		{adopted.map(r=>(
			<li><LinkResolutionDetail r={r}/></li>
		))}
		<li><Link to="/resolutions/2019/Zero-emission-vehicles">2019 - 14 - Resolution Urging WA Adoption of the Zero Emissions Vehicle (ZEV) Program</Link></li>
		<li><Link to="/resolutions/2019/Seattle-needs-rent-control">2019 - 13 - Seattle Needs Rent Control</Link></li>
		<li><Link to="/resolutions/2019/Tree-Urban-Forest-Protection">2019 - 12 - Resolution in Support of the Seattle Urban Forestry Commission’s Draft Tree and Urban Forest Protection Ordinance</Link></li>
		<li><Link to="/resolutions/2019/Do-No-Harm-Act">2019 - 11 - Resolution Urging Enactment Of The “Do No Harm Act”</Link></li>
		<li><Link to="/resolutions/2019/Collective-bargaining-rights-for-educators">2019 - 10 - Supporting collective bargaining rights for educators</Link></li>
		<li><Link to="/resolutions/2019/Prioritize-development-of-conventional-rail">2019 - 09 - Prioritize development of conventional rail</Link></li>
		<li><Link to="/resolutions/2019/Opposing-new-and-expanded-gas-infrastructure">2019 - 08 - Resolution opposing new and expanded gas infrastructure in Washington state</Link></li>
		<li><Link to="/resolutions/2019/Concurrent-construction-light-rail">2019 - 07 - Support for concurrent construction of the NE 130th Street light rail station and Lynnwood Link Extension</Link></li>
		<li><Link to="/resolutions/2019/No-I-976">2019 - 06 - Resolution opposing Initiative Measure No. 976</Link></li>
		<li><Link to="/resolutions/2019/Raising-awareness-of-missing-and-murdered-indigenous-peoples">2019 - 05 - Raising awareness of missing and murdered indigenous peoples (Native)</Link></li>
		<li><Link to="/resolutions/2019/Whole-Washington-health-trust">2019 - 04 - Whole Washington health trust</Link></li>
		<li><Link to="/resolutions/2019/Presidential-candidate-tax-returns">2019 - 03 - Supporting a legislative requirement that presidential and vice presidential candidates disclose eight years of tax returns in order to appear on Washington’s primary election ballot</Link></li>
		<li><Link to="/resolutions/2019/I-1000">2019 - 02 - In support of I-1000</Link></li>
		<li><Link to="/resolutions/2019/Green-new-deal-and-committee">2019 - 01 - Supporting the plan for a green new deal & creation of the select committee for the green new deal</Link></li>
		<li><Link to="/resolutions/2018/Keep-Washington-Working-Act">2018 - 16 - In support of Keep Washington Working Act (SB 5689)</Link></li>
		<li><Link to="/resolutions/2018/Reject-corporate-PAC-money-from-the-fossil-fuel-industry">2018 - 15 - Reject corporate PAC money from the fossil fuel industry</Link></li>
		<li><Link to="/resolutions/2018/Yes-1639-Safe-Schools-Safe-Communities">2018 - 14 - In Support of Yes on 1639, Safe Schools, Safe Communities</Link></li>
		<li><Link to="/resolutions/2018/I-1644-and-I-981">2018 - 13 - Endorsing Initiative 1644 to the people and Initiative 981 to the legislature</Link></li>
		<li><Link to="/resolutions/2018/Protect-and-restore-Snake-River-salmon-and-southern-resident-orcas">2018 - 12 - Protect and restore Snake River salmon and southern resident orcas</Link></li>
		<li><Link to="/resolutions/2018/State-universal-healthcare-I-1600">2018 - 11 - Supporting for state universal healthcare (I-1600)</Link></li>
		<li><Link to="/resolutions/2018/Employee-hours-tax">2018 - 10 - Employee hours tax</Link></li>
		<li><Link to="/resolutions/2018/Alternatives-to-guardianship">2018 - 09 - Urging alternatives to guardianship</Link></li>
		<li><Link to="/resolutions/2018/Gun-reform-to-address-mass-shootings">2018 - 08 - Gun reform to address mass shootings</Link></li>
		<li><Link to="/resolutions/2018/Statewide-transition-to-electric-powered-motor-vehicles-beginning-in-2030">2018 - 07 - Statewide transition to electric-powered motor vehicles beginning in 2030</Link></li>
		<li><Link to="/resolutions/2018/Call-for-resignation-of-KCD-chair">2018 - 06 - Call for resignation of KCD chair</Link></li>
		<li><Link to="/resolutions/2018/Safe-consumption-sites">2018 - 05 - Safe consumption sites in Snohomish County and all municipalities</Link></li>
		<li><Link to="/resolutions/2018/Responsible-gun-legislation">2018 - 04 - Responsible gun legislation</Link></li>
		<li><Link to="/resolutions/2018/Opposition-to-PSE-liquefied-natural-gas-plant-on-the-Tacoma-tide-flats">2018 - 03 - Opposition to PSE liquefied natural gas plant on the Tacoma tide flats</Link></li>
		<li><Link to="/resolutions/2018/Ban-source-of-income-discrimination-in-rental-housing">2018 - 02 - Ban source-of-income discrimination in rental housing</Link></li>
		<li><Link to="/resolutions/2018/Single-payer-health-care">2018 - 01 - Urging our U.S. senators to co-sponsor S.1804, for single-payer health care</Link></li>
	</ul>
	<h2 className="flex">
		<a href={base+"/resolutions/"}>More Resolutions</a>
	</h2>
	</div>
</Layout>
)
}