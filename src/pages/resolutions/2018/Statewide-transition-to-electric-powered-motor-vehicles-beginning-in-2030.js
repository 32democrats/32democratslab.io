import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		Resolution 2018 - 07 - In support of a statewide transition to electric-powered motor vehicles, beginning in 2030
	</title>
</Helmet>
<p>
	Resolution 2018 - 07
</p>
<h1>
In support of a statewide transition to electric-powered motor vehicles, beginning in 2030
</h1>
<p>
<b>WHEREAS</b> various nations have already committed to prohibiting the sale of new vehicles powered by gasoline or diesel internal-combustion engines (the Netherlands, beginning in 2025; Norway and India in 2030; the United Kingdom and France in 2040, and China at a date to be determined); and
</p>
<p>
<b>WHEREAS</b> the cities of London, Paris, Los Angeles, Barcelona, Quito, Vancouver, Mexico City, Cape Town, Milan, Auckland, Seattle, and Copenhagen have signed the C40 Cities Pledge that “a major area” of their cities will be “zero emission” by 2030, as a key step toward fossil-fuel-free streets generally; and
</p>
<p>
<b>WHEREAS</b> Volvo's new vehicle models will be fully or partially battery-powered starting in 2019; General Motors will be introducing at least 20 new electric vehicle models by 2023; and Ford is planning for 70% of its vehicles sold in China to be hybrid or electric by 2025; and
</p>
<p>
<b>WHEREAS</b>, in the face of the above trends, a failure to effect meaningful change in both U.S. automotive production and our domestic market would imperil the United States' competitive position worldwide; and
</p>
<p>
<b>WHEREAS</b> gasoline- and diesel-powered vehicles produced 55.9% of Washington's greenhouse gas emissions in 2015, according to the U.S. Energy Information Agency, and cause 53,000 premature deaths nationwide every year, according to an M.I.T. study;
</p>
<p>
<b>WHEREAS</b> Oregon, California and eight other states, representing 28% of the U.S. automobile market, have embarked on a multi-faceted joint effort based on California's ongoing low-emission vehicle program, to transform their own transportation sectors by incentivizing consumer choices of zero-emission vehicles and assuring the infrastructure necessary therefor, in order to attain an overall goal of 3.3 million zero-emission vehicles (including 15% of all new vehicles sold) on their roadways by 2025;
</p>
<p>
<b>THEREFORE, BE IT RESOLVED</b> that we urge our Governor to enroll Washington in the multi-state, California-based October 24, 2013 Memorandum of Understanding regarding state “Zero-Emission” Vehicle Programs, thereby signaling our commitment to its 11-step “Action Plan” for transitioning to zero-emission vehicles on Washington roadways; and
</p>
<p>
<b>BE IT FURTHER RESOLVED</b> that we urge our State Legislature to promptly enact legislation that will:
</p>
<ul><li>prohibit, starting with model-year 2030 vehicles, registration of any new passenger vehicles fueled solely by gasoline or diesel;
</li><li>provide for the rapid construction of fast-charging infrastructure for electric vehicles on the major highway corridors in the state;
</li><li>enable affordable access to electric-vehicle charging infrastructure in multi-unit dwellings and rural and low-income areas;
</li><li>provide financial incentives for low-income residents to purchase or lease electric vehicles; and
</li><li>facilitate job retraining programs for workers displaced by the foregoing transition; and
</li></ul>
<p>
<b>BE IT FURTHER RESOLVED</b> that we urge the Washington State Legislature to enact such further legislation, and to facilitate regulations as necessary and appropriate, to move our state toward 100% non-gasoline- and non-diesel-powered passenger vehicles by 2040, and
</p>
<p>
<b>BE IT FINALLY RESOLVED</b> that this resolution be sent forthwith to each of our State Senators and Representatives, and to Governor Jay Inslee.
</p>
<hr/>
<p>
Adopted March 14, 2018 by the 32nd Legislative District Democratic Organization; Carin Chase, Chair.
</p>
<p>
Originated by Arvia Morris and Jeremy Erdman of the 43rd District Democrats and edited by Dean Fournier of the 32nd District Democrats. Similar resolutions adopted by the 43rd and 37th District Democrats, and by the Environment and Climate Caucus of the Washington State Democrats on January 17, 2018.
</p>
</Layout>
)

export default Page
