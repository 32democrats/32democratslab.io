import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 12 - Protect and restore Snake River salmon and southern resident orcas
	</title>
</Helmet>
<p>
	2018 - 12
</p>
<h1>
Protect and restore Snake River salmon and southern resident orcas
</h1>
<p><b>WHEREAS</b> Snake River wild spring/summer chinook salmon have not met the recovery objectives established by the Northwest Power Conservation Council, with smolt-to-adult returns of only 1.1 % since the year 2000, despite $16 billion charged to ratepayers and taxpayers for salmon recovery efforts; and
</p><p><b>WHEREAS</b> the National Oceanic and Atmospheric Administration has acknowledged, in its 2017 Snake River Recovery Plan for Spring/Summer Chinook and Steelhead, that its long list of planned salmon recovery actions will not lead to their recovery, but 
<br/>(a) the Army Corps of Engineers has concluded that a breaching of the four dams on the Lower Snake River has the highest probability of meeting federal salmon recovery objectives, and
<br/>(b) results recorded by the National Marine Fisheries Service since 1999 have confirmed that breaching of those dams by itself is likely to lead to recovery of fall chinook and steelhead runs; and
</p><p><b>WHEREAS</b> chinook salmon comprise at least 80% of the diet of endangered Southern Resident Orcas – more than 50% of it from the Columbia River basin (which includes the Snake River watershed) – and the dwindling of their chinook runs has helped cause the decline of those orcas to a critically low breeding population of fewer than 30 individuals, and
</p><p><b>WHEREAS</b> the four lower Snake dams have a benefit-to-cost ratio of only 15¢ on the dollar, and are costing society thousands of jobs and millions of dollars in other potential benefits of a free-flowing river – as well as diverting funds from more reliable dams and restoration work; and
</p><p><b>WHEREAS</b> the Pacific Northwest presently has a 16% surplus of energy, with these four dams generating less than 3% of the region's power production (much of it rarely available to meet peak power demands), and the Northwest Power and Conservation Council has concluded that expected future increases in demand through at least 2030 can be met by energy efficiency and new renewable energy already in the planning stage; and
</p><p><b>WHEREAS</b> the U.S. Court of Appeals for the Ninth Circuit ruled, on April 20, 2018, that the option of breaching the four lower Snake dams must be genuinely evaluated and that, in the meantime, the dam operators must increase springtime spill to aid in salmon return; and
</p><p><b>WHEREAS</b> the Army Corps of Engineers needs no new authority to place the lower-Snake dams into a “non-operational” status, and already has a fiduciary responsibility to do so; and
</p><p><b>WHEREAS</b> breaching the dams can be financed through existing debt reduction and credit mechanisms as a fish mitigation action by the Bonneville Power Administration, making it significantly easier and less costly than originally contemplated; and
</p><p><b>WHEREAS</b> both the Southern Resident Orcas and wild Snake River Salmon are an integral part of the Pacific Northwest culture and economy, and after 30 years of failed mitigation efforts further delays will risk the extinction of both these vitally important species;
</p><p><b>THEREFORE, BE IT RESOLVED</b> that we call upon the Army Corps of Engineers to begin removal of the earthen portion of the four lower Snake dams, starting with the Lower Granite and Little Goose Dams by December 2018; and
</p><p><b>BE IT FURTHER RESOLVED</b> that we urge our federal, state, and local officials to ensure a positive transition for the communities affected by those removals.
</p><hr/><p>
Adopted June 13, 2018 by the 32nd Legislative District Democratic Organization; Carin Chase, Chair. Originated by Aaron Tam of LD48, and adopted in similar form by LD46, LD48, and the Environment and Climate Caucus, Washington State Democrats.
</p>
</Layout>
)

export default Page
