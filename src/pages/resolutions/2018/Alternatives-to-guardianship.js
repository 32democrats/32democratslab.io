import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		Resolution 2018 - 09 - Urging alternatives to guardianship
	</title>
</Helmet>
<p>
	Resolution 2018 - 09
</p>
<h1>
Urging alternatives to guardianship
</h1>
<p><strong>WHEREAS</strong> taking away the rights of a person “in order to protect that person” is seldom justice and almost always undermines the person's rights under the United States Constitution; and</p>
<p><strong>WHEREAS</strong> for a small fee paid to a court, a guardianship can be established, based on hearsay, that will nullify any or all the Constitutional rights of a supposedly Incapacitated Person (“IP”) without notice to the IP's family, friends, or even the supposed IP himself/herself; and</p>
<p><strong>WHEREAS</strong> over 8000 guardianship cases are initiated in Washington every year, a 27% increase over 2000; and</p>
<p><strong>WHEREAS</strong> a guardianship can empower a total stranger to make some or all decisions for an IP – e.g., where they live, whom they see, how they live, and how their money is spent – regardless of any pre-existing Durable Power-of-Attorney, Advance Directive, trust or trust fund, pension, or designated bank accounts or CDs set up for the IP's heirs or for their future wellbeing, even if the IP is still physically able to clearly communicate his/her wishes; and</p>
<p><strong>WHEREAS</strong> being made a ward of the state under guardianship is costly for the state and for the IP, and often unnecessary and humiliating for those who could function with minor assistance or no assistance at all; and</p>
<p><strong>WHEREAS</strong> even a person accused of a crime has a right to a jury trial before having his/her rights removed, but an allegedly Incapacitated Person is accorded no semblance of such a right when being forced into a guardianship; and</p>
<p><strong>WHEREAS</strong> the Constitution guarantees the right to vote and the unalienable rights of Liberty and the Pursuit of Happiness to all in the way of their own choosing, but these rights are often removed under guardianship; and</p>
<p><strong>WHEREAS</strong> removal of a person's Constitutional rights should be a last resort, not the only option; and</p>
<p><strong>WHEREAS</strong> currently any elderly or disabled person under disputed care has no other choice but to become a ward of the state under guardianship; and</p>
<p><strong>WHEREAS</strong> established family caregivers may be forced to divert care-giving time to trying to learn legal issues to become a guardian, when a simple Power-of-Attorney would have sufficed; and</p>
<p><strong>WHEREAS</strong> a professional guardian, once in place, can hire lawyers to fight caring family members who oppose the guardianship, and can then pass the costs on to the IP; and</p>
<p><strong>WHEREAS</strong> RCW 11.92.190 (prohibiting detention of persons in residential placement facilities against their will), and other rights and protections available to persons not under guardianship, are often violated under guardianship;</p>
<p><strong>THEREFORE, BE IT RESOLVED</strong> that we urge our Legislators to create alternatives to guardianship, such as a court-issued Power-of-Attorney for relatives involved in caring for the IP, Assisted Decision-Making programs, authorized bill payers, and limited interference protection for the disabled and aged; and</p>
<p><strong>BE IT FURTHER RESOLVED</strong> that guardianship and other options that remove the Constitutional rights of an individual because of disability or age be considered only as a last resort, after other reasonable options have been tried and failed, rather than the first or only option, or the norm; and</p>
<p><strong>BE IT FINALLY RESOLVED</strong> that this resolution be communicated to the 2018 King County Democratic Convention and Washington State Democratic Convention, and to our State Legislators and the Governor's office.</p>
<hr/><p>Adopted April 11, 2018 by the 32nd District Democratic Organization.</p>
<p>Submitted by Michael Brunson of 32<sup>nd</sup> District Democrats and Jennifer Roach of 37<sup>th</sup> District Democrats</p>
</Layout>
)

export default Page
