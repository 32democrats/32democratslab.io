import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 05 - Supporting safe consumption sites in Snohomish County and all municipalities
	</title>
</Helmet>
<p>
	Resolution 2018 - 05
</p>
<h1>
Supporting safe consumption sites in Snohomish County and all municipalities
</h1>
<p>
</p><p><b>WHEREAS</b> communities in the United State are experiencing opioid related drug use, overdose, crime, and
health disparities that have negative socioeconomic impacts;
</p><p><b>WHEREAS</b> in Washington State's Snohomish County has some of the largest adverse impacts compared to
other communities in Washington State;
</p><p><b>WHEREAS</b> the Snohomish County Sheriff's Department reports that 18% of all heroin-related deaths in
Washington State occur in Snohomish County; and from 2011 - 2013 approximately
1 out of every 5 heroin deaths in Washington State occurred in Snohomish County; and in 2013 alone, heroin
and prescription opioid overdoses represented 2 out of 3 of the 130 accidental overdose deaths in the county;
</p><p><b>WHEREAS</b> the Snohomish County Sheriff's Department is calling on collaborative partnerships, and
approaches to intervene and reverse the effect of opioid addiction in Snohomish County;
</p><p><b>WHEREAS</b> the Snohomish County Sheriff's Department is equipping officers with the overdose intervention
drug naloxone, and turning away criminal bookings that have opioid induced medical issues, because they
overcrowd the medical unit at the Snohomish County Jail;
</p><p><b>WHEREAS</b> other Snohomish County based agencies such as the Everett Police Department, are deploying
embedded social workers to provide resources to to address opioid addicts in the field;
</p><p><b>WHEREAS</b> these are partial solutions that contribute to addressing the opioid crisis in Snohomish County but
do not comprehensively mitigate the adverse impact of opioid addiction in the community;
</p><p><b>WHEREAS</b> safe injection sites are a researched method of harm reduction that have been utilized in many
different countries including Canada, Australia, and European Countries;
</p><p><b>WHEREAS</b> King County and the City of Seattle are discussing the viability of being the United State's first
safe injection site;
</p><p><b>THEREFORE BE IT RESOLVED</b> that the 32nd District Democrats support the use of safe consumption sites
as a harm reduction strategy to address opioid addiction in the county; and
</p><p><b>THEREFORE BE IT FURTHER RESOLVED</b> that the 32nd District Democrats are opposed to the
Snohomish County Council's current, and any future, ban of safe consumption sites in Snohomish County; and
</p><p><b>THEREFORE BE IT FINALLY RESOLVED</b> that the 32nd District Democrats urge municipalities and
elected officials to utilize safe consumption sites as a harm reduction strategy to reduce the impact of opioid use
in Snohomish County.
</p>
<hr/>
<p>
Adopted by the 32nd Legislative District Democratic Organization on 2018-02-14.
<br/>Carin Chase, Chair.
<br/>Submitted by the Young Democrats of Snohomish County.
</p>
</Layout>
)

export default Page
