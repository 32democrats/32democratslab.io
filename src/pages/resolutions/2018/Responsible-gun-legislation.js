import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 04 - Resolution urging support for responsible gun legislation
	</title>
</Helmet>
<p>
	Resolution 2018 - 04
</p>
<h1>
Urging support for responsible gun legislation
</h1>
<p><b>WHEREAS</b> Open Carry is defined as the ability to carry a firearm on one's person in public; and
</p><p><b>WHEREAS</b> Open Carry is lawful in Washington without any permit; and
</p><p><b>WHEREAS</b> RCW 9.41.050 is the primary law which affects gun carrying on a day-to-day basis. This law makes it
unlawful for one to conceal a pistol without a concealed pistol license (CPL), and also makes it unlawful for one to carry
a loaded pistol in any vehicle, whether it be openly carried or concealed carried unless a person has a valid CPL; and
</p><p><b>WHEREAS</b> Open Carry of a loaded long gun is illegal in WA, regardless of CPL possession; and
</p><p><b>WHEREAS</b> anti-gun legislators, armed with the support from AG Ferguson introduced two gun-control bills during the
2017 session; Senate Bill 5050 and House Bill 1387, which would have imposed a registration-licensing system for these
so-called “assault weapons” and “large-capacity magazines”. Washington legislators failed to bring either of these bills
out of committee; and
</p><p><b>WHEREAS</b> there have been six mass shootings in Washington over the last few years, according to the Gun Violence
Archive including the death of 5 students including the shooter at Marysville Pilchuck High School on 10/24/14 bringing
Washington's totals for 2014-2018 to 3,234 incidents involving guns with 791 deaths; and
</p><p><b>WHEREAS</b> the October 1, 2017 mass shooting on a crowd of concert goers in Las Vegas left 58 people dead and 546
injured and marked the 273rd mass shooting in the U.S. in 275 days in 2017 according to the Gun Violence Archives; and
</p><p><b>WHEREAS</b> the shooting event that took place on December 14, 2012 where the fatal shooting of (20) children between
six and seven years of age as well as (6) adult staff members at Sandy Hook Elementary in Connecticut still failed to
bring about any “sensible” gun legislation; and
</p><p><b>WHEREAS</b> the NRA and 2nd Amendment supporters oppose any gun regulation and have lobbied for legislation which
has been filed as HB 2306. Republican Representative Van Werven of the 42nd LD and Democratic Representative Blake
from the 19th LD are the prime sponsors. HB 2306 would allow veterans to carry concealed pistols onto community
colleges campuses which includes all land and buildings owned, leased, or operated by a community college and would
add a new section to the chapter 28B.50 RCW. HB 2306 fails to address why any gun on campus makes students, faculty
or workers safer; and
</p><p><b>WHEREAS</b> HB 2293 sponsored by Democratic Representative Kagi which would restrict firearms and dangerous
weapons in early learning facilities; and
</p><p><b>WHEREAS</b> HB 5992, sponsored by Senators Van De Wege, Zeiger, Dhingra, Fain, Pedersen, Liias, and Nelson, has
been pre-filed for the 2018 legislative session. HB 5992 would prohibit a “trigger modification device” which is
designed to accelerate the rate of fire of a firearm;
</p><p><b>THEREFORE, BE IT RESOLVED</b> the Thirty-Second Legislative District Democrats urge other LD's CD's and State
Elected Officials to join us to bring “Responsible Gun Ownership” front and center during the 2018 legislative session to
include legislation but not limited to legislation that would restrict firearms and dangerous weapons in early learning
facilities and make it illegal in WA State to possess, use, or manufacture “trigger modification devices”; and
</p><p><b>BE IT FURTHER RESOLVED</b> that the Thirty-Second Legislative Districts urges other LDs, CD and State Elected
Officials to oppose legislation that would expand “open carry” laws in WA State including allowing concealed pistols on
community college campuses; and
</p><p><b>BE IT FINALLY RESOLVED</b> that the Thirty-Second Legislative District asks the Chair of the District to send this
resolution to the State Central Committee Meeting for Consideration at their next meeting.
</p><hr/>
<p>The <a href="http://www.gunviolencearchive.org">Gun Violence Archive</a> defines mass shootings on the numeric value of 4 or
more shot or killed, not including the shooter.
</p>
<hr/>
<p>Adopted by the 32nd Legislative Democratic Organization on January 10, 2018.
<br/>Carin Chase, Chair
</p>
</Layout>
)

export default Page
