import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 15 - Reject corporate PAC money from the fossil fuel industry
	</title>
</Helmet>
<p>
	Resolution 2018 - 15
</p>
<h1>
	Reject corporate PAC money from the fossil fuel industry
</h1>
<p>
<b>WHEREAS</b> the Washington State Democrats recognize that the burning of fossil fuels is a primary cause of climate change and poses a clear and present danger to life as we know it; and 
</p>
<p>
<b>WHEREAS</b> over 1000 candidates nationwide, including 24 in Washington State, have signed a No Fossil Fuel Money Pledge; and
</p>
<p>
<b>WHEREAS</b>, while the Washington State Democrats have not publicly stated an official position, they do not currently accept corporate PAC contributions from the fossil fuel industry; and
</p>
<p>
<b>WHEREAS</b> the 2010 Citizens United decision opened the floodgates for anonymously-sourced corporate and special interest monies to flow into our national politics, further amplifying the voice and influence of corporations versus private citizens and resulting in immense pressure on our national committee, elected officials, and candidates; and
</p>
<p>
<b>WHEREAS</b> the DNC Executive Committee passed a resolution on June 9, 2018 “rejecting corporate PAC contributions from the fossil fuel industry”; and
</p>
<p>
<b>WHEREAS</b> the DNC Executive Committee passed another resolution on August 10th, 2018, which effectively reversed the ban on fossil fuel industry PAC money by including a phrase that welcomes contributions from “employers' political action committees”; and
</p>
<p>
<b>WHEREAS</b> the act of accepting corporate PAC contributions from the fossil fuel industry is antithetical to the National Democratic Party Platform, which states, “Democrats believe that climate change poses a real and urgent threat to our economy, our national security, and our children's health and futures, and that Americans deserve the jobs and security that come from becoming the clean energy superpower of the 21st century”; and
</p>
<p>
<b>WHEREAS</b> public trust in government to "do the right thing" is near historic lows, at fewer than one in five Americans, and DNC Member Christine Pelosi notes that we have the “opportunity to reform and revive our party by empowering diverse grassroots Democrats at the leadership table and in our communities, including building on our recent successes with small-donor fundraising programs…”; 
</p>
<p>
<b>THEREFORE BE IT RESOLVED</b> that the Washington State Democrats formally affirm our commitment to rejecting corporate PAC contributions from the fossil fuel industry; and
</p>
<p>
<b>BE IT FURTHER RESOLVED</b> that the Washington State Democrats urge all Democratic candidates in Washington State to sign the No Fossil Fuel Money Pledge; and
</p>
<p>
<b>BE IT FURTHER RESOLVED</b> that the Washington State Democrats urge the Executive Committee of the DNC to pass a new resolution prior to the November Congressional elections reinstating the ban on corporate PAC contributions from the fossil fuel industry; and
</p>
<p>
<b>BE IT FINALLY RESOLVED</b> that a press release be issued by the Washington State Democrats, posted on the official party website and distributed through the party's statewide email list, informing the public about the actions taken in this resolution. 
</p>
<hr/>
<p>
Adopted September 12, 2018, by the 32nd District Democratic Organization
</p>
<p>
Submitted by the Teresa Catford. Originated in August, 2018 by the Environment and Climate Caucus of the Washington State Democrats (ECC) and submitted to the Washington State Democratic Central Committee for consideration at its September 16, 2018 meeting in Spokane. Also submitted by the ECC to the Washington State Progressive Caucus.
</p>
<hr/>
<p>
References:
<br/>No Fossil Fuel Money Pledge: <a href="http://nofossilfuelmoney.org">http://nofossilfuelmoney.org</a>
<br/><br/>
How Citizens United Changed Campaign Finance: <a href="https://www.opensecrets.org/news/2018/02/how-citizens-united-changed-campaign-finance/">https://www.opensecrets.org/news/2018/02/how-citizens-united-changed-campaign-finance/</a>
<br/><br/>
Resolution submitted by Christine Pelosi, passed by the DNC Executive Committee on June 9, 2018: <a href="https://medium.com/@sfpelosi/democrats-lets-clean-up-our-planet-and-our-politics-oilmoneyout-of-the-dnc-b1102d4311ab">https://medium.com/@sfpelosi/democrats-lets-clean-up-our-planet-and-our-politics-oilmoneyout-of-the-dnc-b1102d4311ab</a>
<br/><br/>
Resolution submitted by Tom Perez, passed by the DNC Executive Committee on August 10, 2018: <a href="https://gallery.mailchimp.com/b575b9e5364b5673b6f9df3f1/files/222d3906-d801-4d69-8ed6-5e34991ced18/Exec_Labor_Resolution_8.10.18.pdf">https://gallery.mailchimp.com/b575b9e5364b5673b6f9df3f1/files/222d3906-d801-4d69-8ed6-5e34991ced18/Exec_Labor_Resolution_8.10.18.pdf</a>
<br/><br/>
Open letter from the Environment and Climate Caucus of the WA State Democrats to the WA State DNC delegation: <a href="http://bit.ly/ECClettertoDNC">http://bit.ly/ECClettertoDNC</a>
<br/><br/>
Youth, environmental activists converge on Chicago to protest DNC fossil fuel flip-flop: <a href="https://www.wisconsingazette.com/news/youth-environmental-activists-converge-on-chicago-to-protest-dnc-fossil/article_9727ac6c-a7b4-11e8-bcb1-97cba375d63a.html">https://www.wisconsingazette.com/news/youth-environmental-activists-converge-on-chicago-to-protest-dnc-fossil/article_9727ac6c-a7b4-11e8-bcb1-97cba375d63a.html</a>
<br/><br/>
DNC Pretends Respect for Workers Requires it to Take Corporation PAC Money from Big Oil: <a href="https://theintercept.com/2018/08/13/dnc-fossil-fuel-donations-climate-change/">https://theintercept.com/2018/08/13/dnc-fossil-fuel-donations-climate-change/</a>
<br/><br/>
Resolution Expressing the Sense of the DNC that Climate Change Poses an Urgent and Severe Threat to our National Security (Resolution 8), passed by the DNC on August 25, 2018: <a href="https://drive.google.com/file/d/0B_dTIP5R3g2xaDI2SzVqbDhZelRtZHdoWlVKODFyT2VKaURV/">https://drive.google.com/file/d/0B_dTIP5R3g2xaDI2SzVqbDhZelRtZHdoWlVKODFyT2VKaURV/</a>
</p>
</Layout>
)

export default Page
