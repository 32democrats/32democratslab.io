import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 03 - Opposition to PSE's proposed liquefied natural gas plant on the Tacoma tide flats
	</title>
</Helmet>
<p>
	Resolution 2018 - 03
</p>
<h1>
Opposition to PSE's proposed liquefied natural gas plant on the Tacoma tide flats
</h1>
<p>
<b>WHEREAS</b> Puget Sound Energy (PSE) has proposed construction of a massive, 18-story plant at the Port of Tacoma for the production, storage and distribution of liquefied natural gas (LNG); and
</p>
<p>
<b>WHEREAS</b> PSE has illegally begun construction of its LNG facility despite a lack of key permits and a change in the nature and scope of the project after the relevant EIS was issued, and in blatant disregard of a Treaty with the Puyallup Tribe, a sovereign nation; and
</p>
<p>
<b>WHEREAS</b> the Puyallup Tribe has not given its consent to PSE's project and has filed lawsuits against its construction, based on its significant threat to Treaty-protected fishing rights, and has subsequently garnered significant and broad support from both local community members and the Affiliated Tribes of Northwest Indians; and
</p>
<p>
<b>WHEREAS</b> reducing pollution from greenhouse gases is a critical policy objective of the State of Washington; the Washington Democratic Party Platform calls for “immediate aggressive action to minimize climate change, as global climate change is the foremost threat to survival of Earth as we know it”; and Governor Inslee has affirmed his commitment to supporting the Paris Climate Accords; and
</p>
<p>
<b>WHEREAS</b> natural gas is now recognized as a powerful contributor to climate change, peer-reviewed studies having overwhelmingly found that emissions from fracking and leaks along the supply chain effectively override the otherwise lesser environmental impacts of gas usage; and
</p>
<p>
<b>WHEREAS</b> the PSE facility would require an on-going supply of fracked gas from offsite wells, exposing communities near those wells to deadly toxins and carcinogens used in the fracking process, and to permanent pollution of their local waters; and
</p>
<p>
<b>WHEREAS</b> the siting of the facility in an area of the Cascadia fault zone that is at high risk of earthquakes and tsunamis, and will be regularly flooded by 2050 due to sea-level rise and seasonal inundation, would pose an extreme and unacceptable hazard to local residents; and
</p>
<p>
<b>WHEREAS</b> the current and future financial impacts of the siting of a large LNG plant in an environmentally sensitive area have not been adequately calculated and made public during the decision-making process; and 
</p>
<p>
<b>WHEREAS</b> the facility is sited on and near multiple Superfund sites, exposing thousands of people within a 3-mile radius to toxins and carcinogens during construction and operation, in violation of federal law and common morality, leading to increased risk of cancer, neurological disorders, birth defects, respiratory issues and other chronic health issues; 
</p>
<p>
<b>THEREFORE, BE IT RESOLVED</b> that we firmly oppose construction of PSE's proposed LNG plant in Tacoma, and urge the Tacoma City Council, Tacoma Mayor, Port of Tacoma, the Governor, and all other relevant state, local and regional agencies to reject it forthwith; and
</p>
<p>
<b>BE IT FURTHER RESOLVED</b> that we urge the Washington State Attorney General to conduct a thorough review of the SEPA permitting process for the project, including the many requests for a supplemental EIS that have been made, and of the lawsuits that have been and will be filed for the failings that have thus far occurred; and
</p>
<p>
<b>BE IT FURTHER RESOLVED</b> that this resolution be sent to our Governor and Attorney General, and to our entire delegation in the State Legislature.
</p>
<hr/>
<p>
Adopted by the 32nd Legislative Democratic Organization on January 10, 2018.
<br/>
Carin Chase, Chair
</p>
</Layout>
)

export default Page
