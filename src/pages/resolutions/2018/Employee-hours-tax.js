import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 10 - Resolution in support of employee hours tax
	</title>
</Helmet>
<p>
	Resolution 2018 - 10
</p>
<h1>
	In support of employee hours tax
</h1>
<p><b>WHEREAS</b>, the City of Seattle declared a homelessness emergency November 2nd, 2015; and
</p><p><b>WHEREAS</b> 8,476 people are currently experiencing homelessness in Seattle, including 3,857 living on streets, in tents, vehicles, or encampments according to the January 2017 point-in-time count; and
</p><p><b>WHEREAS</b> homeless shelters in the City of Seattle are full, or otherwise unable to accommodate the entire population of people living homeless, leaving approximately 45% unsheltered; and
</p><p><b>WHEREAS</b>, the average rent for an apartment in Seattle is now $2,101 per month, putting market-rate homes out of reach for many in the city; and
</p><p><b>WHEREAS</b>, in Seattle over 22,000 extremely low-income households (making 0-30% of area median income) are severely rent-burdened, paying over half their income in rent and utilities; and
</p><p><b>WHEREAS</b>, long-term low income housing options are not readily available and have very long waiting lists leaving some unhoused individuals waiting 36 months or more for public housing assistance; and
</p><p><b>WHEREAS</b>, housing people who are homeless has shown to help people and the city become more healthy and safer; and
</p><p><b>WHEREAS</b>, progressive revenue sources such as recommended by the Progressive Revenue Task Force on Homelessness are more equitable funding sources than property tax levies or sales taxes which disproportionately affect working and low income individuals; and
</p><p><b>WHEREAS</b>, it is stated in the Proclamation of Civil Emergency, Section 2, "to not take action and to allow the homelessness problem to continue to grow would be in dereliction of the duties of [the] office and of the welfare and basic human rights of the people of this city"; and
</p><p><b>WHEREAS</b>, an Employee Hours Tax would tax Seattle's biggest businesses, those with between $8 and $10 million dollars in net revenue a year, and would bring in $75 million dollars to build 375 publicly-owned, union built, affordable housing units and would provide homeless services annually; and
</p><p><b>THEREFORE, BE IT RESOLVED</b>, the 32nd District Democrats request that the Seattle City Council and Mayor's Office implement an Employee Hours Tax on large businesses to provide more resources in the fight against homelessness in Seattle.
</p><hr/><p>
Adopted May 9, 2018 by the 32nd Legislative District Democratic Organization; Carin Chase, Chair. Submitted by Brent McFarlane and Sally Soriano. Similar versions passed by the 43rd and 36th Legislative District Democrats.
</p>
</Layout>
)

export default Page
