import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 01 - Urging our U.S. senators to co-sponsor S.1804, for single-payer health care
	</title>
</Helmet>
<p>
	Resolution 2018 - 01
</p>
<h1>
Urging our U.S. senators to co-sponsor S.1804, for single-payer health care
</h1>
<p><b>WHEREAS</b> the current Washington State Democratic Party platform expressly calls for a
universal single-payer health care system to provide the most equitable and effective care for all;
and
</p><p><b>WHEREAS</b> past Democratic state committees and state conventions, as well as this body, have
adopted both platforms and serial resolutions in support of single-payer health care, with little
visible response from our U.S. Senators; and
</p><p><b>WHEREAS</b> Republicans' continued attacks on the Affordable Care Act (“ACA”) have
increased the public's awareness of single-payer health care as an alternative, with over 50% of
voters now supporting a single-payer system; and
</p><p><b>WHEREAS</b> the Republicans' recently enacted “Tax Cuts and Jobs Act” contains a non-germane
provision undermining the continued effectiveness of the ACA by repealing the “individual
mandate” that had brought widespread nationwide enrollment in health insurance; and
</p><p><b>WHEREAS</b> in order to assure the provision of comprehensive health care as a human right,
Senator Bernie Sanders has introduced U.S. Senate bill S. 1804, “The Medicare-for-All Act of
2017,” which would establish a universal single-payer program for improved and expanded
health care for all residents of the United States; and
</p><p><b>WHEREAS</b> sixteen Democratic Senators immediately signed on as co-sponsors of S. 1804, but
neither of our State's two United States Senators has yet done so; and
</p><p><b>WHEREAS</b> our State Democratic platform states, upfront and unequivocally: “In order to
restore progressive democracy, we expect elected Democrats to be accountable and implement
the principles of this platform, using all available legal and parliamentary procedures”;
</p><p><b>THEREFORE, BE IT RESOLVED</b> that Senators Patty Murray and Maria Cantwell be
strongly urged, in compliance with the State Democratic platform, to promptly join with other
Democratic Senators as co-sponsors of S.1804, and
</p><p><b>BE IT FURTHER RESOLVED</b> that this resolution be sent to Senators Murray and Cantwell,
accompanied in each case by a letter inquiring as to their actions and intended actions in
conformity herewith.
</p>
<hr/>
<p>
Adopted by the 32nd Legislative Democratic Organization on January 10, 2018.
<br/>Carin Chase, Chair
</p>
</Layout>
)

export default Page
