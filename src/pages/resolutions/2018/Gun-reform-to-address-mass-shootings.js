import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		Resolution 2018 - 08 - Urging gun reform to address mass school shootings and other gun violence
	</title>
</Helmet>
<p>
	Resolution 2018 - 08
</p>
<h1>
Urging gun reform to address mass school shootings and other
gun violence
</h1>
<p><b>WHEREAS</b> the Colt AR-15 assault rifle is increasingly becoming the weapon of choice in mass shootings at schools and other public venues, and
</p><p><b>WHEREAS</b> the 2nd Amendment does not protect private ownership of assault rifles such as the AR-15, and
</p><p><b>WHEREAS</b> the 1996 Dickey Amendment says, “none of the funds made available for injury prevention and control at the Centers for Disease Control and Prevention (CDC) may be used to advocate or promote gun control”, and
</p><p><b>WHEREAS</b> gun show loopholes do not require private party sellers to ask for identification, perform background checks, or record the sale of firearms, “as long as they do not know or have reasonable cause to believe the person is prohibited from receiving or possessing firearms", and
</p><p><b>WHEREAS</b> “Ghost Guns” allow people to purchase gun parts online that are unregulated and untracked with no serial numbers, in order to assemble a variety of firearms, including the AR-15, thereby circumventing background checks and registration, and
</p><p><b>WHEREAS</b> 14 states impose criminal liability on parents when a child gains access to firearms due to negligent storage (California, Connecticut, Florida, Hawaii, Illinois, Iowa, Maryland, Massachusetts, Minnesota, New Hampshire, New Jersey, North Carolina, Rhode Island, and Texas), and
</p><p><b>WHEREAS</b> liability insurance is a requirement for owning and operating a motor vehicle and other potentially dangerous or deadly items;
</p><p><b>THEREFORE, BE IT RESOLVED</b> that We, the 32nd Legislative District Democratic Organization, demand that elected officials in Olympia and Washington D.C. pass legislation that bans private ownership of semi-automatic weapons as defined by the ATF, and
</p><p><b>BE IT ALSO RESOLVED</b> that We demand a repeal of the 1996 Dickey Amendment, and
</p><p><b>BE IT ALSO RESOLVED</b> that We demand legislation that requires a 5 business day waiting period on the sale of any and all firearms, and
</p><p><b>BE IT ALSO RESOLVED</b> that We urge the swift digitization of all existing gun registration records, and demand that all future firearms sales be recorded digitally, and
</p><p><b>BE IT ALSO RESOLVED</b> that We demand legislation that will close all gun show loopholes, and
</p><p><b>BE IT ALSO RESOLVED</b> that We demand legislation that will require all prospective firearms purchasers to undergo background check and permitting PRIOR to a firearms purchase, and that the burden of these clearances shall be on the purchaser, and
</p><p><b>BE IT ALSO RESOLVED</b> that We demand legislation that will end the sale of unregistered gun parts with no serial numbers.
</p><p><b>BE IT ALSO RESOLVED</b> that We demand legislation that requires firearms owners to secure their firearms with trigger locks or gun safes and
</p><p><b>BE IT ALSO RESOLVED</b> that We demand legislation that will require all firearms owners to purchase liability insurance of no less than $1,000,000 per injury, so long as such insurance is available for purchase and
</p><p><b>BE IT FINALLY RESOLVED</b> that We demand legislation in Washington State, that parents, guardians, and other adult firearm owners may be held criminally liable for minor children accessing that firearm owner's unsecured firearms and using them in the commission of any crime or any accidental injury or death.
</p><hr/><p>Adopted April 11, 2018 by the 32nd Legislative District Democratic Organization; Carin Chase, Chair. Draft
originated by the 2018 Snohomish County Platform Committee 2018-03-15.
</p>
</Layout>
)

export default Page
