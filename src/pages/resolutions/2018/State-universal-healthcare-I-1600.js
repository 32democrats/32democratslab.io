import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 11 - Urging support for state universal healthcare (Initiative 1600)
	</title>
</Helmet>
<p>
	Resolution 2018 - 11
</p>
<h1>
Urging support for state universal healthcare (Initiative 1600)
</h1>
<p>
<b>WHEREAS</b> Article 25 of the United Nations Universal Declaration of Human Rights states that “Everyone has the right to a standard of living adequate for the health and well-being of himself and of his family, including food, clothing, housing and medical care and necessary social services, and the right to security in the event of unemployment, sickness, disability, widowhood, old age or other lack of livelihood in circumstances beyond his control,” thereby declaring a fundamental right to healthcare at an international level;
</p>
<p>
<b>WHEREAS</b> the document that laid the initial groundwork for the United States as an independent nation declared it a responsibility of government to protect certain unalienable rights of its people, prominently including the preservation of “Life, Liberty, and the pursuit of Happiness,” thereby leading inexorably to the conclusion that health care, as a necessary prerequisite therefor, is a human right;
</p>
<p>
<b>WHEREAS</b> more than half a million people in our state are uninsured; more are underinsured; and many lives could be saved by expanding access to health care;
</p>
<p>
<b>WHEREAS</b> the 2016 Platform of the Washington State Democratic Party affirms that “Health care is a basic human right” and that “Government should work to improve our overall health, while assuring access to high-quality, affordable care for everyone;
</p>
<p>
<b>WHEREAS</b> that Platform, in implementation of the above, goes on to specify that we must establish “An affordable universal single-payer system to provide the most equitable and effective health care, serving both individual and public health needs;”
</p>
<p>
<b>WHEREAS</b> a resolution calling specifically for single-payer health care was adopted by the Washington State Democratic Central Committee in April of 2017;
</p>
<p>
<b>WHEREAS</b> people of color, people with disabilities, and low-income people are the populations most likely to lack affordable, comprehensive healthcare under the current system; and
</p>
<p>
<b>WHEREAS</b> Whole Washington has crafted a ballot initiative guaranteeing universal healthcare to all Washingtonians, with a funding mechanism devised by a noted economist, which demonstrates that Washington residents will save over $9 billion dollars annually when universal healthcare is implemented;
</p>
<p>
<b>THEREFORE, BE IT RESOLVED THAT</b> we will facilitate contact with our membership by or on behalf of Whole Washington, and will utilize our mailing list and online presence to mobilize voters and collect signatures for Initiative 1600; and
</p>
<p>
<b>BE IT FURTHER RESOLVED THAT</b>, upon adoption of this resolution, it be sent to the major print and broadcast media that serve our area.
</p>
<hr/>
<p>
Adopted June 13, 2018 by the 32nd Legislative District Democratic Organization; Carin Chase, Chair. Submitted by Dean Fournier.
</p>
</Layout>
)

export default Page
