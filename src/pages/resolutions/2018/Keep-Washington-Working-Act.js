import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 16 - Resolution in Support of the Keep Washington Working Act (SB 5689)
	</title>
</Helmet>
<p>
	Resolution 2018 - 16
</p>
<h1>
	In Support of the Keep Washington Working Act (SB 5689)
</h1>
<p><b>Whereas</b> immigrants have long contributed to the growth and vitality of Washington state's economy, owning 15% of businesses in our state and comprising 16% of the state's workforce (in industries such as agriculture, food processing, timber, construction, healthcare, technology, and the hospitality industry), while spending $44.7 billion annually as consumers; and</p>
<p><b>Whereas</b> the current federal administration has abandoned the exercise of prosecutorial discretion in its treatment of immigrants, by refusing to prioritize whom to focus on when enforcing civil immigration law, thereby making it virtually impossible for recent immigrants to contribute to our state's economy and communities; and</p>
<p><b>Whereas</b> the federal government has, for several decades, failed to reform immigration policies to address the economic needs of our state and the well-being of our working families; and</p>
<p><b>Whereas</b> we wish to see the rights and dignity of immigrant state residents maintained and protected, and our law enforcement agencies trusted to protect public safety in their communities; and</p>
<p><b>Whereas</b> Washington state has no legitimate incentive to expend state resources in the enforcement of federal civil immigration policies that are at odds with the economic needs and public safety of our state;</p>
<p><b>Therefore, be it resolved</b> that we urge the Washington state legislature to reintroduce and enact into law the Keep Washington Working Act, 2SSB 5689, in the 2019-2020 legislative session.</p>
<hr/>
<p>
Adopted September 12, 2018 by the 32nd District Democratic Organization
</p>
</Layout>
)

export default Page
