import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 06 - A call for the immediate resignation of Bailey Stober as chair of the King County Democrats
	</title>
</Helmet>
<p>
	Resolution 2018 - 06
</p>
<h1>
A call for the immediate resignation of Bailey Stober as chair of the King County Democrats
</h1>
<p><b>WHEREAS</b> a charge of violation of the King County Democratic Party Code of Conduct was filed on February 1, 2018, against King County Democratic Party Chair Bailey Stober, for alleged harassment of one Natalia Koss Vallejo, during the period in which Ms. Vallejo was serving under him as the Executive Director of the King County Democratic Party; and
</p><p><b>WHEREAS</b> Democrats in King County, like Democrats elsewhere, are staunchly and unalterably opposed to workplace harassment; and
</p><p><b>WHEREAS</b> the above charge of workplace harassment has later been supplemented by charges of mismanagement of King County Democratic Party funds by Mr. Stober, including extravagance in expenditures; and
</p><p><b>WHEREAS</b> the King County Democratic Party Treasurer reported, on February 27, a balance of $3,886.96 in hand, and known bills due in March totaling over $5800; and
</p><p><b>WHEREAS</b> the above charges against Chair Stober remain unresolved, and have in recent weeks given rise to increasing demands for his resignation, including but not limited to an “open letter” signed by over 175 recognized Democrats, including prominent elected officeholders and Legislative District Chairs from within and without King County; and
</p><p><b>WHEREAS</b> during this period of unresolved allegations against its elected leader, the King County Democratic Party has been subjected to a serious diminution of desperately needed financial support, including suspension of significant donations from Democratic Party leaders, some of its own constituent Legislative District organizations, and normally supportive outside organizations; and
</p><p><b>WHEREAS</b> in light of the above developments, and uncertainty about their resolution, the King County Democratic Party has sunk into serious disarray, giving rise to increased doubt about its ability to function as an effective organ of the Democratic Party in Washington's largest county; and
</p><p><b>WHEREAS</b> a strong and united King County Democratic party organization is needed to help build and sustain the Democratic Party and elect Democratic candidates in the 8th Congressional District, and the 5th, 30th, 31st and 39th Legislative Districts,
</p><p><b>THEREFORE, BE IT RESOLVED</b> that the 32nd Legislative District Democratic Organization respectfully calls upon Bailey Stober, in the best interest of the Democratic Party and its electoral success, to immediately resign as Chair of the King County Democratic Party; and, should he not resign, we urge the Democratic PCOs in King County and the KCDCC Executive Board to call a meeting, <a href="https://www.kcdems.org/bylaws/bylaws.xml#AV-5.9">per Section 5.9 of the Bylaws of the King County Democrats</a>, to vote on Bailey Stober's removal as Chair of the King County Democrats.
</p><hr/><p>Adopted March 14, 2018 by the 32nd District Democratic Organization. Originated March 7, 2018, by Carin Chase, Chair, 32nd District Democrats.
</p>
</Layout>
)

export default Page
