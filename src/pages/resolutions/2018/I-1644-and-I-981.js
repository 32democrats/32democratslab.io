import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		Resolution 2018 - 13 - Endorsing Initiative 1644 to the people and Initiative 981 to the legislature
	</title>
</Helmet>
<p>
	Resolution 2018 - 13
</p>
<h1>
Endorsing Initiative 1644 to the people and Initiative 981 to the legislature
</h1>
<p><b>WHEREAS</b> America's greatest strength is our Diversity, an asset which sets our country apart from any other nation in the world; and</p>
<p><b>WHEREAS</b> according to Washington state's Office of Financial Management (OFM), as of 2017, Washington 7.3 million population was 50% women and over 30% people of color; and</p>
<p><b>WHEREAS</b> 20 years ago on November 3, 1998, Washington State passed Initiative 200, which banned Affirmative Action programs to prevent discrimination based on race and gender in public education, public contracting and public employment; and</p>
<p><b>WHEREAS</b> since 1998, Washington's demographic and economic forces have produced significant education and employment gaps, particularly for women and people of color, and despite the tireless legislative efforts of State Representative Sharon Tomiko-Santos, State Senator Maralyn Chase and other legislators to repeal 1-200, the Legislature has failed to pass a repeal in the last 20 years; and</p>
<p><b>WHEREAS</b> the 32<sup>nd</sup> District Democrats have been leading the way fighting to overturn 1-200, and passed a Resolution supporting the repeal or amendment of 1-200: and</p>
<p><b>WHEREAS</b> the People of Washington State have shown overwhelming support for voting to repeal 1-200 and restore Affirmative Action back in Washington State law to prevent discrimination against women, honorably discharged veterans, the disabled and minorities in public education, public contracting and public employment; and</p>
<p><b>WHEREAS</b> Initiative 1644 and Initiative 981 would create a Governor's Commission on Diversity, Equity &amp; Inclusion; redefine Affirmative Action without quotas and preferential treatment; and expand the veterans covered under Affirmative Action beyond Vietnam era and disabled veterans to include all honorable discharged veterans;</p>
<p><b>NOW THEREFORE BE IT RESOLVED</b> that the 32ND DISTRICT DEMOCRATS hereby endorse Initiatives 1644 and 981 and urge our members to sponsor, sign and vote for both initiatives; and</p>
<p><b>BE IT FURTHER RESOLVED</b> that the 32ND DISTRICT DEMOCRATS urge all members to support the "ONE WASHINGTON" CAMPAIGN'S efforts to obtain the number of signatures necessary to qualify Initiatives 1644 and 981 on the ballot as initiatives to the people and to the legislature respectively; and</p>
<p><b>BE IT FURTHER RESOLVED</b> that we urge all 32ND DISTRICT DEMOCRATS endorsed candidates to endorse Initiatives 1644 and 981 and sign and vote for both initiatives.</p>
<p>Resolution submitted by Carin Chase, May 31, 2018</p>
<hr/><p>Adopted June 13, 2018 by the 32nd District Democratic Organization.</p>
</Layout>
)

export default Page
