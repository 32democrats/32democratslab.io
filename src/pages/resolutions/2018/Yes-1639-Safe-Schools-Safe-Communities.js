import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		Resolution 2018 - 14 - In Support of Yes on 1639, Safe Schools, Safe Communities
	</title>
</Helmet>
<p>
	Resolution 2018 - 14
</p>
<h1>
	In Support of Yes on 1639, Safe Schools, Safe Communities
</h1>
<p>
<b>Whereas</b> gun violence is a public health epidemic which claims tens of thousands of lives in
America every year and results in tens of thousands more injuries; and
</p><p><b>Whereas</b> gun violence traumatizes communities and people across our state; and
</p><p><b>Whereas</b> in Washington state a person is killed with a gun every 14 hours [1] and more
Washingtonians are killed by guns than die in car accidents [2]; and
</p><p><b>Whereas</b> five of the deadliest mass shootings this decade have used semi-automatic assault
weapons and/or high capacity magazines [3]; and
</p><p><b>Whereas</b> there have been more than 50 school shootings in 2018 [4] and more than 187,000
students have been impacted by school shootings since 1999 [5]; and
</p><p><b>Whereas</b> it is currently easier to purchase a semi-automatic assault weapon in Washington
state than it is to buy a handgun; and
</p><p><b>Whereas</b> nearly half of Washington gun owners do not lock up their guns [6]; and
</p><p><b>Whereas</b> the Washington State Democratic Party platform adopted June 16, 2018 calls for
improving gun sale background checks, establishing waiting periods, raising the minimum
purchase age to 21, requiring safety training, and implementing laws that hold adults
accountable for unsafely stored firearms; and
</p><p><b>Whereas</b> Initiative 1639 includes all of these measures to help prevent gun violence in
Washington state.
</p><p><b>Therefore, be it resolved</b> that the 32nd Legislative District Democratic Organization supports
Yes on Initiative 1639, Safe Schools, Safe Communities, and encourages Washingtonians to
vote in favor of Initiative 1639.
</p>
<hr/>
<p>
Adopted August 8, 2018 by the 32nd District Democrats
</p>
<p>
Originated by the “Yes on 1639 Campaign”
<br/>
(206) 909-7641, press@yeson1639.org
</p>
<p>
[1] Chelsea Parsons and Eugenio Vargas. “Gun Violence Across American.” Center for American Progress, 2016, <a href="https://s3.amazonaws.com/interactives.americanprogress.org/maps/raphael/AmericaUnderFire/pdfs/WA-GunViolence-
factsheet.pdf">https://s3.amazonaws.com/interactives.americanprogress.org/maps/raphael/AmericaUnderFire/pdfs/WA-GunViolence-
factsheet.pdf</a>.
<br/>
[2] Ibid
<br/>
[3] “High Capacity Magazines and Assault Weapons.” Everytown, 2018, <a href="https://everytownresearch.org/high-capacity-
magazines-assault-weapons/">https://everytownresearch.org/high-capacity-
magazines-assault-weapons/</a>.
<br/>
[4] <a href="https://everytownresearch.org/gunfire-in-school/#6189">https://everytownresearch.org/gunfire-in-school/#6189</a>
<br/>
[5] <a href="https://www.washingtonpost.com/graphics/2018/local/us-school-shootings-history">https://www.washingtonpost.com/graphics/2018/local/us-school-shootings-history</a>
<br/>
[6] <a href="https://www.kingcounty.gov/depts/health/violence-injury-prevention/violence-prevention/gun-violence/LOK-IT-
UP/~/media/depts/health/violence-injury-prevention/documents/firearm-facts.ashx">https://www.kingcounty.gov/depts/health/violence-injury-prevention/violence-prevention/gun-violence/LOK-IT-
UP/~/media/depts/health/violence-injury-prevention/documents/firearm-facts.ashx</a>
</p>
</Layout>
)

export default Page
