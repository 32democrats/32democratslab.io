import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const Page = () => (
<Layout>
<Helmet>
	<title>
		2018 - 02 - Ban source-of-income discrimination in rental housing
	</title>
</Helmet>
<p>
	Resolution 2018 - 02
</p>
<h1>
Ban source-of-income discrimination in rental housing
</h1>
<p><b>WHEREAS</b> housing instability and homelessness continue to grow in our immediate communities and across
Washington State as a whole; and
</p><p><b>WHEREAS</b> for example, rental costs for 2- and 3-bedroom apartments in Snohomish County increased by
26.2% and 23.8% respectively, between 2011-15, thereby increasing an already serious need for supplementary
sources of funds to pay for rent; and
</p><p><b>WHEREAS</b> there exists an array of governmental and private programs that provide verifiable funds usable
toward paying tenants' housing costs, among them being “Section 8” and other HUD housing vouchers, Social
Security and Supplemental Security Income, unemployment compensation, veteran's benefits, and child-support
payments; and
</p><p><b>WHEREAS</b> many individuals and families that are entitled to and do receive such financial assistance are
nevertheless denied rental housing because of landlords' groundless disfavor of reliance on those sources of
income; and
</p><p><b>WHEREAS</b> communities of color, people with disabilities, and other populations long subjected to
discrimination have been and are disproportionately impacted by such denials of housing opportunities; and
</p><p><b>WHEREAS</b> 10% of the housing-discrimination cases investigated by the Seattle Office of Civil Rights in the last
eight years have involved denials based on would-be tenants' use of Section 8 housing vouchers; and
</p><p><b>WHEREAS</b> 1,066 Snohomish County residents, and 11,643 King County residents, were experiencing
homelessness in the January 2017 Point-in-Time Count, and thousands more – including many elderly, disabled,
or low-income people of color – face rental restrictions and discrimination due to their use of programs that help
offset their rent payments; and
</p><p><b>WHEREAS</b> a growing number of cities across the state have enacted protections against source-of-income
discrimination in rental housing; and
</p><p><b>WHEREAS</b> our State Legislature has authority to enact legislation to promote fairness and equity in rental
housing throughout the State, including prohibition of discrimination against renters who use alternative sources
of income to pay for housing; and
</p><p><b>WHEREAS</b> assuring fair housing opportunities for all will be an important step in carrying out our longstanding
commitment to promoting racial equity and social justice;
</p><p><b>THEREFORE, BE IT RESOLVED</b> that we support enactment of protections to ensure fair treatment in the
rental housing market, specifically including legislation prohibiting discrimination based on the source(s) of the
funds from which rental payments are made; and
</p><p><b>BE IT FURTHER RESOLVED</b> that we urge our State Senator and State Representatives to work with their
legislative colleagues toward enactment of legislation to prohibit housing discrimination based on source of
income, and that said Senator and Representatives be promptly queried as to their actions and intended actions in
furtherance of the above; and
</p><p><b>BE IT FURTHER RESOLVED</b> that this resolution be sent to all jurisdictions within the 32nd Legislative
District and to our entire delegation in the State Legislature.
</p><hr/><p>Submitted by the 32nd Legislative Democratic Organization to the Washington State Democratic Central
Committee for consideration at its January 28, 2018 meeting in Bellingham. (Date Submit 2018-01-12)
<br/>Carin Chase, Chair
</p>
</Layout>
)

export default Page
