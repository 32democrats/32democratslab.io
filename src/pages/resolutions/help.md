import Layout from '../../components/layout'
import {wsdcc_action, wsdcc_action_names, wsdcc_issue, wsdcc_issue_doc, wsdcc_issue_names} from '../../components/wsdcc'

export const title = "Resolution Help"

<Layout title={title}>
<h1>{title}</h1>

[WSDCC Resolution Rules](https://www.wa-democrats.org/wp-content/uploads/2020/02/WSDCC-Rules-for-Resolutions.pdf)

Resolutions should be in a format that can be easily edited.
WSDCC uses MSWord, but Markdown works for version control.

[MDX](https://mdxjs.com/), an extension of Markdown, was required for integration with this website.
See an [example resolution in Markdown](https://gitlab.com/32democrats/32democrats.gitlab.io/-/blob/master/src/pages/resolutions/2020/R-90.mdx) and [the result](/resolutions/2020/R-90).

- `title`, concise
- `title_short` for headers
- 1-2 pages print preview. 250 words per page is the rule of thumb.
- 2 to 5 “Whereas” statements
  - supported by documented references.
  - (Citations will not be included for passage by the full body but are recommended for context and background for the committee.)
- 1 to 3 “Therefore” statements
- “Whereas” and “Therefore” statements are related.
- Written from the point of view of the WSDCC.
- End:
  - organization
  - contact person
  - contact email
  - date submitted

# Header
Upper right of each page.

`WSDCCRES – 522 – 550918 – SUB – ARTS – Commemorating Star Wars Day`

- organization
- identifying number
- date of last action (YYMMDD) 
- Last action
- issue abbreviation
- title, shortened

To ease formatting, use the component:
<pre>{
`<WsdccHeader w={{
  action_date: '2020-01-01',
  number: 1,
  organization: 'WSDCC',
  title_short: 'Title',
  wsdcc_action: wsdcc_action.submitted,
  wsdcc_issue: wsdcc_issue.unknown,
}}/>`
}</pre>

Use a `wsdcc_action`, which is an unambiguous mapping for WSDCC actions.
<ul>
{Object.entries(wsdcc_action).map(([name, id])=>(
  <li><code>{name}</code>: {wsdcc_action_names[id]}</li>
))}
</ul>

## Issues and Abbreviations
Select a `wsdcc_issue`, which is an unambiguous mapping for WSDCC abbreviations.
<ul>
{Object.entries(wsdcc_issue).map(([name, id])=>(
  <li><code>{name}</code>: {wsdcc_issue_doc[id]} ({wsdcc_issue_names[id]})</li>
))}
</ul>

</Layout>
