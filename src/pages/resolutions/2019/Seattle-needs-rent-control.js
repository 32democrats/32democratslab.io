import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = `Resolution: Seattle Needs Rent Control`
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-10-09">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p>WHEREAS, Seattle is in the throes of a stunning and unprecedented affordable housing and homelessness crisis with sky-high rents driving working-class households, seniors, students, and especially working families of color out of our city, and sometimes into homelessness; and</p>
<p>WHEREAS, Fifty percent of residents, in Seattle City Council District 5 which partially encompasses the 32nd LD, are renters; and</p>
<p>WHEREAS, the loss of affordable housing in Seattle is a crisis deeply impacting the lives of people throughout our city and region and disproportionately harms people of color, immigrants, the LGBTQ community, indigenous peoples’ communities, disabled community members, and women, who already struggle against entrenched inequality; and</p>
<p>WHEREAS, Washington State has banned any form of residential rent control since 1981 when, under real estate lobby influence, a law was passed in Olympia stating “No city or town of any class may enact, maintain, or enforce ordinances or other provisions which regulate the amount of rent to be charged”; and</p>
<p>WHEREAS, On April 15, renter rights advocates from the Tenants Union of Washington State, the City of Seattle Renters’ Commission, union members, renters organizing their buildings, immigrant rights activists, socialists, and Seattle City Councilmember Kshama Sawant announced plans to advocate for a citywide rent control ordinance in Seattle this year, that would become effective the moment the State Legislature lifts the rent control ban; and</p>
<p>WHEREAS, over the following three months, activists in Seattle collected more than 10,000 signatures from fellow residents demanding that the City take action on rent control,</p>
<p>NOW THEREFORE BE IT RESOLVED, the 32nd LD Democrats find that addressing the rapid loss of affordable housing in Seattle is of paramount importance to our members and residents of Seattle, and we fully support efforts expand the development of affordable social housing, and universal rent control policies free of corporate loopholes; and</p>
<p>BE IT FURTHER RESOLVED, the 32nd LD Democrats urge the Seattle City Council and the Mayor to provide urgent protection to all renters against exorbitant rent increases, by adopting a comprehensive citywide rent control policy, free of corporate loopholes, effective the moment Washington State repeals its rent control ban; and</p>
<p>BE IT FURTHER RESOLVED, the 32nd LD Democrats commit to urge its members to sign the rent control petition sponsored by Councilmember Kshama Sawant, and turn out as able for the Sept. 17 City Council Renters Rights Committee, at which Councilmember Sawant, as chair of the committee, will unveil draft rent control legislation; and</p>
<p>BE IT FURTHER RESOLVED, the 32nd LD Democrats urge the Washington State Legislature to lift the statewide ban on rent control by repealing RCW 32.21.830.</p>
<hr/>
<p>Submitted by:<br/>
Brent McFarlane<br/>
Sally Soriano</p>
</Layout>
)
