import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution Urging Enactment Of The “Do No Harm Act”'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-06-12">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p><strong>Whereas</strong> the Religious Freedom Restoration Act, 107 Stat. 1488 (1993) (“RFRA”), was enacted expressly to protect every person’s right to practice his or her religious faith, free of substantial burdens imposed by state or federal government; and</p>
<p><strong>Whereas</strong> the United States Supreme Court, in its infamous 2014 decision in <i>Burwell v. Hobby Lobby Stores</i>, embraced the ludicrous notion that even a commercial for-profit corporation is capable of having a religious faith and is therefore among the “persons” protected by RFRA, and</p>
<p><strong>Whereas</strong> the immediate result of the <i>Hobby Lobby</i> decision was to exempt closely held corporations, based on the religious faith of their controlling stockholders, from a federal requirement that employers facilitate their employees’ access to an array of birth control services; and</p>
<p><strong>Whereas</strong>, in the further wake of the <i>Hobby Lobby</i> decision, RFRA has been used to protect discrimination against women, religious and other minorities (particularly sexual minorities), all under the guise of religious freedom; and</p>
<p><strong>Whereas</strong> Congressmen Joe Kennedy (MA) and Bobby Scott (VA) and Senator Kamala Harris (CA) have introduced a proposed “Do No Harm Act” (H.R. 1450 and S. 593) that would amend RFRA to prevent it from being used to deny access to health care, undermine antidiscrimination laws, evade laws protecting children, or circumvent workplace protections;</p>
<p><strong>Therefore, be it resolved</strong> that we call on our Senators and Representatives in the U.S. Congress to sign on as co-sponsors of the “Do No Harm Act,” and to work to assure its prompt passage in their respective chambers; and</p>
<p><strong>Be it further resolved</strong> that our Senators and Representatives be queried within four months as to their actions and intended actions in furtherance of the above.</p>
<hr/>
<p>Adopted June 12, 2019 by the 32nd District Democrats</p>
<p>Submitted by Carin Chase, Edmonds, WA</p>
<p>Carin.Chase@32democrats.org</p>
</Layout>
)
