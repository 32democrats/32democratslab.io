import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution Opposing Initiative Measure No. 976'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-02-20">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p><strong>WHEREAS</strong> Tim Eyman, Mike Fagan, and Jack Fagan have sponsored Initiative 976, filed on March 19th, 2018, as an initiative to the 2019 Legislature;</p>
<p><strong>WHEREAS</strong> on January 15th, 2019, Washington Secretary of State Kim Wyman certified that Initiative 976 had received sufficient signatures to qualify;</p>
<p><strong>WHEREAS</strong> Initiative 976 seeks to repeal vehicle fees that provide essential funding to Amtrak Cascades and freight mobility projects administered by the Washington State Department of Transportation;</p>
<p><strong>WHEREAS</strong> Initiative 976 attempts to nullify the voter-approved Sound Transit 3 proposal, a plan to bring light rail and express buses to communities like Everett, Tacoma, West Seattle, and Ballard, by eliminating Sound Transit’s voter-approved motor vehicle excise tax (MVET) and requiring that Sound Transit defease bonds backed by motor vehicle excise tax revenue;</p>
<p><strong>WHEREAS</strong> the cities of Anacortes, Bainbridge Island, Battle Ground, Black Diamond, Bremerton, Bridgeport, Buckley, Burien, Carbonado, Covington, Des Moines, DuPont, Edgewood, East Wenatchee, Eatonville, Electric City, Elmer City, Edmonds, Enumclaw, Everett, Fife, George, Grandview, Granite Falls, Kalama, Kelso, Kenmore, Kittitas, Lake Forest Park, Lakewood, Longview, Lynnwood, Mabton, Maple Valley, Mercer Island, Moses Lake, Mountlake Terrace, Normandy Park, Olympia, Orting, Port Orchard, Prosser, Richland, Roy, Royal City, Seattle, Sedro-Woolley, Shoreline, Snoqualmie, Soap Lake, Spokane, Tacoma, Toppenish, University Place, Vancouver, Wapato, Wenatchee, Wilkeson, Yakima, and Zillah all currently levy vehicle fees to fund transportation improvements like road resurfacing and street maintenance which would be wiped out by I-976;</p>
<p><strong>WHEREAS</strong> Initiative 976 threatens good-paying jobs in construction and the building trades that allow thousands of workers to share in Washington’s economic prosperity;</p>
<p><strong>WHEREAS</strong> the availability, frequency, and quality of alternatives to solo driving that I-976 would gut, are crucial to Washington’s efforts to reduce emissions of pollutants that are damaging our climate and harming our air quality;</p>
<p><strong>WHEREAS</strong> implementation of I-976 would reduce voter-approved funding to expand light rail, build new express bus service, maintain bus service, build sidewalks, resurface roads, and calm traffic on both sides of the Cascade Mountains;</p>
<p><strong>NOW, THEREFORE BE IT RESOLVED</strong> that the 32nd LD Democrats take a position opposing I-976, and urge all Washington voters to oppose I-976.</p>
<p><strong>BE IT FURTHER RESOLVED</strong> that the 32nd LD Democrats encourage all who support Washingtonians’ freedom of mobility to join the coalition opposing I-976.</p>
</Layout>
)
