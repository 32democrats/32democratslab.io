import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution in support of I-1000'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-01-09">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p>
<b>Whereas,</b> Initiative 1000 was written to <b>restore Affirmative Action</b> for women, Native
Americans, African Americans, Asian Americans, Hispanic Americans, veterans and the disabled
in public education, employment and contracting; and
</p>
<p>
<b>Whereas;</b> diversity is America’s greatest asset, yet, since the passage of I-200 in 1998,
Washington State has been prohibited from using Affirmative Action to address inequities
arising from systemic racism, sexism, and other biases and is only one of 8 states in the country
that bans Affirmative Action; and
</p>
<p>
<b>Whereas,</b> since the passage of Initiative 200 over 20 years ago,
</p>
<ul>
<li>
Women and minority owned businesses have lost over $3.5 billion dollars in state
contracts; <i>Source: Office of Minority & Women Business Enterprises (OMWBE)</i>
</li>
<li>
Before I-200, nearly 70% of qualified American Indian freshmen who applied to
the UW were admitted. But in 2017, only 48% of qualified American Indian
freshmen who applied to the UW were admitted. <i>Source: UW</i>
</li>
<li>
Since the passage of I-200, American Indian teachers have dropped to 0.7% of the
state’s nearly 54,000 teachers. <i>Source: UW</i>
</li>
<li>
Since the passage of I-200, African-American unemployment has risen to nearly
7%, the highest of any ethnic community in the state. <i>Source: Washington State</i>
Employment Security
</li>
<li>
Since the passage of I-200, Hispanic unemployment has risen to nearly 6%, the
2nd highest in the state. <i>Source: Washington State Employment Security</i>
</li>
<li>
Since the passage of I-200, white males, who are only 38% of the state
population, have received over 80% of the Washington State Department
of Transportation contracts; <i>Source: Washington State Depart. of Transportation</i>
</li>
</ul>
<p>
<b>Whereas,</b> Initiative 1000’s goal is to remedy discrimination so everyone can benefit from our
state’s prosperous economy. I-1000 has already attracted nearly 200,000 signatures from
voters all across the state. This resolution supports the passage of I-1000 which will help
educate and secure support for the passage of Initiative 1000 in the next election; and
</p>
<p>
<b>Therefore, be it resolved,</b> the 32nd District Democrats support I-1000 which would repeal
I-200, to mitigate racial, gender, and other imbalances by bringing equity, justice, and fairness
back into public policy through Affirmative Action without the use of quotas or preferential
treatment; and
</p>
<p>
<b>Be it further resolved,</b> the 32nd District Democrats hereby support the passage of Initiative
1000 to restore Affirmative Action for Women, Native Americans, African Americans, Asian
Americans, Hispanic Americans, veterans and the disabled in public education, employment,
contracting and equitable treatment of all people of color.
</p>
<hr/>
<p>
Adopted January 9, 2019 by the 32nd District Democrats
</p>
</Layout>
)
