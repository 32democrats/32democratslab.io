import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Prioritize development of conventional rail'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-04-10">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p><strong>WHEREAS</strong> the world's foremost body of climate scientists, the Intergovernmental Panel on Climate Change, together with many members of Congress, recommend the development of an economic plan to rapidly mitigate the impending worldwide climate crisis and hardships associated with that crisis;</p>
<p><strong>WHEREAS</strong> it is incumbent upon us to respect the science underlying the urgency of the climate crisis and the need to mitigate it by rapidly reducing greenhouse gas emissions to about half of 2010 levels before the end of year 2030;</p>
<p><strong>WHEREAS</strong> doing so requires that the most effective and efficient solutions be implemented as quickly as possible in response to the urgency of this crisis, including transitioning to renewable energy;</p>
<p><strong>WHEREAS</strong> Washington State is likely to have a population influx of about 1.5 million new residents by 2030, which will put additional pressure on our congested highways, and will require transportation solutions beyond the scope or area served by Sound Transit;</p>
<p><strong>WHEREAS</strong> the electric power requirements to transition to renewable energy will be substantial and challenging to provide in sufficient quantity across all sectors;</p>
<p><strong>WHEREAS</strong> the transportation sector is a large contributor of carbon emissions and roadway vehicles amount to the largest portion of transportation carbon emissions;</p>
<p><strong>WHEREAS</strong> transportation by rail requires about 1/3 the energy requirement of roadway vehicles and conventional rail projects can be steadily implemented in segments thus providing immediate benefits in terms of carbon emissions reductions and service connectivity, and reducing roadway congestion, and electrification of rail provides an additional environmental benefit;</p>
<p><strong>WHEREAS</strong> the potential for rail development as an effective strategy for addressing climate solutions has been almost completely excluded in transportation legislation and environmental planning and the continuing addition of more highways for roadway vehicles has been the legislative priority;</p>
<p><strong>THEREFORE BE IT RESOLVED</strong> that conventional rail development, expansion, improvement, and electrification be preeminent strategies for transitioning to a renewable energy economy in Washington, the Pacific Northwest and beyond, and that it be recommended and prioritized in legislative planning concerning state and regional transportation funding and development and improvement projects, and transportation electrification projects, and that conventional rail be given priority over expanded highway development;</p>
<p><strong>THEREFORE BE IT FURTHER RESOLVED</strong> that we take a position recommending the rapid development, improvement and implementation of conventional rail, including electrification where feasible, as an effective and efficient climate mitigation solution;</p>
<p><strong>THEREFORE BE IT FINALLY RESOLVED</strong> that this resolution be sent to members of the Washington State legislature and the Governor for consideration while planning budget and transportation legislation.</p>
<hr/>
<p>Submitted by Washington State Democrats Environment and Climate Caucus members, Lael White, Thomas White, and Dean Fournier (32nd LD ECC) and Arvia Morris (43rd LD ECC).</p>
<p>Date of submission: 2019 March 7.</p>
<p>Intergovernmental Panel on Climate Change: <a href="https://www.ipcc.ch/sr15/">https://www.ipcc.ch/sr15/</a></p>
<p><a href="https://www.congress.gov/bill/116th-congress/house-resolution/">https://www.congress.gov/bill/116th-congress/house-resolution/</a></p>
<p>Central/Eastern WA Rail Service and Development Project: <a href="https://drive.google.com/file/d/17U9j0vwqZqhZ3fBo3vB8ltryevIgCtjW/view">https://drive.google.com/file/d/17U9j0vwqZqhZ3fBo3vB8ltryevIgCtjW/view</a></p>
</Layout>
)
