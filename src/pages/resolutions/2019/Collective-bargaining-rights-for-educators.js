import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Supporting collective bargaining rights for educators'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-04-10">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p><strong>WHEREAS</strong> the platforms of the 32nd District Democrats and the Washington State Democrats
call for the support of education employees’ rights to engage in collectively bargaining, receive
fair wages, and for Washington to fulfill its paramount duty to make ample provisions for the
education of all children; and
</p>
<p><strong>WHEREAS</strong> our platforms oppose charter schools and their lack of labor protections for teachers
and staff; and
</p>
<p><strong>WHEREAS</strong> State Senator Guy Palumbo, in partnership with Senator Mark Mullet, has
championed an amendment to Senate Bill SB 5313 that would effect a reduction in teacher pay
and restrictions on their collective bargaining rights; and
</p>
<p><strong>WHEREAS</strong> the amendment to SB 5313 would benefit charter schools in the state, in direct
opposition to the principles of the Democratic Party;
</p>
<p><strong>THEREFORE, BE IT RESOLVED</strong> that the 32nd Legislative District Democratic Organization
condemn the actions of Senator Guy Palumbo and Senator Mark Mullet for their support of this
attack on collective bargaining rights of public teachers and on full funding of our educational
system, and for supporting charter school funding; and
</p>
<p><strong>THEREFORE, BE IT FINALLY RESOLVED</strong> that a copy of this resolution be sent to Senator
Guy Palumbo and Senator Mark Mullet and posted on the Web site of the 32nd Legislative
District Democratic Organization.
</p>
<hr/>
<p>Submitted by the Labor Caucus of the Washington State Democratic Central Committee for
consideration at its April 7, 2019 meeting in Pasco. (Date Submitted 2019-04-06)</p>
<p>The Washington State Democratic Central Committee passed this resolution on April 7, 2019.</p>
<p>Modified and submitted to the Fifth District Democrats by Joshua Trupin, 5th LD state committee
member on April 8, 2019</p>
<p>Modified and submitted to the 32nd District Democrats by Carin Chase, 32nd LD state committee
member on April 10, 2019</p>
<p>Adopted by 32nd Legislative District Democratic Organization April 10, 2019 - Alan Charnley,
Chair</p>
</Layout>
)
