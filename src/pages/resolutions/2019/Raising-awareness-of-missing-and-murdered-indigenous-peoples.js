import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Raising awareness of missing and murdered indigenous peoples (Native)'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-02-20">2019 - [Adopted]</Link>
</p>
<p>
Timely and relevant: current House bill under consideration:
<br/>
<a href="https://app.leg.wa.gov/billsummary?BillNumber=1713&Initiative=false&Year=2019">
HB 1713 - 2019-20 Improving law enforcement response to missing and murdered Native American women.
</a>
<br/>
Sponsors: Mosbrucker, Gregerson, Caldier, Dye, Barkis, Corry, Sells, Lekanoff, Schmick, Orwall, Chandler, Hudgins, Ryu
</p>
<h1>
Raising awareness of missing and murdered indigenous peoples (Native)
</h1>
<p>
<b>WHEREAS,</b> the 32nd Legislative District Democratic Organization stands in solidarity with tribal, state and the federal government in raising awareness of Missing and Murdered Indigenous (Native) peoples; and 
</p>
<p>
<b>WHEREAS,</b> little data exists on the number of missing Native peoples in the United States and no comprehensive data base exists for Missing and Murdered Indigenous Peoples including women, men, children, and LGBTQ2/NB; and 
</p>
<p>
<b>WHEREAS,</b> recent Washington State legislation calling for a study to count missing Native women was approved and signed by the Governor, and federal legislation is also being proposed; and 
</p>
<p>
<b>WHEREAS,</b> Washington State Patrol reports 89 Native American Missing Persons as of May 5, 2018; and
</p>
<p>
<b>WHEREAS,</b> Washington State is rated second in the National Missing and Unidentified Persons System (NamUs) with 32 missing Native men and women currently listed; and 
</p>
<p>
<b>WHEREAS,</b> according to the Department of Justice, Washington is one of five states in which the percent of Native murder victims (4%) exceeds the state’s percentage of resident population (1.8%); and
</p>
<p>
<b>WHEREAS,</b> according to the Centers for Disease Control and Prevention, homicide was the third leading cause of death among Native peoples between 10 and 24 years of age; and
</p>
<p>
<b>WHEREAS,</b> Native trauma experts note the public is generally unaware that the disproportionate disappearances and murders of Native peoples directly correlates to multi-generational trauma with roots in a history of forced relocation, forced assimilation, boarding school legacy of abuse, familial separation, and cultural and spiritual practice deprivation, extreme generational poverty, adverse childhood experiences (ACEs), epigenetics, domestic violence, sexual assault, and human trafficking; and
</p>
<p>
<b>WHEREAS,</b> nearly half of all Native women in the United States have been raped, beaten, or stalked by an intimate partner; are 2.5 times more likely to experience sexual assault; one in three Native women will be raped in her lifetime; and Native women are murdered at a rate 10 times higher than the national average; and
</p>
<p>
<b>WHEREAS,</b> there is clear need for increased education, awareness, and transparent reporting of this crisis.
</p>
<p>
<b>NOW, THEREFORE BE IT RESOLVED,</b> that the 32nd Legislative District Democratic Organization does hereby support May 5th, annually, as the Day of Awareness for Missing and Murdered Indigenous Peoples; now
</p>
<p>
<b>THEREFORE, BE IT FINALLY RESOLVED</b> that the 32nd Legislative District Democratic Organization calls upon local, city, state and county law enforcement agencies to increase transparency and reporting of missing persons through mandatory data entry of all missing or unidentified persons found within their jurisdiction into National Missing and Unidentified Persons System (NamUs) within thirty days.
</p>
<hr/>
<p>
<b>DONE AND DATED</b> on this 13th day of February, 2019 by the 32nd Legislative District Democratic Organization.
</p>
<p>
Resolution originated by South Central Federation of Democratic Womxn 2019-01-23, adopted by Washington State Democratic Central Committee 2019-01-27, Washington State Federation of Democratic Women 2019-01-26
</p>
<p>
Submitted 2019-01-30 by Carin Chase PCO Holly
</p>
</Layout>
)
