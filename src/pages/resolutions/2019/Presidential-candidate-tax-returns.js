import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution supporting a legislative requirement that presidential and vice presidential candidates disclose eight years of tax returns in order to appear on Washington’s primary election ballot'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-01-09">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p>
<b>WHEREAS</b> a democratic norm has been established in that presidential candidates of all major parties have, for over forty years, following the failure of President Richard Nixon to fully comply with the tax code, made public disclosure of their tax returns; and
</p>
<p>
<b>WHEREAS</b> tax returns, as pointed out by former Republican presidential candidate Mitt Romney, “provide the public with its sole confirmation of the veracity of a candidate's representations regarding charities, priorities, wealth, tax conformance, and conflicts of interest,” and may reveal “inappropriate associations with foreign entities, criminal organizations, or other unsavory groups”; and
</p>
<p>
<b>WHEREAS</b> President Donald J. Trump, both while a candidate and continuing to this day, has not made public disclosure of his tax returns despite having vowed at least annually, from 2011-2017, to do so; and
</p>
<p>
<b>WHEREAS</b> we have a longstanding and revered tradition of supporting democratic norms and promoting greater transparency in government; and
</p>
<p>
<b>WHEREAS</b> the Washington state Democratic platform expressly calls for “Requiring presidential candidates to disclose their tax returns and all vested interests prior to getting their names on Washington’s ballot”; and
</p>
<p>
<b>WHEREAS</b> Washington state Senator Patty Kuderer (48th Legislative District) and Representative Derek Stanford (1st Legislative District) are introducing in 2019, in the State Senate and House, respectively, legislation to establish a procedural requirement that any and all presidential and vice presidential candidates, regardless of party, disclose their most recent eight years of tax returns in order to appear on a primary election ballot in the State of Washington;
</p>
<p>
<b>THEREFORE, BE IT RESOLVED</b> that we formally and enthusiastically support the Kuderer and Stanford Senate and House bills described above (official 2019 numbers to be determined);
</p>
<p>
<b>BE IT FURTHER RESOLVED</b> that this resolution be forwarded to our own state legislators, governor, and others, so that they may understand our firm belief that requiring presidential candidate tax-return disclosure is an essential democratic norm that must be restored for the benefit all voters, regardless of party affiliation, in all future primary elections; and
</p>
<p>
<b>BE IT FURTHER RESOLVED</b> that our own state legislators be queried, at an appropriate time, as to what action they have taken and intend to take in response to this resolution.
</p>
<hr/>
<p>
Adopted January 9, 2019 by the 32nd District Democrats
</p>
<hr/>
<p>
Originated by Carl Larson, carl.larson@presidentialtransparency.org
<br/>
PCO, Mountlake Terrace 15 (1st Legislative District)
</p>
</Layout>
)
