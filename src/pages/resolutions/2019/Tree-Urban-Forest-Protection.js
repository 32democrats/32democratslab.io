import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution in Support of the Seattle Urban Forestry Commission’s Draft Tree and Urban Forest Protection Ordinance'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-09-11">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p><strong>Whereas</strong> Seattle is losing not only its biggest trees, but much more of its green cover, as developers frequently scrape properties clean of trees to maximize their buildable area; and</p>
<p><strong>Whereas</strong> Seattle has failed to require developers to replace all “exceptional” trees and trees over 24” DBH (diameter at 54” high) as prescribed by SMC 25.11.090; and</p>
<p><strong>Whereas</strong> Seattle, unlike Portland and other major cities, has not instituted a permit system to govern tree removal on developed property or property being developed, but relies instead on a complaint-based system that is applicable only to already-developed property and does not protect trees even there; and</p>
<p><strong>Whereas</strong> the Seattle City Council voted in 2009, and again by Resolution 31870 in March 2019, to support an updating of its Tree Protection Ordinance; and</p>
<p><strong>Whereas</strong> the City concluded, in its 2017 Tree Regulations Research Report, that “Current code is not supporting tree protection” and that “we are losing exceptional trees (and groves) in general”; and</p>
<p><strong>Whereas</strong> Seattle’s trees and urban forest comprise a vital green infrastructure that (a) reduces air pollution, stormwater runoff, and climate change impacts like heat-island effects, while providing essential habitat for birds and other wildlife, and (b) is important for the physical and mental health of Seattle residents: and</p>
<p> <strong>Whereas</strong> the Seattle Urban Forestry Commission has drafted, at the suggestion of several City Council members, an updated Tree and Urban Forest Protection Ordinance consistent with the eight recommendations specified by the Council in Section 6 of its Resolution 31870, which would:</p>
<ol style={{'list-style-type':'none'}}>
<li>a. increase protections for Seattle trees and tree canopy volume by requiring, in each land-use zone in the city, a permit for removal and replacement of any tree over 6” DBH, whether on developed property or property being developed;</li>
<li>b. require two-week on-site posting of tree removal and replacement applications (as SDOT already requires);</li>
<li>c. require on-site tree replacement equivalent, in 25 years, to the tree canopy volume removed, <em><u>or</u></em> payment of a fee into a Tree Replacement and Preservation Fund for planting and 5-year maintenance of trees elsewhere in the city;</li>
<li>d. retain current protections for exceptional trees and reduce the definitional upper threshold for exceptional trees to 24” DBH;</li>
<li>e. allow no more than two significant non-exceptional trees to be removed over 3 years on developed property;</li>
<li>f. require registration of all tree services providers with the city;</li>
<li>g. track all significant tree loss and replacement; and</li>
<li>h. provide adequate funds to administer and enforce the ordinance.</li>
</ol>
<p><strong>Therefore, be it resolved</strong> that, in recognition of the environmental importance of trees and the urgent need to update and strengthen Seattle’s current ordinance, we urge the Seattle City Council to enact, and the Mayor to sign and enforce, the Seattle Urban Forestry Commission’s June 14, 2019 draft Tree and Urban Forest Protection Ordinance.</p>
<hr/>
<p>Adopted 2019-09-11 by the 32nd Legislative District Democrats Organization</p>
<p>Originated by the Coalition for a Stronger Tree Ordinance, stevezemke@TreePAC.org</p>
</Layout>
)
