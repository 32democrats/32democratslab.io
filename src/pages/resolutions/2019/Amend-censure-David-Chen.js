import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'
import {resolution_status} from '../../../components/resolution'

const file = 'Amend-censure-David-Chen'
const title = `A resolution to amend the September 11, 2019, censure of Shoreline City Council candidate David Chen in regards to his employer CRISTA Ministries for their anti-LGBTQ+ actions and Statements`
const minutes_date = '2019-10-09'
const status = resolution_status.rejected

export const r = {file, minutes_date, status, title}
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-10-09">2019 - [Rejected]</Link>
</p>
<h1>{title}</h1>
<p>Whereas the platform of the 32nd Legislative District Democrats declares support for:</p>
<p>"Privacy as a basical human right that the government and the private sector at all levels must acknowledge in their rules and regulations." (118/119),</p>
<p>"The right of every adult to marry another person without regard to gender and to enjoy the same civil and legal rights accorded to all married persons." (120/121);</p>
<p>Whereas the platform of the 32nd Legislative District Democrats declares opposition to:</p>
<p>"Discrimination in voting, employment, housing, public accommodations, healthcare, military service and veterans' status, insurance, licensing, or education based on race, ethnicity, religion, age, sex, sexual orientation, gender identity, disability, size, socioeconomic status, political affiliation, and national origin or immigration status." (161-164)</p>
<p>Whereas David Chen publicly announced in a letter Thursday morning, Sept. 12, 2019:</p>
<p> </p>
<p>“The letter sent by my former employer in July detailing their anti-LGBTQ+ beliefs made working for them untenable. The positions reflected in CRISTA’s letter are wrong. This kind of discrimination by an organization that is supposed to support and educate students is completely unacceptable. As the father of three young children, I’m proud to stand up for what I believe in and lead by example.”</p>
<p>Whereas, in a phone interview Wednesday, Sept. 11, 2019, with the Everett Herald ("Shoreline candidate leaves King’s Schools over LGBTQ stance"), reported Chen said the same and that Chen told the <em>Herald</em> he already had informed CRISTA President and CEO Jacinta Tegman on Monday, Sept. 9, 2019, of his planned resignation after learning of the ongoing discrimination at CRISTA schools, <em>be it Resolved</em>;</p>
<p>1. That David Chen's censure and consequential opprobrium be rescinded by the 32nd Legislative District Democrats.</p>
<p>2. That the 32nd Legislative District Democrats censure remain in place on his employer CRISTA Ministries and its President and CEO Jacinta Tegman.</p>
<p><em>This resolution will be distributed to press and governmental bodies by the 32nd Legislative District Democrats.</em></p>
<hr/>
<p>PCO SEA 32-2537<br />
(509) 818-0638</p>
</Layout>
)
