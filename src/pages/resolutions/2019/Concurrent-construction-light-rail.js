import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Support for concurrent construction of the NE 130th Street light rail station and Lynnwood Link Extension'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-03-13">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p><strong>Whereas</strong> Sound Transit has begun preliminary construction on the Lynnwood Link light rail extension approved by voters in 2008; and</p>
<p><strong>Whereas</strong> a future light rail station at NE 130th Street and Interstate 5 on the Lynnwood Link line was included in the Sound Transit 3 package approved by voters in 2016; and</p>
<p><strong>Whereas</strong> North Seattle community members came together in a visible and effective advocacy campaign for the NE 130th Street Light Rail Station; and</p>
<p><strong>Whereas</strong> voters in North Seattle overwhelmingly approved Sound Transit 3; and</p>
<p><strong>Whereas</strong> it can take over an hour to drive a single-occupancy vehicle from NE 130th Street to downtown Seattle on I-5 during rush hour; and</p>
<p><strong>Whereas</strong> travel from the NE 130th Street Station to Westlake Station in downtown Seattle is expected to take fewer than 20 minutes; and</p>
<p><strong>Whereas</strong> the NE 130th Street Station will serve the Lake City and Bitter Lake Hub Urban Villages, which are home to large concentrations of immigrants, families, and seniors; and</p>
<p><strong>Whereas</strong> carbon emissions from travel on light rail are a small fraction of emissions from travel using fossil-fuel powered single-occupancy vehicles; and</p>
<p><strong>Whereas</strong> Sound Transit has noted potential cost savings by constructing the NE 130th Station concurrently with Lynnwood Link, when compared to the cost of constructing the NE 130th Street station as an infill station; and</p>
<p><strong>Whereas</strong> Sound Transit’s Graham Street Station was originally budgeted at a level of $5.2 million in 1999 when it was scheduled to be built concurrently with ST1 projects, and is now estimated to cost $66 to $71 million and open in 2031 as an infill station; and</p>
<p><strong>Whereas</strong> Sound Transit’s Board unanimously approved Motion No. M2019-04 at its Jan 24th, 2019 meeting, approving design work on the NE 130th Street Station which will inform the Board’s future consideration of constructing NE 130th Street Station concurrently with the Lynnwood Link projects; and</p>
<p><strong>Whereas</strong> Sound Transit’s Board has recognized that constructing NE 130th Street Station concurrently with Lynnwood Link will eliminate the potential for harmful service disruptions to King and Snohomish Counties associated with the construction of an infill station; and</p>
<p><strong>Whereas</strong> we stand in solidarity with our neighbors and fellow Lynnwood LINK light rail users in Snohomish County who deserve to use Lynnwood LINK without service disruption due to infill construction; and</p>
<p><strong>Whereas</strong> Lynnwood Link is scheduled to open in 2024, and NE 130th Street is currently scheduled to open as an infill station in 2031;</p>
<p><strong>Therefore, be it resolved</strong> that we support the concurrent construction of Lynnwood Link and the NE 130th Street Station, with both opening for service in 2024 or sooner; and</p>
<p><strong>Be it further resolved</strong> that copies of this resolution be sent to the Sound Transit Board, and to our Legislative District Representatives.</p>
<hr/>
<p>Passed <u>2019-03-13</u> by 32nd LD, <u>Alan Charnley</u> Chair</p>
</Layout>
)
