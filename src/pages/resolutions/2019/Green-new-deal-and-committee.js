import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution supporting the plan for a green new deal and creation of the select committee for the green new deal.'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-01-09">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p>
<b>WHEREAS,</b> on April 22, 2016, world leaders from 175 countries recognized the threat of climate change and the urgent need to combat it by signing the Paris Agreement and agreeing to “pursue efforts to limit the temperature increase to 1.5°C,” which is already an alarmingly high level;
</p>
<p>
<b>WHEREAS,</b> on October 8, 2018, the United Nations released a special report from the Intergovernmental Panel On Climate Change, criticized by many leading climate scientists as overly conservative, which projected that limiting warming to the already unsafe 1.5°C target this century will require an unprecedented transformation of every sector of the global economy over the next 12 years;
</p>
<p>
<b>WHEREAS,</b> the death and destruction already wrought by global warming of approximately 1.1°C above late 19​th Century levels demonstrates that the Earth is already too hot for safety and justice as attested to by increased and intensifying wildfires, floods, rising seas, diseases, droughts and other extreme weather events.
</p>
<p>
<b>WHEREAS,</b> on November 23, 2018 the Federal Government issued a climate study detailing the massive threat that climate change poses to the American economy and underscoring the need for immediate climate emergency action at all levels of government;
</p>
<p>
<b>WHEREAS,</b> the global economy’s overshoot of ecological limits may be driving the sixth mass extinction of species, and has caused a 60% decline in global wildlife populations since 1970, and could devastate much of life on Earth for the next 10 million years, and may pose as great a risk to humanity as climate change, according to the Intergovernmental Science-Policy Platform on Biodiversity and Ecosystem Services; and the United States of America has disproportionately contributed to these climate emergencies and has repeatedly obstructed global efforts to transition toward a green economy, and thus bears an extraordinary responsibility to rapidly address these existential threats by enacting measures to remediate the problem as an urgent priority;
</p>
<p>
<b>WHEREAS,</b> restoring a safe and stable climate requires a massive mobilization at all levels of society on a scale not seen since the civilian mobilization of World War II, and such mobilization is needed now in order to reach zero greenhouse gas emissions across all sectors quickly, to rapidly and safely drawdown or remove all the excess carbon from the atmosphere, to prevent the 6th mass extinction of species, and to implement measures to protect oceans, atmosphere, land, and species from the increasingly severe consequences of climate change;
</p>
<p>
<b>WHEREAS,</b> Representative Alexandria Ocasio-Cortez has proposed that Congress create a House Select Committee for the Green New Deal which would be charged with “developing a detailed national, industrial, economic mobilization plan for the transition of the United States economy to become carbon neutral and to significantly draw down and capture greenhouse gases from the atmosphere and oceans and to promote economic and environmental justice and equality”;
</p>
<p>
<b>WHEREAS,</b> the Plan for a Green New Deal requires a just transition from a carbon-based economy to a carbon-neutral economy. This includes dramatically expanding renewable power sources and deploying new production capacity with the goal of meeting 100% of the national power demand through renewable sources; building a national energy-efficient “smart” grid; upgrading building construction for state-of-the-art efficiency, comfort, and safety, curtailing greenhouse gas emissions from manufacturing, agricultural, transportation, and other industries, which will include investing in local-scale, sustainable agriculture, upgrading water infrastructure to ensure the universal right to clean water; funding massive investment in the drawdown and removal of greenhouse gases from all sources; and making “green” technology, industry, expertise, products, and services a major export of the United States, with the aim of becoming an international leader in helping other countries transition to completely greenhouse gas neutral economies and bringing about a global Green New Deal.
</p>
<p>
<b>WHEREAS,</b> the Plan for a Green New Deal, due to the national, industrial, and economic mobilization of this scope and scale is an historic opportunity to eliminate poverty in the United States and to make prosperity, wealth, and economic security available to everyone while transitioning to a green economy. This includes federal job guarantees; diversification of local and regional economies away from fossil fuels; strong enforcement of labor, workplace safety, and wage standards; protecting communities from climate change and environmental pollution; protecting the sovereign rights and land rights of tribal nations; mitigating deeply entrenched racial, regional, and gender-based inequalities in income and wealth; ensuring federal and other investment will be equitably distributed to historically impoverished, low-income, deindustrialized or other marginalized communities in such a way that builds wealth, ownership, and equity at a community level; implementing additional measures which promote economic security such as basic income programs, universal health care, universal housing security, labor market flexibility and entrepreneurship, and deeply involving national and local labor unions to take a leadership role in the process of job training and worker deployment;
</p>
<p>
<b>WHEREAS,</b> the plan for a Green New Deal must be implemented, overseen, and protected as a program that functions for the express benefit of the public good and environmental preservation and not become dominated by the goals of corporate enterprise.
</p>
<p>
<b>THEREFORE BE IT RESOLVED,</b> that we support the Plan for a Green New Deal and creation of the Select Committee for the Green New Deal, and we demand accountability from industry and government, and respect for and adherence to best practices in science and law regarding quickly promoting and enacting climate justice measures;
</p>
<p>
<b>BE IT FURTHER RESOLVED,</b> that we demand that the Democratic leadership in Congress, create a Select Committee for A Green New Deal, with the full powers that select committees have had in the past rather than reactivating an out of date select committee with no subpoena power.
</p>
<p>
<b>BE IT FINALLY RESOLVED,</b> that we will thank Representative Pramila Jayapal and other supportive members of Congress, that we will send this resolution to U.S. Representative Rick Larsen, U.S. House Speaker Nancy Pelosi, U.S. Senator Patty Murray and U.S. Senator Maria Cantwell to demand that they support the Plan for a Green New Deal as well as the creation of the Select Committee for a Green New Deal. We also request their response to this resolution and how they intend to support the Plan for a Green New Deal as well as the Select Committee being proposed for that purpose.
</p>
<hr/>
<p>
Adopted January 9, 2019 by the 32nd District Democrats
</p>
<hr/>
<p>
Sponsored by Gray Petersen, Snohomish County Committee Member
<br/>
Co-sponsored by Liz Brown, Carolyn Ahlgreen, Lael White, Brent McFarlane
<br/>
Resolution partial origination: <a href="​https://www.theclimatemobilization.org/">​https://www.theclimatemobilization.org/</a>
<br/>
IPCC Report: <a href="https://www.ipcc.ch/sr15/">https://www.ipcc.ch/sr15/</a>
<br/>
Climate Study Report from the U.S. Government: <a href="https://nca2018.globalchange.gov/">https://nca2018.globalchange.gov/</a>
<br/>
Green New Deal Committee Resolution: <a href="http://jdems.us/GND">http://jdems.us/GND</a>
<br/>
IPBES: <a href="https://www.ipbes.net/">https://www.ipbes.net/</a>
<br/>
A future with the Green New Deal: <a href="https://theintercept.com/2018/12/05/green-new-deal-proposal-impacts/">https://theintercept.com/2018/12/05/green-new-deal-proposal-impacts/</a>
</p>
</Layout>
)
