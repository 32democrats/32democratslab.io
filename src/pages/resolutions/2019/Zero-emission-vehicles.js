import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution Urging WA Adoption of the Zero Emissions Vehicle (ZEV) Program'
export default () => (
<Layout>
<div className="article">
<Helmet>
	<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-11-13">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>

<p><strong>WHEREAS</strong> the climate crisis requires bold action to reduce carbon emissions<a href="#fn1" className="footnote-ref" id="fnref1" role="doc-noteref"><sup>1</sup></a>; and</p>
<p><strong>WHEREAS</strong> gasoline is the single largest source of carbon emissions in Washington State<a href="#fn2" className="footnote-ref" id="fnref2" role="doc-noteref"><sup>2</sup></a>; and</p>
<p><strong>WHEREAS</strong> the electricity that powers electric vehicles in Washington State is overwhelmingly produced by renewable energy<a href="#fn3" className="footnote-ref" id="fnref3" role="doc-noteref"><sup>3</sup></a>; and</p>
<p><strong>WHEREAS</strong> electric vehicles do not emit carbon or other pollutants, nor leak motor oil into roads, soils, or waterways, thereby reducing risks to human health, the environment, and the survival of wildlife; and</p>
<p><strong>WHEREAS</strong> the cost of electric vehicles is at or in approximate parity with comparable gasoline-powered vehicles on a lifetime cost-of-ownership basis<a href="#fn4" className="footnote-ref" id="fnref4" role="doc-noteref"><sup>4</sup></a>; and</p>
<p><strong>WHEREAS</strong> ten states – California, Oregon, New York, Massachusetts, New Jersey, Connecticut, Maine, Maryland, Rhode Island, and Vermont – have adopted the Zero Emissions Vehicle (ZEV) program<a href="#fn5" className="footnote-ref" id="fnref5" role="doc-noteref"><sup>5</sup></a> first developed in California, and the state of Colorado is now close to adopting it as well<a href="#fn6" className="footnote-ref" id="fnref6" role="doc-noteref"><sup>6</sup></a>; and</p>
<p><strong>WHEREAS</strong> the ZEV program requires that 2.5% of each automaker’s annual vehicle sales in the participating state be electric by 2019, increasing to approximately 8% by 2025<a href="#fn7" className="footnote-ref" id="fnref7" role="doc-noteref"><sup>7</sup></a>; and</p>
<p><strong>WHEREAS</strong> the ZEV program has already resulted in a greater availability and variety of electric vehicles, more consumer choice, and lower prices, in the states that have adopted it<a href="#fn8" className="footnote-ref" id="fnref8" role="doc-noteref"><sup>8</sup></a> – which do not yet include Washington;</p>
<p><strong>THEREFORE, BE IT RESOLVED</strong> that we urge the Washington State Legislature to promptly enact legislation to adopt the ZEV program; and</p>
<p><strong>BE IT FURTHER RESOLVED</strong> that this resolution, when adopted, be sent promptly to Governor Inslee and to each of our State Senators and Representatives.</p>
<hr />
<p><strong>Adopted 2019-11-13 by the 32nd Legislative District Democratic Organization</strong></p>
<p>Submitted by Lael White &amp; Dean Fournier;</p>
<p>Previously adopted by LDs 43, 44, 5, and 46.</p>
<section className="footnotes" role="doc-endnotes">
<hr />
<ul style={{listStyle:'none',margin:0}}>
<li id="fn1" role="doc-endnote"><a href="#fnref1" role="doc-backlink">[1]</a> <a href="https://www.ipcc.ch/2018/10/08/summary-for-policymakers-of-ipcc-special-report-on-global-warming-of-1-5c-approved-by-governments/">https://www.ipcc.ch/2018/10/08/summary-for-policymakers-of-ipcc-special-report-on-global-warming-of-1-5c-approved-by-governments/</a>
</li>
<li id="fn2" role="doc-endnote"><a href="#fnref2" role="doc-backlink">[2]</a> <a href="https://fortress.wa.gov/ecy/publications/documents/1802043.pdf">https://fortress.wa.gov/ecy/publications/documents/1802043.pdf</a></li>
<li id="fn3" role="doc-endnote"><a href="#fnref3" role="doc-backlink">[3]</a> <a href="https://www.eia.gov/state/?sid=WA#tabs-4">https://www.eia.gov/state/?sid=WA#tabs-4</a>
</li>
<li id="fn4" role="doc-endnote"><a href="#fnref4" role="doc-backlink">[4]</a> <a href="https://www.forbes.com/sites/constancedouris/2017/10/24/the-bottom-line-on-electric-cars-theyre-cheaper-to-own/#38ebac7a10b6">https://www.forbes.com/sites/constancedouris/2017/10/24/the-bottom-line-on-electric-cars-theyre-cheaper-to-own/#38ebac7a10b6</a></li>
<li id="fn5" role="doc-endnote"><a href="#fnref5" role="doc-backlink">[5]</a> <a href="https://www.c2es.org/document/us-state-clean-vehicle-policies-and-incentives/">https://www.c2es.org/document/us-state-clean-vehicle-policies-and-incentives/</a></li>
<li id="fn6" role="doc-endnote"><a href="#fnref6" role="doc-backlink">[6]</a> <a href="http://blogs.edf.org/climate411/2019/05/10/colorado-charges-forward-with-zero-emission-vehicle-proposal/">http://blogs.edf.org/climate411/2019/05/10/colorado-charges-forward-with-zero-emission-vehicle-proposal/</a>
</li>
<li id="fn7" role="doc-endnote"><a href="#fnref7" role="doc-backlink">[7]</a> <a href="https://www.ucsusa.org/clean-vehicles/california-and-western-states/what-is-zev">https://www.ucsusa.org/clean-vehicles/california-and-western-states/what-is-zev</a></li>
<li id="fn8" role="doc-endnote"><a href="#fnref8" role="doc-backlink">[8]</a> <a href="http://ev-vin.blogspot.com/2016/07/current-lease-offers-for-selected-evs.html">http://ev-vin.blogspot.com/2016/07/current-lease-offers-for-selected-evs.html</a></li>
</ul>
</section>
</div>
</Layout>
)
