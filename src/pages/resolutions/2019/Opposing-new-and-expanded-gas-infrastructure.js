import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution opposing new and expanded gas infrastructure in Washington state'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-03-13">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p>WHEREAS the world’s foremost body of climate scientists, the Intergovernmental Panel On Climate Change reports that we have until 2030 to limit global warming to 1.5C to mitigate devastating worldwide climate impacts such as extreme temperatures, heavy rains, drought, biodiversity loss, and depletion of freshwater sources;</p>
<p>WHEREAS if we do not take immediate action to radically reduce our emissions in line with the Paris Target of 1.5C, these climate impacts will intensify, with communities of color, low-income communities, Tribes and coastal communities being often impacted first and worst;</p>
<p>WHEREAS methane has a global warming potential 86 times higher than carbon dioxide over a 20-year period and “natural” gas is comprised of 85-95% methane, and methane leaks from fracking and gas pipelines eliminate any beneficial climate impacts of gas production, and make gas just as bad or worse for the climate compared to coal;</p>
<p>WHEREAS the Interior Department tripled the amount of federal lands leased out for oil and gas parcels in the past fiscal year and simultaneously the EPA rolled back protections for wildlife, air quality, and groundwater supplies, placing the profits of the fossil fuel industry before the health of United States residents;</p>
<p>WHEREAS Washington State is already feeling the impacts of climate change: record-breaking wildfires have devastated our communities and caused toxic air pollution state-wide; our snow packs have plummeted, decreasing available drinking water and hydroelectric capacity; our salmon and orca populations have become critically endangered; our agricultural, fisheries and shellfish industries have suffered billion-dollar losses; and the Quinault Nation has already been forced to relocate two villages because of rising sea levels;</p>
<p>WHEREAS Washington is currently facing multiple proposals for fracked gas mega-projects such as the Kalama methanol refinery and the Tacoma liquefied natural gas (LNG) facility that is currently being built without all required permits on Medicine Creek Treaty Territory and without the Puyallup Tribe's consent and in violation of treaties;</p>
<p>WHEREAS such fracked gas mega-projects undermine Washington’s climate leadership and efforts to reduce carbon pollution, and pose staggering health, safety and environmental risks to local communities by causing toxic air and water pollution at every stage of production;</p>
<p>WHEREAS there is a well-documented rise in incidents of violence against Indigenous women and women of color in communities near gas extraction sites, including those in British Columbia where the majority of fracked gas consumed in Washington is extracted;</p>
<p>WHEREAS proposed fracked gas projects like the Tacoma LNG facility use non-commercial ratepayer dollars for private gain - 43% of project construction costs are passed on to utility customers, although they will only see 2% of the project’s benefit;</p>
<p>WHEREAS in January 2019, King County Council approved a 6-month moratorium on new and expanded fossil fuel infrastructure, joining four other local jurisdictions in Washington, Portland Oregon, Portland Maine and Baltimore Maryland in enacting a de facto ban on new fossil fuel infrastructure projects;</p>
<p>WHEREAS investment in new and expanded gas infrastructure and exports will lock us into decades of climate pollution and will divert money from renewable energy development, slowing our essential immediate greenhouse gas reduction and repressing the growth of green jobs in Washington;</p>
<p>WHEREAS Washington ports serve as a gateway for fossil fuel companies to export gas from the interior of North America to Asian markets, and by opposing new and expanded gas infrastructure Washington can play a critical leadership role in transitioning our global economy to clean energy and keeping fossil fuels in the ground;</p>
<p>THEREFORE BE IT RESOLVED that we strongly oppose federal efforts to roll back regulations on gas production, processing, storage and export, especially when those regulatory changes lead to increased health and safety risks for Washington communities;</p>
<p>THEREFORE BE IT FURTHER RESOLVED that we call on the Washington State Department of Ecology to consider a proposed project’s complete lifecycle greenhouse gas emissions, including analysis of methane leakage during extraction and transport, and reject permits for any project which would increase emissions;</p>
<p>THEREFORE BE IT FURTHER RESOLVED that we call for a permanent moratorium on new gas pipelines, processing, and storage facilities in Washington other than maintenance or replacement for safety purposes, and a stop-work order on projects currently under construction that do not have all their required permits;</p>
<p>THEREFORE BE IT FINALLY RESOLVED that we call on each and every one of our Washington State and federal representatives to oppose new and expanded gas infrastructure in WA, including the Kalama methanol refinery and Tacoma LNG facility.</p>
<hr/>
<p>Submitted by Jessica Wallach with co-sponsor Stacy Oaks, 350 Seattle, and Lael White, Environmental and Climate Caucus of the Washington State Democrats for consideration by the Washington State Democratic Central Committee at its April 2019 meeting in Pasco, Washington.</p>
<p>References:
<br/>
IPCC Report <a href="https://www.ipcc.ch/2018/10/08/summary-for-policymakers-of-ipcc-special-report-on-global-warming-of-1-5c-approved-by-governments/">https://www.ipcc.ch/2018/10/08/summary-for-policymakers-of-ipcc-special-report-on-global-warming-of-1-5c-approved-by-governments/</a>
<br/>
Disproportionate climate impacts: <a href="https://cig.uw.edu/wp-content/uploads/sites/2/2018/08/AnUnfairShare_WashingtonState_August2018.pdf">https://cig.uw.edu/wp-content/uploads/sites/2/2018/08/AnUnfairShare_WashingtonState_August2018.pdf</a>
<br/>
Methane emissions 86 times higher global warming potential than carbon dioxide: <a href="http://science.sciencemag.org/content/361/6398/186">http://science.sciencemag.org/content/361/6398/186</a>
<br/>
“Natural” gas comprised of 85-95% methane: <a href="https://www.sightline.org/2019/02/12/methane-climate-change-co2-on-steroids/">https://www.sightline.org/2019/02/12/methane-climate-change-co2-on-steroids/</a>;
<br/>
Methane leakage eliminates promised benefits of gas compared to coal: <a href="https://thinkprogress.org/methane-leaks-erase-climate-benefit-of-fracked-gas-countless-studies-find-8b060b2b395d/">https://thinkprogress.org/methane-leaks-erase-climate-benefit-of-fracked-gas-countless-studies-find-8b060b2b395d/</a>
<br/>
Triple the number of federal lands leased for drilling; regulation rollbacks: <a href="https://www.nytimes.com/2018/10/27/climate/trump-fracking-drilling-oil-gas.html">https://www.nytimes.com/2018/10/27/climate/trump-fracking-drilling-oil-gas.html</a>
<br/>
Washington already feeling impacts of climate change: <a href="https://cig.uw.edu/wp-content/uploads/sites/2/2019/02/NoTimeToWaste_CIG_Feb2019.pdf">https://cig.uw.edu/wp-content/uploads/sites/2/2019/02/NoTimeToWaste_CIG_Feb2019.pdf</a>
<br/>
Quinault Tribe relocates two villages: <a href="https://earthjustice.org/blog/2018-march/climate-change-forces-the-quinault-tribe-to-seek-higher-ground">https://earthjustice.org/blog/2018-march/climate-change-forces-the-quinault-tribe-to-seek-higher-ground</a>
<br/>
Climate change impacts vulnerable communities: <a href="https://www.huffingtonpost.com/rosaly-byrd/climate-change-is-a-socia_b_5939186.html">https://www.huffingtonpost.com/rosaly-byrd/climate-change-is-a-socia_b_5939186.html</a>
<br/>
Kalama Methanol refinery: <a href="https://www.sightline.org/research_item/fracked-fuel-and-petrochemical-proposals-in-oregon-washington/">https://www.sightline.org/research_item/fracked-fuel-and-petrochemical-proposals-in-oregon-washington/</a>
; <a href="https://www.sei.org/publications/assessing-gas-methanol-plant/">https://www.sei.org/publications/assessing-gas-methanol-plant/</a>
<br/>
Utility customers bear 43% of construction costs and receive 2% benefit: <a href="https://actionnetwork.org/petitions/fracked-gas?fbclid=IwAR0oLSCH3zUCll6nn1TG2KeyPekZEBi8YEqFunKiecl0lf_77POCa-8Pg2k">https://actionnetwork.org/petitions/fracked-gas?fbclid=IwAR0oLSCH3zUCll6nn1TG2KeyPekZEBi8YEqFunKiecl0lf_77POCa-8Pg2k</a>
<br/>
Gas industry’s history of violating treaties: <a href="https://www.policyalternatives.ca/sites/default/files/uploads/publications/BC%20Office/2017/06/ccpa-bc_Fracking-FirstNations-Water_Jun2017.pdf">https://www.policyalternatives.ca/sites/default/files/uploads/publications/BC%20Office/2017/06/ccpa-bc_Fracking-FirstNations-Water_Jun2017.pdf</a>
; <a href="https://lrinspire.com/2017/03/13/puyallup-tribes-treaty-right-to-fish-threatened-by-proposed-liquefied-natural-gas-plant/">https://lrinspire.com/2017/03/13/puyallup-tribes-treaty-right-to-fish-threatened-by-proposed-liquefied-natural-gas-plant/</a>
<br/>
Toxic air and water pollution: <a href="https://www.psr.org/wp-content/uploads/2018/05/too-dirty-too-dangerous.pdf">https://www.psr.org/wp-content/uploads/2018/05/too-dirty-too-dangerous.pdf</a>
<br/>
Violence, sexual assault near extraction sites: <a href="http://www.honorearth.org/man_camps_fact_sheet">http://www.honorearth.org/man_camps_fact_sheet</a>
; <a href="http://nativenewsonline.net/currents/un-special-rapporteur-oil-gas-mining-operations-brings-increased-sexual-violence/">http://nativenewsonline.net/currents/un-special-rapporteur-oil-gas-mining-operations-brings-increased-sexual-violence/</a>
</p>
</Layout>
)
