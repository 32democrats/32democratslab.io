import { Link } from 'gatsby'
import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../../components/layout'

const title = 'Resolution Supporting Whole Washington State Universal Healthcare Bill (SB 5222)'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p>
<Link to="/minutes/2019-02-20">2019 - [Adopted]</Link>
</p>
<h1>{title}</h1>
<p>
<strong>WHEREAS,</strong> Article 25 of the United Nations Universal Declaration of Human Rights sets forth that "Everyone has the right to a standard of living adequate for the health and well-being of himself and of his family, including food, clothing, housing and medical care and necessary social services, and the right to security in the event of unemployment, sickness, disability, widowhood, old age or other lack of livelihood in circumstances beyond his control" establishes a fundamental right to healthcare <a href="#c1">[1]</a>;
</p>
<p>
<strong>WHEREAS,</strong> in the Declaration of Independence, a foundational document of the Constitution of the United States, declares the responsibility of government to protect certain unalienable rights which include but are not limited to the preservation of "Life, Liberty, and the pursuit of Happiness," we conclude that healthcare is a prerequisite for these unalienable rights, and as such is indeed a human right;
</p>
<p>
<strong>WHEREAS,</strong> 522,000 Washington State Residents have no health coverage, more are underinsured, and lives could be saved by ensuring access to healthcare;
</p>
<p>
<strong>WHEREAS,</strong> the 2018 platform of the Washington State Democratic Party affirms that "healthcare is a basic human right," and that "An affordable universal single-payer system to provide the most equitable and effective health care, serving both individual and public health needs <a href="#c2">[2]</a>" and;
</p>
<p>
<strong>WHEREAS,</strong> the 2018 State Democratic Party Platform calls on "Washington State to establish a single-payer system for Washington residents until such time as a single-payer system is enacted nationally;"
</p>
<p>
<strong>WHEREAS,</strong> a single-payer resolution was adopted by the Washington State Democratic Central Committee in April of 2017 <a href="#c3">[3]</a>;
</p>
<p>
<strong>WHEREAS,</strong> people of color, people with disabilities, and low income people are the most likely to lack affordable, comprehensive healthcare <a href="#c4">[4]</a>;
</p>
<p>
<strong>WHEREAS,</strong> a vast majority of Americans now support Medicare-for-all, otherwise known as single-payer healthcare, according to an August 2018 Reuters survey- 85 percent of Democrats and 52 percent of Republicans <a href="#c5">[5]</a>;
</p>
<p>
<strong>WHEREAS,</strong> Whole Washington has crafted a bill <a href="#c6">[6]</a> guaranteeing universal healthcare to all Washingtonians, with a funding mechanism devised by a noted economist, reviewed by the Department of Revenue and the Employment Securities Department, which demonstrates that Washington state residents will save billions of dollars annually when universal healthcare is implemented;
</p>
<p>
<strong>THEREFORE, BE IT RESOLVED</strong> that the 32nd Legislative District Democrats endorse Whole Washington state universal healthcare bill (SB 5222);
</p>
<p>
<strong>THEREFORE, BE IT FURTHER RESOLVED</strong> that the 32nd Legislative District Democrats will use its online presence to mobilize the public;
</p>
<p>
<strong>THEREFORE, BE IT FINALLY RESOLVED</strong> that the upon adoption of this resolution, it be sent to all local newspapers and blogs, and all local representatives.
</p>
<p>
<span id="c1">[1] <a href="https://www.un.org/en/udhrbook/pdf/udhr_booklet_en_web.pdf">https://www.un.org/en/udhrbook/pdf/udhr_booklet_en_web.pdf</a></span><br/>
<span id="c2">[2] <a href="https://www.wa-democrats.org/sites/wadems/files/documents/Washington%20State%20Democrats%20-%20Platform%20-%20180616.pdf">https://www.wa-democrats.org/sites/wadems/files/documents/Washington State Democrats - Platform - 180616.pdf</a></span><br/>
<span id="c3">[3] <a href="https://www.wa-democrats.org/sites/wadems/files/resolutions/WSDCCRES%20-%20817%20-%20170422%20-%20PASS%20-%20HEA%20-%20Single%20Payer.pdf">https://www.wa-democrats.org/sites/wadems/files/resolutions/WSDCCRES - 817 - 170422 - PASS - HEA - Single Payer.pdf</a></span><br/>
<span id="c4">[4] http://www.epi.umn.edu/let/nutri/disparities/causes.shtm</span><br/>
<span id="c5">[5] <a href="https://www.reuters.com/investigates/special-report/usa-election-progressives/">https://www.reuters.com/investigates/special-report/usa-election-progressives/</a></span><br/>
<span id="c6">[6] <a href="https://wholewashington.org/faqs">https://wholewashington.org/faqs</a></span><br/>
</p>
</Layout>
)
