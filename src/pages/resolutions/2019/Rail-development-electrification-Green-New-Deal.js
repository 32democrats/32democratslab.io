import Helmet from 'react-helmet'
import React from 'react'
import { Link } from 'gatsby'
import Layout from '../../../components/layout'

const title = 'Resolution for rail development and electrification as a Green New Deal solution'
export default () => (
<Layout>
<Helmet>
<title>{title}</title>
</Helmet>
<p className="highlight">
	Replaced by: <Link to="/resolutions/2019/Prioritize-development-of-conventional-rail">Prioritize development of conventional rail</Link>
</p>
<p>
<Link to="/minutes/2019-03-13">2019 - [Withdrawn]</Link>
</p>
<h1>{title}</h1>
<p><strong>WHEREAS</strong> the Green New Deal H.RES 109 recommends the development of an economic plan to rapidly mitigate impending worldwide climate crisis and hardships associated with this crisis;</p>
<p><strong>WHEREAS</strong> it is incumbent upon us to respect the science underlying the urgency to initiate such mitigation by rapidly reducing greenhouse gas emissions by 45% of 2010 levels by 2030;</p>
<p><strong>WHEREAS</strong> to do so requires that the most effective and efficient solutions be implemented as quickly as possible in response to the urgency of this crisis;</p>
<p><strong>WHEREAS</strong> Washington State is likely to have a population influx of about 1.5 million new residents by 2030, which will be putting additional pressure on our congested highways;</p>
<p><strong>WHEREAS</strong> the electric power requirements to transition to renewable energy will be substantial and challenging to provide in sufficient quantity across all sectors;</p>
<p><strong>WHEREAS</strong> the transportation sector is a large contributor of carbon emissions and when electrified to the extent possible can contribute significantly to climate mitigation solutions;</p>
<p><strong>WHEREAS</strong> transportation by rail requires about 1/3 the energy requirement of roadway vehicles and conventional rail projects can be steadily implemented in segments thus providing immediate benefits in terms of carbon emissions and service connectivity, and reducing roadway traffic;</p>
<p><strong>WHEREAS</strong> the potential for rail development and electrification as an effective strategy for addressing climate solutions has been excluded in transportation legislation and environmental planning and the continuing addition of more highways for roadway vehicles has been the legislative priority;</p>
<p><strong>WHEREAS</strong> High Speed Rail (HSR) (at speeds over 150 mph generally) requires entirely new right of way and infrastructure and will take too long to provide short term reductions in carbon emissions, such delay putting us in danger of irreversible and dangerous climate trends, and HSR will not adequately connect with other service modes and will do little to decrease roadway traffic, and investment in HSR is money better applied to conventional rail development in the short term;</p>
<p><strong>THEREFORE BE IT RESOLVED</strong> that conventional rail development, expansion, improvement and electrification be a preeminent strategy for transitioning to a renewable energy economy in Washington State and be recommended and prioritized in legislative planning concerning Washington State transportation and transportation electrification projects;</p>
<p><strong>THEREFORE BE IT FURTHER RESOLVED</strong> that HSR be developed only secondarily to or concurrently with, conventional rail systems due to the inability of HSR to rapidly reduce carbon emissions because of its long timeline to completion, and that the Washington legislature prioritize funding for and implementation of conventional rail instead of HSR, and that conventional rail be given priority over expanded highway development;</p>
<p><strong>THEREFORE BE IT FINALLY RESOLVED</strong> that we take a position recommending the rapid implementation of conventional rail as a GND solution.</p>
<hr/>
<p>Submitted by the 32nd LD Washington State Democrats Environment and Climate Caucus by Lael White, with co-sponsors Thomas White, Arvia Morris.</p>
</Layout>
)
