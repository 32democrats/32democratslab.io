import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'tax-big-businesses-fund-green-housing-seattle'
// const title = `RESOLUTION TO SUPPORT I-131: AN ACT TO TAX BIG BUSINESSES TO FUND AFFORDABLE GREEN HOUSING IN THE CITY OF SEATTLE`
const title = `Resolution to Support I-131: An Act to Tax Big Businesses to Fund Affordable Green Housing in the City of Seattle`
const title_short = `Tax Big Businesses`
const minutes_date = '2020-05-13'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 7,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.housing,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p><strong>WHEREAS</strong> Seattle already had an extreme affordability crisis before the coronavirus pandemic, with the highest level of homelessness per capita in the country coupled with the most regressive tax structure in the state of Washington (which has the most regressive tax structure in the entire U.S., according to the Economic Opportunity Institute); and</p>
<p><strong>WHEREAS</strong> the coronavirus pandemic has caused a once in a hundred years healthcare crisis, loss of lives and livelihoods throughout our state, with massive sudden unemployment; and</p>
<p><strong>WHEREAS</strong> at the same time climate crisis threatens the very stability of human societies around the world, including the 32nd Legislative District; and</p>
<p><strong>WHEREAS</strong> Amazon.com and the wealthiest businesses in Seattle make billions in profits annually, benefit from extraordinary corporate tax breaks and pay little or nothing in taxes; and</p>
<p><strong>WHEREAS</strong> ordinary people — disproportionately people of color — bear the brunt of COVID-19, housing, unemployment, and climate crises, while continuing to shoulder the burden of taxation for services and infrastructure; and</p>
<p><strong>WHEREAS</strong> Seattle Councilmembers Kshama Sawant and Tammy Morales have introduced <a href="https://council.seattle.gov/2020/03/04/councilmembers-sawant-morales-co-sponsor-amazon-tax-legislation/">big business tax legislation</a> to fund coronavirus relief, affordable housing, and jobs, but we have no guarantee that the city council majority will stand up to big business pressure and we need the backup option of a ballot initiative;</p>
<p><strong>WHEREAS</strong> the <a href="https://www.scribd.com/document/452529997/Amazon-Tax-Ballot-Initiative">Seattle Ballot Initiative I-131, ‘AN ACT TO TAX BIG BUSINESSES TO FUND AFFORDABLE GREEN HOUSING IN THE CITY OF SEATTLE’</a>, which would enact a 0.7% corporate payroll tax on Seattle’s top 2% of wealthy corporations (approximately 800 businesses, excluding all small businesses, non-profits, grocery stores, co-op’s, and hospitals), taxing the profits of our city’s biggest businesses, not workers; and</p>
<p><strong>WHEREAS</strong> 75% of the big business tax will fund the construction and acquisition of Affordable Green Housing — high quality, <u><a href="https://sawant.seattle.gov/wp-content/uploads/2020/04/Summary-Tax-on-Corporate-Payroll.pdf">affordable</a></u> (pg. 5, 2), green and energy-efficient social housing, built with Priority Hire (per Seattle Municipal Code); and 25% of the funds will be used for Green New Deal programs, including transitioning houses from natural gas and heating oil to electricity, installing solar power, and weatherizing homes; and this will be a major jobs program, creating tens of thousands of high-quality, union jobs over the next decade (the similar Sawant-Morales Tax Amazon bill would create an estimated 34,000 jobs); and the entire funding plan prioritizes racial, economic and environmental justice;</p>
<p><strong>THEREFORE BE IT RESOLVED,</strong> the 32nd LD Democrats join in endorsing the grassroots ballot initiative I-131; and</p>
<p><strong>BE IT FURTHER RESOLVED,</strong> the 32nd LD Democrats endorse and encourage volunteers for the campaign to collect 22,000 valid signatures of registered Seattle voters by late June, 2020, in order to guarantee a spot on the November ballot (note: these signatures may not be turned in if the Seattle City Council passes a strong big business tax); and</p>
<p><strong>BE IT FINALLY RESOLVED,</strong> the 32nd LD Democrats recognize that I-131 can set a powerful example and inspire working people throughout the 32nd LD, Washington State, and around the country to work towards progressive taxation, greater social equity, and greener infrastructure.</p>
<hr/>
<p>Originated by:<br/>
Barbara Phinney, Member<br/>
Sally Soriano, PCO, SEA 32-2216<br/>
Brent McFarlane, PCO, SEA 32-2204</p>
</Layout>
)
export {render as default}
