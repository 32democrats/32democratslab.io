import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'protect-orcas-curtailing-suction-dredge-mining'
const title_short = `End Suction Dredge Mining`
const title = `Protect our orcas by curtailing suction dredge mining`
const minutes_date = '2020-01-08'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 13,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.environment,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p><strong>Whereas</strong> the three Southern Resident Orca pods, that inhabit Puget Sound and Washington coastal waters during most of the year, rely on salmon as their principal food; and</p>
<p><strong>Whereas</strong> those three orca pods are the only orca population formally listed as “endangered” under the <a href="https://en.wikipedia.org/wiki/Endangered_Species_Act">Endangered Species Act</a>, having been so designated in 2005 by both the <a href="https://en.wikipedia.org/wiki/National_Marine_Fisheries_Service">National Marine Fisheries Service</a> and the <a href="https://en.wikipedia.org/wiki/National_Oceanic_and_Atmospheric_Administration">National Oceanic and Atmospheric Administration</a>; and</p>
<p><strong>Whereas</strong> Ken Balcomb, a whale researcher for over forty years and the founder of the Center for Whale Research in Friday Harbor, has documented the declining numbers of the Southern Resident Orcas and confirmed – in a KING-5 TV segment on August 8, 2019 – that their decline is primarily due to their not getting enough to eat; and</p>
<p><strong>Whereas</strong> Suction Dredge Mining, a destructive form of mining that uses motorized equipment in rivers and streams in order to extract gold and other metals, is sometimes used in Washington streams and rivers, where it wreaks serious damage to salmon spawning beds; and</p>
<p><strong>Whereas</strong> natural resource scientists, anglers, and members of the general public have expressed concern regarding the adverse impacts of Suction Dredge Mining on salmon spawning beds in Washington streams and rivers; and</p>
<p><strong>Whereas</strong> the Washington State Legislature passed four bills in 2019 to protect our Southern Resident Orca pods, but failed to address the critical need to protect salmon spawning beds; and</p>
<p><strong>Whereas</strong> the Oregon Legislature has addressed Suction Dredge Mining by prohibiting such practice in 20,700 miles of streams and rivers it has designated as Critical Spawning Areas; and</p>
<p><strong>Whereas</strong> the State of California, as of August 6, 2019 (and continuing to this day), has banned Suction Dredge Mining in all of the State's streams and rivers;</p>
<p><strong>Therefore</strong>, be it resolved that we formally request that the Washington State Legislature, in its 2020 Supplemental Session, ensure that the Southern Resident Orca pods have a prey base sufficient to rebuild their seriously depleted numbers by promptly (1) requiring designation and protection of Critical Spawning Areas in specified Washington streams and rivers (as in Oregon), <em><u>or</u></em> (2) banning outright the practice of Suction Dredge Mining in all Washington streams and rivers (as in California).</p>
<hr/>
<p>Adopted 2020-01-08 by the 32nd Legislative District Democratic Organization</p>
<p>Previously adopted by the Jefferson County Democrats;</p>
<p>Submitted by Jim Freeburg, 206-245-0059, <a href="mailto:jifreeburg@yahoo.com">jifreeburg@yahoo.com</a></p>
</Layout>
)
export {render as default}
