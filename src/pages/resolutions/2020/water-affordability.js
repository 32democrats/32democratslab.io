import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'water-affordability'
const title_short = 'Water Affordability'
// const title = `RESOLUTION CALLING FOR A SUSTAINABLE, FEDERALLY FUNDED WATER AFFORDABILITY PLAN SIMILAR TO THE LOW INCOME HOME ENERGY ASSISTANCE PROGRAM (LIHEAP)`
const title = `Resolution calling for a sustainable, federally funded water affordability plan similar to the Low Income Home Energy Assistance Program (LIHEAP)`
const minutes_date = '2020-05-13'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 8,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.economic,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p><strong>Whereas</strong> access to clean, safe water, while always essential to human existence, is particularly critical at this time – not only for drinking, but also for hand washing, surface cleaning, and other sanitation measures necessitated by the COVID-19 pandemic, as well as for necessary sewerage service – and the need for long-term, sustainable and affordable access to water will not end after COVID-19 is brought under control; and</p>
<p><strong>Whereas</strong> the cost of providing clean, safe drinking water in the United States, including compliance with federal regulations, has brought an alarming rise in retail water rates across the country – crippling the ability of low-income families to pay for needed water service and preventing many municipalities from providing free or subsidized water to such families; and</p>
<p><strong>Whereas</strong> the inability of low-income families to pay for water service, unless responsibly addressed at some level, is and will remain a threat to public health in the United States; and</p>
<p><strong>Whereas</strong> the U.S. Congress, to address the needs of low-income families in the parallel context of their gas and electric heating costs, created the Low Income Home Energy Assistance Program (“LIHEAP”), which provides approximately $3.5 billion annually in subsidies for the 1% of customers who cannot pay their heating bills at any given time – thus promoting public health through continuity of heating service for the elderly, the disabled, and families in crisis throughout the United States, while keeping those customers on file to pay their bills, without accumulation of late fee, at such time as they may become solvent; and</p>
<p><strong>Whereas</strong>, based on a concern for public health akin to that which led to creation of LIHEAP for subsidization of heating costs, it is now time for Congress to establish a similar program of nationwide subsidies for the sustainable and affordable provision of clean, safe drinking water to low-income families; and</p>
<p><strong>Whereas</strong> LIHEAP-type relief can come in a variety of forms, including bill discounts, flexible terms (e.g., timing and predictive billing to avoid seasonal highs), lifeline rates, debt forgiveness, temporary assistance, and water-efficiency assistance; and</p>
<p><strong>Whereas</strong> water supply in many low-income communities is delivered through aging infrastructure, which (1) undermines the safety of drinking water and increases wastage through leaky pipes, with resultant higher costs to resident families, (2) is found in many apartment buildings and complexes that lack individual water metering, makes it difficult for those families to monitor and manage their water usage, and (3) will require costly retrofits to address;</p>
<p><strong>Therefore, be it resolved</strong> that we urge the U.S. Congress to create and fund a national water affordability program for low-income families, modeled on the LIHEAP program; and</p>
<p><strong>Be it further resolved</strong> that we urge Congress to adopt implementing legislation including tax credits, grants or other assistance for cities, multi-unit properties and individual homeowners to install water efficiency devices, and to make plumbing system retrofits and water pipe repair to abate water loss from public and/or private water lines.</p>
<hr/>
<p>Originated by Chris Roberts,<br/>
WSD Delegate from LD32</p>
</Layout>
)
export {render as default}
