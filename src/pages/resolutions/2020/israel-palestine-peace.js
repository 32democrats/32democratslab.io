import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'israel-palestine-peace'
const title_short = 'Israeli-Palestinian Peace'
const title = `Resolution in Support of Washington Democrats’ Commitment to Israeli-Palestinian Peace, and in Opposition to any Further Occupation, Settlement Expansion, or Other Unilateral Annexation of West Bank Territory`
const minutes_date = '2020-02-12'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 10,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.foreign,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p>WHEREAS the Washington Democratic Party’s 2018 platform rightly states support for serious negotiations between Israel and Palestine in order to evolve a solution that would include a sovereign state of Palestine and a sovereign state of Israel; and</p>
<p>WHEREAS the Trump administration has upended decades of bipartisan support for such a two-state solution, and the Israeli government has accelerated settlement expansion and implemented several steps toward serial annexation of the West Bank, seriously threatening a peaceful two-state solution; and</p>
<p>WHEREAS the U.S. State Department issued an opinion in 1978, defining Israeli settlements in the West Bank as illegal under the Fourth Geneva Convention – an official position in effect for over 40 years, until reversed by the Trump administration on November 18, 2019; and</p>
<p>WHEREAS, in response to the above reversal by the Trump administration, 106 members of Congress wrote to Secretary of State Mike Pompeo on November 21, 2019, expressing their vehement opposition to the State Department's decision to consider Israeli settlements legal – a decision that greenlights further annexations and threatens the two-state solution; and</p>
<p>WHEREAS the U.S. House of Representatives, on December 6, 2019 – with 192 cosponsors, including all seven Washington Democrats – adopted H. Res. 326, expressly supporting a two-state solution and opposing settlement expansion, drawing a red line against unilateral annexation of West Bank territory, and recognizing that such roadblocks to a peace agreement “pose a threat to the ability to maintain a Jewish and democratic state of Israel and [to] the establishment of a viable, democratic Palestinian state”; and</p>
<p>WHEREAS Rep. Pramila Jayapal (WA-7) has endorsed J Street U’s #ChangeThePlatform campaign, to add to the Democratic Party’s national platform a statement of opposition to the Israeli occupation, illegal settlements, and annexation, calling such a statement “essential”;</p>
<p>THEREFORE, BE IT RESOLVED that we support the right of both Israelis and Palestinians to govern themselves, each in their own viable state, and we oppose the further occupation of territory in the West Bank, whether by settlement expansion or unilateral annexation; and</p>
<p>BE IT FURTHER RESOLVED that this resolution be sent to the Chair and Co-Chair of the Democratic National Committee, our state’s members of the DNC, and Washington’s Democratic Party Chair.</p>
<hr/>
<p>Originated by Jeremy Voss, Co-Chair, J Street U at UW;
<br/>co-sponsored by U/W Young Democrats</p>
<hr/>
<p>References:</p>
<ol>
<li><blockquote>
<p>UN Security Council, “Special Coordinator Reports Largest Expansion of West Bank Settlements in 2 Years” (June 20, 2019), <a href="https://www.un.org/press/en/2019/sc13853.doc.htm">https://www.un.org/press/en/2019/sc13853.doc.htm</a></p>
</blockquote></li>
<li><blockquote>
<p>David M. Halbfinger, “Netanyahu, Facing Tough Israel Election, Pledges to Annex a Third of West Bank,” New York Times, (September 23, 2019) <a href="https://www.nytimes.com/2019/09/10/world/middleeast/netanyahu-israel-west-bank.html">https://www.nytimes.com/2019/09/10/world/middleeast/netanyahu-israel-west-bank.html</a></p>
</blockquote></li>
<li><blockquote>
<p>Letter of the State Department Legal Advisor, Mr. Herbert J. Hansell, Concerning the Legality of Israeli Settlements in the Occupied Territories of 21 April 1978, <a href="https://www.hlrn.org/img/documents/USSDLegalAdvisorHansell_ltr.pdf">https://www.hlrn.org/img/documents/USSDLegalAdvisorHansell_ltr.pdf</a></p>
</blockquote></li>
<li><blockquote>
<p>The United States Congress, “Letter to the Honorable Mike Pompeo, Secretary of State” <a href="https://andylevin.house.gov/sites/andylevin.house.gov/files/112119%20Letter%20to%20SecState%20on%20Israeli%20settlements%20FINAL.pdf">https://andylevin.house.gov/sites/andylevin.house.gov/files/112119%20Letter%20to%20SecState%20on%20Israeli%20settlements%20FINAL.pdf</a></p>
</blockquote></li>
<li><blockquote>
<p>The United States Congress, “H. Res. 326: Expressing the sense of the House of Representatives regarding United States efforts to resolve the Israeli-Palestinian conflict through a negotiated two-state solution,” (April 25, 2019) <a href="https://www.congress.gov/116/bills/hres326/BILLS-116hres326rh.pdf">https://www.congress.gov/116/bills/hres326/BILLS-116hres326rh.pdf</a></p>
</blockquote></li>
<li><blockquote>
<p>J Street Gala: Keynote Session (November 4th, 2019) <a href="https://www.youtube.com/watch?v=BScGL6ltdXQ&amp;t=1797">https://www.youtube.com/watch?v=BScGL6ltdXQ&amp;t=1797</a></p>
</blockquote></li>
</ol>
</Layout>
)
export {render as default}
