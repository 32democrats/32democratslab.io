import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'national-infrastructure-bank'
const title_short = `National Infrastructure Bank`
const title = `Resolution Urging Congress to Endorse National Infrastructure Bank`
const minutes_date = '2020-05-13'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 6,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.economic,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p><strong>Whereas</strong> there is a widely acknowledged shortfall in infrastructure spending in the nation. The American Society of Civil Engineers (ASCE) gave the country a D+ in its 2017 Report Card and estimates the nation must spend at least $4.6 trillion to bring current infrastructure to a state of good repair. At least $2.1 trillion is unfunded, and the remainder is inadequately funded. Funding for new projects, including broadband, water projects, rail and High Speed rail are unfunded; and,</p>
<p><strong>Whereas</strong> the impact of the coronavirus pandemic on peoples’ lives and the economy has been devastating. At least 26 million people are unemployed and large numbers of businesses face bankruptcy or closing. This ongoing catastrophe has thrown the economy into recession or worse; and,</p>
<p><strong>Whereas</strong> a new National Infrastructure Bank (NIB) could directly aid in fostering an economic recovery and build the infrastructure projects we have neglected. Legislation to create such a bank has been introduced into Congress this spring, HR 6422. The new NIB is modeled on four previous institutions created by Presidents George Washington, John Quincy Adams, Abraham Lincoln, and Franklin D. Roosevelt, which helped spur massive economic growth; and,</p>
<p><strong>Whereas</strong> the new National Infrastructure Bank would be able to lend $4 trillion for urgently needed projects. The NIB would be capitalized by repurposing existing Treasury debt and create no new debt. It would create 25 million new jobs, pay Davis-Bacon wages, and help hire those made jobless by the coronavirus pandemic. It would also require Project Labor Agreements and Buy American provisions for all projects; and,</p>
<p><strong>Whereas</strong> the National Infrastructure Bank would increase growth from 1.8% per year to 5% per year, have no new taxes, but increase tax revenues coming into state, local and federal governments. This would foster the kind of growth not seen since the Kennedy Space Program; and,</p>
<p><strong>Whereas</strong> many organizations have endorsed the new National Infrastructure Bank. 17 state legislatures have introduced or passed bi-partisan resolutions in support of the new bank, as have city and county councils. The National Association of Counties has endorsed the new bank. Other endorsements include the National Latino Farmers and Ranchers, the National Congress of Black Women, the National Federation of Federal Employees, the Democratic Municipal Officials, and many others. HR 6422 has been introduced into Congress to create the new National Infrastructure Bank; now, therefore,</p>
<p><strong>Therefore Be it Resolved,</strong> that the 32nd LD hereby endorses HR 6422 and urges Congress to pass this legislation; and, be it further</p>
<p><strong>Be It Finally Resolved,</strong> that copies of this resolution be forwarded to the King County and Snohomish County Labor Councils, the Washington State Labor Council, the Washington State Congressional Delegation, and to the President of the United States.</p>
<hr/>
<p>Originated from<br/>
The National Association of Counties</p>
<p>Sponsored by<br/>
Maralyn Chase<br/>
Fmr. State Senator, 32nd LD</p>
</Layout>
)
export {
  render as default,
  title,
}