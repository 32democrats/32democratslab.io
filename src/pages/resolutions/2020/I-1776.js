import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'I-1776'
// const title = `I-1776 RESOLUTION - WASHINGTON ANTI-DISCRIMINATION ACT (WADA)`
const title = `I-1776 Resolution — Washington Anti-Discrimination Act (WADA)`
const title_short = `Approve I-1776`
const minutes_date = '2020-06-10'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 5,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.civil_rights,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p>
<b>Whereas,</b> Initiative 1000 (I-1000) – Washington’s Initiative to the Legislature, was signed by nearly 400,000 voters, voted into law by the Washington State Legislature and came just 1% away from passage by the voters last November; and 
</p>
<p>
<b>Whereas,</b> I-1000 has been reintroduced as <b>Initiative 1776 (I-1776)</b> to the People and filed with the WA Secretary of State as the Washington Anti-Discrimination Act (WADA); and 
</p>
<p>
<b>Whereas, I-1776</b> finds that, while Washington is one of America’s most economically and ethnically diverse states, seniors, women, the disabled, veterans, the LGBTQ+ community & people of color have experienced harsh discrimination, thus creating deadly health disparities; and 
</p>
<p>
<b>Whereas, I-1776</b> finds that the COVID-19 pandemic has unmasked the discriminatory educational, employment, economic & health disparities which have long plagued seniors, women, persons with disabilities, persons of color, military veterans, the LGBTQ+ community and other vulnerable communities in WA State; and 
</p>
<p>
<b>Whereas, I-1776</b> is America’s only initiative demanding COVID-19 vaccines for all Washingtonians by prohibiting discrimination in the vaccine’s availability and accessibility based on age, race, gender, sexual orientation, disability, citizenship, county, income or employment; and 
</p>
<p>
<b>Whereas, I-1776</b> prohibits age discrimination in public jobs, schools and contracting and thus enables workers to be judged by their abilities and not their age; and 
</p>
<p>
<b>Whereas, I-1776</b> prohibits gender discrimination and helps our state colleges and universities increase the number of women pursuing degrees in the lucrative fields of science, technology, engineering, art and math (STEAM); and 
</p>
<p>
<b>Whereas, I-1776</b> strengthens veteran’s benefits by expanding Affirmative Action in employment beyond the Vietnam-era and disabled veterans to include all Veterans; and 
</p>
<p>
<b>Whereas, I-1776</b> helps small businesses compete for $3.5 billion in state contracting opportunities which state officials reported were unfairly denied to qualified small, women and minority businesses since the passage of Tim Eyman’s Initiative 200 22 years ago; 
</p>
<p>
<b>Therefore, be it Resolved</b> that the 32nd LD Democrats endorse <b>I-1776</b> as “do sign and vote yes”, to strengthen WA State’s discrimination statutes to protect individuals in public health, education, employment and contracting; and 
</p>
<p>
<b>Therefore, be it Further Resolved,</b> that the 32nd LD Democrats urge the Washington State Democrats, and all of the County Democratic Central Committees to support the signature gathering and passage of <b>I-1776</b> ; and 
</p>
<p>
<b>Therefore, be it Finally Resolved,</b> that copies of this resolution be sent to local news outlets and to the <b>I-1776</b> campaign.
</p>
<hr/>
<p>
Submitted by Carin Chase, PCO Holly
</p>
</Layout>
)
export {
  render as default
}
