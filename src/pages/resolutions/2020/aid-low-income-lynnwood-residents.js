import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
// import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'aid-low-income-lynnwood-residents'
// const title_short = 'Aid Low-Income Lynnwood Residents'
const title = `Resolution in support of aid for low-income Lynnwood residents`
const minutes_date = '2020-02-12'
const status = resolution_status.adopted
// const wsdcc_header = {
// 	action_date: minutes_date,
// 	number: 11,
// 	organization: '',
// 	title_short,
// 	wsdcc_action: wsdcc_action.unknown,
// 	wsdcc_issue: wsdcc_issue.housing,
// }

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
{/* <WsdccHeader w={wsdcc_header}/> */}
<ResolutionHeader r={r}/>
<p><strong>Whereas</strong> the Snohomish County Housing Authority plans to close and demolish the low cost apartment complex known as Whispering Pines in Lynnwood; and</p>
<p><strong>Whereas</strong> more than 200 low cost apartments will be removed from the rental market at a time that there is an increasing demand for low cost housing; and</p>
<p><strong>Whereas</strong> plans provided by the Snohomish County Housing Authority for the rebuilding of housing on the property, now known as Whispering Pines, relied on an upzone that was voted down by the Lynnwood City Council; and</p>
<p><strong>Whereas</strong> the Snohomish County Housing Authority and the City of Lynnwood have no plans to assist displaced tenants by providing finances to offset the additional costs and trauma of moving;</p>
<p><strong>THEREFORE, BE IT RESOLVED</strong> that the 32<sup>nd</sup> Legislative District Democrats recognizes the ability of Cities to enact a Displaced Low-Income Tenant Ordinance, per RCW 59-18.440, that will provide financial assistance to current Lynnwood low income residents forced from their current homes, and</p>
<p><strong>THEREFORE, BE IT RESOLVED</strong> that the 32<sup>nd</sup> Legislative District Democrats urge all members of the Lynnwood City Council to pass such an Ordinance that will provide support for displaced low income residents in the City, and</p>
<p><strong>THEREFORE, BE IT RESOLVED</strong> that the 32<sup>nd</sup> Legislative District Democrats urge the Snohomish County Housing Authority to assist the City of Lynnwood in its efforts to aid displaced low income residents in their efforts to alleviate the hardships endured by their displacement, and</p>
<p><strong>THEREFORE, BE IT FINALLY RESOLVED</strong> that copies of this resolution be sent to the Lynnwood Mayor, all members of the Lynnwood City Council, the Snohomish County Housing Authority Board and the Snohomish County Council.</p>
</Layout>
)
export {render as default}
