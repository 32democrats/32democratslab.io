import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'scrap-the-cap-social-security-payroll-deductions'
const title = `Resolution: “Scrap the Cap” on Social Security payroll deductions`
const title_short = `Uncap Social Security Tax`
const minutes_date = '2020-08-12'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 3,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.economic,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p><strong>Whereas</strong> the Social Security national insurance program has operated successfully for 85 years and, as long as people are working in this country, payroll taxes will continue to fund Social Security and the program will continue to pay out benefits; and</p>
<p><strong>Whereas</strong>, although Social Security funding always has been structured in a manner that does not contribute even one penny to the federal deficit, its original actuaries were understandably unable to foresee the last 40 years of extreme increases in income inequality when wages for the top 1% of earners increased 158%, while wages for the bottom 90% increased only 24%; and</p>
<p><strong>Whereas</strong> wages subject to the Social Security payroll tax presently are “capped” at an annual $137,700 per person, thereby exempting from such taxation any individual’s income exceeding that figure – comprising an estimated aggregate total of $1.2 trillion of income annually that is not taxed to support Social Security; and</p>
<p><strong>Whereas</strong> reliable estimates project that if no change is made to capping income subject to Social Security tax, the Social Security trust fund reserves (the excess contributions collected since 1990) will be depleted and tax income will be sufficient to pay only 76% of scheduled benefits; and</p>
<p><strong>Whereas</strong> relatively few working Americans are slated to receive traditional retirement pensions through employers, making Social Security benefits likely to be even more vital for millennials and other generations than for current retirees, while the amounts they can save in Individual Retirement Accounts and 401(k) savings plans are limited, by statute, to levels not adequate for retirement needs — and skewed to benefit workers at the highest income levels; and</p>
<p><strong>Whereas</strong> the economic outlook for younger generations is characterized by stagnant wage growth, part-time or contract jobs with less-predictable income, no benefits, and unprecedented levels of education debt, while the costs of housing, healthcare and raising a family are soaring, making it exceedingly difficult or impossible for many, if not most, Americans to save for retirement; and</p>
<p><strong>Whereas</strong> a major recession in the American economy, such as in 2008-09 and the one now underway, can reduce the value of life savings by half; and</p>
<p><strong>Whereas</strong> Social Security is likely to be the only regular income many Americans will have in their old age; and</p>
<p><strong>Whereas</strong> removing the cap on income subject to the Social Security payroll tax, to allow the excess income to be taxed, would eliminate the projected post-2035 shortfall <em>and</em> enable improved benefits for orphans, disabled young adults, and other current and future beneficiaries; and</p>
<p><strong>Whereas</strong> legislation is pending in both the U.S. Senate and House of Representatives that would close the funding gap <em>and</em> allow for expanded benefits for current and future generations;</p>
<p><strong>Therefore, be it resolved</strong> that we strongly urge enactment of federal legislation to “scrap the cap” on the amount of income subject to Social Security payroll tax deductions, specifically (at this time) Rep. John Larson’s “Social Security 2100 Act,” H.R. 860, a bill with at least 208 co-sponsors in the House (including all seven of Washington’s Democrats); and.</p>
<p><strong>Be it further resolved</strong> that we urge our U.S. Senators Patty Murray and Maria Cantwell to co-sponsor and advocate for passage of S.269, the Senate version of Congressman Larson’s bill; and</p>
<p><strong>Be it further resolved</strong> that we urge that “Scrapping the Cap” on Social Security payroll deductions be included in the 2020 National Democratic Platform.</p>
</Layout>
)
export {render as default}
