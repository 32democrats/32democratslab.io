import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'

const file = 'defund-the-seattle-police'
const title = `Resolution to: Defund the Seattle Police Department`
const minutes_date = '2020-08'
const status = resolution_status.withdrawn

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<ResolutionHeader r={r}/>
<p>WHEREAS the 32nd District Democrats stand in solidarity with protesters against the systemic violence and excessive force continually demonstrated by police against Black people across our country, resulting in the murder of Black Americans nationwide and in Seattle including George Floyd, Breonna Taylor, David McAtee, Charleena Lyles, and so many others; and</p>
<p>WHEREAS we acknowledge that these tactics are nothing new, and have been utilized by police against Black people and other minority communities for decades; and</p>
<p>WHEREAS our values as Democrats supports the 2018 Washington State Democratic Party platform which calls for “police forces to be demilitarized, be held accountable for abusive practices, and exercise restraint in their dealing with peaceful assemblies and protests”; and</p>
<p>WHEREAS the Seattle Police Department has recently targeted peaceful protesters and children with tear gas, pepper spray, flash bangs, and other “crowd control” measures endangering not only the protesters and media present but those obeying the curfew and sheltering in their homes nearby; and</p>
<p>WHEREAS it is unacceptable that the police feel free to attack and arrest Black people and peaceful protesters with impunity;</p>
<p>THEREFORE, be it resolved that the 32nd LD Democrats support these four provisions, now before the Seattle City Council, to defund the Seattle Police Department:</p>
<p><u>Proposed by *De-criminalize Seattle and King County Equity Now</u></p>
<p>1. Defund the police department by 50%.</p>
<p>2. A community led safety and health infrastructure and a participatory budget project led by those communities most affected by police terrorism.</p>
<p>3. Drop charges for the hundreds of protestors that have been arrested.</p>
<p><u>Proposed by Council Member Kshama Sawant</u></p>
<p>4. Total 2020 police department budget is $409 million, defund by 50% = $85M; into affordable housing, services in Black and Brown working-class communities, renter organizing and eviction defense, youth programming, alternatives to repressive policing.</p>
<p>Submitted by:</p>
<p>Barbara Phinney</p>
<p>Brent McFarlane</p>
<p>Sally Soriano</p>
<p><a href="https://docs.google.com/document/d/1_rJ41bw1TgKz01H9HpOr5HKBLyJyt0h5WRldffZ_mOE/edit?link_id=4&amp;can_id=6ef223f0c2b4aaf679adf107324b1600&amp;source=email-june-16th-meeting-information&amp;email_referrer=&amp;email_subject=june-16th-meeting-information">Resolution to Defund the Seattle Police Department</a> was passed by the 43rd LD, June 16, 2020.</p>
<p>*The Decriminalize Seattle Coalition includes: WA-BLOC, Seattle Peoples Party, Trans Women of Color Solidarity Network, COVID-19 Mutual Aid, CID Coalition, People of Color Sex Worker Outreach Project, PARISOL, APICAG, Block the Bunker, No New Youth Jail, BAYAN, Asians for Black Lives, and La Resistencia.</p>
</Layout>
)
export { render as default }
