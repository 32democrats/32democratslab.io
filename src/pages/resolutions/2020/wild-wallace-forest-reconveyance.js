import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
// import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'wild-wallace-forest-reconveyance'
const title = `Wild Wallace Forest Reconveyance`
const minutes_date = '2020-08-12'
const status = resolution_status.adopted
// const wsdcc_header = {
// 	action_date: minutes_date,
// 	number: 1,
// 	organization: '',
// 	title_short: title,
// 	wsdcc_action: wsdcc_action.unknown,
// 	wsdcc_issue: wsdcc_issue.agriculture,
// }

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
{/* <WsdccHeader w={wsdcc_header}/> */}
<ResolutionHeader r={r}/>
<p><strong>WHEREAS</strong>, increased outdoor recreation improves the physical and mental health of people in our region, and supports the overall health of the community; and</p>
<p><strong>WHEREAS</strong>, having more trails and space for recreation in the Sky Valley gives each person more space for hiking, mountain biking, horseback riding, and connects the existing trails together; and</p>
<p><strong>WHEREAS</strong>, recreational tourism is a growing economic benefit to our region, including in Sky Valley; and</p>
<p><strong>WHEREAS</strong>, the ‘<em><u><a href="https://rco.wa.gov/wp-content/uploads/2020/01/HikingBikingStudy.pdf">Economic, Environmental, and Social Benefits of Recreational Trails in Washington State</a></u></em>’ (October 1, 2019) report by the Washington State Recreation and Conservation Office shows benefits that accrue from protecting forests and trails for recreation, including a case study of Lake Serene trail that shows a yearly economic contribution of $834,000 to Snohomish County from those trails alone; and</p>
<p><strong>WHEREAS</strong>, this report recommends an increase in trails as it leads to an increase in trail use and improved health outcomes and reduced health expenditures; and</p>
<p><strong>WHEREAS</strong>, preserving forests protects clean water and air, reduces carbon emissions, and conserves wildlife habitat; and</p>
<p><strong>WHEREAS</strong>, Wild Wallace Forest, which is just north of Gold Bar, thirty minutes from Everett, is 5,300 acres of largely mature forest that connects two existing recreational gems: the Wild Sky Wilderness to the east and Wallace Falls State Park to the west; and</p>
<p><strong>WHEREAS</strong>, an increase in trails and connecting existing trails promotes multi-day trips, leading to higher economic contributions; and</p>
<p><strong>WHEREAS</strong>, Gold Bar, Sky Valley, and Snohomish County will realize community health, economic, recreational, and environmental benefits from an increase in county parks and trail access in southeast Snohomish County; and</p>
<p><strong>WHEREAS</strong>, the Snohomish County Comprehensive Plan identifies the Gold Bar Urban Growth Area as an underserved area by city and county parks in which to consider acquiring new facilities to support future level of service standards, and the Snohomish County General Policy Plan calls for acquisition or development of new parks and facilities as appropriate, including unique unanticipated opportunities; and</p>
<p><strong>WHEREAS</strong>, <u><a href="https://app.leg.wa.gov/RCW/default.aspx?cite=79.22.300">RCW 79.22.300</a></u>, Reconveyance back when use ceases, enables a county to take over management of their own forest board land from the State of Washington in order to use the land as a park;</p>
<p><strong>NOW THEREFORE BE IT RESOLVED</strong>, that the 32nd District Democrats support the reconveyance of 5,300 acres of forest known as Wild Wallace in order to become a county park; and</p>
<p><strong>THEREFORE, BE IT FURTHER RESOLVED,</strong> that the 32nd District Democrats urge the Snohomish County Council and the County Executive to support and pass an ordinance to reconvey the Wild Wallace, 5300 acres of forest to become a Snohomish County Park; and</p>
<p><strong>THEREFORE, BE IT FURTHER RESOLVED,</strong> that a copy of this resolution be sent to all of the Snohomish County Council Members and the Snohomish County Executive; and</p>
<p><strong>THEREFORE, BE IT FINALLY RESOLVED,</strong> that we encourage the members of the 32nd District Democrats to contact their Snohomish County Council members and the Snohomish County Executive to ask for their support of the Wild Wallace Forest Park.</p>
<hr/>
<p>Proposed by</p>
<p>2020-07-09</p>
</Layout>
)
export { render as default }
