import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'safeguards-to-guardianship-law-SB-5604'
const title_short = 'Safeguards to Guardianship Law'
const title = `Resolution adding safeguards to guardianship law SB 5604 – 2019-20`
const minutes_date = '2020-01-08'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 12,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.human,
}

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p>WHEREAS in Spring 2019 the Washington State legislature enacted Senate Bill 5604 which is a new adult guardianship law for Washington State and is based upon the proposed Uniform Guardianship Law which was formulated a few years ago by the national commission on uniform state laws, and has so far been embraced by only Maine and Washington; and</p>
<p>WHEREAS from the standpoint of human rights, the old existing adult guardianship law in Washington is very unfair because it gives for-profit guardianship businesses far too much power over the lives of other people; and</p>
<p>WHEREAS in the old existing guardianship law there are very few real safeguards for the rights of “alleged incapacitated persons” (AIP) and adjudged “incapacitated persons” (IP) and families, and precisely because existing safeguards are so few, it follows that none of them should be lost in the transition to the new law; and</p>
<p>WHEREAS the old law contains a safeguard provision which sometimes works to protect patients against being forced out of their homes and put into a facility to in effect be warehoused, without even the requirement of a prior court order;</p>
<p>THEREFORE BE IT RESOLVED that this safeguard provision should be retained in the new law.</p>
<p>BE IT FINALLY RESOLVED that this resolution, when adopted, be sent to the Delegation of the 32<sup>nd</sup> Legislative District, Governor Inslee and Lt. Governor Habib, all members of WA State Senate Law and Justice Committee, all members of WA State House Judiciary Committee, WA State Democrats, King and Snohomish County Democrats.</p>
<hr/>
<p>Adopted 2020-01-08 by the 32nd Legislative District Democratic Organization</p>
<p>Sponsored by Michael Brunson of the 32<sup>nd</sup> Legislative District Dec 29, 2019</p>
<p><a href="https://app.leg.wa.gov/billsummary?BillNumber=5604&amp;Year=2019&amp;Initiative=false">SB 5604</a> 2019-20 Concerning the uniform guardianship, conservatorship, and other protective arrangements act.</p>
</Layout>
)
export {render as default}
