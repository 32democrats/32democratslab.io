import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'ahmaud-arbery'
// const title = `RESOLUTION CALLING FOR JUSTICE IN THE KILLING OF UNARMED JOGGER AHMAUD ARBERY`
const title = `Resolution calling for justice in the killing of unarmed jogger Ahmaud Arbery`
const title_short = `Justice for Ahmaud Arbery`
const minutes_date = '2020-06-10'
const status = resolution_status.adopted
const wsdcc_header = {
	action_date: minutes_date,
	number: 4,
	organization: '',
	title_short,
	wsdcc_action: wsdcc_action.submitted,
	wsdcc_issue: wsdcc_issue.civil_rights,
}
export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
<WsdccHeader w={wsdcc_header}/>
<ResolutionHeader r={r}/>
<p><strong>Whereas</strong>, on February 23, 2020, Ahmaud Arbery, a 25-year-old African-American man, went out for a mid-day exercise jog on his day off, near his home in Brunswick, Georgia, wearing running shorts and a white T-shirt; and</p>
<p><strong>Whereas</strong> Mr. Arbery’s chosen route took him by the home of one Gregory McMichael, a retired law-enforcement investigator, who reportedly deemed him to be a burglar who had recently been targeting the neighborhood; and</p>
<p><strong>Whereas</strong> Mr. McMichael immediately recruited his adult son, Travis McMichael, and – equipped with a .357 Magnum and shotgun, respectively – they jumped into their pickup truck and, followed by a third individual, one “Roddy” Bryan (equipped with a cellphone camera), drove off in pursuit of Mr. Arbery; and</p>
<p><strong>Whereas</strong>, after the McMichaels’ truck caught up with and passed Mr. Arbery, they stopped their vehicle in front of him and demanded that he too stop; and</p>
<p><strong>Whereas</strong>, after Mr. Arbery refused to stop, Travis McMichael disembarked with his shotgun and, as documented by Roddy Bryan’s camera, physically attacked Mr. Arbery and, during the ensuing struggle, shot him at least twice – whereupon Mr. Arbery fell to the street and died; and</p>
<p><strong>Whereas</strong> Mr. Arbery’s bodily attire throughout this confrontation consisted solely of his T-shirt and running shorts, making it quite evident at all times that he was unarmed; and</p>
<p><strong>Whereas</strong> approximately three months passed before any of Mr. Arbery’s assailants was charged with murder or any other felonious crime; and</p>
<p><strong>Whereas</strong> we may confidently assume that charges would have been brought far earlier if the murdered jogger’s skin color had denoted European, rather than African, ancestry;</p>
<p><strong>Therefore, be it resolved</strong> that we condemn both these unwarranted vigilante actions against an unarmed jogger, resulting in his murder, and the protracted delay by Georgia authorities in bringing charges against his attackers; and</p>
<p><strong>Be it further resolved</strong> that we deem the above events, including the lengthy inaction by law enforce-ment authorities – having occurred during the period between the active-duty police “I can’t breathe” killings of Eric Garner in New York and George Floyd in Minneapolis – as particularly offensive to 21<sup>st</sup>-Century race-relations in the United States, the last major nation to abolish human slavery; and</p>
<p><strong>Be it finally resolved</strong> that a copy of this resolution be sent to Attorney General William Barr, with a request for formal action by the U.S. Justice Department in vindication of our existing civil rights laws and of the Fourteenth Amendment upon which they are based.</p>
</Layout>
)
export {
	render as default
}