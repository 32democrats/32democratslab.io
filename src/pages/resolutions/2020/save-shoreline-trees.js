import React from 'react'
import Layout from '../../../components/layout'
import {ResolutionHeader, resolution_status} from '../../../components/resolution'
// import {wsdcc_action, wsdcc_issue, WsdccHeader} from '../../../components/wsdcc'

const file = 'save-shoreline-trees'
const title = `Resolution: Save Shoreline Trees`
// const title_short = `Save Shoreline Trees`
const minutes_date = '2020-02-12'
const status = resolution_status.adopted
// const wsdcc_header = {
// 	action_date: minutes_date,
// 	number: 9,
// 	organization: '',
// 	title_short,
// 	wsdcc_action: wsdcc_action.unknown,
// 	wsdcc_issue: wsdcc_issue.agriculture,
// }

export const r = {file, minutes_date, status, title}
const render = () => (
<Layout>
{/* <WsdccHeader w={wsdcc_header}/> */}
<ResolutionHeader r={r}/>
<p>
WHEREAS, a Healthy Urban Forest contributes to human health in a variety of ways:
oxygen, beauty, air and water quality, wildlife habitat, helps to retain rainwater, creates
ecosystems that support people of all ages, and promotes happiness and well being, and
</p>
<p>
WHEREAS, the City of Shoreline has been declared a “Tree City USA” by the Arbor Day
Foundation, and
</p>
<p>
WHEREAS, the City of Shoreline has passed numerous Climate Change reduction
measures, and
</p>
<p>
WHEREAS, unfortunately the City of Shoreline’s development and density goals which
include: infrastructure projects such as sidewalks; and theories on affordable housing and
other needs; are now at odds with the City’s Tree Canopy and Climate Change Reduction
Goals, and
</p>
<p>
WHEREAS, current and proposed projects such as: WSDOT headquarters redevelopment
adjacent to Dayton Ave N, Fircrest School redevelopment; Light Rail Development, Shoreline
School District redevelopment projects; and over 140 possible Town Home developments;
have caused or will cause the destruction of 2-3000 significant trees, and
</p>
<p>
WHEREAS, these trees are extremely important to both the 32nd District Democrats,
because the 32nd platform includes strong provisions for Clean Air, Clean Water-and Climate
Protection, and to the Citizens of Shoreline who value them for both their beauty and for
creating a Healthy City,
</p>
<p>
THEREFORE, BE IT RESOLVED that the 32nd District Democrats urge the Shoreline City
Council to direct staff to take action to prevent further loss of significant trees by developing
alternative policies for sidewalk development and for new and more effective tree and
development codes; support proposals at Fircrest School, public schools and other public
properties to protect all existing Urban Forest; work proactively with citizens to achieve these
goals; and appropriate the funding required, and
</p>
<p>
BE IT FURTHER RESOLVED that we request this Resolution be published and sent to the
Shoreline City Council, the 32nd State delegation, the King County Council and the King
County Executive, requesting a positive acknowledgment and action steps to succeed.
</p>
<hr/>
<p>
Originated from Janet Way, 32nd LD PCO
</p>
</Layout>
)
export {render as default}
