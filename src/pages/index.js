import React from 'react'
import {Link} from 'gatsby'
import {base} from '../config'
// import {meetings} from './minutes/index'
import {proposed} from './resolutions/index'
// import {LinkMinutesDetail} from '../components/minutes'
import {LinkResolution} from '../components/resolution'
// import {base} from '../config'

import Layout from '../components/layout'
// import {vote_info} from './endorsements/2020'

export default () => (
<Layout>
	{/*
	<h1 className="title">
		32<sup>nd</sup> District Democrats
	</h1>
	*/}
	{/*
	<p>
		We are the Democrats of the 32<sup>nd</sup> legislative district of the state of Washington.
	</p>
	<p>
		Taking back the country starts at the grassroots.
		Make a difference by vetting, supporting, and speaking with local candidates and officials.
	</p>
	<ul className="menu">
		<li><LinkMinutesDetail m={meetings[0]}/></li>
	</ul>
	<hr/>
	*/}
	{/*
	<dl className="links">
	<dt><a href="https://results.vote.wa.gov/results/20201103/default.htm">Election Results</a></dt>
	<dd><ul className="menu">
	<li><a href="https://cdb.kingcounty.gov/results.pdf">King County</a></li>
	<li><a href="https://snohomishcountywa.gov/5737/Get-Current-and-Past-Election-Results-an">Snohomish County</a></li>
	</ul></dd>
	*/}
	{/* <dt><Link to="/endorsements/2020">Endorsements</Link></dt> */}
		{/* <dd>{vote_info}</dd> */}
	{/* </dl> */}
	{/* <hr/> */}
	<dl className="links">
		<dt><a href="https://democrats.org/">Democrats</a></dt>
		<dd><ul className="menu">
			<li><a href={base}>32nd Legislative District</a></li>
			<li><a href="https://www.kcdems.org/">King County</a></li>
			<li><a href="http://snocodems.org/">Snohomish County</a></li>
			<li><a href="https://www.wa-democrats.org/">Washington</a></li>
			<li><a title="More" href="https://www.wa-democrats.org/links/">&hellip;</a></li>
		</ul></dd>
	</dl>
	<hr/>
	<dl className="links">
		{/* <dt><Link to="/resolutions">Resolutions</Link></dt> */}
		<dd>
			<ul className="menu">
				{proposed.map(r=>(
					<li><LinkResolution r={r}/></li>
				))}
			</ul>
		</dd>
	</dl>
	<ul className="menu">
		<li><Link to="/resolutions/help">Resolution drafting help</Link></li>
	</ul>
	<hr/>
	<ul className="menu">
		<li><a href="/doc/2020/election-rights-wa-2020.pdf">Know Your Rights, Know the Law</a></li>
		<li><Link to="/state-legislative-agenda">State Legislative Agenda</Link></li>
		<li><Link to="/mission-values">Mission & Values</Link></li>
	</ul>
</Layout>
)
