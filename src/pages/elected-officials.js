import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../components/layout'

export default () => (
<Layout>
<Helmet>
	<title>
		Elected Officials
	</title>
</Helmet>
	<h1>Elected Officials (2020)</h1>
	<ul className="endorsements">
	<li><strong><a href="https://www.usa.gov/">Federal</a></strong>
	<ul>
	<li><a href="https://www.senate.gov/">Senate</a>
		<ul>
			<li><a href="http://murray.senate.gov/public/">Patty Murray</a></li>
			<li><a href="http://cantwell.senate.gov/">Maria Cantwell</a></li>
		</ul>
	</li>
	<li><a href="https://www.house.gov/">House</a>
		<ul>
			<li>2nd Congressional District – <a href="https://larsen.house.gov/">Rick Larsen</a></li>
			<li>7th Congressional District – <a href="https://jayapal.house.gov/">Pramila Jaypal</a></li>
		</ul>
	</li>
	</ul>
	</li>
	<li><strong><a href="https://access.wa.gov/">State</a></strong>
	<ul>
	<li>Senate – <a href="http://sdc.wastateleg.org/salomon/">Jesse Salomon</a></li>
	<li>House Position 1 – <a href="https://housedemocrats.wa.gov/ryu/">Cindy Ryu</a></li>
	<li>House Position 2 – <a href="https://housedemocrats.wa.gov/davis/">Lauren Davis</a></li>
	<li><a href="https://www.courts.wa.gov/appellate_trial_courts/SupremeCourt/?fa=supremecourt.justices">Supreme Court</a></li>
	</ul>
	</li>
	<li><strong>County</strong>
	<ul>
	<li><a href="https://www.kingcounty.gov/elected.aspx">King</a>
		<ul>
			<li><a href="https://www.kingcounty.gov/elected/executive.aspx">Executive – Dow Constantine</a></li>
			<li>Council, District 1 – <a href="https://www.kingcounty.gov/council/dembowski.aspx">Rod Dembowski</a></li>
			<li>Council, District 4 – <a href="https://www.kingcounty.gov/council/kohl-welles.aspx">Jeanne Kohl-Welles</a></li>
			<li><a href="https://www.kingcounty.gov/courts/district-court/locations.aspx">District Court</a>, <a href="https://directory.kingcounty.gov/GroupDetail.asp?GroupID=7120">West Division</a></li>
			<li><a href="https://www.kingcounty.gov/courts/superior-court/directory/judges.aspx">Superior Court</a></li>
		</ul>
	</li>
	<li><a href="https://www.snohomishcountywa.gov/3840/Your-Elected-Officials">Snohomish</a>
		<ul>
			<li><a href="https://www.snohomishcountywa.gov/1922/About">Executive – Dave Somers</a></li>
			<li>Council, District 3 – <a href="https://snohomishcountywa.gov/766/District-3">Stephanie Wright</a></li>
			<li>Council, District 4 – <a href="https://snohomishcountywa.gov/767/District-4">Terry Ryan</a></li>
			<li><a href="https://snohomishcountywa.gov/Directory.aspx?DID=77">District Court, South Division</a></li>
			<li><a href="https://snohomishcountywa.gov/1345/Judicial-Officers">Superior Court</a></li>
		</ul>
	</li>
	</ul>
	</li>
	<li><strong>Municipal</strong>
	<ul>
	<li><a href="http://www.edmondswa.gov/">Edmonds</a>
	<ul>
	<li><a href="http://www.edmondswa.gov/city-council.html">City Council</a></li>
	<li><a href="http://www.edmondswa.gov/government/mayor.html">Mayor</a></li>
	<li><a href="http://www.edmondswa.gov/judge-s-welcome.html">Municipal Court, Position 1</a></li>
	</ul>
	</li>
	<li><a href="http://www.lynnwoodwa.gov/">Lynnwood</a>
	<ul>
	<li><a href="http://www.lynnwoodwa.gov/Government/City-Council.htm">City Council</a></li>
	<li><a href="http://www.lynnwoodwa.gov/Government/MayorSmith.htm">Mayor</a></li>
	</ul>
	</li>
	<li><a href="https://www.cityofmlt.com/587/City-Council">Mountlake Terrace City Council</a></li>
	<li><a href="http://www.seattle.gov/cityclerk/agendas-and-legislative-resources/find-your-council-district">Seattle City Council</a>
	<ul>
	<li>District 5 – <a href="http://www.seattle.gov/council/juarez">Debora Juarez</a></li>
	<li>At-Large 8 – <a href="http://www.seattle.gov/council/mosqueda">Teresa Mosqueda</a></li>
	<li>At-Large 9 – <a href="http://www.seattle.gov/council/gonzalez">Lorena González</a></li>
	</ul>
	</li>
	<li><a href="http://www.shorelinewa.gov/government/elected-and-appointed-officials/shoreline-city-council">Shoreline City Council</a>
	<ul>
	<li>Mayor <a href="http://www.shorelinewa.gov/government/elected-and-appointed-officials/shoreline-city-council/hall">Will Hall</a></li>
	<li><a href="http://www.shorelinewa.gov/government/elected-and-appointed-officials/shoreline-city-council/roberts">Chris Roberts</a></li>
	<li><a href="http://www.shorelinewa.gov/government/elected-and-appointed-officials/shoreline-city-council/mcconnell">Doris McConnell</a></li>
	<li><a href="http://www.shorelinewa.gov/government/elected-and-appointed-officials/shoreline-city-council/mcglashan">Keith McGlashan</a></li>
	<li><a href="http://www.shorelinewa.gov/government/elected-and-appointed-officials/shoreline-city-council/scully">Keith Scully</a></li>
	<li><a href="http://www.shorelinewa.gov/government/elected-and-appointed-officials/shoreline-city-council/chang">Susan Chang</a></li>
	</ul>
	</li>
	<li><a href="http://www.townofwoodway.com/your_government/officials.php">Woodway City Council</a></li>
	</ul>
	</li>
	<li><strong>Regional</strong>
	<ul>
	<li>King County
	<ul>
	<li>Conservation District</li>
	<li><a href="http://northcitywater.org/about/our-commissioners/">North City Water District</a></li>
	<li>Shoreline Fire District</li>
	<li>Shoreline School Board</li>
	<li>Ronald Water District</li>
	</ul>
	</li>
	<li>Snohomish County
	<ul>
	<li><a href="https://www.edmonds.wednet.edu/cms/One.aspx?portalId=306754&pageId=451666">Edmonds School Board</a></li>
	<li>Fire District 1</li>
	<li>Hospital District 2</li>
	<li>Olympic View Water District</li>
	<li>Port of Edmonds</li>
	<li><a href="https://www.snopud.com/">Public Utility District No. 1</a></li>
	<li><a href="https://www.snopud.com/AboutUs/Leadership/commission.ashx?p=1236">PUD Commissioner, District 2</a></li>
	<li>Conservation District</li>
	</ul>
	</li>
	<li>Planning Boards</li>
	</ul>
	</li>
	</ul>
	<h1>Election Results</h1>
	<dl className="links">
	<dt><a href="https://results.vote.wa.gov/results/20191105/Measures.html">2019 General Election Results</a></dt>
	<dd><ul className="menu">
		<li><a href="https://results.vote.wa.gov/results/20191105/king/">King County</a></li>
		<li><a href="https://results.vote.wa.gov/results/20191105/snohomish/">Snohomish County</a></li>
	</ul></dd>
</dl>
</Layout>
)
