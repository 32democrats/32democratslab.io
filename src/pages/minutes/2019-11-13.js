import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-11-13 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, November 13, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<dl className="agenda">
			<dt>6:00</dt><dd>Dinner <span className="money">$10 suggested donation</span></dd>
			<dt>6:30</dt><dd>Open mic and social time</dd>
			<dt>7:00</dt><dd>Call to order</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land acknowledgment</dd>
			<dt>7:03</dt><dd><Link to="/minutes/2019-10-09" className="link">Approve of previous meeting's minutes</Link></dd>
			<dt>7:05</dt><dd>Candidate thanks</dd>
			<dt>7:30</dt><dd>Election results</dd>
			<dt>7:40</dt><dd>Caucus update</dd>
			<dt>7:45</dt><dd>PCO update</dd>
			<dt>7:50</dt><dd>Break & Raffle</dd>
			<dt>8:00</dt><dd>Snohomish County Democrats chair, Hillary Moralez</dd>
			<dt>8:20</dt><dd><Link to="/resolutions">Resolutions</Link>
				<ul className="menu">
					<li><Link to="/resolutions/2019/Zero-emission-vehicles">2019 - Urging WA Adoption of the Zero Emissions Vehicle (ZEV) Program</Link></li>
				</ul>
			</dd>
			<dt>8:15</dt><dd>Treasurer's report</dd>
			<dt>8:30</dt><dd>Good of the order</dd>
			<dt>8:45</dt><dd>Adjourn</dd>
		</dl>
<h1>Minutes</h1>
<p>CALL TO ORDER<br />
<br />
PLEDGE OF ALLEGIANCE<br />
<br />
LAND ACKNOWLEDGE<br />
Acknowledgement made to land of indigenous people [Which First Nations specifically] meeting took place on.<br />
<br />
APPROVAL OF PREVIOUS MEETING'S MINUTES<br />
Previous meetings minutes approved unanimously.<br />
<br />
CANDIDATE "THANK YOU" TIME<br />
1. Betsy Robertson, Shoreline City Council member Pos. 6, spoke on behalf of Shoreline City Council member Pos. 4 and Deputy Mayor Doris McConnell, reading a letter from her. McConnell, an incumbent endorsed by the 32nd LD Democrats, said in the letter she was taking some time to recuperate from campaign season before going back to work on the council, most immediately in her role on the board of the Asian Pacific American Municipal Officials (APAMO). In the letter, McConnell thanked the org for its support, especially in doorbelling and calling.<br />
<br />
As of meeting time, McConnell led over her challenger David Chen by 368 votes, per King County's Nov. 12 release, despite having trailed Chen on election night.<br />
<br />
2. Robertson then spoke on her own behalf. She had been appointed unanimously to the council in January, but this was her first time to be elected as a candidate. Endorsed, Robertson also thank the 32nd LD Democrats for their support, in particular Carolyn Ahlgreen (PCO coordinator), Anne Udaloy, and Carin Chase, who helped her to cut turf. She said she reached 91 percent of people in her precinct. As of the Nov. 12 update, she was leading with 77.0 percent of the vote.<br />
<br />
Robertson said she was surprised how excited she was by being a Precinct Committee Officer. Other than voting, she had not previously been involved in elections due to her previous work in television news not allowing it.<br />
<br />
3. Sara Betnel, Shoreline School Board District 5 director-elect, spoke next, praising turnout. Betnel, endorsed by the 32nd LD Democrats, took time to praise opponent Joe Cunningham who had also been endorsed by the org. She said that their campaigns never criticized the other. Betnel said she had been talking with Cunningham about work they could do for the district after she was sworn in Dec. 9.<br />
<br />
Later in the meeting, after the break, Cunningham also was able to appear and thanked to org for endorsing him as well. He said that both her and Betnel weren't running against each other but "running in the same direction", and she had got there faster. Cunningham said he looked forward to working with Betnel on issues related to special education and children with disabilities.<br />
<br />
4. Meghan Jernigan, Shoreline School Board District 1 director-elect and enrolled member of the Choctaw Nation of Oklahoma, said "thank you" to the 32nd LD Democrats, first in Choctaw then in English, for the endorsement the organization gave because she's new to politics in this district. Jernigan was leading with 57.1 percent of the vote as of the Nov. 12 election update. <br />
<br />
5. Carin Chase, Edmonds School District 15 Director, District 1, praised the "Shoreline Sweep" in general. Chase said the Shoreline ground game was strong, with members' efforts cutting turf and walking with candidates being decisive in close races. Chase the said the results will be obvious in areas where there were door-knockers versus not, and she said she'd love to carpool with the other Edmonds endorsees who won.<br />
<br />
6. Mike Nelson, mayor-elect of Edmonds, had been endorsed by the 32nd District Democrats and thank the org for its help. Nelson said he was working to transition from campaign to governing. With a shout out to our Chair at the end.<br />
<br />
7. Laura Johnson, Edmonds City Council Pos. 7, had been endorsed by the 32nd District Dems and said the org is where she found strength and support. Johnson thanked Stephanie Harris for help with public speaking, and said that former State Sen. Marilyn Chase had encouraged her to run three years ago, but she wasn't quite ready for it yet, and thanked Sen. Chase for the push.<br />
<br />
8. Superior Court Judge Pos. 14 John Paul Thompson had been appointed by Gov. Inslee and personally knocked on four thousand doors in his first election. Thompson said he called in teams of retired judges to help him with his canvassing as well. He ended up winning with 55.0 percent of the vote.<br />
<br />
Thompson called attention to the absence of his frequent companion in his visits to political organizations, Edirin Okoloko who had been appointed to Superior Court Judge Pos. 7 but did not retain his position, drawing only 48.7 percent of the vote. Thompson thanked the work the 32nd District Dems did on their behalf and hoped Judge Okoloko would continue in public service in the future. Thompson informed the org he himself will be on the ballot again in 2020, and therefore campaigning soon.<br />
<br />
9. Jenna Nand, endorsed candidate for Edmonds City Council Pos. 4, said that she did not win; her campaign was crushed. Nand said she had hoped to break the color barrier in Edmonds because she wanted her city to finally represent the people she saw living there, but neither Nand nor 32nd LD Dems' endorsed candidate for Pos. 5 Alicia Crank were able to do so this time.<br />
<br />
10. Ted Hikel, endorsed for South Snohomish County RFA Commissioner District 1, spoke next. Hikel did not win his race, but reminded the organization that his opponent never came to any 32nd LD meetings. Hikel said he spent hundreds of his own money on yard signs. Hikel acknowledged that he was one of five candidates opposed by local unions but was glad the other four were able to make it on the RFA board. Hikel said he thought it was interesting that he didn't get the unions support when he had been a union man in some of his previous occupations, such as a Teamster.<br />
<br />
Hikel said that despite this being the first elections for the positions, there was no coverage of the races. Hikel said he will not run again, but his goal is to get more news coverage of these races in the future.<br />
<br />
11. Laura Johnson spoke again, this time on behalf Susan Paine, elected to Edmonds City Council Pos. 6. Johnson said Paine wanted to give a huge thank you to everyone.<br />
<br />
12. Carin Chase came up to speak again, this time for George Hurst, the incumbent endorsed for Lynnwood City Council Pos. 6 who retained his seat. Chase said Hurst sent his thanks.<br />
<br />
13. Alan Charnley, 32nd District Dems Chair, passed along a message of thanks from Jeanne Kohl-Welles, King County District 4's incumbent and from newly elected Port of Seattle Commissioner, District 2 Sam Cho, both endorsed by the org and having secured their races.<br />
<br />
<br />
ELECTION SUMMARY<br />
An election summary of national and local results followed. (See slides: <a href="https://drive.google.com/file/d/1sM3pmaWL1HauioWq9GQU06IwZ-FS-iy_/view?usp=sharing">https://drive.google.com/file/d/1sM3pmaWL1HauioWq9GQU06IwZ-FS-iy_/view?usp=sharing</a>)<br />
<br />
Highlights nationally were Democrats fully retaking the Virginia State Legislature and Kentucky governorship, but there were losses in other statewide Kentucky races and Democrats lost ground in Mississippi as well.<br />
<br />
Locally, the 32nd District Democrats' candidates were successful in Shoreline, and Amazon-backed candidates lost handily after as almost all ballots have been tallied. Edmonds was a mixed bag as two white women endorsees got on the city council, but the two women of color did not. Finally, Lynnwood went very poorly for 32nd District endorsees outside of Hurst; Okoloko, a black man, lost the election to the seat he'd been appointed to; and Snohomish County Sheriff Ty Trenary lost his election to a much more conservative subordinate in his department.<br />
<br />
Following this, there was a vote of acclimation to approve Robin McClelland to fill the vacant PCO 32-SHL-0605, having been elected in the past.<br />
<br />
The meeting had its scheduled break.<br />
<br />
COUNTY CHAIR HELLO AND REPORT WITH SNOCO CHAIR HILLARY MORALEZ<br />
Snohomish County Chair Hillary Moralez said this general election had some victories like Snohomish County Council District 2 winner Megan Dunn, but most real heartbreak. This makes Moralez want to sweep in 2020. Moralez said her father taught her that winning helps teach you to maintain but losing teaches you how to win.<br />
<br />
Moralez said the two initiatives at the top of the ballot (Referendum 88 on allowing affirmative action in the state once again and Initiative 976 slashing car tabs at the expense of transportation funding) motivated Republicans and conservative voters more than Democrats in an off-year. She said local and municipal elections still have a color barrier to breach.<br />
<br />
Referring to the Democratic National Convention planned for "beautiful, sunny, gorgeous Milwaukee", Moralez said people traveling to it as part of the Washington State delegation should be aware that the hotel is more than an hour away, so people should be prepared to bring a good book or download one on their e-reader.<br />
<br />
Moralez said Washington State's Delegate Selection and Allocation plan had been submitted and approved. They had fought to make sure there was an accurate count of the demographics for a diversity floor because previously, it had been much too low, particularly for Hispanic people and for Asian and Pacific Islanders (API): of the 107 delegates sent, 9 will be black, 16 Hispanic, 5 Native American, 15 API, 9 LGBTQ+, 12 identifying with a disability, and 30 youth. She said that a person could count for more than one of these categories but stressed it was a floor, not a cap.<br />
<br />
Finally, Moralez spoke to Snohomish County's upcoming annual fundraising gala which still had some details to be worked out but had settled on the theme of the 100 year anniversary of the legal right of most American women to vote with the adoption of the 19th Amendment. The new Gala chair will be Brenda Dejardin, and all the speakers this year will be women.<br />
<br />
In questioning, Michael Brunson reminded the body of the history of the "Sewer Socialists" of Milwaukee who governed the city for a half-century from 1910 to 1960.<br />
<br />
Another unidentified person from the crowd asked about the next Snohomish County Democrats general meeting, which will be Saturday, Jan. 25, 2020, at the Everett Labor Temple, 2810 Lombard Ave, Everett, WA 98201.<br />
<br />
Following this, Joe Cunningham arrived and spoke, as detailed previously.<br />
<br />
RESOLUTIONS<br />
Next, Snohomish County DCC Alternate 1 Lael White and King County DCC Committee 1 Dean Fournier came to present their Resolution Urging WA Adoption of the Zero Emissions Vehicle (ZEV) Program (see: <a href="https://32democrats.gitlab.io/resolutions/2019/Zero-emission-vehicles">https://32democrats.gitlab.io/resolutions/2019/Zero-emission-vehicles</a>). This would require 2.5 percent of each automaker's annual vehicle sales in the state to be electric by 2019, increasing to 8 percent by 2025; California, Oregon, New York, Massachusetts, New Jersey, Connecticut, Maine, Maryland, Rhode Island and Vermont already have developed the program following California's initiation, and Colorado looks to be the 11th. Washington State would, therefore, be the 12th.<br />
<br />
White said the resolution aligned with the 32nd LD Democrats' stated goals and platform as well as the state party's draft priorities under climate change.<br />
<br />
Fournier added that the 43rd, 44th, 5th, and 46th District had previously adopted the resolution.<br />
<br />
Carl Lewis, PCO SEA-32-2217, asked the presenters how electric vehicles had done with the recent fires and power outages in California.<br />
<br />
Jenna Nand, 32nd District 2nd Vice Chair, said that specific crisis had to do with the mismanagement of resources and infrastructure by the utility company in charge of it, Pacific Gas and Electric Company (PG&amp;E).<br />
<br />
Carin Chase, State Committee 1 for the 32nd District, asked White and Fournier to go more into the advocacy committee for the Washington State Democrats, with the pair deferring to Chase who said the committee was comprised of state committee members who would bring forth legislative priorities in mid-July.<br />
<br />
Nand asked how the ZEV program would interact with the additional fee placed on electric vehicles for their annual registration. After clarification, some discussion from the crowd ensued to the appropriateness of the additional fee as a substitute for the gasoline taxes electric vehicles would not be subject to.<br />
<br />
Subsequently, the motion to adopt the resolution passed.<br />
<br />
GOOD OF THE ORDER<br />
Carin Chase announced that presidential candidate Sen. Bernie Sanders (I-Vt.)had gotten enough signatures (2,000) to appear on the Washington State ballot, joining Sen. Elizabeth Warren (D-Mass.) and Sen. Kamala Harris (D-Calif.).<br />
<br />
Nand spoke to the 50-state effort to make sure Harris appeared on the ballots of all 50 states in the upcoming Democratic primary.<br />
<br />
Michael Brunson spoke to the upcoming 101st anniversary of the Proclamation Day of the Republic of Latvia on Nov. 18, 1918. [Michael?] said the event will be 11:30 a.m. Sunday, Nov. 17, at the Latvian Community Center in northeast Seattle, 11710 3rd Ave NE, Seattle, WA 98125, following services at the Seattle Latvian Evangelical Lutheran Church next door.<br />
<br />
Amber King announced that the progressive political action organization Our Revolution would be having its [Plan Summit to Win] in Olympia, Jan. 24-26.<br />
<br />
Liz Brown, Snohomish County DCC Committee Pos. 2, reminded the body that Monday, Nov. 11, was the 100th anniversary of the Centralia massacre (<a href="http://www.lawyersgunsmoneyblog.com/2011/11/this-day-in-labor-history-november-11-1919">http://www.lawyersgunsmoneyblog.com/2011/11/this-day-in-labor-history-november-11-1919</a>) in which an Industrial Workers of the World (IWW) hall was attacked by the American Legion on Armistice Day, leading to the death of one Wobbly lynched from jail and sentencing of seven more on charges of murder.<br />
<br />
Adrienne Fraley-Monillas, Edmonds City Council Pos. 3, said she was glad to be part of an all-woman city council but issues of racism still prevalent in some electoral contests, so this is an issue that's not just in the past.<br />
<br />
Finally, Chair Alan Charnley thanked everyone for their work in voter registration efforts nationally and locally.<br />
<br />
The motion to adjourn then passed.</p>
	</Layout>
)
