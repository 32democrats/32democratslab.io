import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>32LD Meeting Minutes – December 12, 2018</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Minutes of Biennial Re-organization Meeting, December 12, 2018
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
		</p>
		<p>
			Pursuant to electronic notice sent to all Democratic PCOs within the 32nd Legislative District, and following delivery of several pizzas and availability of hot and cold beverages, cookies et al. (the Masonic Center’s kitchen being in renovation), our biennial reorganization meeting was called to order at 7 p.m. sharp by District Chair Carin Chase. After opening with the Pledge of Allegiance, the undersigned was named Temporary Secretary and Dakota Solberg as Temporary Parliamentarian.
		</p>
		<p>
			An initial Credentials Report showed 51 PCOs present. Following a brief review of the nature of an LDDO reorganization and the general rules governing same, M/S/C to adopt the agenda as proposed.
		</p>
		<h2>
			Election of LDDO Officers
		</h2>
		<p>
			Alan Charnley was nominated to serve as our <b>LD Chair</b> for the forthcoming term. Further nominations were solicited, but there were none. M/S/C to elect Alan by acclamation – whereupon the gavel was turned over to him.
		</p>
		<p>
			Andrew Shogren (a resident of Snohomish County) was nominated to serve as <b>First Vice-Chair</b>. He spoke briefly and, there being no further nominations, M/S/C to elect him by acclamation.
		</p>
		<p>
			Jenna Nand was nominated to serve as <b>Second Vice-Chair</b>. M/S to waive our rule that this office may only be held by a PCO of at least 2 years standing. Jenna spoke briefly, whereupon the motion for waiver carried by more than a 3/4 vote. There being no further nominations, M/S/C to elect her by acclamation.
		</p>
		<p>
			Dean Fournier was nominated to serve as a <b>Representative to the Executive Board of the King County Democratic Central Committee</b>, and spoke briefly – following which Janet Way was nominated to serve as another representative to the same body. There being no further nominations, M/S/C to elect them both by acclamation.
		</p>
		<p>
			David Johnson was nominated to serve as an <b>Alternate representative to the KCDCC Executive Board</b>. Sally Soriano was nominated to serve as an alternate representative to the same body, but declined – following which Carolyn Ahlgreen was nominated to serve as an alternate representative to that body. There being no further nominations, M/S/C to elect both David and Carolyn by acclamation.
		</p>
		<p>
			Gray Petersen was nominated to serve as a <b>Representative to the Executive Board of the Snohomish County Democratic Central Committee</b>, and Liz Brown was nominated to serve as another representative to the same body. There being no further nominations, M/S/C to elect them both by acclamation.
		</p>
		<p>
			Lael White was nominated to serve as an <b>Alternate representative to the Snoho County Dems Executive Board</b>. Robert Petersen was nominated to serve as another alternate representative to that body. M/S/C (by a 3/4 vote) to waive our rule that these offices may only be filled by PCOs. There being no further nominations, M/S/C to elect both Lael and Robert by acclamation.
		</p>
		<p>
			Carin Chase was nominated to serve as one or our <b>representatives to the Washington State Democratic Central Committee</b>. Chris Roberts was nominated to serve as our other representative to that body. There being no further nominations, M/S/C to elect Carin by acclamation, and M/S/C to elect Chris by acclamation.
		</p>
		<p>
			Eric Valpey was nominated to serve as our <b>Treasurer</b>. There being no further nominations, M/S/C to elect Eric by acclamation.
		</p>
		<p>
			Sally Soriano was nominated to serve as our <b>Secretary</b>. There being no further nominations, M/S/C to elect Sally by acclamation.
		</p>
		<h2>
			Appointive Positions
		</h2>
		<p>
			Alan announced the following appointments, each subject to ratification by vote of the LDDO:
		</p>
		<table>
			<tr><td>Membership Chair</td>
			<td>Jeff Sandys</td></tr>
			<tr><td>Programs Coordinator</td>
			<td>Stephanie Harris</td></tr>
			<tr><td>Fundraising</td>
			<td>Rosamaria Graziani</td></tr>
			<tr><td>Diversity, Inclusion, and Affirmative Action</td>
			<td>Cathy Baylor</td></tr>
			<tr><td>Feder’n/Democratic Women - YOMAH</td>
			<td>Carol McMahon</td></tr>
			<tr><td>Parliamentarian</td>
			<td>Dakota Solberg</td></tr>
			<tr><td>Senior Issues Advocate</td>
			<td>Trudy Bialic</td></tr>
		</table>
		<p>
			Each of the appointees spoke, following which M/S/C to ratify all seven for the positions indicated.
		</p>
		<p>
			The following PCO appointments were then announced:
		</p>
		<table>
			<tr><td>Douglas Mormann</td>
			<td>SHL 32-0296</td></tr>
			<tr><td>Marilyn Hannan</td>
			<td>Lynnwood 11</td></tr>
			<tr><td>Mark Weiss</td>
			<td>32-0711</td></tr>
			<tr><td>Annette Ademasu</td>
			<td>SHL-0494</td></tr>
			<tr><td>Lael White (acting)</td>
			<td>MT-9</td></tr>
		</table>
		<p>
			Each of the appointees spoke, following each of which it was M/S/C to approve his/her appointment.
		</p>
		<h2>
			Concluding Business
		</h2>
		<p>
			Following a series of tributes to our retiring Senator Maralyn Chase, commenced by Rosamaria, the meeting was adjourned at 8:12 p.m. and – per the agenda – immediately morphed into our annual Winter Festivus (Party), including the raffling off of a wide variety of unusually desirable objects.
		</p>
		<p>
			Prepared and submitted by Dean Fournier, LD32 Reorg Secretary Pro Tem
		</p>
	</Layout>
)
