import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-05-08 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, May 8, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>

		<dl className="agenda">
		<dt>6:00</dt><dd>Dinner <span className="money">$10 suggested donation</span></dd>
		<dt>6:30</dt><dd>Open mic and social time</dd>
		<dt>7:00</dt><dd>Call to order</dd>
		<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
		<dt>7:03</dt><dd><Link to="/minutes/2019-04-10" className="link">Approve of previous meeting's minutes</Link></dd>
		<dt>7:05</dt><dd>PCO coordinator update</dd>
		<dt>7:15</dt><dd>Judicial endorsements</dd>
		<dt>7:35</dt><dd>Endorsement considerations
		<ul>
		<li>Judge <b>Edirin Okoloko</b>, Snohomish County Superior Court</li>
		<li>Judge <b>Paul Thompson</b>, Snohomish County Superior Court Position 14, endorsed</li>
		<li>Judge <b>Jennifer Langbehn</b>, Snohomish County Superior Court Position 13</li>
		{/* Judge Michael Ryan, King County Superior Court Position 37 */}
		{/* Judge Marshall Ferguson, King County Superior Court Position 31 */}
		{/* Judge Aimee Sutton, King County Superior Court Position 49 */}
		{/* Judge Averil Rothrock, King County Superior Court Position 16 */}
		<li><b>Jenna Nand</b> for Edmonds City Council, Pos. 4</li>
		<li><b>Diane Buckshins</b>, for re-election to Edmonds City Council Position 4, not endorsed</li>
		<li><b>Susan Paine</b>, Edmonds City Council Position 6, endorsed</li>
		</ul>
		</dd>
		<dt>8:05</dt><dd>Break & Raffle</dd>
		<dt>8:15</dt><dd>Endorsement considerations (continued)
		<ul>
		<li><b>Debora Juarez</b> for Seattle City Council, Dist. 5, endorsed</li>
		<li><b>John Lombard</b> for Seattle City Council, Dist. 5, endorsed</li>
		<li><b>Eric Blimhagen</b> for Seattle School Board, Pos. 1, endorsed</li>
		<li><b>Betsy Robertson</b> for Shoreline City Council, Pos. 6, endorsed</li>
		<li><i>(Rules suspended for the following candidates nor originally on the agenda.)</i></li>
		<li><b>Dorris McConnell</b> Shoreline City Council, Pos. 4, endorsed</li>
		<li><b>Cindy Gobel</b> for Snohomish County Auditor, endorsed</li>
		<li><b>Brian Sullivan</b> for Snohomish County Treasurer, endorsed</li>
		<li><b>Carin Chase</b> for Edmonds School Board, endorsed</li>
		<li><b>Ted Hikel</b> for Snohomish County Regional Fire District 1, endorsed</li>
		</ul>
		</dd>
		<dt>8:50</dt><dd>Treasurer's report</dd>
		<dt>9:00</dt><dd>Good of the order</dd>
		<dt>9:15</dt><dd>Adjourn</dd>
		</dl>
		<p>
			"Elections remind us not only of the rights but the responsibilities of citizenship in a democracy."
			<br/>
			- Robert Francis Kennedy
		</p>
		<div className="article">
		<p><strong>32nd Legislative District Democratic Organization<br />
Minutes of Monthly Meeting, May 8, 2019<br />
</strong>Richmond Masonic Center, 753 N. 185th, Shoreline, WA 98133 </p>
<p>https://32democrats.org/ • info@32democrats.org</p>
<p><strong>Thanks Rosamaria Graziani and team for another delicious dinner </strong>starting at 6 pm, with our social time, then followed by open mic at 6:30 pm. And also, thank you Rosamaria for your instantaneous Spanish translation during our 32nd LD meetings making them more accessible to people in our community! At 7:00 pm <strong>District Chair, Alan Charnley</strong> called the meeting to order.</p>
<p>After opening with the Pledge of Allegiance, <strong>M/S/C (Move/Seconded/Carried) to approve the minutes</strong> of our last re-scheduled meeting, April 10, 2019. <strong> </strong></p>
<ol>
	<li><strong> PCO Coordinator Update (Carolyn Ahlgreen)</strong></li>
</ol>
<p>Carolyn introduced Betsey Robertson, new PCO candidate for endorsement.</p>
<p>M/S/C</p>
<p>Carolyn announced PCO training is scheduled for 5/22 at the Shoreline Library. The meeting will begin with socializing and introductions at 5 pm. The VoteBuilder training will begin at 6:45 pm.</p>
<ol start="2">
	<li><b>Treasurer's Report (Eric Valpey)</b></li>
</ol>
<p>We're still solvent with funds in our treasury!</p>
<ol start="3">
	<li><strong> Judicial Endorsements (Alan Charnley)</strong></li>
</ol>
<p>Judicial candidates attending tonight's meeting:</p>
<ul>
	<li>Judge Paul Thompson, Candidate for Snohomish County Superior Court, Pos. 14</li>
	<li>Judge Edirin Okoloko, Candidate for Snohomish County Superior Court, Pos. 7</li>
</ul>
<p>Because both candidates received a recommended endorsement from the 32nd LD Executive Board, a first and second for nomination was not required. However, each candidate is required to entertain questions from the body, which these candidates did, after which it was M/S/C to endorse each candidate.</p>
<p>Candidate Judge Jennifer Langbehn was nominated from the floor. After several minutes of discussion it was M/S/C to endorse Judge Langbehn</p>
<ol start="3">
	<li><strong> Municipal Endorsements - Early Endorsements (Alan Charnley)</strong></li>
</ol>
<p>Municipal candidates attending tonight's meeting:</p>
<ul>
	<li>Susan Paine, Edmonds City Council, Pos. 6</li>
</ul>
<p>  2-minute intro, then M/S/C to early endorse Candidate Susan Paine</p>
<ul>
	<li>Jenna Nand, Edmonds City Council, Pos. 4</li>
</ul>
<p>  2-minute intro, then M/S/C to early endorse Candidate Jenna Nand</p>
<ul>
	<li>Diane Buckshnis, Edmonds City Council, Pos. 4</li>
</ul>
<p>  2-minute intro, then M/S/C not to early endorse Candidate Diane Buckshnis</p>
<ul>
	<li>Betsy Robertson, Shoreline City Council, Pos. 6</li>
</ul>
<p>  2-minute intro, then M/S/C to early endorse Councilmember Betsy Robertson</p>
<ul>
	<li>Debora Juarez, Seattle City Council, Dist. 5</li>
</ul>
<p>  2-minute intro, then M/S/C to early endorse Councilmember Debora Juarez</p>
<ul>
	<li>John Lombard, Seattle City Council, Dist. 5</li>
</ul>
<p>  2-minute intro, then M/S/C to early endorse Candidate John Lombard</p>
<ul>
	<li>Eric Blumhagen, Seattle School Board, Dist. 1</li>
</ul>
<p>  2-minute intro, then M/S/C to early endorse Candidate Eric Blumhagen</p>
<ol start="4">
	<li><strong> Municipal Endorsements - Motion to Suspend Rules for Consideration (Alan Charnley)</strong></li>
</ol>
<ul>
	<li>Doris McConnell, Shoreline City Council, Pos. 4</li>
</ul>
<p>  M/S/C to suspend the rules for early consideration</p>
<p>  2-minutes intro, then M/S/C to early endorse Councilmember Doris McConnell</p>
<ul>
	<li>Cindy Gobel, Snohomish County Auditor</li>
</ul>
<p>  M/S/C to suspend the rules for early consideration</p>
<p>  2-minutes intro, then M/S/C to early endorse Candidate Cindy Gobel for Snohomish County Auditor</p>
<ul>
	<li>Brian Sullivan, Snohomish County Treasurer</li>
</ul>
<p>  M/S/C to suspend the rules for early consideration</p>
<p>  2-minutes intro, then M/S/C to early endorse Councilmember Brian Sullivan for</p>
<p>  Snohomish County Treasurer</p>
<ul>
	<li>Carin Chase, Edmonds School Board, Pos. 1</li>
</ul>
<p>  M/S/C to suspend the rules for early consideration</p>
<p>  2-minutes intro, then M/S/C to early endorse School Board Member Carin Chase for</p>
<p>  Edmonds School Board</p>
<ul>
	<li>Ted Hikel, South Sno Co Fire &amp; Rescue RFT, Dist, 1</li>
</ul>
<p>  M/S/C to suspend the rules for early consideration</p>
<p>  2-minutes intro, then M/S/C to early endorse Candidate Ted Hikel for</p>
<p>  South Sno Co Fire &amp; Rescue RFT, Dist 1</p>
<ol start="5">
	<li><strong> Good of the Order<br />
<br />
</strong></li>
	<li><strong> M/S/C Meeting Adjourned at 9:15pm</strong></li>
	<li><strong> Next Meeting, Wednesday, June 12th (2nd Wed. of the month) </strong></li>
</ol>
<p>6:00 pm Dinner</p>
<p>6:30 pm Open Mic</p>
<p>7:00 pm Meeting </p>
<p>Richmond Beach Masonic Hall, 753 N. 185th St</p>
<p>(one block west of Fred Meyer @ 185th &amp; Linden)</p>
<p>Prepared and submitted by Carin Chase, LD32 Substitute Secretary</p>
<p>carin.chase@32democrats.org</p>
</div>
</Layout>
)
