import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-08-14 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, August 14, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>

		<dl className="agenda">
			<dt>6:00</dt><dd>Dinner <span className="money">$10 suggested donation</span></dd>
			<dt>6:30</dt><dd>Open mic and social time</dd>
			<dt>7:00</dt><dd>Call to order</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:03</dt><dd><Link to="/minutes/2019-06-12" className="link">Approve of previous meeting's minutes</Link></dd>
			<dt>7:05</dt><dd>Legislative update with Rep. Cindy Ryu</dd>
			<dt>7:20</dt><dd>Primary Results Update</dd>
			<dt>7:45</dt><dd>Break & Raffle</dd>
			<dt>8:30</dt><dd>Good of the order</dd>
			<dt>8:45</dt><dd>Adjourn</dd>
		</dl>
<h1>Minutes</h1>
<p>
The meeting was called to order by 2nd Vice Chair Jenna Nand at 7:04 p.m.
</p>
<p>
The pledge of allegiance was led by 2nd Vice Chair Nand.
</p>
<p>
The minutes of the June 12, 2019 meeting were moved for adoption, seconded, and passed unanimously.
</p>
<p>
Representative Cindy Ryu provided a legislative update.<br/>
She mentioned she was one of the most senior members of the legislature and there was no need for term limits. She currently serves as Chair of the Housing, Community Development & Veterans Committee, the Appropriations Committee, and the Consumer Protection & Business Committee. She said that her goal was to attend at least one legislative district meeting a year. She spoke about work to pass requirements that hospitals provide meal and rest breaks for nurses.
</p>
<p>
Representative Ryu took questions about what the legislature can do about gun control next session, about presidential tax return legislation, what the state can do about immigration policies, alternatives to payday loans, and why she endorsed a Republican candidate running in Lynnwood.
</p>
<p>
State Committee member Chris Roberts provided a legislative update.
</p>
<p>
Stephanie Harris gave a report of the endorsement committee, and mentioned that that committee is in the process of interviewing several candidates who were not in the August primary.
</p>
<p>
A motion was M/S/P to skip the Treasurer’s report.
</p>
<p>
Carolyn Alhgreen gave a PCO update and encouraged members to meet by city to talk about how to doorbell.
</p>
<h2>Good of the Order</h2>
<p>
Maralyn Chase spoke on behalf of Carin Chase for Edmonds School Board.<br/>
Jeanne Monger talked about food trucks in Echo Lake on August 15 and August 22.<br/>
Susan Paine encouraged people to volunteer for her campaign.<br/>
Jenna Nand said that the Taste of Edmonds is looking for volunteers.<br/>
Rosamaria Graziani asked Representative Ryu to intercede in a police dispute involving a campaign volunteer and Julieta Altamirano-Crosby.<br/>
Sara Betnel thanked the District for the endorsement of her campaign.
</p>
<p>
The meeting was adjourned at 8:34 p.m.
</p>
<p>
Chris Roberts, interim secretary
</p>
<h1>Notes</h1>
<h2>State Legislative Update</h2>
<p>
State Representative Cindy Ryu serves on committes for finance, housing, and consumer protection.
Ryu brought up pay-day lending regulations and noted that Federal protections are in danger of repeal.
</p>
<p>
The state passed affordable housing and eviction reform with $175 million in funding. Tenant protection was extended from 3 to 14 days.
</p>
<h3>Questions (paraphrased)</h3>
<dl>
	<dt>
		Gun regulation?
	</dt>
	<dd>
		Most Democrats are open to it. There are proposed bills for magazine limits. Already passed bills requiring background checks, lockboxes, as well as a ban on 3D printed guns.
	</dd>
	<dt>
		Presidential candidate tax publishing?
	</dt>
	<dd>
		Only have 60 days and Senate is slow.
	</dd>
	<dt>
		Ban on executions was passed by one body.
	</dt>
	<dd>
		Hope to pass.
	</dd>
	<dt>
		Vote on charter schools?
	</dt>
	<dd>
		No such vote. Last year was accused of receiving donations, but "where's the check?".
	</dd>
	<dt>
		Why endorse Republicans like Shannon Sessions in Lynnwood 7 and another?
	</dt>
	<dd>
		She endorses who she wants and "grills" them first.
	</dd>
	<dt>
		You endorsed Lynnwood Forward and there's a web link; possible ethics violation?
	</dt>
	<dd>
		Will look into it. Please email.
	</dd>
	<dt>
		DOL gave info of 350 people per month to ICE
	</dt>
	<dd>
		Called DOL to demand why for 11 months after the Governor said to end it.
		They now only do it with court order.
		Will put this into law next.
	</dd>
	<dt>
		Speak to pay-day lenders and undocumented people that are denied banking.
	</dt>
	<dd>
		One credit union does it, limited to 8 per year. It is 75% APR, takes 3 years to pay back. Going to ask BECU to do it.
		This is economical, considering that homeless services cost 40k up to 200k per year.
	</dd>
</dl>
<h2>Primary Results</h2>
<p>
	Most endorsed candidates finished in the top 2 to advance to the general election, including but not limited to:
	Doris McConnell, Joe Cunningham, Sarah Betnel, Sam Cho, Mike Nelson, and Rory Graves. Finishing 3rd were Rosamaria Graziani, Jennifer Cail, and John Lombard.
</p>
<h2>Endorsements Committee</h2>
<p>
	Not recommending one judicial candidate. Vetting candidates for Sept/Oct.
</p>
<p>
Treasurer's report postponed till next meeting due to absence.
</p>
<h2>Good of the order</h2>
<ul>
	<li>Former State Senator Maralyn Chase spoke of a Rick Steves video on fascism in Europe (and had a sign for Carin Chase for Edmonds School District.)</li>
	<li><a href="http://www.shorelinewa.gov/our-city/events-meetings/calendar">Shoreline events</a>: Food trucks on Wednesdays at Richmond Beach Saltwater Park and Thursdays at Echo Lake Park.</li>
	<li>Susan Paine for Edmonds council has yard signs.</li>
</ul>
<p>
A resolution for a tree ordinance was tabled till next meeting.
</p>
<h2>PCO Committee</h2>
<p>
	Carolyn Ahlgreen spoke of her ongoing PCO training sessions.
</p>
<p>
	Looking for multi-precinct coordinators for each city for flyer distribution and canvassing.
</p>
<p>
	She answered a few questions, including "How to access locked buildings?".
	The answer was to call the building manager or resident Democrats (found in VoteBuilder) for assistance.
</p>
<p>
	The meeting concluded with volunteers and PCOs grouped by city.
</p>
</Layout>
)
