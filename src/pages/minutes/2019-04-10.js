import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-04-10 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, April 10, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>

		<dl className="agenda">
		<dt>6:00</dt><dd>Dinner <span className="money">$10 suggested donation</span></dd>
		<dt>6:30</dt><dd>Open mic and social time</dd>
		<dt>7:00</dt><dd>Call to order</dd>
		<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
		<dt>7:03</dt><dd><Link to="/minutes/2019-03-13" className="link">Approve of previous meeting's minutes</Link></dd>
		<dt>7:05</dt><dd>Constance McBarron, political director for Pramila Jayapal</dd>
		<dt>7:10</dt><dd>Meet and greet candidates</dd>
		<dt>8:05</dt><dd>Break & Raffle</dd>
		<dt>8:10</dt><dd>Early endorsement considerations
		<ul>
		<li>King County Council Member <b><a href="https://jeannekohlwelles.com/">Jeanne Kohl-Wells</a></b>, District 4</li>
		<li>King County Judge <b>Maureen McKee</b> <a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-5-judge-maureen-mckee/">[info]</a></li>
		<li>Snohomish County
			<ul>
				<li>Council Member <b><a href="https://stephwright.com/">Stephanie Wright</a></b>, District 3</li>
				<li>Executive <b><a href="https://www.davesomers.org/">Dave Somers</a></b></li>
				<li>Sheriff <b><a href="https://www.tyforsheriff.org/">Ty Trenary</a></b></li>
			</ul>
		</li>
		<li>Edmonds
			<ul>
				<li><b><a href="https://alicia4edmonds.com/">Alicia Crank</a></b> for City Council, Pos. 5</li>
				<li><b><a href="https://www.votelaurajohnson.com/">Laura Johnson</a></b> for City Council, Pos. 7</li>
				<li><b><a href="https://www.votenelson.org/">Mike Nelson</a></b> for Mayor</li>
			</ul>
		</li>
		<li>Lynnwood City Council
			<ul>
				<li>Councilmember <b>Shirley Sutton</b>, Pos. 4</li>
				<li><b>Rosamaria Graziani</b> for Pos. 5</li>
				<li>Councilmember <b><a href="http://www.hurst4lynnwood.com/">George Hurst</a></b> Pos. 6</li>
			</ul>
		</li>
		<li>Port of Seattle Commissioner <b><a href="https://www.fredforport.com/">Fred Felleman</a></b></li>
		<li>Shoreline City Council Member, Pos. 2 <b>Keith Scully</b></li>
		</ul>
		</dd>
		<dt>8:45</dt><dd><Link to="/resolutions">Resolutions</Link>
		<ul>
		<li><Link to="/resolutions/2019/Prioritize-development-of-conventional-rail">Prioritize development of conventional rail</Link></li>
		</ul>
		</dd>
		<dt>9:05</dt><dd>Treasurer's report</dd>
		<dt>9:10</dt><dd>Good of the order</dd>
		<dt>9:15</dt><dd>Adjourn</dd>
		</dl>
		<p>
			"Those who contemplate the beauty of the earth find reserves of strength that will endure as long as life lasts. There is something infinitely healing in the repeated refrains of nature the assurance that dawn comes after night, and spring after winter."
			<br/>
			- Rachel Carson
		</p>
		<h1>
		(Draft) Minutes of Monthly Meeting, April 10, 2019
		</h1>
<p><strong>Thanks to Rosamaria Graziani and team for another delicious dinner</strong> starting at 6 pm, with our social time, then followed by open mic at 6:30 pm. Also, Rosamaria's instantaneous translation of our 32nd LD meetings into Spanish is so fantastic! At 7:00 pm <strong>District Chair, Alan Charnley</strong> called the meeting to order.</p>
<blockquote>
<p>After opening with the Pledge of Allegiance, <strong>M/S/C (Move/Seconded/Carried) to approve the minutes</strong> of our last re-scheduled meeting, March 13, 2019. </p>
</blockquote>
<p><strong>Old Business</strong></p>
<p><strong>1. Report from Rep. Pramila Jayapal by Constance McBarron, Political Director:</strong></p>
<p>constance@pramilaforcongress.com</p>
<p>Many people in the 32nd LD worked hard in the last election for Pramila and she received 83% of the vote. Pramila serves on the House Education and Labor Committee, on the House Budget Committee (where she advocates for as much domestic spending as military spending) and is Co-Chair of the Progressive Congressional Caucus.</p>
<p>Last Saturday (4/6), Pramila held a "Medicare for All" Speak Out with over 200 people in attendance all sharing their views — small business people, labor union members, and community members. This progressive bill, <a href="https://jayapal.house.gov/medicare-for-all/medicare-for-all-act-of-2019/">The Medicare for All Act of 2019</a> will improve Medicare for everyone in the United States and right now we need everyone's help to build momentum. Medicare for All activist, <strong>Mark Weiss talked about this bill earlier during open mic</strong> and please see him tonight to sign the petition or to take copies to circulate.</p>
<p>It has been 100 days since the 2019 session began, these are the bills <a href="https://www.billtrack50.com/LegislatorDetail/19901">Jayapal is sponsoring</a> and here is some of the important legislation that has been introduced or passed:</p>
<p>-<a href="https://www.npr.org/2019/02/27/698512397/house-passes-most-significant-gun-bill-in-2-decades">Background Check System for Firearm Purchases</a></p>
<p>-<a href="https://jayapal.house.gov/2019/03/08/h-r-1-the-for-the-people-act-passes-in-house-with-significant-amendments-by-jayapal/">HR 1, For the People Act</a>: reform package will end the culture of corruption in Washington D.C.</p>
<p>-A new <a href="https://thehill.com/latino/433759-house-dems-reintroduce-the-dream-act">Dream and Promise Act</a></p>
<p>-<a href="https://www.theguardian.com/us-news/2019/apr/02/pramila-jayapal-speech-equality-act-gender-nonconforming-child">Equality Act</a> which would provide non-discrimination protections for LGBTQ people across the country.</p>
<p>-Historic <a href="https://www.commondreams.org/news/2019/02/13/historic-house-approves-war-powers-resolution-end-us-complicity-yemen">War Powers Resolution</a> that would require President Trump to end U.S. military support for the on-going Saudi-led war in Yemen.</p>
<p><u>Sign-up for Pramila's email newsletter</u>: https://jayapal.house.gov</p>
<p><u>Contact info for Constance McBarron, Political Director</u>:</p>
<p>425-802-3525 constance@pramilaforcongress.com PO Box 21912, Seattle, WA 98111</p>
<p>[Alan Charnley, 32nd LD Chair: We hope to have someone from Rep. Rick Larsen’s office come to give us an update at our May 8th meeting]</p>
<p><strong>2. Candidate Speed-Dating-Interviewing &amp; Early-Endorsement Session (Ted Hikel, Alan Charnley, Stephanie Harris)</strong></p>
<p><strong>Ted Hikel, 32nd LD Endorsement Chair</strong>:</p>
<p>councilmantedhikel@hotmail.com</p>
<p>These 13 candidates have been all interviewed by the Endorsement Committee or/and previously endorsed by the 32nd LD. Because of this extensive vetting, they were viewed as qualified for "early endorsement" by the 32nd Executive Board.</p>
<p>There is a 14th candidate who qualifies for this list as well because he was previously endorsed by the 32nd— Van AuBuchon who is running for the Lynnwood City Council. The endorsement committee highly recommended him when they last endorsed him and I ask the body tonight for a 2/3rds vote to approve Van to be included on this list.</p>
<p><strong>M/S/C - Yes, with over a 2/3rds vote to include Van AuBuchon on the "consider for early endorsement" list.</strong></p>
<p><strong>Alan Charnley, 32nd LD Chair:</strong></p>
<p>alan.charnley@32democrats.org</p>
<p>Here is the list of 14 candidates who qualify for "early endorsement." Candidates are sitting at various tables and we'll all be "speed-dating interviewing" for an hour and 15 minutes.</p>
<p>Snohomish County Council Stephanie Wright</p>
<p>Snohomish County Executive Dave Somers</p>
<p>Snohomish County Sheriff Ty Trenary</p>
<p>King County Council Jeanne Kohl-Wells</p>
<p>Port of Seattle Commissioner Fred Felleman</p>
<p>Shoreline City Council Keith Scully </p>
<p>Edmonds Mayor Mike Nelson</p>
<p>Edmonds City Council Laura Johnson</p>
<p>Edmonds City Council Alicia Crank</p>
<p>Lynnwood City Council Rosamaria Graziani</p>
<p>Lynnwood City Council Shirley Sutton</p>
<p>Lynnwood City Council George Hurst</p>
<p>Lynwood City Council Van AuBuchon</p>
<p>King County Superior Court Judge Maureen McKee</p>
<p><strong>Alan Charnley, 32nd LD Chair:</strong> I would like a motion from the floor to accept this slate of 14 candidates for "early endorsement."</p>
<p><strong>Dakota Solberg, 32nd LD Parliamentarian</strong>:</p>
<p>dakota.solberg@32democrats.org</p>
<p>It is my reading of the endorsement rules that speed-dating-interviewing can substitute for interviewing a candidate but the rules would have to be suspended by a 2/3 vote to suspend the Q&amp;A period for a candidate. If anyone wants to remove a candidate from the list, a motion to amend the early endorsement slate can be made.</p>
<p><strong>Alan Charnley</strong>: I would like a motion to suspend the Q&amp;A period for candidates.</p>
<p><strong>M/S/C - Yes, the Q&amp;A period for candidates is suspended with a majority vote (3 nay votes).</strong></p>
<p><strong>Alan Charnley</strong>: I would like a motion to endorse these 14 candidates as a slate.</p>
<p><strong>M/S/C - Yes, the 14 candidates are endorsed as a slate with a majority vote (2 nay votes).</strong></p>
<p><strong>Alan Charnley:</strong> These 14 candidates now have the endorsement of the 32nd LD, but not necessarily the sole endorsement. We will be endorsing more candidates at our next monthly meeting on May 8th.</p>
<p><strong>3. State Democratic Central Committee Votes for the Primary, Carin Chase, 32nd LD State Committee Person</strong></p>
<p>carin.chase@32democrats.org</p>
<p><strong>Carin Chase:</strong> Last weekend the State Democratic Central Committee voted to no longer hold precinct caucuses but to hold a state primary. Chris Roberts voted for the primary and I voted for the caucus. I was pleased by the professionalism of the debate and discussion.</p>
<p><strong>Jenna Nand, Question:</strong> Where was the division?</p>
<p><strong>Carin Chase:</strong> People who favored the caucus felt that it was important to see your neighbors face to face. In a caucus vote if your candidate loses, your neighbors might be able to persuade you to vote for their candidate. In the primary, you vote once and don't have a second chance to vote. In some of the rural areas, people said having a caucus was the only way you would be able to find out that there were Democrats in your neighborhood. There was definitely an urban rural split. Other people said they had to park 10 blocks away from the caucus meeting and they would never attend one again. People argued that it was a party activity and why should government have to pay for it. My perspective is that it generates a lot of passion for party-building and this is from where our future leadership has always emerged. Going into 2020 though we have to be able to disagree but continue to have unity.</p>
<p><strong>4. Resolutions</strong></p>
<p><strong>a. <a href="https://32democrats.org/download/resolution-prioritize-development-of-conventional-rail/">2019 - Prioritize Development of Conventional Rail</a></strong></p>
<p><strong>Alan Charnley, 32nd LD Chair:</strong> Rail uses only 1/3rd of the energy that roadway vehicles require. This resolution speaks to utilizing our rail system to help us transition away from fossil fuel, modernize this system and serve more sectors of our state.</p>
<p><strong>M/S/C - Yes, this resolution is endorsed with a majority vote (2 nay votes).</strong></p>
<p><strong>b. Supporting Collective Bargaining Rights for Educators</strong></p>
<p><strong>Carin Chase, 32nd LD State Committee Person:</strong> This resolution is urgent and was passed by the Labor Caucus at the State Central Committee this past weekend. Two of our "Democratic" Senators (Palumbo and Mullet) are proposing to reduce teacher pay, restrict teachers' collective bargaining rights and benefit charter schools. The Labor Caucus was horrified! I did not submit this resolution within the 10-day period so I therefore ask to you to please suspend the rules to consider this urgent resolution.</p>
<p><strong>M/S/C - Yes, rules are suspended to consider this resolution with over a 2/3rds vote (unanimous)</strong></p>
<p><strong>Carin Chase:</strong> The two "Therefore, Be It Resolved" read: "that the 32nd Legislative District Democratic Organization condemn the actions of Senator Guy Palumbo and Senator Mark Mullet for their support of this attack on collective bargaining rights of public teachers and on full funding of our educational system, and for supporting charter school funding;"</p>
<p>"A copy of this resolution be sent to Senator Guy Palumbo and Senator Mark Mullet and posted on the website of the 32nd Legislative District Democratic Organization."</p>
<p><strong>M/S/C - Yes, this resolution is endorsed with a majority vote (unanimous).</strong></p>
<p><strong>5. Raffle &amp; State Senate Bill 5178</strong></p>
<p><strong>Janet Way, 32nd LD Raffle Chair:</strong> The Raffle has gone well tonight as we've made over $150. I wanted to also urge you to call your representatives and the Rules Committee as SB 5178 on Presidential Tax Release is stuck in Rules.</p>
<p><strong>6. Treasurer Report</strong></p>
<p><strong>Eric Valpey, 32nd LD Treasurer:</strong></p>
<p>eric.valpey@32democrats.org</p>
<p>It's great that the raffle is doing so well. The Executive Board will be putting together a budget so if you are interested in contributing ideas, contact a member of the E-Board. Historically we've budgeted for: campaign activity, paying for our picnic and paying our share of the Snohomish County Fair booth.</p>
<p><strong>7. New Business &amp; Good of the Order</strong></p>
<p><strong>-Jenna Nand, 32nd LD Second Vice-President:</strong></p>
<p>jenna.nand@32democrats.org</p>
<p>This past Monday I announced that I will be running for Edmonds City Council. As I start to put my campaign together I would like to discussion my vision with many of you in the 32nd LD because I've enjoyed working with this organization over many years and</p>
<p><strong>-Michael Brunson, 32nd LD PCO:</strong></p>
<p>State Senate Bill 5782 should never have passed the Senate and must be stopped in the House. It is a bill which allows people to carry switchblade knives. Senator Frockt was the only Senate member who opposed the bill and I was the only person who testified against it.</p>
<p><strong>-Cena Conteh:</strong> Thank you for being here tonight. I'm here because I'm Standing with Shirley Sutton! She has led the way so that I feel comfortable with this microphone here tonight talking with you. My name is Cena Conteh.</p>
<p><strong>-Alan Charnley, 32nd LD Chair:<br />
</strong>As an LD we met the goal of 1000 registered voters before the State Committee meeting last weekend, and as of tonight we were at 1009 registered voters for the year.</p>
<p><strong>-Mark Weiss, 32nd LD PCO:<br />
</strong>Please contact me about signing the "Medicare for All" petition.</p>
<p><strong>8. M/S/C Meeting Adjourned at 8:50pm</strong></p>
<p><strong>9. Next Meeting, Wednesday, May 8th</strong></p>
<p>6:00 pm Dinner</p>
<p>6:30 pm Open Mic</p>
<p>7:00 pm Meeting</p>
<p>Richmond Beach Masonic Hall, 753 N. 185th St</p>
<p>(one block west of Fred Meyer @ 185th &amp; Linden)</p>
<p>Prepared and submitted by Sally Soriano, LD32 Secretary</p>
<p>sally.soriano@32democrats.org</p>
</Layout>
)
