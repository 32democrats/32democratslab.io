import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-06-12 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, June 12, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<dl className="agenda">
		<dt>6:00</dt><dd>Dinner <span className="money">$10 suggested donation</span></dd>
		<dt>6:30</dt><dd>Open mic and social time</dd>
		<dt>7:00</dt><dd>Call to order</dd>
		<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
		<dt>7:03</dt><dd><Link to="/minutes/2019-05-08" className="link">Approve of previous meeting's minutes</Link></dd>
		<dt>7:05</dt><dd>Emily Parzybok: Yes! Seattle Libraries Levy</dd>
		<dt>7:10</dt><dd>Election of King County Alternate Non-Male</dd>
		<dt>7:15</dt><dd>Endorsement considerations</dd>
		<dt>8:05</dt><dd>Break & Raffle</dd>
		<dt>8:15</dt><dd>Endorsement considerations (continued)</dd>
		<dt>9:05</dt><dd>Treasurer's report</dd>
		<dt>9:10</dt><dd>Good of the order</dd>
		<dt>9:15</dt><dd>Adjourn</dd>
		</dl>
		<p>
			"With the new day comes new strength and new thoughts."
			<br/>
			- Eleanor Roosevelt
		</p>
		<ul className="menu">
			<li><Link to="/resolutions/2019/Do-No-Harm-Act">Resolution Urging Enactment Of The “Do No Harm Act”</Link></li>
		</ul>
		<h2><Link to="/endorsements/2019">Endorsements</Link></h2>
		<ul>
			<li>Edmonds School District Director, District 3
				<ul>
					<li><b>Jennifer Cail</b></li>
					<li><b>Rory Graves</b></li>
				</ul>
			</li>
			<li>Edmonds School District Director, District 5: <b>Nancy Katims</b></li>
			<li>Port of Seattle: Commissioner, Position 2: <b>Sam Cho</b></li>
			<li>Shoreline School District Director, District 1: <b>Meghan Jernigan</b></li>
			<li>Shoreline School District Director, District 4: <b>Rebeca Rivera</b></li>
			<li>Shoreline School District Director, District 5
				<ul>
					<li><b>Joe Cunningham</b></li>
					<li><b>Sara Betnel</b></li>
				</ul>
			</li>
		</ul>
<div>
<p>
<b>32<sup>nd</sup> Legislative District Democratic Organization</b>
</p>
<p>
<b>Minutes of Regular Monthly Meeting, June 12, 2019,</b>
</p>
<p>
<b>at the Richmond Masonic Center</b>
</p>
<p>
Following 15 minutes of "Open Mic" time, including remarks by Mark
Weiss, Jenna Nand and various judicial candidates and incumbents, and a
fine dinner prepared by Rosamaria Graziani's volunteer crew, the meeting
was called to order at 7:02 pm by District Chair Alan Charnley. After
opening with the Pledge of Allegiance, M/S/C to approve the minutes of
our previous meeting.
</p>
<p>
<b><u>Guest speaker: Seattle Libraries Levy</u></b>
</p>
<p>
Ben Anderstone, of the "Yes! Seattle Libraries" levy" campaign,
explained and answered questions on this Seattle ballot measure, which
would largely be a renewal of a previous Seattle levy for support of its
library system. M/S to endorse the measure. Following remarks by
Stephanie Harris in favor of the motion, it was voted to endorse the
measure.
</p>
<p>
<b><u>Election of Non-male Rep to KCDems</u></b>
</p>
<p>
In view of Janet Way's recent resignation as our female representative
to KCDCC, M/S to nominate Amber King to fill that position. There being
no further nominations, Amber spoke and was questioned, whereupon our
PCOs voted to elect her.
</p>
<p>
<b><u>Endorsements</u></b>
</p>
<p>
M/S to endorse Nina Martinez for <b>Seattle Port Commission, Position
2</b>. M/S to endorse Sam Cho for the same position. Mario Brown spoke in
favor of Ms. Martinez, and Mr. Cho spoke for himself. Following Q & A
for each, it was voted (by apparent unanimity) to endorse Mr. Cho. The
ensuing vote on endorsing Ms. Martinez attained a 57 ½% favorable vote,
slightly short of the 60% threshold required for our endorsement in this
situation.
</p>
<p>
Incumbent Port Commissioner Fred Felleman, whose re-election had already
been endorsed, then spoke briefly.
</p>
<p>
Nancy Katims spoke persuasively on behalf of her candidacy for <b>Edmonds
School Board, District 5</b>. M/S to endorse her. Following Q&A, it was it
was voted (by apparent unanimity) to endorse her.
</p>
<p>
Two candidates for <b>Edmonds School Board, District 3</b>, were
introduced: Jennifer Cail, and Rory Graves. Each spoke, and was
subjected to Q & A. M/S to endorse Ms. Cail. Following remarks by Carin
Chase (as a continuing Edmonds School Board member) in favor of
endorsing Ms. Cail, it was voted (by apparent unanimity) to endorse her.
M/S to endorse Ms. Graves. Following remarks by Carin Chase in favor of
endorsing Ms. Graves, it was voted to endorse her too.
</p>
<p>
Following an 8-minute break to carry out a scheduled raffle, the meeting
resumed at 8:15 pm.
</p>
<p>
M/S (by Endorsement Committee) to endorse Rebecca Rivera, Ph.D.,
candidate for <b>Shoreline School Board, District 5</b> — who is currently
unopposed, due to withdrawal of the incumbent. She spoke briefly,
answered questions, and was endorsed unanimously.
</p>
<p>
Meghan Jernigan., candidate for <b>Shoreline School Board, District 1</b>,
spoke briefly and was subjected to Q & A. M/S to endorse her. Carin
spoke on her behalf, following which Ms. Jernigan was endorsed
unanimously.
</p>
<p>
Joe Cunningham and Sara Betnel, competing candidates for <b>Shoreline
School Board, District 5</b>, were introduced, spoke briefly, and were
subjected to Q & A. M/S to endorse Mr. Cunningham. Following speakers
pro and con, it was voted (62% to 38%) to endorse him. M/S to endorse
Ms. Betnel as well: Following one or more speakers in favor of doing so,
it was voted (85% to 15%) to endorse her too.
</p>
<p>
<b><u>Other Business</u></b>
</p>
<p>
Carolyn Ahlgreen announced upcoming KCDCC training to be conducted on
the use of "Minivan" in campaigns.
</p>
<p>
Two PCO appointments were announced: Margaret Brennand for SHL 32-1228,
and Michael Bachety for SHL 32-1030. Each spoke. M/S/C to approve
Margaret's appointment. M/S/C to approve Michael's appointment.
</p>
<p>
M/S to endorse Anna Alexander for Judge in Position 7, Snoco Superior
Court. She spoke, and was subjected to Q & A. Upon it being pointed out
that she had not yet been interviewed by our Endorsement Committee, and
that we'd already endorsed another candidate for that position (Judge
Edirin Okoloko, who spoke), it was M/S/C to postpone a vote on Ms.
Alexander's candidacy until our August meeting.
</p>
<p>
<b><u>Good of the Order</u></b>: Jenna invited all comers to attend
a scheduled rally, and Carin announced a July 21 Commemoration of the
Life of Elaine Phelps (to be held at the Innis Arden Clubhouse). Ted
Hikel thanked us for endorsing his candidacy for a Snoco Fire District
Commissioner position, and urged others to file for such Fire District
positions. Lael White urged each of us to contact our Congressmembers in
support of H.R. 1384 ("Medicare for All"), and H.J.R. 48 (to override
the infamous <i>Citizens United</i> decision).
</p>
<p>
M/S/C to suspend the rules to enable consideration of a Resolution
Urging Enactment of the "Do No Harm Act," which would amend and limit
the scope of the 1993 "Religious Freedom Restoration Act." Printed
copies of the resolution had been available throughout the meeting, and
Carin projected its text onto a screen and read it aloud. M/S/C to adopt
the resolution.
</p>
<p>
M/S/C to adjourn, at 9:42 pm, whereupon Alan announced that our next
"meeting" will be an informal picnic on July 13, to be held at Hamlin
Park in conjunction with LD46.
</p>
<p>
Prepared and submitted by Dean Fournier, Acting Secretary <i>pro tem</i>.
</p>
</div>
</Layout>
)
