import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-10-09 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, October 9, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<dl className="agenda">
			<dt>6:00</dt><dd>Dinner <span className="money">$10 suggested donation</span></dd>
			<dt>6:30</dt><dd>Open mic and social time</dd>
			<dt>7:00</dt><dd>Call to order</dd>
			<dd>Chair statements</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land acknowledgment</dd>
			<dt>7:03</dt><dd><Link to="/minutes/2019-09-11" className="link">Approve of previous meeting's minutes</Link></dd>
			<dt>7:05</dt><dd>Legislative update with Rep. Lauren Davis</dd>
			<dt>7:35</dt><dd>PCO update</dd>
			<dt>7:50</dt><dd>Break</dd>
			<dt>8:00</dt><dd>Campaign updates</dd>
			<dt>8:10</dt><dd><Link to="/resolutions">Resolutions</Link>
				<ul>
					<li><Link to="/resolutions/2019/Amend-censure-David-Chen">A resolution to amend the September 11, 2019, censure of Shoreline City Council candidate David Chen in regards to his employer CRISTA Ministries for their anti-LGBTQ+ actions and Statements</Link></li>
					<li><Link to="/resolutions/2019/Seattle-needs-rent-control">Seattle Needs Rent Control</Link></li>
				</ul>
			</dd>
			<dt>8:15</dt><dd>Treasurer's report</dd>
			<dt>8:30</dt>
				<dd>Unfinished business</dd>
				<dd>New business</dd>
				<dd>Good of the order</dd>
			<dt>8:45</dt><dd>Adjourn</dd>
		</dl>
<p><strong>32<sup>nd</sup> Legislative District Democratic Organization</strong></p>
<p><strong>(Draft) Minutes of Regular Monthly Meeting, October 9, 2019,</strong></p>
<p><strong>at the Richmond Masonic Center</strong></p>
<p>Following an “Open Mic” time at the end of a “social hour” that included another fine dinner prepared by Rosamaria Graziani’s volunteer crew, the meeting was called to order at 7 pm by District Chair Alan Charnley and opened with the Pledge of Allegiance. After an expressed acknowledgment of our presence on Native American Lands, M/S/C to approve the minutes of our previous meeting – that of Sept. 11 – subject to correction of two judicial candidates’ position nos.: namely, that Judge Okoloko should be shown as a candidate for Position 7, and Judge Thompson for Position 14.</p>
<p><strong><u>Legislative Update</u></strong></p>
<p>Our newest Representative, Lauren Davis, presented a detailed report on her work in the State Legislature this year, including her service on the Public Safety Committee, and in particular about “Cascade Care,” a state-level health care program originated by the “Whole Washington” organization. Rep. Davis was subjected to Q &amp; A.</p>
<p><strong><u>PCO Update</u></strong></p>
<p>Carolyn Ahlgreen, assisted by Anne Udaloy of LD46 (and KCDCC), stressed the role of PCOs in just about all of our political activities, and the importance of having all precincts filled with someone who will do the necessary work.</p>
<p><strong><u>Campaign Updates</u></strong></p>
<p>After an 18-minute break for a raffle and socializing, we reconvened for updates by or on behalf of our endorsed candidates and/or campaigns, allowed 90 seconds each. The following spoke:</p>
<ul>
<li>Keith Scully</li>
<li>Amber King, on behalf of Whole Washington</li>
<li>Anne Udaloy, on behalf of the “No on 976” campaign</li>
<li>Carin Chase</li>
<li>Jenna Nand</li>
<li>Doris McConnell</li>
<li>Clara Manahan, for Jeanne Kohl-Welles</li>
<li>Ted Hikel</li>
<li>Shirley Sutton</li>
</ul>
<p><strong><u>Resolutions</u></strong></p>
<p>M/S to amend the resolution adopted at our previous meeting, regarding Shoreline City Council candidate David Chen. That motion was deemed superseded, under applicable rules, by an immediately subsequent motion: to suspend our rules to enable consideration of a new resolution, submitted by David Johnson (and posted on or website): “… <strong>to Amend the September 11, 2019 Censure of Shoreline City Council Candidate David Chen in regards to his Employer Crista Ministries for Their Anti-LGBTQ+ Actions and Statements</strong>.” After alternation of two speakers for (David Johnson and Keith Scully) and two against (Janet Way and MaryLou Eckart), the motion to consider the newly proposed resolution carried.</p>
<p>We then debated the substance of the Johnson-proposed resolution, with Liz Brown, Elizabeth Lunsford, and MaryLou Eckart speaking for its adoption, in alternation with Janet Way, Carolyn Ahlgreen, and Amber King speaking against it, following which adoption failed by a vote of 17-28.</p>
<p>M/S to adopt a posted resolution – contemporaneously available to all in printed form – titled “<strong>Seattle Needs Rent Control</strong>.” After speakers pro and con, it was voted, in apparent unanimity, to adopt it.</p>
<p>M/S to adopt a resolution – also ostensibly available to all in printed form – “<strong>Requiring Zero Emission Vehicle (ZEV) Program for WA State</strong>.” After brief discussion, the motion was withdrawn – to be considered at our next meeting.</p>
<p><strong><u>Other Business</u></strong></p>
<p>Jon Waters expressed some concern about unpleasantness that had arisen during our September meeting, regarding certain actions in Lynnwood. Following a reply by Van Aubuchon, Jenna Nand suggested that we seek an amicable process to resolve those concerns.</p>
<p>Treasurer Eric Valpey reported that we have a current bank balance of about $1000.</p>
<p>Following some further remarks as “Good of the Order,” it was M/S/C to adjourn – at 9:04 pm.</p>
<p>Prepared and submitted by Dean Fournier, Acting Secretary <em>pro tem</em>.</p>
</Layout>
)
