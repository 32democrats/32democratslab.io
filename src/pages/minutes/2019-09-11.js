import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-09-11 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, September 11, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<dl className="agenda">
			<dt>6:00</dt><dd>Dinner <span className="money">$10 suggested donation</span></dd>
			<dt>6:30</dt><dd>Open mic and social time</dd>
			<dt>7:00</dt><dd>Call to order
				<ul>
					<li>Chair statement</li>
					<li>Remember</li>
					<li>Thank you</li>
				</ul>
			</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land acknowledgment</dd>
			<dt>7:03</dt><dd><Link to="/minutes/2019-08-14" className="link">Approve of previous meeting's minutes</Link></dd>
			<dt>7:05</dt>
			<dd>
				<a className="link" href="https://dontclearcutseattle.org">Don't Clearcut Seattle</a>
				<Link className="link" to="/resolutions/2019/Tree-Urban-Forest-Protection">Resolution in Support of the Seattle Urban Forestry Commission’s Draft Tree and Urban Forest Protection Ordinance</Link>
			</dd>
			<dt>7:15</dt><dd>Terique Scott: <a href="https://balanceourtaxcode.com">Balance Our Tax Code</a></dd>
			<dt>7:35</dt><dd>Amber King: <a href="https://wholewashington.org/">Whole Washington</a></dd>
			<dt>7:55</dt><dd>Break & Raffle</dd>
			<dt>8:00</dt><dd>Campaign updates</dd>
			<dt>8:10</dt><dd>Endorsement considerations
				<ul>
					<li>
						<a className="link" href="https://shorelinewa.gov/prop1">Shoreline Proposition 1</a>
						General Obligation Bonds for Aquatic, Recreation and Community Center and Parks and Recreation Improvements
					</li>
					<li>
						<a className="link" href="https://ballotpedia.org/Washington_Referendum_88,_Vote_on_I-1000_Affirmative_Action_Measure_(2019)">
							Washington Referendum 88
						</a>
						Vote on I-1000 Affirmative Action Measure
					</li>
				</ul>
			</dd>
			{/* <dt>8:15</dt><dd>Treasurer's report</dd> */}
			<dt>8:30</dt><dd>Good of the order</dd>
			<dt>8:45</dt><dd>Adjourn</dd>
		</dl>
		<h1>Minutes</h1>
<p><span id="_Hlk509073926" className="anchor"><span id="_Hlk524787359" className="anchor"></span></span><strong>32<sup>nd</sup> Legislative District Democratic Organization</strong></p>
<p><strong>Minutes of Regular Monthly Meeting, September 11, 2019, </strong></p>
<p><strong>at the Richmond Masonic Center </strong></p>
<p>Following 10-15 minutes of “Open Mic” time at the end of a “social hour” including another fine dinner prepared by Rosamaria Graziani’s volunteer crew, the meeting was called to order at about 7 pm by District Chair Alan Charnley. After opening with the Pledge of Allegiance, M/S/C to approve the minutes of our previous meeting – whereupon, in the absence of our regular Secretary, the undersigned was asked to fill in and prepare (these) minutes.</p>
<p><span id="_Hlk530232210" className="anchor"></span></p>
<p>M/S to adopt a “Resolution in Support of the Seattle Urban Forestry Commission’s Draft Tree and Urban Forest Protection Ordinance,” drafted and explained by Steve Zemke of LD46. Following his explanation, it was voted to adopt the resolution, and various elected officials and candidates present were introduced thereafter.</p>
<p><strong><em>Program: Speakers</em></strong></p>
<p>Terique Scott, Outreach Director of “Balance Our (Washington) Tax Code,” spoke on the improving prospects for a capital gains tax in Washington, <em>possibly</em> beginning next year. As proposed – and passed by the House – the tax would apply only to capital gains of $100,000 or more in any given year, and thereby reach only about 14,000 Washingtonians, significantly fewer than are subjected to the federal capital gains tax. Despite its having passed the House, a few Democratic Senators prevented the measure from passing in the Senate. Mr. Scott was subjected to Q&amp;A regarding his presentation.</p>
<p>Our own Amber King then spoke on behalf of “Whole Washington,” a health care plan to be enacted at the state level – specifically, ours – while the federal government continues indecisive debate that fails to produce action. The cost to individuals can be determined through a calculator on Whole Washington’s website. If this program is not enacted by our Legislature, it will be presented as an initiative, with <em>volunteer</em> signature-gatherers. Amber too was subjected to Q&amp;A regarding her presentation.</p>
<p>Following a break, brief reports were presented by or on behalf of a series of our endorsed candidates (some of them incumbents), subject in each case to precise time limits:</p>
<blockquote>
<p>Mike Nelson for Edmonds Mayor</p>
<p>Carin Chase for Edmonds School Board</p>
<p>Paul Thompson for Snoco Superior Court Judge, Position 14</p>
<p>Edirin Okoloko for Snoco Superior Court Judge, Position 7</p>
<p>Debora Juarez (by surrogate Arnie Nelson), for Seattle City Council, District 5</p>
<p>George Hurst for Lynnwood City Council, Position 6</p>
<p>Shirley Sutton for Lynnwood City Council, Position 7</p>
<p>Van Aubuchon for Lynnwood City Council, Position 4</p>
<p>Sara Betnel for Shoreline School Board, Position 5</p>
<p>Ted Hikel for a Snoco Regional Fire District Commissioner position</p>
<p>Sam Cho, for Port of Seattle, Position 2</p>
<p>Doris McConnell, for Shoreline City Council, Position 4</p>
</blockquote>
<p><strong><em>Endorsements </em></strong></p>
<p>On behalf of our Endorsement Committee, M/S (Ted Hikel) to endorse Rebeca Muniz for <strong>Seattle School Board, Position 3</strong>. Following remarks on behalf of Ms. Muniz (and Q&amp;A as warranted), it was voted to endorse her. Next, it was M/S to endorse Chandra Hampson for the same position. Ms. Hampson too was represented by a speaker on her behalf, and – following Q&amp;A – it was voted to endorse her too.</p>
<p>Speakers next explained the nature and purpose of <strong>Shoreline Proposition 1</strong>, Parks &amp; Recreation Bonds. Following Q&amp;A, it was M/S/C to endorse the measure.</p>
<p>M/S (Carin) to support <strong>Referendum 88</strong>, a measure that will be on the statewide ballot in November and would, if approved by the voters, allow the Legislature-approved Initiative 1000 to go into effect (I-1000 being the measure meant to reinstate affirmative action, other than by quotas, racial or otherwise, as a tool toward racial justice). Following Q&amp;A, it was voted to recommend an “approve” vote on Referen-dum 88.</p>
<p>M/S (MaryLou Eckart) to suspend the rules in order to consider a motion of censure against Julieta Altamirano-Crosby, a candidate for Lynnwood City Council Position 5. It was agreed to defer the motion until later in the meeting.</p>
<p>M/S/C to close our discussion of Endorsements.</p>
<p><strong><em>Other Business</em></strong></p>
<p>M/S/C to suspend the rules in order to consider “<strong>A</strong> <span id="_Hlk19871829" className="anchor"></span><strong>resolution to censure Shoreline City Council candidate David Chen and his employer Crista Ministries for their anti-LGBTQ+ actions and statements</strong>.” In support of the resolution, various newspaper articles were cited. M/S to postpone consideration of the resolution but, after brief discussion, the motion for postponement failed and the resolution to censure Mr. Chen passed by an apparently overwhelming voice vote.</p>
<p>We then returned to MaryLou’s deferred motion of censure against Lynnwood candidate Julieta Altamirano-Crosby, based on Ms. Altamirano-Crosby’s having filed a police report accusing a 12-year-old boy of stealing her campaign fliers from Lynnwood doorsteps. After brief discussion, the motion carried.</p>
<p>M/S (Ted Hikel) to suspend the rules in order to consider another endorsement, but his motion failed to garner the 60% vote necessary to consider the matter at that time.</p>
<p>State Representative Mike Pellicciotti (LD30) spoke of his upcoming candidacy for State Treasurer, an office currently held by a Republican due to the 2016 primary-election fluke of too many qualified Democrats having so thoroughly split the vote, that only Republicans survived into the general election.</p>
<p>M/S/C to adjourn (at 9:28 pm).</p>
<p>Prepared and submitted by Dean Fournier, Emergency Secretary <em>pro tem</em>.</p>
</Layout>
)
