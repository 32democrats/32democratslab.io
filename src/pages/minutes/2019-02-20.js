import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-02-20 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, February 20, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<h2>Homeless issues</h2>
		<dl className="agenda">
		<dt>6:00</dt><dd>Dinner</dd>
		<dt>6:30</dt><dd>Open mic and social time</dd>
		<dt>7:00</dt><dd>Call to order</dd>
		<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
		<dt>7:02</dt><dd><Link to="/minutes/2019-01-09" className="link">Approve of previous meeting's minutes</Link></dd>
		<dt>7:05</dt><dd>Homelessness presentation by Erica C. Barnett, Q&A</dd>
		<dt>7:45</dt><dd>Break</dd>
		<dt>7:50</dt>
		<dd>
			Judicial candidates
			<ul className="">
				<li>King County Superior Court
					<ul>
						<li>Pos. 5 Judge Maureen McKee</li>
						<li>Pos. 31 Judge Marshall Ferguson</li>
					</ul>
				</li>
				<li>Snohomish County Superior Court
					<ul>
						<li>Pos. 7 Judge Edirin Okoloko</li>
						<li>Pos. 13 Judge Jennifer Langbehn</li>
						<li>Pos. 14 Judge Paul Thompson</li>
					</ul>
				</li>
			</ul>
		</dd>
		<dt>8:15</dt><dd>John Lombard, candidate for Seattle City Council, District 5</dd>
		<dt>8:25</dt><dd>Chris Porter, candidate for King Conservation District Supervisor</dd>
		<dt>8:35</dt><dd><Link to="/resolutions">Resolutions</Link>
			<ul>
				<li><Link to="/resolutions/2019/Whole-Washington-health-trust">Whole Washington health trust</Link></li>
				<li><Link to="/resolutions/2019/Raising-awareness-of-missing-and-murdered-indigenous-peoples">Raising awareness of missing and murdered indigenous peoples (Native)</Link></li>
			</ul>
		</dd>
		<dt>8:45</dt><dd>Treasurer's report, committee reports</dd>
		<dt>9:00</dt><dd>New business</dd>
		<dt>9:10</dt><dd>Good of the order</dd>
		<dt>9:15</dt><dd>Adjourn</dd>
		</dl>
		<p>
			"Darkness cannot drive out darkness; only light can do that.
			Hate cannot drive out hate; only love can do that."
			- Rev. Martin Luther King, Jr.
		</p>
		<div className="article">
<h1>Minutes of Rescheduled Monthly Meeting, February 20, 2019</h1>
<p>Email: <a href="mailto:info@32democrats.org">info@32democrats.org</a></p>
<p>Thanks to Rosamaria Graziani and the volunteer crew who provided food, drink and amazing flan for all! At 6:50pm there was 10 minutes of "open mic" time for any and all to express views and concerns. At 7:00pm District Chair, Alan Charnley called the meeting to order.</p>
<p>After opening with the Pledge of Allegiance, M/S/C (Move/Seconded/Carried) to approve the minutes of our last meeting, January 9, 2019.</p>
<h2>1. Guest Speaker: <a href="mailto:erica@thecisforcrank.com">Erica C. Barnett</a> on Homelessness in King County</h2>
<p>I've been a writer/editor at PubliCola, Stranger and the Seattle Weekly. I've been in business for over 20 years, have seen some of the biggest changes in journalism and make my living through people's support of my blog, I believe in community media and one of the areas I cover is homelessness.</p>
<h3>King County Statistics</h3>
<p>Last year in King County there were 12,000 homeless people, 52% who are "unsheltered homeless." Others are "unstably housed" and could become homeless at any minute. The biggest increase of these was 46% of the 12,000 are now living in their cars — an astonishing number. The reasons people become homeless: cost of housing is too expensive; poverty level wages; disabilities (reported by 40% of homeless); and severe mental illness (25%). It's often asked, "Why don't people just work?" Not all disabilities are visible. In the mid-1980s mental hospitals began to be emptied out, resulting in diminishing support for mentally ill people from their communities. One third of people who are homeless have experienced domestic violence. Many began to self-medicate and developed addictions, which is an extremely difficult to overcome when you are homeless. Others with criminal records have a hard time finding housing (Seattle City Council has been working on this problem with some success.)</p>
<h3>Myths & Realities</h3>
<p>King County now has a much better picture about homelessness.</p>
<ul>
<li>Myth: People who are homeless don't want housing.
<br/>Reality: People experiencing homelessness say they'd move into housing if it were affordable and safe.
</li><li>Myth: Homeless people just don't want to work.
<br/>Reality: The majority of people experiencing homelessness who are able to work are either employed or actively searching for work. More often they have experienced bankruptcy or illness.
</li><li>Myth: Homelessness affects people across all demographic categories equally.
<br/>Reality: People experiencing homelessness are disproportionately people of color and LBGTQ.
</li><li>Myth: Homeless people move from other parts of the country to take advantage of Seattle's generous benefits.
<br/>Reality: Most people who are homeless in King County did not move here from outside King County. They lived here, then become homeless. (This is our problem.)
</li><li>Myth: Most homeless people are drug addicts.
<br/>Reality: About one-third of people experiencing homelessness have an addiction. People who are homeless often use alcohol to self medicate because their entire lives are a struggle. It is extremely difficult to get treatment when on the street. The necessary first thing is housing which makes treatment possible.
</li><li>Myth: Homeless people are dangerous and likely to commit crimes if they are allowed to live on the street.
<br/>Reality: Homeless people are no more likely to commit crimes than housed people, with one exception: sleeping outdoors is still a crime.
</li>
</ul>
<h3>So What Can Progressive Governments Do?</h3>
<ol>
<li>Provide shelter and housing in communities, including suburban communities, for all types of people experiencing homelessness, including single men.</li>
<li>Fund mental health and addiction treatment. Governor Inslee is now advocating smaller mental health facilities — the result of which remains to be seen.</li>
<li>Support long-term solutions, not just quick fixes: enhanced shelters, permanent housing, long-term vouchers. Cities are moving away from short-term housing and substituting rapid re-housing, which is a voucher for private market housing. For example, a person would receive a voucher for $800 for an $1,100 rental. This works well in cities like Phoenix that are more affordable but may not work in Seattle where rents continue to go up. What works here are long-term vouchers — people just need a stable place to live. Providing long-term vouchers would cost municipalities only one quarter of what it costs to have a homeless person on the streets.</li>
<li>Advocate for eviction reform, which gives renters more time to pay their rent. Right now renters only have a 3-day grace period. Landlords have great power over renters. This state law would give renters more flexibility. (Rep. Nicole Macri (D-43rd) is working on this legislation)</li>
<li>Fund free legal aid for homeless people and cap fees that lawyers can charge. (Rep. Nicole Macri (D-43rd) is working on this legislation)</li>
</ol>
<h3>3 Categories of Housing (this distinction should be clearly made)</h3>
<ol>
<li>Very low-income housing</li>
<li>Low-income housing</li>
<li>Median (average) income housing or affordable housing</li>
</ol>
<p>I think most people in this region are compassionate. There was the forum in Ballard that was shocking but I'm actually hopeful about our community.</p>
<h2>2. <a href="mailto:brobertson@shorelinewa.gov">Betsy Robertson</a>, Newly Appointed to the Shoreline City Council, Pos. #6</h2>
<p>I applied for the position on the Shoreline City Council because I live on the eastside of Shoreline and this neighborhood is not well represented on the Council. However, it is where the bulk of the increased housing density is now occurring. I want my neighborhood to be included in this discussion. I've been Chair of the Parks Board and have been working on park issues for the past six years. A bond measure will probably be come up soon for a new community and aquatic center. I've lived in Shoreline for 12 years, grew up on Bainbridge, and studied journalism at the UW. I worked at KING TV as their community-relations director and then worked for the American Red Cross. I now look forward to working on behalf of the people of Shoreline, making sure they receive clear information regarding changes in zoning and development. I want to listen to peoples' opinions as much as possible. I appreciate being here with the 32nd LD as you are a smart audience and ask the tough questions.</p>
<ul>
<li>Q: Why are pesticides like Roundup still being used in Shoreline?
<br/>A: You're right, they shouldn't be used.
</li>
<li>Q: At the next School Board Meeting the issue of using crumb rubber on fields is coming up. I think Parks and the School District should work together to prohibit crumb rubber on public fields. What is your opinion?
<br/>A: Six months ago Parks used cork on a new field. We agree with you and are no longer using crumb rubber.
</li>
<li>Q: What is your position on expanding publicly owned housing in Shoreline?
<br/>A: I listened closely to Erica Barnett's discussion on homelessness earlier, particularly what she said about homegrown homelessness. I definitely want to work on publicly-owned housing. I think we are now in the early stages of such discussions. 
</li>
<li>Q: I live in Seattle and the rent keeps going up. Right now I can afford Shoreline. Do you think it's possible to stabilize rent and housing prices in Shoreline?
<br/>A: Decisions have to be made on up-zoning but sometimes what looks good on paper is not what looks good for people in a neighborhood when it is constructed.
</li>
</ul>
<h2>3. Other Electeds and Former Electeds Attending Tonight's Meeting (Alan)</h2>
<p>Chris Roberts, Shoreline City Council Member; Carin Chase, Edmonds School Board Member; Maralyn Chase, former WA State Senate; Sally Soriano, former Seattle School Board Member.</p>
<h2>4. Snohomish County Superior Court Judge Candidates</h2>
<h3>Judge Paul Thompson, Position #14 - www.judgepaulthompson.com</h3>
<p>I was appointed to fill a vacancy and will stand for election this November. I will run again in 2020 for a four-year term. I live about 30 blocks north of here in Edmonds. I spent 14 years as a public defender, first in Chelan and then in Snohomish. Many of my clients had great difficulty with issues such as language and addiction. I have become most interested in juvenile justice issues.</p>
<h3>Judge Jennifer Langbehn, Position #13 - www.judgejenniferlangbehn.com</h3>
<p>I was appointed to the bench last June. I'm a Pacific NW native, went to The Evergreen State College and Seattle University Law School. I worked in a small law firm in Lynnwood that focuses on families in crisis and dependency. At times it is gut-wrenching work but these years have been very precious for me. I'll bring to the bench an extensive background in family law. My goal is to serve with humility every day, to reverse my decisions when they are wrong, to make thoughtful decisions, and to be able to work with families and children in crisis.</p>
<h3>Judge Edirin Okoloko, Position #7 - www.judgeokoloko.com</h3>
<p>I was born in Nigeria and grew up with five siblings. My mother was a homemaker who taught the importance of love, empathy and humanity toward others. My father was a professor of microbiology and taught us about the divine discovery of DNA. Because of human rights issues in Nigeria, I studied law, then moved to the U.S. and got a second law degree. I served as a law clerk for former Snohomish County Superior Court Judge Michael Downes. I prosecuted violent crimes, homicides and assaults against vulnerable members of our community. I feel that I am mindful of the power of the courts to affect lives and I attempt to serve with compassion as a part of a system that strives for peace and justice. I have two children, was appointed to the bench last year and want to contribute to this country that has given me so much.</p>
<ul>
<li>Q: The 32nd LD has been having a conversation about getting rid of cash bail. What is your opinion about this?
<br/>A: Judge Jennifer Langbehn: I would look at this issue on a case-by-case basis, not based solely on the crime. Judge Paul Thompson: As judges, we cannot comment on some of these issues. I think that cash bail may be going away however. Judge Edirin Okoloko: I think you need to be very mindful of asking for bail — the type of case, if two people are charged with same crime.
</li><li>Q: Would you support e-filing by an attorney in another county?
<br/>A: Judge Jennifer Langbehn: Yes, this is a huge issue to drive all the way to Snohomish from Seattle for example. It's a big issue for the state court. Judge Paul Thompson: It is difficult for us to answer questions that may come before the court.
</li>
</ul>
<h2>5. Resolutions</h2>
<h3><Link to="/resolutions/2019/Whole-Washington-health-trust">Resolution Supporting Whole Washington's State Universal Healthcare Bill: SB 5222</Link></h3>
<p>Submitted by: <a href="mailto:lael.white@32democrats.org">Lael White</a> and <a href="mailto:sally.soriano@32democrats.org">Sally Soriano</a></p>
<p>Originated with: <a href="https://wholewashington.org">Whole Washington</a></p>
<p>WA State Universal Healthcare legislation was introduced by Senator Bob Hasegawa on January 16th (co-sponsored by Hunt and Keiser). It's similar to last year's I-1600. Volunteers were able to collect over 104,000 signatures for this initiative. Outgoing State Senator Maralyn Chase worked on this legislation, as well.</p>
<p>SB 5222 would create state health insurance that covers every person in the state with comprehensive benefits. There would be one set of rules, one pool of people and one network of health professionals. SB 5222 would redirect federal funds to the state to cover various healthcare programs such as Medicaid and Medicare. Revenues from that fund (as well as fees and taxes) would cover all state residents. Patients, not insurance companies, would choose where people go for care and patients and their health professionals would decide what care is appropriate.</p>
<p>It is critical that SB 5222 has a hearing immediately. Contact Sen. Annette Cleveland, chair of the Health & Long Term Care Committee, to urge her to schedule a hearing (360-786-7696) — see action sheet. The 32nd LD has staunchly supported Universal / Single Payer / Medicare for All Healthcare over the past two years and has passed three resolutions. This will be the fourth. Also, the State Democratic Party supports Medicare for All in it's platform. Polls show that nationwide, 86% of Democrats support Universal / Medicare for All Healthcare. In Congress, Rep. Pramila Jayapal is taking the lead on the "Medicare for All Act of 2019." It has 97 co-sponsors including Rep. Adam Smith. She will introduce it next week (2/25/19), therefore it's important that we contact the rest of the WA State Democratic Party Reps (Larsen, Schrier, Heck, DelBene and Kilmer) to urge them to co-sponsor this bill (see action sheet).</p>
<p><b>M/S/C (Move/Seconded/Carried) - Unanimously</b></p>
<h3><Link to="/resolutions/2019/Raising-awareness-of-missing-and-murdered-indigenous-peoples">Raising awareness of missing and murdered indigenous peoples (Native)</Link></h3>
<p>Submitted by: <a href="mailto:carin.chase@32democrat.org">Carin Chase</a></p>
<p>Originated with: South Central Chapter of the WA State Federation of Democratic Women</p>
<p>This resolution was passed at the Native American Caucus and Women's Caucus, Washington State Democratic Party State Central Committee meeting in January. This resolution presents statements of principles and it's important for the 32nd LD to show our solidarity.</p>
<ul>
<li>WA is one of the five states in which the percent of Native murder victims (4%) exceeds the state's percentage of resident population (1.8%).</li>
<li>One in three Native women will be raped in her lifetime.</li>
<li>Native women are murdered at a rate 10 times higher than the national average.</li>
</ul>
<p>We need increased education, awareness, and transparent reporting of this crisis. Snohomish County Human Rights Commission adopted this resolution today.</p>
<p>Timely and relevant, current State House bill under consideration:</p>
<p>HB 1713 - 2019-20 Improving law enforcement response to missing and murdered Native American women.</p>
<p>Sponsors: Mosbrucker, Gregerson, Caldier, Dye, Barkis, Corry, Sells, Lekanoff, Schmick, Orwall, Chandler, Hudgins, Ryu</p>
<p><b>M/S/C (Move/Seconded/Carried) - Unanimously</b></p>
<h3><Link to="/resolutions/2019/No-I-976">Resolution opposing Initiative Measure No. 976</Link></h3>
<p>Submitted by: <a href="mailto:lael.white@32democrats.org">Lael White</a></p>
<p>Originated with: Permanent Defense PAC & <a href="https://www.no976.org">Coalition Opposing I-976</a></p>
<p>Suspend the Rules for Discussion</p>
<p><b>M/S/C (Move/Seconded/Carried) - Unanimously</b></p>
<p>I-976 is an initiative to the 2019 Legislature written by Tim Eyman. It would repeal car tabs that fund transit investments at the state, regional, and local levels. If passed it would wipe out voter approved funding for Amtrak Cascades, Sound Transit Link light rail, Sounder commuter rail, express bus, bus rapid transit, and neighborhood bus services in communities across Washington State. These projects are crucial to alleviating severe congestion in the Puget Sound region. I-976 would also eliminate bus routes and service in communities outside of the Puget Sound region.</p>
<p>Amendments:</p>
<ul>
<li>line 10: after whereas I-976 attempts to nullify light rail, add: AND EXPRESS BUSES</li>
<li>line 25: comma after: would gut,</li>
<li>line 28: after light rail, add: BUILD NEW EXPRESS BUS SERVICE,</li>
</ul>
<p><a href="https://www.nwprogressive.org/weblog/2019/01/sound-transit-tim-eymans-i-976-would-wipe-out-nearly-7-billion-in-revenue-through-2041.html">
	Cascadia Advocate (2019-01): Tim Eyman's I-976 would wipe out nearly $7 billion in revenue through 2041
</a></p>
<p><b>M/S/C (Move/Seconded/Carried) - Unanimously</b></p>
<h2>6. King County Democrats Central Committee Report and 
Snohomish County Central Committee Democrats Report</h2>
<h3>King County - Alan Charnley</h3>
<p>At the last meeting: greeting of new delegates and discussion about how to be a stronger organization.</p>
<h3>Snohomish County - Gray Peterson, SCDCC Committee Member</h3>
<p>At the last meeting: passed six resolutions: Rank Choice Voting, Reduction of Garbage, Presidential Candidates Submission of Tax Returns, Affirmative Action I-1000, Supporting Whole Washington's State Universal Healthcare Bill SB 5222, and the Green New Deal. The theme for the 2019 SnoCo Dems Annual Gala will be The Environment. The theme for 2020 will be Women's Suffrage. Robert Petersen was appointed Communications Chair.
	<a href="https://www.facebook.com/SCDCC">Follow us on Facebook: SCDCC.</a>
	At our next meeting we will be discussing the state primary vs. the state caucus.
</p>
<h2>7. Treasurer's Report - Eric Valpey, Treasurer</h2>
<p>It's sad to report that I failed to register printing expenses in October 2018 so we had a debt of $1,200 that was paid. We are starting to plan our budget for 2019 so let us know your committee's ideas and costs so that the executive board can give their approval. We now have $1,568.00 in our treasury.</p>

<h2>7B. Precinct Committee Organizer (PCO) Election - Jacqueline Powers, SH 32-4070</h2>
<p>I lived in Snohomish County for 15 years and was a PCO, then I took a vacation in Ballard. I'm now back in Shoreline in King County. I've walked this precinct and have met a group of people who will be assisting me as PCO.</p>
<p><b>M/S/C (Move/Seconded/Carried) - Unanimously</b></p>

<h2>8. Good of the Order</h2>
<h3><a href="mailto:lael.white@32democrats.org">Lael White</a></h3>
<p>It's important to call your state legislators tomorrow (Thursday) on these bills:</p>
<ul>
<li>To support SB 5222 please contact Senator Cleveland, Chair of the Senate Health and Long Term Care Committee IMMEDIATELY Thursday 2/21 to demand she schedule a hearing. 360-786-7696</li>
<li>To support Missing and Murdered Indigenous Women please <a href="https://app.leg.wa.gov/pbc/bill/1713">comment on HB 1713</a>, and contact the House Public Safety Committee Chair, Rep. Roger Goodman 360-786-7878 to move the bill forward.</li>
<li>To OPPOSE I-976 that will gut our public transit, please <a href="https://app.leg.wa.gov/billsummary?BillNumber=976&Initiative=true">comment on the bill!</a></li>
<li>To support solutions to housing the homeless, support HB 1406, to encourage investment in affordable and supportive housing, please <a href="https://app.leg.wa.gov/pbc/bill/1406">comment on the bill</a> and call the House Finance committee chair 2/21 in the morning, and before 2/25 to urge them to support the bill. Chair Gail Tarleton, 360-786-7860.</li>
</ul>
<h3><a href="mailto:maralynchase@gmail.com">Sen. Maralyn Chase</a></h3>
<p>The "fun" raising committee will be working hard to brainstorm on how to help our treasury — please join us.</p>
<h3>Michael Brunson </h3>
<p>Celebrate the 101st Anniversary of Estonian Independence at the Latvian Hall this Sunday, February 24th.</p>
<h2>9. M/S/C Meeting Adjourned at 9:25pm.</h2>
<h2>10. Next Meeting on Wednesday, March 13, 2019, at the Richmond Masonic Center</h2>
<p>Prepared and submitted by <a href="mailto:sally.soriano@32democrats.org">Sally Soriano</a>, LD32 Secretary</p>
</div>
</Layout>
)
