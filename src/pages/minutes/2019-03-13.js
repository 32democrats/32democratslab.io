import promoImg from '../../../static/img/2019/caucus-primary-daily-struggle.png'
import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'

const promo = (
	<a
		href={promoImg}
		style={{
			float:'right',
			height:'4.5em',
			overflow:'hidden',
		}}
	>
		<img
			alt=""
			src={promoImg}
			style={{height:'200%'}}
		/>
	</a>
)

export default () => (
	<Layout>
		<Helmet>
			<title>2019-03-13 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, March 13, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>

		<dl className="agenda">
		<dt>6:00</dt><dd>Dinner <span className="money">$10 suggested donation</span></dd>
		<dt>6:30</dt><dd>Open mic and social time</dd>
		<dt>7:00</dt><dd>Call to order</dd>
		<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
		<dt>7:02</dt><dd>Chair's 2020 pledge</dd>
		<dt>7:03</dt><dd><Link to="/minutes/2019-02-20" className="link">Approve of previous meeting's minutes</Link></dd>
		<dt><h2><b>7:05</b></h2></dt><dd>
			{promo}
			<h2>Caucus, primary, or a hybrid in 2020?</h2>
			Q&A with Chris Roberts, Julie Anne Kempf, and Carin Chase
		</dd>
		<dt>7:40</dt><dd>Break</dd>
		<dt>7:45</dt><dd>Debora Juarez, Seattle City Council, District 5</dd>
		<dt>7:55</dt><dd>John Lombard, candidate for Seattle City Council, District 5</dd>
		<dt>8:05</dt><dd>Garth Fell, candidate for Snohomish County Auditor</dd>
		<dt>8:15</dt><dd>King County Superior Court:
			<ul>
			<li>Judge Maureen McKee, Pos.5</li>
			<li>Judge Marshall Ferguson, Pos. 31</li>
			<li>Judge Aimée Sutton, Pos. 49</li>
			</ul>
		</dd>
		<dt>8:35</dt><dd>Chris Porter, candidate for King County Conservation District Supervisor</dd>
		<dt>8:40</dt><dd>PCO Training and KCDCC Work with Carolyn Ahlgreen</dd>
		<dt>8:45</dt><dd><Link to="/resolutions">Resolutions</Link>
		<ul>
		<li><Link to="/resolutions/2019/Concurrent-construction-light-rail">Concurrent construction of the NE 130th St. light rail station and Lynnwood Link Extension</Link></li>
		<li><Link to="/resolutions/2019/Rail-development-electrification-Green-New-Deal">Rail development and electrification as a Green New Deal solution</Link></li>
		<li><Link to="/resolutions/2019/Opposing-new-and-expanded-gas-infrastructure">Opposing new and expanded gas infrastructure in Washington state</Link></li>
		</ul>
		</dd>
		<dt>8:55</dt><dd>Treasurer's report</dd>
		<dt>9:00</dt><dd>New business</dd>
		<dt>9:05</dt><dd>Good of the order</dd>
		<dt>9:15</dt><dd>Adjourn</dd>
		</dl>
		<p>
			"Let us remember: One book, one pen, one child, and one teacher can change the world."
			<br/>
			- Malala Yousafzai
		</p>
		<div className="article">
<h1>Minutes of Monthly Meeting, March 13, 2019</h1>
<p><a href="https://32democrats.org/">https://32democrats.org/</a> • <a href="mailto:info@32democrats.org">info@32democrats.org</a></p>
<p>Thanks to Rosamaria and team for the delicious and healthy dinner! At 6:30 pm there was "open mic" time for any and all to express views and concerns. At 7:00pm <strong>District Chair, Alan Charnley</strong> called the meeting to order.</p>
<p>After opening with the Pledge of Allegiance, <strong>M/S/C (Move/Seconded/Carried) to approve the minutes</strong> of our last re-scheduled meeting, February 20, 2019.</p>
<p><u>New Business</u></p>
<p><strong>1. For 2020, should our state party adopt a hybrid caucus system or switch to a regular mail ballot primary to determine delegates in the selection of our presidential candidate?</strong></p>
<p>Under new Democratic Party rules if we choose the caucus primary, no one can be excluded from voting and all buildings holding caucuses will be ADA compliant.</p>
<p><strong>Carin Chase, 32nd LD State Committeemember (Chris Roberts, Committeemember, is out of town) and Julie Anne Kempf, Chair of the 46th LD answer questions</strong>: what happens if a presidential candidate drops out; costs of the different proposals; the different options for voting; how many languages will the ballot be in; will the ballot be secure; and why not try rank choice voting?
<br/><a href="mailto:carin.chase@32democrats.org">carin.chase@32democrats.org</a>
<br/><a href="mailto:chris.roberts@32democrats.org">chris.roberts@32democrats.org</a></p>
<p>Advantages of the caucus process: talk with your neighbors, increases activism, inspires new party leaders, and is less vulnerable to dark money. Advantages of the primary process: less expensive and could involve more voters.</p>
<p>Carin: We'll send out the 32nd LD straw poll again, including the comments. The tally was 64% in favor of the hybrid caucus and 32% in favor the ballot primary.</p>
<p><strong>Action:</strong> Please read the two proposals and <strong>write your comments to the WA State Democrats here: WAelectioncenter.com</strong> — comment deadline is April 4th. The WA State Democratic Central Committee will vote on these proposals on April 7th.</p>
<p><strong>2. Resolution: Support for Concurrent Construction of the NE 130th St Light Rail Station and Lynnwood Link Extension</strong>, presented by <strong>Seattle City Council Member Debora Juarez</strong>. This resolution proposes that the 130th Light Rail Station be completed 7 years earlier (in 2024) than originally planned. The decision to re-schedule construction for this earlier date will be made by Sound Transit this fall.
<br/><a href="mailto:Debora.Juarez@seattle.gov">Debora.Juarez@seattle.gov</a></p>
<p><strong>Vote on Resolution: Passes</strong></p>
<p><strong>Action:</strong> Give Sound Transit feedback: <a href="mailto:emailtheboard@soundtransit.org">emailtheboard@soundtransit.org</a></p>
<p><strong>3. Introducing John Lombard - Candidate for Seattle City Council, District 5</strong> I've worked on community development issues in Lake City and have experience running homeless programs. I also helped create the D5 network as an association of community groups and worked with the Thornton Creek Alliance on watershed restoration. <a href="https://votejohnlombard.com">https://votejohnlombard.com</a></p>
<p><strong>4. Introducing Garth Fell - Candidate for Snohomish County Auditor</strong></p>
<p>I am currently Elections Manager and am running for County Auditor. The auditor is responsible for elections and managing legal documents. The current auditor position is open as the office is term limited. I've had over 20 years of experience in elections and view elections as the life-blood of our democracy. Dave Sommers, Snohomish County Executive, endorses me as does the County Sheriff. Insuring people have secure documents gives them real security. I'm proud that Washington State is a leader in voter rights with same day registration.
<br/><a href="https://electgarthfell.com">https://electgarthfell.com</a></p>
<p><strong>5. Introducing Judicial Candidates, King County Superior Court</strong></p>
<p><strong>a) Judge Aimee Sutton, Pos. 49</strong></p>
<p>As a college undergraduate I was a Latin America Studies major, spent time in South America, and volunteered teaching ESL at El Centro de la Raza. I wanted to do direct service, went to UW Law School and then became a public defender. I worked for 8 years in a law firm, mainly with non-English speaking clients. It's important for everyone to understand the system and not be confused by the process. I'm a member of the Latino Bar Association of Washington. There are 53 judges in the King County Superior Court system, each bringing something unique to the bench. What I feel I bring is a passion for justice and equality under the law.
<br/><a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-49-judge-aimee-sutton/">https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-49-judge-aimee-sutton/</a></p>
<p><strong>b) Judge Marshall Ferguson, Pos. 31</strong></p>
<p>I grew up in a large family and graduated from high school on Lopez Island. I learned a passion for law from my mother who was an attorney. A role model for me was a judge in San Juan County, Judge John Linde. Everyone who went before him knew they would be treated fairly and that he took a long-term view. He wanted everyone to have the best shot at success. He would work with families, schools, and treatment centers. His approach was to balanced accountability with a restorative justice. I believe this approach works.
<br/><a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-31-judge-marshall-ferguson/">https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-31-judge-marshall-ferguson/</a></p>
<p><strong>c) Judge Maureen McKee, Pos. 5</strong></p>
<p>I met many of you last year when I was running for Seattle Municipal Court. I then applied to the Superior Court and the Governor appointed me. I grew up in Tulsa, Oklahoma in a biracial family where I first experienced being in a community where people didn't look like me. As an undergraduate I had an African American Studies major and became aware of how critical it was to learn about communities. I was a VISTA volunteer, worked with people with disabilities and then went to law school. I have been a public defender for 16 years. I've worked closely with every marginalized community in King County. I bring a commitment to being a judge who is fair, thoughtful and compassionate, and one who serves the community.
<br/><a href="https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-5-judge-maureen-mckee/">https://31stdistrictdemocrats.org/king-county-superior-court-judge-pos-5-judge-maureen-mckee/</a></p>
<p><strong>6. Introducing Mike Nelson, Edmonds City Council Member, Candidate for Mayor of Edmonds</strong></p>
<p>I keep this Dr. Martin L. King, Jr. quote in mind as I govern as a city council member: "It may be true that the law cannot change the heart, but it can restrain the heartless." I've tried to stand up and protect our environment from those who want to pollute our marsh and I've been able to get the Dept. of Ecology to provide more protections for our marsh. When our government pulled out of the Paris Climate Accords, I said, "What can our city government do to set an example?" We've called for 100% renewable energy electricity in our city buildings by 2019 and citywide by 2025. When our state legislature was refusing to act on gun violence, as one of only two cities in the state we passed requirements to: 1) report the loss of a firearm, and 2) to safely store firearms. I'm proud of this and also proud to say the NRA is suing us. Council Member Morales and myself have created our first diversity commission. We've created task forces to combat homelessness and the opioid crisis. We've created a Youth Commission. When we govern with compassion for one another, include one another, care for one another — love wins, our children win, our democracy wins, our common humanity wins. We all win.
<br/><a href="https://www.votenelson.org">https://www.votenelson.org</a></p>
<p><strong>Great membership support of Mike Nelson for Mayor of Edmonds</strong></p>
<p><strong>7. Snohomish County Democrats Central Committee Report - Liz Brown</strong></p>
<p>The next meeting for the Snohomish County Democrats is Saturday, April 20th at the Labor Temple. The Snohomish County Democrats Gala will be held on August 10th at the Edmonds Yacht Club.
<br/><a href="mailto:liz.brown@32democrats.org">liz.brown@32democrats.org</a></p>
<p><strong>8. PCO (Precinct Committee Organizer) Training Report &amp; King County Democrats Central Committee Work - Carolyn Ahlgreen</strong></p>
<p>Next Monday evening, March 18th, 5pm, we'll have PCO Training at the Mountlake Terrace Library (23300 58th Ave W, Mountlake Terrace, WA 98043). The King County Democrats' PCO Committee is meeting on a regular basis with a goal of 75% coverage of PCOs across the county.</p>
<p><strong>Action:</strong> Anyone who would like to become a mini-coordinator for a small geographic area around your neighborhood, please contact me:
<br/><a href="mailto:cahlgreen@32democrats.org">cahlgreen@32democrats.org</a></p>
<p><strong>9. Two Environmental Resolutions - Lael White</strong>
<br/><a href="mailto:lael.white@32democrats.org">lael.white@32democrats.org</a></p>
<p><strong>a) Opposing New and Expanded Gas Infrastructure in WA State</strong></p>
<p>Stacy Oaks and Jess Wallach are here from 350.org to present this resolution: Washington State has been a target for new gas infrastructure and with the climate report coming out of the UN we have very little time to transition off of fossil fuel. This resolution speaks to the fact that fuel from fracking is not a bridge fuel. Science is showing us that fracked gas is actually worse than coal. It's important for Democrats to be a part of the coalition taking on this fight.</p>
<p><strong>Vote to Endorse: Passes Unanimously</strong></p>
<p><strong>b) For Rail Development and Electrification: As a Green New Deal Solution</strong></p>
<p>Lael: Our transportation sector creates a huge amount of CO2 emissions and I'd like to thank Tom White for his work on this resolution and on this concept. Rail uses only 1/3rd of the energy that roadway vehicles require. This resolution speaks to utilizing our rail system to help us transition away from fossil fuel. This is something we can do right now. We can modernize this system and serve more sectors of our state.</p>
<p><strong>Vote to Table until the next meeting to give people time to read additions to the text: Passes</strong></p>
<p><strong>10. Treasurer's Report - Eric Valpey</strong>
<br/><a href="mailto:eric.valpey@32democrats.org">eric.valpey@32democrats.org</a></p>
<p>Thank you all for your membership dues and donations — it's really helping. We are now paying out support (which we voted on last year) for our reps to go to state meetings. I think the good news is that we are running a fairly efficient low-cost ship!</p>
<p><strong>11. Introducing King Conservation District</strong></p>
<p><strong>Board of Supervisor Candidate - Rachel Molloy</strong>
<br/>electrachaelmolloy.com</p>
<p>You can request your ballot as a landowner. You pay into this benefit so you should have a say where this money is allocated in our community. I was born and raised in the farmlands, I've worked in the fields on family farms, and I have a climate justice background. The issue of language translation for various communities is also important to me. Be sure and get your ballot in.</p>
<p><strong>Action:</strong> You can go to the King Conservation District website and opt in:
<br/><a href="http://kingcd.org">http://kingcd.org</a></p>
<p><strong>12. M/S/C to Adjourn, 9:30pm</strong></p>
<p><strong>13. Next Meeting, April 10th</strong></p>
<p>6:00 pm Dinner
<br/>6:30pm Open Mic
<br/>7:00pm Meeting
<br/>Richmond Beach Masonic Hall, 753 N. 185th St (one block west of Fred Meyer)</p>
<p>Respectfully,
<br/>Prepared and submitted by Sally Soriano, LD32 Secretary
<br/><a href="mailto:sally.soriano@32democrats.org">sally.soriano@32democrats.org</a></p>
</div>
</Layout>
)
