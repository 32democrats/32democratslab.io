import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>32LD Meeting Minutes – August 8, 2018</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Minutes of Regular Monthly Meeting, August 8, 2018
		</h1>
		<p>
			<b>at the Richmond Masonic Center </b>
		</p>
		<p>
			Following about 20 minutes of "open mic" time for any and all to express
			briefly their views, concerns, and announcements, the meeting was called
			to order at 7 pm by District Chair Carin Chase.  After opening with the
			Pledge of Allegiance, M/S/C to adopt the agenda as proposed. M/S/C to
			approve the minutes of our June 13 meeting. M/S/C to approve the minutes
			of our most recent regular meeting, held in conjunction with our annual
			picnic on July 15.
		</p>

		<p>
			Following recognition of various elected officials present, Rosamaria
			Graziani introduced five young volunteers who had performed admirably in
			GOTV work leading up to the August 7 "winnowing" primary.  Online-posted
			interim results of that primary were reviewed, including Democrats Carolyn
			Long having come from virtually nowhere in CD3, to a position closely
			behind GOP Rep. Jamie Herrera Beutler; Dr. Kim Schrier as the apparent
			leader among three strong Democrats in CD8 (where turnout in King County
			has been running higher than in the past), to oppose GOP perennial Dino
			Rossi; and Spokane legislator Lisa Brown looking quite strong in her
			attempt to replace GOP leader Cathy McMorris Rogers in CD5.  Meanwhile,
			incumbent Democratic Reps. Pramila Jayapal and Rick Larsen are enjoying
			comfortable leads.
		</p>

		<p>
			In our own LD, Rep. Cindy Ryu has a big lead in her campaign for
			re-election, and our endorsee Rebecca Wolfe is leading in her race for
			Snoco PUD Commissioner Position 2.  Senator Chase and Jesse Salomon are
			comfortably ahead of the GOP candidate for our Senate seat, while fellow
			Democrat Lauren Davis seems way ahead of both our endorsee Chris Roberts
			and their GOP opponent.
		</p>

		<p>The lesson from all these races: Voting is important!</p>

		<p>
			Dean Fournier reported that the Metropolitan Democratic Club will be
			holding a Seattle-area fundraiser for Carolyn Long, at a date to be
			determined, and Debbie Viertel reported on opportunities to help Lisa
			Brown.
		</p>

		<p>
			<b>Other Business</b>
			:
		</p>

		<p>
			Carin reminded us of our opportunity to take a turn at our assigned table
			at the Evergreen State Fair, at its annual "Children's Day."
		</p>

		<p>
			Following a break for a raffle, Eric Valpey reported a current bank
			balance of $1628 (exclusive of tonight's receipts), and that presently
			unpaid bills total about $400.  Carin added that Eric has also been named
			the new Treasurer of KCDCC!
		</p>

		<p>
			It was further reported that switching from our present "throwaway" name
			tags, to permanent ID buttons, is under consideration.
		</p>

		<p>
			Logan Rysemus of the Alliance for Gun Responsibility presented for our
			consideration a resolution "In Support of Yes on 1639, Safe Schools, Safe
			Communities."  M/S/C to suspend our Rules' advance-notice requirement, to
			enable timely consideration of the proposed resolution. M/S to adopt the
			resolution. It was reported that several Democratic organizations have
			already endorsed it, whereupon it was voted to adopt the resolution –
			which Carin undertook to disseminate as appropriate.
		</p>

		<p>
			M/S/C (Dean) to endorse and support a "Yes" vote on Initiative 1631, a
			ballot measure intended to protect against climate change by curbing
			pollution from carbon emissions.
		</p>

		<p>
			As a final note, Carin reported on the death of our beloved nonagenarian
			member, Elaine Phelps, a few days after our July meeting.  True to form,
			Elaine made sure she signed her primary ballot before she passed away.
			Carin went on to play a video of the Seattle City Council's adopting, on
			July 30, a resolution honoring Elaine's contributions and longtime
			dedication to the values we hold dear.
		</p>

		<p>
			<b>Good of the Order</b>
			:  Senator Chase thanked our members for their continued support.  Carin,
			supported by her Edmonds School Board colleague Deborah Kilgore, asserted
			the inadequacy of the Legislature's supposedly <i>McCleary</i>
			-based appropriations for school funding
		</p>

		<p>M/S/C to adjourn, at 9:29 pm.</p>

		<p>Prepared and submitted by Dean Fournier, LD32 Secretary</p>
	</Layout>
)