import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../../components/layout'
import {dates} from '../../../components/dates'
import {LinkResolution} from '../../../components/resolution'
import {MinutesHeader, minutes_type, minutes_status} from '../../../components/minutes'

import {r as israel_palestine_peace} from '../../resolutions/2020/israel-palestine-peace'
import {r as save_shoreline_trees} from '../../resolutions/2020/save-shoreline-trees'
import {r as aid_low_income_lynnwood_residents} from '../../resolutions/2020/aid-low-income-lynnwood-residents'
const resolutions = [israel_palestine_peace, save_shoreline_trees, aid_low_income_lynnwood_residents]

const date = '2020-02-12T19:00-07:00'
const status = minutes_status.draft
const type = minutes_type.general

export const m = {date, status, type}
const {prev_path} = dates(date)
export default () => (
	<Layout>
		<MinutesHeader m={m}/>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<dl className="agenda">
			<dt>7:00</dt><dd>Call To Order</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land Acknowledgment</dd>
			<dt>7:03</dt><dd><Link to={prev_path} className="link">Approval of previous minutes</Link></dd>
			<dt>7:04</dt><dd>Conversation with Washington State Commissioner of Public Lands Hilary Franz</dd>
			<dt>7:40</dt><dd>Hello with Judges Okoloko and Thompson</dd>
			<dt>7:50</dt><dd>Break</dd>
			<dt>8:00</dt><dd>It's time to select our representatives for committees for our State Democratic Convention. This is an exciting and vital opportunity to shape the state party platform and agenda.
				<ul>
					<li>Rules Committee</li>
					<li>Platform Committee</li>
					<li>Credentials Committee</li>
				</ul>
			</dd>
			<dt>8:25</dt><dd><Link to="/resolutions" className="link">Resolutions</Link>
				<ul className="menu">
					{resolutions.map(r=><li><LinkResolution r={r} status={true}/></li>)}
				</ul>
			</dd>
			<dt>8:40</dt><dd>Lael White</dd>
			<dt>8:45</dt><dd>Good of the order</dd>
			<dt>8:50</dt><dd>Adjourn</dd>
		</dl>
		<p>Section 8 of the Rules:</p>
		<p>B. The motions will then be debated and voted upon in the order in which they were made, starting with the Executive Board motion(s). <strong>Up to four (4) speakers, alternating pro (two) and con (two),</strong> will be allowed. Each speaker may take up to <strong>thirty (30) seconds</strong>. If no one requests recognition to speak against the motion after the first pro speaker has spoken, then debate will be closed, and the vote taken. This sequence of “debate and vote” will be followed for each of the motions, in turn</p>
		<p>
			"It is reason, and not passion, which must guide our deliberations, guide our debate, and guide our decision."
			<br/>
			- Congresswoman Barbara Jordan
		</p>
		<pre className="article">{`
DRAFT MINUTES
February 12, 2020
32nd Legislative District Monthly Meeting
7pm, Masonic Hall, 753 N.185th, Shoreline

Thanks again to Rosamaria Graziani for her great English-to-Spanish
translation that is projected on the wall during our meetings— so
meaningful in bridging Spanish-English cultures!

7:05-7:07 PM - MEETING CALLED TO ORDER
Chair Alan Charnley called the meeting to order and followed with the pledge of allegiance.
Alan acknowledged that this land we are meeting on tonight was originally the land that
belonged to Coast Salish people.
• The January 8, 2020 LD Minutes were approved, M/S/C (Moved / Seconded / Carried).
• Alan recognized the many elected officials attending our meeting.
7:08 - Alan proposed an agenda modification: Give a 15-minute time slot to the judgeship
and county council candidates to speak, just before our break.
The motion was M/S/C.

7:15-7:40 - GUEST SPEAKER
Hillary Franz, WA State Commissioner of Public Lands
https://www.dnr.wa.gov/commissioner
Commissioner Franz discussed her job of managing nearly 6 million acres of public lands in
our state— 2.6 million acres of aquatic lands and 2 million acres of timber. She overseas the
wild fire fighting team, state geological issues— five live volcanoes, tsunami alert system,
flood alert system and recovery, and landslide alert and recovery. She said 2018 was the
most devastating wildfire year with 1,850 wildfires; resulting in over 440,000 acres burnt and
the worst air quality the state has ever experienced. She said our forests have become
tinderboxes. She is therefore introducing climate impact legislation so our forests will become
less fragile and our communities more resilient. She has hired forest scientists to lead this
work. She is proposing a $62.5 million budget per year to fund wildfire control and
reforestation to be paid through a surcharge on home and auto insurance at $15 per year.
She said, "When we have the resources, we can keep the forest fires small and under
control. Also, some fires should actually NOT be fought."
Commissioner Franz took audience questions: replanting forests; protecting the forest
surrounding Fircrest and the future use of that land; timber sales; Federal forests and wild
fires; salmon rehab; getting the forests back to a more sustainable level; suction dredge
mining; survival of wild animals; and she announced the State Attorney General's latest
victory regarding Atlantic salmon.

7:50-8:20 - CANDIDATES FOR JUDGESHIPS &
SNOHOMISH COUNTY COUNCIL

• Carolyn Ladd, running for King County Superior Court Judge, Pos. 51
I watched Anita Hill and then years later Christine Blasey Ford and realized how important it
is to have more women on the bench. I've worked Judge Pro Tempore (substitute judge) in
the Seattle Municipal Court. I'll work to make sure there are more diverse juries.
https://www.carolynladdforjudge.com/

• Cassandra Lopez Shaw, Snohomish Superior Court Judge
Last year I ran against Paul Thompson who won the seat. Working as a trial attorney, I
continually represent people who are disadvantaged, do not speak English and do not see
people who look like themselves in the courts. I am running so I can work to make
Snohomish County courts more inclusive. I haven't decided yet which judge position I will be
running for.
https://www.cassandrashawforjudge.com/

• Collin McMahon, Snohomish County Council, Pos. 4
I am running to fill the Council vacancy, Pos. 4. The selection will be made by the Council
later this month. I've worked as a public defender in Snohomish County and became a public
defender so I could work to provide advocacy and a voice for individuals who might be facing
inequality and unfairness in the judicial system.
https://www.snocopda.org/staff-members/colin/

• Anna Alexander, Snohomish County Superior Court Judge, Pos. 7
Thank you for being so encouraging when I was running last year. I am a political refugee
from the Soviet Union. I won the position at the polls to fill a retired position. You will see my
name on the 2020 ballot. I started work the day before Thanksgiving, 2019. I look forward to
being interviewed by your 32nd LD endorsement committee.
https://www.alexanderforjudge.com/

• Edirin Okoloko, Snohomish County Superior Court Judge, Pos. 4
I'd like to thank the 32nd LD for being so encouraging when I was running last time. After
Governor Inslee appointment me in 2018 I served on the bench. I was then defeated by Anna
Alexander, whom you just heard from. I received many messages of encouragement. I
wanted to continue to be a voice for diversity, both race and gender from within the legal
community. Governor Inslee reappointed me back to the bench in January, 2020.
https://www.facebook.com/Judge-Edirin-Okoloko-2357077551227062/?ref=py_c
https://www.judgeokoloko.com/ (2019 campaign)

• Paul Thompson, Snohomish County Superior Court Judge, Pos. 14
It's exciting to be here in the 32nd LD as I live close to here. Governor Inslee appointed me in
2018, and then for the upcoming election in 2019, I was endorsed by every Democratic Party
LD. I did retain my seat, and now I'm back to seek your endorsement. Before 2018, I had
first-hand experienced with individual client representation, for example— the difficulty of just
getting to court and the many mental health issues. Governor Inslee is helping to change the
path to help reform the system and it has been an amazing opportunity.
https://www.judgepaulthompson.com/

8:20-8:35 - BREAK

8:35-8:45 - SELECTION OF LD REPS TO THE
STATE CONVENTION COMMITTEES
• Rules Committee Rep - Chris Roberts - M/S/C unanimous
• Platform Committee Candidates: - Gary McCaig: Served on the Platform Committee twice
and want to focus on PCO issues as well. Started with the Rev. Jesse Jackson campaign in
'88. I will be able to well represent this group.
-Gray Petersen: Am serving as the Snohomish County Rep and was just elected to the
Platform Committee. We have to be centered in our democratic principles. Two years ago my
husband wrote the housing justice plank. Justice and equity is incorporated into everything I
do.
• Vote: Gary McCaig = 30 votes; Gray Petersen = 20 votes
• Platform Committee Rep - Gary McCaig
• Credentials Committee Rep - Deborah Viertel - M/S/C unanimous

8:45-9:00 - RESOLUTIONS

1) In Support of Washington Democrats' Commitment to Israeli-Palestinian Peace, and
in Opposition to any Further Occupation, Settlement Expansion, or Other Unilateral
Annexation of West Bank Territory
Originated from Jeremy Voss, Co-Chair, J Street U at UW
Guest Speakers from J Street UW: Kevin Kenegue, Ethan Schwartz, Coley Ring
We support the right of both Israelis and Palestinians to govern themselves, each in their
own viable state, and we oppose the further occupation of territory in the West Bank, whether
by settlement expansion or unilateral annexation.
Amendment: the resolution is meant to include Gaza as part of the intended Palestinian
state, and should specifically say so.
Concerns expressed in Q&A: Two-state solution could be problematic if Israel takes all the
national resources; what about the right to return; Bernie strongly supports a two-state
solution; actions of Netanyahu should be condemned as well; and Israel must remove all
settlements they have placed on the West Bank otherwise there will never be peace in the
region.
https://www.facebook.com/JStreetUUW/
M/S/C - Passed as amended

2) Save Shoreline Trees (see text at end of minutes)
Originated from Janet Way, 32nd LD PCO
Additional Speaker: Bergith Kayyali
We urge the Shoreline City Council to direct staff to take action to prevent further loss of
significant trees by developing alternative policies for sidewalk development and for new and
more effective tree and development codes; support proposals at Fircrest School, public
schools and other public properties to protect all existing Urban Forest; work proactively with
citizens to achieve these goals; and appropriate the funding required.
Fmr. CM Way: Development in Shoreline, including light rail could result in the loss of 2-
3,000 trees. This would be an Armageddon for trees in Shoreline. This resolution proposes
alternatives for Shoreline — permeable pavement, etc. I'll send these alternatives to the 32nd
Facebook.
Bergith: Our organization, Save Shoreline Trees has members from Lynnwood, Edmonds
and Seattle as well working with us on this tree canopy project which helps keep a much
needed local focus on our climate issues.
https://saveshorelinetrees.com/
M/S/C - Passed

3) In Support of Aid for Low Income Lynnwood Residents
Originated from Ted Hikel, 32nd LD PCO
Additional Speaker: George Hurst, Lynnwood City Council Member
Recognize the ability of Cities to enact a Displaced Low-Income Tenant Ordinance, per RCW
59- 18.440, that will provide financial assistance to current Lynnwood low income residents
forced from their current homes, and urge all members of the Lynnwood City Council to pass
such an Ordinance that will provide support for displaced low income residents in the City,
and urge the Snohomish County Housing Authority to assist the City of Lynnwood in its
efforts to aid displaced low income residents in their efforts to alleviate the hardships
endured by their displacement.
Fmr. CM Hikel: 230 very low-income apartments are being destroyed and two Lynnwood
Council Members are proposing to use state funds to hire movers and for first and last rent
for new apartments.
CM Hurst: The city and light rail can pay up to $1,000 for 200 families from the $2 million
fund. There are seven other organizations giving support. This is a worthy cause and we
have modified this resolution for tonight's consideration.
Q&A:
Q: Could the resolution call on the Housing Authority to not demolish those apartments?
A: They are over 50 years old, have black mold, the sewer system is in disrepair and it is not
cost effective to repair the apartment complex.
M/S/C - Passed unanimously

9:00-9:05 - 32ND LD ENVIRONMENT & CLIMATE CAUCUS
LAEL WHITE, CHAIR
Lael: The 32nd LD has been invited to sign a letter to phase out gas cars and convert to
electric by 2030. There was a bill proposing this legislation in our State Legislature but it did
not pass out of the first committee. We are hoping it will be introduced again in the 2021
session.
Letter originated from WA State Environmental & Climate Caucus and 350.org
Lael reads: HB2515 - Deployment of electric Vehicles (see text at end of minutes)
M/S That the 32LD Democrats sign onto the WA State ECC and 350.org letter to phase out
gas cars and convert to electric by 2030.
M/S/C - Passed unanimously

9:05-9:10 - PCO ELECTIONS
Lynnwood 22 - Gary McCaig - M/S/C - Elected unanimously

9:10-9:20 - GOOD-OF-THE-ORDER SPEAKERS
• Riall Johnson, Organizer for #FillEverySeat PCO Recruitment Initiative!
King County Democrats plan on filling every PCO seat around the County and have set a
goal by the end of April to have 150 new PCOs. We think this will increase votes by 30,000.
• Mark Weiss, Member: I've been working on Medicare for All and state legislation, Limit the
Price of Insulin, which got through the State Senate. Pramila Jayapal had a great Town Hall
Meeting with over 600 people.
• Fmr. Sen. Maralyn Chase, No War on Iran 32nd LD Task Force: We're holding a Task
Force Zoom call next Thursday, February 20th. Call-in information and discussion points are
on the back table. At Pramila's recent Town Hall meeting, that Mark just mentioned, Pramila
said, in the recent conflict with Iran, "what Trump was doing was reckless actions of military
brinksmanship." We must prepare for the possibility that Trump may start a war with Iran just
to gain political power. maralynchase@gmail.com
• Charles Gracia with Tulsi 2020: My candidate who is running for President is a
progressive Democrat and has distinguished herself in the past decade in the military, in
Congress, and now attempting to re- direct military resources to our economy. Please pick up
a brochure in the back of the room and I hope you'll consider canvassing for Tulsi.
Charles.gracia@tulsi2020.com 206-771-2584
• Kara Jess, Regional Field Organizer for Bernie: I just ran over from Iowa, post caucus!
I'm with Bernie because I feel deeply that Healthcare is a human right. Bernie proposed
legislation to lower drug prices and he's not taking corporate money. Please check out
https://map.berniesanders.com/ to help canvass or sign up.
• Carolyn Ahlgreen, Canvass Organizer, Bernie Sanders Campaign: We're making
canvassing for Bernie a social experience. See the Bernie event website for dates and times
near you: https://events.berniesanders.com/?tag_ids=3
• Carol McMahon, Chair, YoMa Women's Organization: People gather at 196th & 75th for
the Planned Parenthood demonstration each Tuesday at 9am. We do have some opposition
but we feel the demonstration is highly informative for the community. Also, we are working
with the 1st LD Democrats on a women's forum that will be held in the IEU Hall in Bothell to
celebrate the 19th Amendment and the hopeful passage of the ERA.
• Jenna Nand, 32nd LD Substitue Parliamentarian/Vice Chair: I've always regarded the
beautiful dinner, before our LD meeting, made by Rosamaria, to be one of the most important
times of the evening for bonding and sense of inclusion. I encourage all of you to make your
voices heard about continuing to have dinners before our meetings as the only place this
discussion is happening right now is in the e-board and I think it's important for the 32nd LD
to model democracy and hear from members as well.
• Lael White, Chair, 32nd LD Climate and Environment Caucus: Check out Coltura.org — Let's
Move Beyond Gasoline to Cheaper, Cleaner Alternatives

9:25 - M/S/C ADJOURN

Submitted by Sally Soriano, Secretary
`}<hr/>{`
Resolution: Save Shoreline Trees
WHEREAS, a Healthy Urban Forest contributes to human health in a variety of ways:
oxygen, beauty, air and water quality, wildlife habitat, helps to retain rainwater, creates
ecosystems that support people of all ages, and promotes happiness and well being, and
WHEREAS, the City of Shoreline has been declared a “Tree City USA” by the Arbor Day
Foundation, and
WHEREAS, the City of Shoreline has passed numerous Climate Change reduction
measures, and
WHEREAS, unfortunately the City of Shoreline’s development and density goals which
include: infrastructure projects such as sidewalks; and theories on affordable housing and
other needs; are now at odds with the City’s Tree Canopy and Climate Change Reduction
Goals, and
WHEREAS, current and proposed projects such as: WSDOT headquarters redevelopment
adjacent to Dayton Ave N, Fircrest School redevelopment; Light Rail Development, Shoreline
School District redevelopment projects; and over 140 possible Town Home developments;
have caused or will cause the destruction of 2-3000 significant trees, and
WHEREAS, these trees are extremely important to both the 32nd District Democrats,
because the 32nd platform includes strong provisions for Clean Air, Clean Water-and Climate
Protection, and to the Citizens of Shoreline who value them for both their beauty and for
creating a Healthy City,
THEREFORE, BE IT RESOLVED that the 32nd District Democrats urge the Shoreline City
Council to direct staff to take action to prevent further loss of significant trees by developing
alternative policies for sidewalk development and for new and more effective tree and
development codes; support proposals at Fircrest School, public schools and other public
properties to protect all existing Urban Forest; work proactively with citizens to achieve these
goals; and appropriate the funding required, and
BE IT FURTHER RESOLVED that we request this Resolution be published and sent to the
Shoreline City Council, the 32nd State delegation, the King County Council and the King
County Executive, requesting a positive acknowledgment and action steps to succeed.
Originated from Janet Way, 32nd LD PCO
`}<hr/>{`
HB 2515 SUPPORT LETTER FINAL
January __ 2020
Washington State Representatives
Legislative Building
Olympia, WA 98504
Re: Support for HB 2515
Dear Representatives,
We, the organizations listed below, support HB 2515, a bill which accelerates Washington’s
deployment of electric vehicles (EVs).
HB 2515, introduced by Rep. Macri, and co-sponsored as of this date by Reps. Doglio,
Fitzgibbon, Gregerson, Ramel, Senn, Tarleton, Peterson, Pollet, and Hudgins, requires that
all light-duty and passenger vehicles sold or registered in the state be electric, beginning with
the 2030 model year. Model year 2029 and earlier vehicles can be kept and sold indefinitely.
A 2030 vehicle electrification date creates certainty for state and local governments,
automakers, charging companies, utilities, investors, landlords, and employers regarding the
EV market and helps them plan for the EV charging infrastructure needed for widespread EV
adoption. It enables a smooth transition to EVs with minimal burden on taxpayers or the state
budget.
Fifteen countries and Elizabeth Warren, Bernie Sanders, Jay Inslee, Pete Buttigieg and
Andrew Yang all support a vehicle electrification requirement at the national level, most with
a 2030 deadline.
Switching to local electricity to power our cars will save the citizens of Washington billions of
dollars in fuel and maintenance costs, boost our economy and create jobs. Fueling with
electricity costs on average less than 30% of the cost of gasoline in Washington. Battery
costs have been dropping 15 to 20% a year, and electric vehicle sticker prices are forecast to
be lower than those of comparable gas cars by the mid-2020s.
The transition to electric vehicles will substantially lower volumes of gasoline released from
motor vehicles into Puget Sound and other waterways, reducing a significant threat to
Chinook salmon and other marine life.
For these reasons, we strongly urge you to support HB 2515.
Sincerely,
[add logo here]
Originated from WA State Democrats Environmental & Climate Caucus and 350.org
`}
</pre>
	</Layout>
)
