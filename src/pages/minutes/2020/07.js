import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../../components/layout'
import {dates} from '../../../components/dates'
import {MinutesHeader, minutes_type, minutes_status} from '../../../components/minutes'

const date = '2020-07-08T18:30-07:00'
const remote = true
const status = minutes_status.approved
const type = minutes_type.general

export const m = {date, remote, status, type}
const {prev_path} = dates(date)
export default () => (
	<Layout>
		<MinutesHeader m={m}/>
		<dl className="agenda">
		<dt>6:30</dt><dd>Sign In</dd>
			<dt>7:00</dt><dd>Call to order</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land Acknowledgment</dd>
			<dt>7:03</dt><dd><Link to={prev_path} className="link">Approve of previous meeting's minutes</Link></dd>
			<dt>7:04</dt><dd>Moment of Silence for Racial Justice</dd>
			<dt>7:05</dt><dd>PCO Appointments</dd>
			<dt>7:10</dt><dd>Regional Field Director, Savanna Steele: State Coordinated Campaign conversation</dd>
			<dt>7:40</dt><dd>Sen. Joe Nguyen</dd>
			<dt>8:00</dt><dd><strike>Resolutions</strike> (None submitted on time for this month.)</dd>
			<dt>8:15</dt><dd>New Business</dd>
			<dt>8:20</dt><dd>Good of the order</dd>
			<dt>8:30</dt><dd>Adjourn</dd>
		</dl>
		<p>
			“If there is no struggle, there is no progress.”
			<br/>– Frederick Douglass
		</p>
{/* <p><strong><u>DRAFT MINUTES</u></strong></p> */}
<p><strong>32nd Legislative District Meeting</strong></p>
<p><strong>Wednesday, July 8, 2020, 7:00 PM</strong></p>
<p>Via Zoom &amp; Call-in</p>
<p>32democrats.org • chair@32democrats.org</p>
<p><strong>6:30 PM Sign-In</strong></p>
<p><strong>7:00 Call To Order by Alan Charnley, Chair</strong></p>
<p><strong>7:01 Amend Agenda (Alan)</strong></p>
<p><strong>M/S/C to Consider and M/S/C to Approve</strong></p>
<p>A "thank you" from Endorsed Candidates. Lauren Davis, and Shirley Sutton, will be added.</p>
<p><strong>7:05 Pledge of Allegiance</strong></p>
<p><strong>7:08 Land Acknowledgment</strong></p>
<p>We in the 32nd LD acknowledge that we live and work on the ancestral lands of the Duwamish, Tulalip, and the Puyallup Tribes, and next to their ancestral waters, the Salish Sea. We honor with gratitude this land, water and the Puyallup, Tulalip, and Duwamish Peoples.</p>
<p><strong>7:40 ~ Moment of Silence for Racial Justice ~</strong></p>
<p><strong>7:42 Approval of Minutes - <a href="https://32democrats.org/download/2020-06-10/">June 10, 2020</a>, M/S/C - Approved</strong></p>
<p><strong>7:43 'Thank You to members of the 32nd LD,' from Endorsed Candidates Lauren Davis, 32nd State Rep., Pos. 2, and Shirley Sutton, 32nd State Rep., Pos. 1</strong></p>
<p><strong>7:44 PCO Appointment - Jerry Crook, SHL-32-0346</strong></p>
<p><u>Jerry Crook:</u> I thought it was important that you had someone in this 32nd LD organization who voted for Adlai Stevenson! I've had a long experience in the Democratic Party and have been a PCO off and on. I've just retired from being a practicing attorney and look forward to becoming a very active PCO. <strong>M/S/C - Approved</strong></p>
<p><strong>7:46 Savanna Steele, WA State Coordinated Campaign, Regional Field Director</strong></p>
<p><u>Savanna Steele:</u> I cover NW Washington State. The campaign works on behalf of Democrats up and down the ticket to make sure we are turning out the base Democrats, so Districts can focus more on persuadable voters. In the past we've had campaign offices, canvassing events, and phone banking. This year we are doing everything virtual. We will have two phone banks a day leading up to the Primary on Zoom and digital platforms. Folks from across the state get to see each other and we are actually making more contacts than we have made in the past. We've been ID-ing voters and next we will focus on GOTV (Get Out The Vote). We would like to see 32nd members hop on and help out with this statewide effort. We have the process fine-tuned and the training has been simplified. There's a 90% chance that people who turn out in the Primary will also turn out in the General. <a href="https://www.wa-democrats.org/organizingevents/">https://www.wa-democrats.org/organizingevents/</a></p>
<p>Q&amp;A:</p>
<p>Q: How do you deal with people not picking up on cold calls? A: Twice a week we run a "dialer" which puts you on the line after the voter answers. It is highly effective.</p>
<p>Q: If I go on the Coordinated Campaign phone-bank, can I call my own Precinct? or LD?</p>
<p>A: I'm happy to pull your own Precinct but you have to take the time to call the entire Precinct list. Yes we have specific LD lists.</p>
<p>Q: How do we know which voters we want to turn out?</p>
<p>A: We are reaching out to Strong Democrats who will vote. The bottom line is making sure people turn in their ballot.</p>
<p>Q: Will the Coordinated Campaign be going door-to-door?</p>
<p>A: No, for the safety of everyone involved. Also, we want to have phone conversations about turning in ballots.</p>
<p>Q: Can I phone without using Zoom?</p>
<p>A: Yes, if you can get trained on Zoom initially, afterwards you can call on whatever device you want.</p>
<p><strong>8:05 Carol McMahon, 32nd SnoCo PCO Co-Coordinator</strong></p>
<p><u>Carol McMahon:</u> I'm glad I'm able to follow Savanna. The 32nd PCO Coordinators (Lynnwood, MLT, Edmonds, Shoreline, Seattle) will be sending PCOs an email tomorrow with our new COVID-19 strategy for contacting voters in your Precinct. It is similar to what Savanna just discussed. For the 32nd GOTV, we will only be contacting voters by phone for the Primary. You will receive a PDF with your Precinct map, phone numbers and a sample script. Ballots will be mailed out next Wednesday, July 15th, and voters will receive them by Friday the 17th. Ten percent of voters fill out their ballot the first weekend after they have received it. So we hope you'll have time to call your Precinct July 15th, 16th and 17th. I just want to know we have your back. Feel free to let us know how phone calling is working out for you.</p>
<p><strong>8:10 WA State Budget for 2021: State Senator Joe Nguyen, 34th LD (West Seattle)</strong></p>
<p><u>State Senator Nguyen:</u> We're looking at potential budget options for Washington State, given the current crisis we are facing. We are now seeing a significant decrease in our budget and we are faced with how to address that in an equitable way — with a racial and economic justice lens associated. We will be facing an $8.9 billion dollar shortfall through fiscal year 2023. Eighty-five percent of our budget is constrained — either because of a Constitutional issue, federal, or matching healthcare dollars. This then leaves about $7.7 billion dollars on the potential chopping block. This amount is flexible, it directly supports people, higher education, early learning, child care, social services, affordable housing. All issues important for those on the margins who need the help to become stable. In 2008, there was an all cuts budget and we then saw some of the worst increases in homelessness and in mental health issues. At the same time we saw some of the greatest growth for some of the wealthiest people in the entire world, who live here. One of the reasons I'm hoping to have this conversation with your LD is because we want to stand strong against having an austerity budget and making sure that we are actually putting together a package that reflects the values of people here.</p>
<p>There are a few revenue options that are being discussed: the payroll option or "boss tax", what they've been doing in Seattle; capital gains; and carbon pricing. We have to make sure that we are moving away from our regressive tax structure, because folks at the bottom have been paying a lot more than folks at the top.</p>
<p>To look at a comparison, there is one person, here in Washington State, during this pandemic, who has made $33 billion dollars. In contrast, the Seattle "boss tax" they just passed this past week generates $200 million dollars every year, which is .61% of $33 billion dollars. Hypothetically, if this one person were paying this modest taxation amount, it would take them 165 years to pay out all the money ($33B) they made in the past four months. For myself, and a lot of legislators, we can see that this state has great wealth but a regressive tax structure.</p>
<p>I just wanted to be a resource for your LD, and the great leadership you have to help fight back, so we don't just have an austerity budget for the people of Washington State.</p>
<p>Q&amp;A:</p>
<p>Q: What can be done with the budget for small farms, under 120-acres?</p>
<p>A: Just like small businesses, some of the tax referrals should be forgiven or at least renegotiated. We need to not just plug the budget, but to raise enough revenue to mitigate some of these taxes. Given our tax structure, they are probably paying more in taxes than the larger farms or businesses are.</p>
<p>Q: What services might fall off the chopping block with the $7.7 billion dollars that is flexible?</p>
<p>A: One example is conversations with Corrections. There are people who have been incarcerated by way of laws that are historically unjust and racist in their origin. One of the ways I think we could make a dent in saving money for the state is letting go of inmates over the age of 65 who pose no threat. That's very difficult, politically. Another cut might be laying off state employees. This is what happened in 2008, and would be unfortunate. Higher education might be another. Early learning and childcare is potentially on the chopping block. Also, long term care in nursing homes. The amount of federal help will be a fairly big indicator of what we can do in Washington. In 2008, we were able to get $8 billion dollars of relief. Right now there is an exercise to cut all departments and none of them are good.</p>
<p>Q: Likelihood of a capital gains tax?</p>
<p>A: I think it's fairly high. Many ideas that were off the table in the past are now back on the table: that payroll or "boss tax", as they call it in Seattle; and capital gains — even though it will be challenged. As Sen. Chase knows, the Senate has not been the problem. The average people who pay capital gains have a net worth of a million dollars. We are only one of seven states without capital gains or a state income tax.</p>
<p>Q: Additional sources of income...a state bank? Would you support that?</p>
<p>A. For sure! Sen. Hasegawa and I are big supporters. Philosophically, we shouldn't be sending millions of state tax dollars to Wall St. each year, and it would give us a chance to build state infrastructure. I wrote a policy paper for a State Bank that I would be glad to share. We've been discussing how, in this economic recovery, would a state bank, play a role in infrastructure? We would be leveraging those tax dollars for infrastructure for our state. Right now if you are a big city you can go to Wall St. and get financing but that's not a possibilities for smaller cities. A state bank would enable Lynnwood and Edmonds to borrow more easily, and also, people's taxes would be staying in their communities. A state bank is a huge factor in an economic recovery and I will be working on it with Sen. Hasegawa.</p>
<p>Q: What kind of resistance are you seeing for a state income tax?</p>
<p>A: I'm from the 34th in Seattle and my district supports it. The State Constitution doesn't actually prohibit tax on income or tax on property. This decision was made in 1930, during a very different time. I would encourage the State Supreme Court to revisit that decision, even though they chose not to this year, in a Seattle case. One of the possibilities, to push back on this Constitutional issue, would be to go with a flat tax, which is not completely progressive but might open the door to a state income tax. I want to lower taxes for the vast majority of residents in Washington State. If you are lower or middle-income, you have a higher tax rate than folks who make over $500,000 a year. We need a progressive income tax to rebalance this inequity.</p>
<p>Q: Are you going to lead changing police practices such as chokeholds and 'qualified immunity'?</p>
<p>A: Heck yes! We already have significant policies ready to go which would make accountability much more real — banning chokeholds, demilitarizing the police, body cameras. Also, if you are a law-enforcement official who violated some sort of rule, you can technically resign and be hired some place else. 'Qualified immunity' is a federal level rule, but we can do 'claims,' which is a civil level rule against the officer. For me, it is a multifaceted approach. The Colorado legislature, right now, is a model for moving good legislation forward. It will be a top priority for us to pass this legislation to show that Black Lives actually do Matter.</p>
<p><strong>8:40 New Business</strong></p>
<p>• <u>Gary McCaig, 32nd WA State Democrats 2020 Platform Rep</u></p>
<p>I'm posting the 2020 WA State Democratic Platform on my Facebook. There are parts of it that are spectacular. Also, I'd like to do a QR coded (a square that can be accessed with a smart phone) poster to help people, particularly immigrants, register to vote.</p>
<p><u>Carin Chase:</u> I think those are both excellent ideas.</p>
<p>• <u>Carin Chase, 32nd Rep to State Democrats Central Committee</u></p>
<p>The DNC Presidential Convention, scheduled to be held in Milwaukee, will now be virtual, with participation via Zoom. There is work being done right now on the platform. I was honored to be elected a Sanders delegate and was appointed to the DNC Credentials Committee.</p>
<p><strong>8:50 Good of the Order</strong></p>
<p>• <u>Brent McFarlane, 32nd ECC:</u> The DNC Environment &amp; Climate Caucus put out their platform for 2020. It looks very good. It very much parallels the Green New Deal policies that the 32nd LD has supported. Lael White just posted it to our 32nd Facebook. I urge you to look it over and I hope we will consider endorsing it.</p>
<p>• <u>Edmonds City Council President, Adrienne Fraley-Monillas</u> : We had some issues with our council that you might have read about in the newspapers, regarding some disrespectful ways of referring to people of color. I think that some lessons have been learned and I'm hoping we can get some training soon so that folks can better realize how things have changed.</p>
<p>• <u>Candidate Cindy Ryu</u>: I will be having a virtual coffee discussion on July 16th at 6pm with guest Rep. Lauren Davis.</p>
<p>• <u>Edmonds School Board Member, Carin Chase:</u> I'm glad that Joe was able to join us tonight because as we all know, our state is stronger when we all stand together. I wanted to let people know that I'll be joining Joe tomorrow at noon on Facebook to talk about schools and SROs (School Resource Officers). <a href="https://www.facebook.com/carin.chase?fref=search&amp;__tn__=%2Cd%2CP-R&amp;eid=ARDq_ejtGhMRk39pwZoYIl971N__uLXJRrH1bCJi6TTrzLUlStxVI2i9dDMuBI-a1T51N8tmn6CdyEFA">https://www.facebook.com/carin.chase?fref=search&amp;__tn__=%2Cd%2CP-R&amp;eid=ARDq_ejtGhMRk39pwZoYIl971N__uLXJRrH1bCJi6TTrzLUlStxVI2i9dDMuBI-a1T51N8tmn6CdyEFA</a></p>
<p>Also, the Edmonds School District is moving forward with a hybrid model for re-opening schools in September. I invite everyone to join us in crafting this plan. We have over 20 work groups discussing how to start school in an equitable way. <a href="https://www.edmonds.wednet.edu/">https://www.edmonds.wednet.edu/</a></p>
<p>• <u>Carol McMahon:</u> Just to let you know that Candidate Shirley Sutton will be holding a "Conversation" on Friday, July 31st, 7:30pm and we hope you will join us.</p>
<p>• <u>Ted Hikel:</u> I'm reporting on the Lynnwood City Council activity. The Mayor appointed a Salary Commission, which was approved by the Council. This Salary Commission has to make their recommendation for an upcoming budget for 2021-22. Their decision was supposed to have been made before the end of April but they did not meet because of CV-19. The Mayor then asked the Council for an extension. All seven Council Members had already signed a letter saying that they did not want any increases in salaries because of lack of revenue. The Commission, however, was able to then get four votes from the Council allowing the Commission to now have input. We'll see what happens.</p>
<p><strong>9:10 PM M/S/C - Meeting Adjourned</strong></p>
<p><strong>Submitted by,</strong></p>
<p>Sally Soriano, 32LD Secretary</p>
<p>sally.soriano@32democrats.org</p>

	</Layout>
)



