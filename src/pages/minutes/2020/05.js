import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../../components/layout'
import {dates} from '../../../components/dates'
import {LinkResolution} from '../../../components/resolution'
import {MinutesHeader, minutes_type, minutes_status} from '../../../components/minutes'

import {r as water_affordability} from '../../resolutions/2020/water-affordability'
import {r as tax_big} from '../../resolutions/2020/tax-big-businesses-fund-green-housing-seattle'
import {r as national_infrastructure_bank} from '../../resolutions/2020/national-infrastructure-bank'
const resolutions = [national_infrastructure_bank, water_affordability, tax_big]

const date = '2020-05-13T19:00-07:00'
const remote = true
const status = minutes_status.approved
const type = minutes_type.general

export const m = {date, remote, status, type}
const {prev_path} = dates(date)
export default () => (
	<Layout>
		<MinutesHeader m={m}/>
		<dl className="agenda">
			<dt>7:00</dt><dd>Call to order</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land Acknowledgment</dd>
			<dt>7:03</dt><dd><Link to={prev_path} className="link">Approve of previous meeting's minutes</Link></dd>
			<dt>7:04</dt><dd>Welcome by the Chair</dd><dd>Become a PCO</dd>
			<dt>7:06</dt><dd>Congratulations to our delegates</dd>
			<dt>7:10</dt><dd>State Rep. Gael Tarleton, candidate for WA Secretary of State</dd>
			<dt>7:35</dt><dd>Candidates</dd>
			<dt>8:00</dt><dd>Become a PCO</dd>
			<dt>8:02</dt><dd><Link to="/resolutions" className="link">Resolutions</Link>
				<ul className="menu">
					{resolutions.map(r=><li><LinkResolution r={r} status={true}/></li>)}
				</ul>
			</dd>
			<dt>8:20</dt><dd>New Business </dd>
			<dt>8:25</dt><dd>Good of the order</dd>
			<dt>8:30</dt><dd>Adjourn</dd>
		</dl>
		<p>
			“There are many little ways to enlarge your child’s world. Love of books is the best of all.”
			<br/>– Jackie Kennedy
		</p>
<p><strong><u>MINUTES</u></strong></p>
<p><strong>32nd Legislative District Meeting</strong></p>
<p><strong>Via Zoom &amp; Call-in</strong></p>
<p><strong>Wednesday, May 13, 2020, 7:00 PM</strong></p>
<p><strong>32democrats.org</strong></p>
<p><strong>7:00 Call To Order by Chair Alan Charnley</strong></p>
<p><strong>7:01 Pledge of Allegiance</strong></p>
<p><strong>7:02 Land Acknowledgment</strong></p>
<p><strong>7:03 <a href="https://32democrats.org/download/2020-04-15-minutes-draft/">April 15th Minutes</a></strong> Approved</p>
<p><strong>7:04 Welcome by the Chair/Don't forget to run for PCO</strong></p>
<p><strong>7:06</strong> <strong><a href="https://32democrats.org/2020/03/washington-state-2020-democratic-state-delegate-registration/">Congrats to our Delegates</a></strong></p>
<p><strong>7:10 Conversation with Candidate for WA Secretary of State— State Rep. Gael Tarleton</strong></p>
<p>I am running to protect our right to vote, secure the elections, ensure that every vote is counted, regardless of who we voted for. I have decades of experience addressing threats to our nation and will help election workers be prepared to deal it ay potential interference. I will also become the champion of voter education and outreach. It is time to help future voter know not just how to vote, but why we vote. This is how our collective voice can be heard.</p>
<p><strong>7:40 Other Candidates</strong></p>
<p><strong>• Judge Anna Alexander, Snohomish Superior Court, Judge Pos. 7</strong></p>
<p><strong>• Judge Edirin Okoloko, Snohomish Superior Court, Judge Pos. 4</strong></p>
<p><strong>• Judge Paul Thompson, Snohomish Superior Court, Judge Pos. 14</strong></p>
<p><strong>• Attorney Cassandra Lopez Shaw, Snohomish Superior Court, Judge Pos. 8</strong></p>
<p><strong>• Judge Linda Coburn, Snohomish County Court of Appeals, Div 1, Dist 2, Judge Position 2 </strong></p>
<p><strong>8:02 Resolutions</strong></p>
<p><strong>• <a href="https://32democrats.org/download/resolution-urging-congress-to-endorse-national-infrastructure-bank/">Urging Congress to Endorse a National Infrastructure Bank</a></strong> M/S/C</p>
<p><strong>• <a href="https://32democrats.org/download/resolution-calling-for-a-sustainable-federally-funded-water-affordability-plan-similar-to-the-low-income-home-energy-assistance-program-liheap/">Calling for Sustainable, Federally Funded Water Affordability Plan</a></strong> M/S/C</p>
<p><strong>• <a href="https://32democrats.org/download/tax-big-businesses-fund-green-housing-seattle/">Support I-131: An Act to Tax Big Business to Fund Affordable Green Housing in Seattle</a></strong> M/S/C</p>
<p><strong>8:50 New Business</strong></p>
<p><strong>• State Platform Report by 32nd LD Platform Rep Gary McCaig</strong></p>
<p><strong>9:10 Good of the Order</strong></p>
<p><strong>• Mark Weiss, update on Rep. Jayapal's proposed healthcare legislation during COVID</strong></p>
<p><strong>9:15 Adjourn, M/S/C</strong></p>
<p><strong>Submitted by,</strong></p>
<p><strong>Sally Soriano, 32LD Secretary</strong></p>
	</Layout>
)
