import React from 'react'
import Layout from '../../../components/layout'
import {MinutesHeader, minutes_type, minutes_status} from '../../../components/minutes'

const date = '2020-04-15T19:00-07:00'
const remote = true
const status = minutes_status.approved
const type = minutes_type.general

export const m = {date, remote, status, type}
export default () => (
	<Layout>
		<MinutesHeader m={m}/>
		<p><strong><u>Meeting Minutes</u></strong></p>
<p>32nd LD Monthly Meeting</p>
<p><strong>Wednesday, April 15, 2020, 7:00 PM</strong></p>
<p>Via ZOOM &amp; Telephone Call-in</p>
<p><strong>7:00 PM - CALL TO ORDER</strong><br />
<strong>Chair Alan Charnley</strong> called the meeting to order and followed with the pledge of allegiance. He acknowledged that this land we are meeting on was originally, and still is, land belonging to First Nations peoples— Coast Salish, Tulalip and Duwamish.</p>
<p>Very soon, the 32nd LD will be making bylaw changes so we will be able to legally conduct business when using Zoom and Telephone call-in.</p>
<hr/>
<p><strong>7:05 PM -</strong> <strong>Candidate for WA State Lt. Governor</strong></p>
<p><em>State Senator Marcus Liias, running for an open seat</em><br />
<a href="http://markoforwa.com/"><u>http://markoforwa.com/</u></a></p>
<p><strong>Senator Liias</strong> said there were three reasons why he is uniquely qualified to become Lt. Governor: for the past 3-years he has been Senate Majority Floor leader; if elected he would be our first LBGTQ statewide executive; and current Lt. Governor, Cyrus Habib, has endorsed him. He has continually supported climate action; worked on equity issues; and promoted international trade and workforce training. In the Q&amp;A he said he supported Single Payer healthcare, and even more so now, as African Americans are being hit the hardest by the coronavirus pandemic.</p>
<hr/>
<p><strong>7:25 PM - How to Run for Delegate for the 2020 State and/or National Conventions</strong></p>
<p><em>Carin Chase and Chris Roberts, 32nd LD State Committee Members</em></p>
<p><a href="https://www.waelectioncenter.com/"><u>https://www.waelectioncenter.com/</u></a></p>
<p><a href="https://www.wa-democrats.org/becomeadelegate/"><u>https://www.wa-democrats.org/becomeadelegate/</u></a></p>
<p><strong><u>1. Why Become a Delegate?</u></strong></p>
<p>Delegates will be choosing the Democratic candidate, Joe Biden, and they will also be able to have an impact on the platform regarding such issues as healthcare for all, homelessness, and student debt. Delegates will have the opportunity to network with other community activists.</p>
<p>The <u>32nd Legislative District</u> has been allocated <u>20 delegates for Sanders</u> and <u>19 delegates for Biden</u> (resulting from the 32nd LD primary vote). At the Congressional District level, the <u>2nd CD has been allocated</u> <u>5 delegates for Sanders and 5 for Biden</u>; and the <u>7th CD has been allocated 15 for Sanders and 14 for Biden</u>. You can run as either a Biden delegate or a Sanders delegate.</p>
<p><strong><u>2. Steps to Run as a State Delegate from the 32nd LD</u></strong></p>
<p>a. Go to this 32nd LD web link and click on the bottom turquoise box "Click here to view who has registered": <a href="https://wademscaucus2020.azurewebsites.net/caucus/ld32"><u>https://wademscaucus2020.azurewebsites.net/caucus/ld32</u></a></p>
<p>b. You can review the written comments, of people running for delegate positions, in the following categories: Preferred Candidate, Congressional District, LBGTQ+, Race, Native, Age Category, Union, Chronic Condition, Disability, Disability/Accommodation, Why you are running? and, How have you supported Democrats in the past year?</p>
<p>c. Write out your responses.</p>
<p>d. Return to: <a href="https://wademscaucus2020.azurewebsites.net/caucus/ld32"><u>https://wademscaucus2020.azurewebsites.net/caucus/ld32</u></a></p>
<p>and click on the top blue box: "Register to run to be a delegate...."</p>
<p>e. You'll create an account and then fill in the categories listed above.</p>
<p>f. The <strong><u>deadline for registering to be a delegate</u></strong> is coming up soon — <strong><u>Friday, April 24th, 5pm</u>.</strong></p>
<p>g. We hope everyone in attendance this evening will consider running to be a delegate.</p>
<p><strong><u>3. Steps to Run as a National Delegate from the 2nd CD or the 7th CD</u></strong></p>
<p>a. Here are FAQ's about how to become a national delegate.</p>
<p><a href="https://www.wa-democrats.org/wp-content/uploads/2020/02/Delegate-Selection-FAQ.pdf"><u>https://www.wa-democrats.org/wp-content/uploads/2020/02/Delegate-Selection-FAQ.pdf</u></a></p>
<p>b. Complete and sign this letter of intent. It must be received by State Chair Tina Podlodowski by <u>the <strong>deadline on May 4th, 5pm.</strong></u></p>
<p><a href="https://www.wa-democrats.org/wp-content/uploads/2020/04/Fillable-Letter-of-Intent-to-Run-for-National-Delegate.pdf">https://www.wa-democrats.org/wp-content/uploads/2020/04/Fillable-Letter-of-Intent-to-Run-for-National-Delegate.pdf</a></p>
<p><u><strong> 4. How State and National Delegates are Selected</strong></u></p>
<p>a. The 32nd LD <u>PCOs (Precinct Committee Officers) will select state delegates</u>.</p>
<p>PCOs will receive electronic ballots by 9am on Sunday, April 26th and</p>
<p>they <u>must be completed by</u> <strong><u>9pm, Sunday, May 3rd</u>.</strong></p>
<p>b. Right now, PCOs can start familiarizing themselves with the list of 32nd LD community members who are running to be state delegates at:</p>
<p><a href="https://wademscaucus2020.azurewebsites.net/caucus/ld32"><u>https://wademscaucus2020.azurewebsites.net/caucus/ld32</u></a></p>
<p>c. Elected <u>state delegates will then vote for national delegates.</u> The 2nd and 7th Congressional District will soon schedule a date for these <em>virtual</em> caucuses.</p>
<p>d. National delegates will represent our state at the <u>National Convention in Milwaukee the week of August 17th</u>. There are still conversations going on about whether or not the convention will be a virtual convention. If the convention is in-person, each delegate will have to raise about $3,000 for lodging and travel. Don't let this cost be an impediment for you, however, as we have many GoFundMe experts who will help you raise these funds.</p>
<p><strong><u>5. Time for PCOs to Register for Their Election: May 11 - May 15</u></strong></p>
<p>Every two years, PCOs must register to run for office. This year registration forms are due between the dates of May 11 - May 15. Since we are currently encouraging our 32nd LD community members to run to be delegates, let's also urge them to run to be PCOs. Just ask them to fill out the form below for their specific county and email the form to— pco@32democrats.org</p>
<p>What does a PCO do?</p>
<p><a href="https://32democrats.org/2016/05/pco-filing-week/">https://32democrats.org/2016/05/pco-filing-week/</a></p>
<p>King County PCO Application</p>
<p><a href="http://www.kcdems.org/wp-content/uploads/2015/02/PCOform2015.pdf">http://www.kcdems.org/wp-content/uploads/2015/02/PCOform2015.pdf</a></p>
<p>Snohomish County PCO Application</p>
<p><a href="http://snocodems.org/wp-content/uploads/2018/03/PCO-Application.pdf">http://snocodems.org/wp-content/uploads/2018/03/PCO-Application.pdf</a></p>
<p><strong><u>6. Questions?</u></strong></p>
<p>Contacts—</p>
<p>Carin: carinchase@gmail.com</p>
<p>Chris: chris.roberts@32democrats.org</p>
<p>Alan: chair@32democrats.org</p>
<hr/>
<p><strong>7:45 PM -</strong> <strong>Candidate for King County Superior Court Judge</strong></p>
<p><em>Hillary Madsen, Attorney, will run for an open seat</em><br />
www.hillaryforjudge.com</p>
<p><strong>Hillary Madsen</strong> has spent her legal career representing low-income, homeless and foster children as well as people incarcerated in Washington jails, and prisons through class actions, impact litigation, and coalition building. She was a staff attorney with the Children &amp; Youth Project at Columbia Legal Services. She has volunteered with King County Family Law and Dependency CASA, the King County Prosecutor’s Office, and the U-District Youth Shelter (now ROOTS). In Q&amp;A she was asked about the King County Youth Jail. Madsen said she helped sue King County for holding children and youth in solitary confinement. She was asked about her union membership. She said she was a union member at Columbia Legal Services which had their own union. She said she plans to run for an open seat.</p>
<hr/>
<p><strong>7:55 PM -</strong> <strong>Candidate for WA State's 2nd Congressional District</strong></p>
<p><em>Jason Call, Former High School Math Teacher, is challenging the incumbent</em><br />
<a href="https://www.callforcongress.com/">https://www.callforcongress.com/</a></p>
<p><a href="https://twitter.com/callforcongress">https://twitter.com/callforcongress</a></p>
<p><strong>Jason Call</strong> is running a progressive, people-powered campaign for Congress— Medicare for All, Green New Deal, criminal justice reform, and living wage jobs. He said, now is the time for <u>two</u> Democrats to be on the 2020 November ballot because those two Democrats are at opposite ends of the political spectrum. Q: Explain why you support the Green New Deal. A: We have just 11 years before scientists say we'll be facing irreversible climate change and species extinction. Q: What do you believe you have to offer moderate Democrats? A: I marched in opposition to wars. In my opinion, any public option will still allow health insurance companies to escalate healthcare costs. I'm against 'profit over people' and such moderate positions are establishment positions — not positions held by the majority of people. I stand by progressive values.</p>
<hr/>
<p><strong>8:04 PM -</strong> <strong>Candidate for King County Superior Court Judge, Pos. 19</strong></p>
<p><em>Judge Nelson Lee, recently appointed to this position, now seeking a 4-year term</em><br />
<a href="https://www.judgenelsonlee.com/">https://www.judgenelsonlee.com/</a></p>
<p>Governor Inslee appointed <strong>Judge Lee</strong> to the King County Superior Court in February 2020 to replace Judge Bowman. His parents were immigrants from China and Japan, and he is the father of three children. After law school, Lee spent the next 17 years as a Deputy Prosecuting King County Attorney covering cases from DUIs to homicides. In 2010, Lee and his wife started their own practice and developed experience in civil litigation and immigration law. He believes it is important for a judge to have a wide variety of life experiences. Q: What are your thoughts on bail bonds? A: Too often they are used in a punitive manner. I think the decisions on bail should be considered individually and take multiple factors into consideration.</p>
<hr/>
<p><strong>8:09 PM -</strong> <strong>Candidate for WA State's 2nd Congressional District</strong></p>
<p><em>Congressman Rick Larsen, incumbent</em></p>
<p><a href="https://www.ricklarsen.org/">https://www.ricklarsen.org/</a></p>
<p><strong>Congressman Larsen</strong> said that COVID-19 is creating huge health and economic impacts. He said the CARES bill is delivering vital financial assistance — checks are being sent to people and businesses and he has a 4-step graphic, that you can share with friends, on how to help people sign up. He said the ILWU (longshore) and the NEA (teachers) unions have endorsed him, as has Snohomish County Executive Dave Somers. Q: What is your position on the Green New Deal? A: I have written an environmental white paper that lays out the actions I am taking to fight climate change in the Transportation Committee and on the floor of the House. It is going to take more than just a resolution. Q: Is there another stimulus package in the works? A: Yes, a 2.0 stimulus package is in the works as the first stimulus was not enough. For example, hospitals need more help. If you have ideas, let me know.</p>
<hr/>
<p><strong>8:14 PM -</strong> <strong>Candidate for King County Superior Court Judge, Pos. 51</strong></p>
<p><em>Judge Cindi Port, recently appointed to this position, now seeking a 4-year term</em><br />
<a href="http://judgecindiport.com/">http://judgecindiport.com/</a></p>
<p><strong>Judge Port</strong> was appointed by Governor Inslee to the King County Superior Court to replace Judge John Erlick starting on April 1, 2020. She grew up in a small logging town in northern Idaho, attended Whitworth College on scholarship, and then attended Gonzaga University Law School. She has worked in the King County Prosecutor's Office for 23 years, first in the criminal division and then in the civil division. She volunteers at Angeline's Day Center that serves the needs of homeless women. She and her spouse Jennifer have been together for 25 years. Q: What is your opinion of cash bail? A: I believe Nelson Lee covered this issue well. I think lately judges have been doing a good job, during the coronavirus pandemc, by making decisions on an individual basis. I'm very supportive of bail reform. Q: Given the coronavirus pandemic, do you support reducing jail population? A: I do know that the King County jail has been doing a good job. There have been no outbreaks. Also currently, judges are allowing extra time for court cases.</p>
<hr/>
<p><strong>8:18 PM -</strong> <strong>Candidate for the WA State Court of Appeals</strong></p>
<p><em>Judge Linda Coburn, running for an open seat</em><br />
<a href="https://www.judgecoburn.com/about-judge-linda-coburn/">https://www.judgecoburn.com/about-judge-linda-coburn/</a></p>
<p><strong>Judge Coburn</strong> said she is running for an open seat on the WA State Court of Appeals. The Court of Appeals is just below the Supreme Court and is divided into geographical areas. All voters in Snohomish County will be able to vote for her. She was a law clerk for Judge Stephen Dwyer of the State Court of Appeals, and is pleased that he has endorsed her for this position. She said it is important that courts function well because of their impact. She has trial experience, and has worked as a public defender and at every court level except the Supreme Court level. She thanked the 32nd LD for supporting her when she ran for Edmonds Municipal Court in 2017. Q: What is your position on body cams? A: Before having them, we had 'he said, she said' situations. In my experience as a public defender, in those situations, defendants rarely won. Q: What are your thoughts on climate activism and the 'necessity defense' with corporate polluters? A: It is available as a Code of Judicial Conduct and as a Judicial Officer, I would look at the facts.</p>
<hr/>
<p><strong><u>Updates &amp; Good of the Order</u></strong></p>
<p><strong>• Snohomish County Democrats Report</strong></p>
<p><em>Liz Brown, Snohomish County Democrats Central Committee Member</em></p>
<p>The Endorsement Committee had two full-day Saturday meetings. We voted to let the LDs interview candidates (from their districts) and make recommendations to us first, then we will conduct interviews.</p>
<p><strong>• Lynnwood Food Bank Update</strong></p>
<p><em>Deborah Kilgore</em></p>
<p>The Lynnwood Food Bank is dying for cash. They do get some federal money but this past week over 418 people visited the food bank and half of the families are new. We are spending $1,200 a week more with the CV-19 pandemic. Please help if you can: <a href="http://www.lynnwoodfoodbank.org/donate">http://www.lynnwoodfoodbank.org/donate</a></p>
<p><strong>• Training for PCOs and Training to Run a Campaign</strong></p>
<p><em>Amber King</em></p>
<p>Check out <em>Uphill Media</em> with John Culver — online training on YouTube.</p>
<p><em>Uphill Media</em> is an all-volunteer independent media network focused on informing the electorate through engagement.</p>
<p><a href="https://www.uphillmedia.org/">https://www.uphillmedia.org/</a></p>
<hr/>
<p><strong>8:30 PM - ADJOURN, M/S/C</strong></p>
<p>Submitted by</p>
<p>Sally Soriano, 32nd LD Secretary</p>
	</Layout>
)
