import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../../components/layout'
import {LinkResolution} from '../../../components/resolution'
import {MinutesHeader, minutes_type, minutes_status} from '../../../components/minutes'

import {r as protect_orcas} from '../../resolutions/2020/protect-orcas-curtailing-suction-dredge-mining'
import {r as safeguards_SB_5604} from '../../resolutions/2020/safeguards-to-guardianship-law-SB-5604'
const resolutions = [protect_orcas, safeguards_SB_5604]

const date = '2020-01-08T19:00-07:00'
const status = minutes_status.approved
const type = minutes_type.general

export const m = {date, status, type}
export default () => (
	<Layout>
		<MinutesHeader m={m}/>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<dl className="agenda">
			<dt>7:00</dt><dd>Call to order</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land acknowledgment</dd>
			<dt>7:03</dt>
			<dd>Washington primary info</dd>
			<dd>Edmonds school board president bond proposal</dd>
			<dt>7:35</dt><dd>Break</dd>
			<dt>7:45</dt><dd>Election for State Convention for Rules, Platform and Credentials Committee</dd>
			<dt>8:15</dt><dd><Link to="/resolutions">Resolutions</Link>
				<ul className="menu">
					{resolutions.map(r=><li><LinkResolution r={r} status={true}/></li>)}
				</ul>
			</dd>
			<dt>8:15</dt><dd>Treasurer's report</dd>
			<dt>8:25</dt><dd>Good of the order</dd>
			<dt>8:30</dt><dd>Adjourn</dd>
		</dl>
		<p>
			"Love is the only force capable of transforming an enemy into a friend."
			<br/>
			- Martin Luther King Jr.
		</p>
<p><strong><em>MINUTES</em></strong></p>
<p><strong>32nd LD Meeting, Wednesday, January 8, 2020</strong></p>
<p>Masonic Hall Shoreline, 753 N 185th St, Shoreline WA</p>
<p>submitted by Carin Chase, Substitute Secretary</p>
<p><strong>Agenda </strong></p>
<p><strong>7:00 Call To Order</strong>, Pledge of Allegiance, Land Acknowledgment <em>by Alan Charnley, Chair</em></p>
<p><strong>7:05 2020 Legislative Priorities</strong> <em>by Cindy Ryu, State Representative</em></p>
<p>Chair Alan Charnley read a statement from Rep. Ryu on her priorities for the 32nd LD and for her committees. Priorities include: transportation funding; Shoreline CC Dental Hygiene program, safe use of personal flotation devices; natural disaster preparedness; increased housing supply; and finding new funds in Appropriation Committee. See full text at end of minutes.</p>
<p><strong>7:10 Election Updates</strong></p>
<p><strong>• <em>New Structure</em> of the WA Presidential Primary</strong> <em>by Chris Roberts, 32nd State Committee Member and Shoreline City Councilmember</em></p>
<p>CM Roberts provided information on the new structure of the Washington Presidential Primary and procedures for selecting presidential nominating convention delegates. Ballots will be mailed out February 19th and must be returned by March 10th.</p>
<p>There are 13 Democratic presidential candidates and 1 Republican presidential candidate on the primary ballot. When voters receive their ballot in the mail, they will have to self-select either the Democratic Party or the Republican Party list if they want their vote to count to send delegates to either of the nominating conventions. If voters decline to choose from either the Democratic or Republican list, their vote will not be counted in determining the number of delegates each presidential candidate receives.</p>
<p>A threshold of 15% of the vote must be met for any candidate to receive any convention delegates for the presidential nominating convention.</p>
<p>If a candidate suspends their campaign they will continue to hold any delegates they have through the national convention. If a candidate withdraws from the presidential race, they will not maintain control of any delegates they have.</p>
<p>WA State Democratic Party Presidential Convention Delegates will be allocated according to the vote received in each of the 10 Congressional Districts. The 32<sup>nd</sup> Legislative District will caucus on April 26, 2020 at the Shoreline Community College to select their individual delegates, the number of which is determined by the presidential primary. Our Legislative District caucus will elect delegates to the County District caucuses and Congressional Caucuses. At the Congressional caucuses, national delegates will be elected. (See timeline at end of minutes.)</p>
<p><strong>• Edmonds School Bond Election - 2020 Capital Technology Levy / Construction Bond</strong></p>
<p><em>by Deborah Kilgore, President, Edmonds School Board </em></p>
<p>Dir. Kilgore informed us that this bond is a continuation of the current level of taxpayer support for our Edmonds public schools. She said, the bond shall not result in an increase in taxes.</p>
<p>The bond is necessary because some of our old school buildings are out-of-date relics with substandard infrastructure. Furthermore, we have far too many portable classrooms that have little or no infrastructure, such as plumbing. This is reminiscent of schools from the 1940s.</p>
<p>Our newly built schools are a welcoming space for our children with good lighting, clean air and reliable infrastructure. All of our children, at all grades, deserve to attend schools in well-built, modern schools. See Edmonds School Board website for more information: <a href="https://www.edmonds.wednet.edu/community/2020_capital__technology_levy___construction_bond" className="uri">https://www.edmonds.wednet.edu/community/2020_capital__technology_levy___construction_bond</a></p>
<p><strong>7:40 No War With Iran!</strong> <em>by Maralyn Chase, Former State Senator</em></p>
<p>Sen. Chase discussed how all Americans were shocked to hear that President Trump ordered an unprovoked assassination of Iran’s Maj. Gen. Qasem Soleimani on 2020-01-02. The U.S. House of Reps. is voting this week on a measure to restrict the president’s ability to take our country to war, by rescinding the 2001 Authorization for Use of Military Force. Chase said, we also learned last Sunday (2020-01-04), that U.S. border security targeted between 80 and 100 Iranian-Americans at the Blaine border crossing — reminding many of us of the racist treatment of West Coast Japanese-Americans during WWII. Our U.S. Rep. Pramila Jayapal and U.S. Senators Murray and Cantwell have asked the Dept. of Homeland Security to explain why Americans of Iranian ancestry were held for secondary screenings and in some cases held up to 12 hours before they could leave: <a href="https://jayapal.house.gov/2020/01/09/jayapal-delbene-murray-cantwell-lead-letter-to-dhs-demanding-answers-on-iranian-americans-being-detained-at-border/" className="uri">https://jayapal.house.gov/2020/01/09/jayapal-delbene-murray-cantwell-lead-letter-to-dhs-demanding-answers-on-iranian-americans-being-detained-at-border/</a></p>
<p>The National Lawyers Guild stated that recent illegal U.S. actions in Iraq, including the killing of Iranian and Iraqi nationals and threats of military attacks on Iran are clear violations of both U.S. and international law. They are calling all people of conscience to mobilize in opposition to war with Iran and to call on Congress to block access to funding for any military actions against Iran and to stop the repression of Iranian nationals at the U.S. border: <a href="https://nlginternational.org/2020/01/nlg-ic-strongly-condemns-illegal-targeted-assassinations-by-the-united-states-the-increased-repression-of-iranian-nationals-at-u-s-border/" className="uri">https://nlginternational.org/2020/01/nlg-ic-strongly-condemns-illegal-targeted-assassinations-by-the-united-states-the-increased-repression-of-iranian-nationals-at-u-s-border/</a></p>
<p>32nd LD — No War With Iran!— Task Force</p>
<p>We'll formulate our response to these events (using zoom meetings) and bring our recommendations to the next 32nd LD meeting on February 12th. Thirteen people joined the task force. If you would also like to join, contact: maralynchase@gmail.com</p>
<p>Calendar—</p>
<p>• Sat., Jan 11th, noon, National People's Forum/Webinar —</p>
<p>available for re-play at: https://www.facebook.com/AnswerCoalition/</p>
<p>• Sat., Jan 25th, Global Day of Protest: http://www.answercoalition.org</p>
<p>Other Resources—</p>
<p>• American Friends Service Committee: 5 Things to Know About Iran &amp; Take Action <a href="https://www.afsc.org/blogs/news-and-commentary/5-things-to-know-about-iran" className="uri">https://www.afsc.org/blogs/news-and-commentary/5-things-to-know-about-iran</a></p>
<p>• Veterans for Peace, No War on Iran</p>
<p><a href="https://www.veteransforpeace.org/take-action/no-war-iran" className="uri">https://www.veteransforpeace.org/take-action/no-war-iran</a></p>
<p>• Voices for Creative Non-Violence, Toward a Peace Process</p>
<p><a href="http://vcnv.org/" className="uri">http://vcnv.org/</a></p>
<p><strong>7:50-8:10 Break </strong></p>
<p><strong>8:10 32nd LD Representation on Committees Preparing for State Convention </strong></p>
<p><em>by Carin Chase, 32nd State Committee Member and Edmonds School Board Member</em></p>
<p>Dir. Chase informed us that at our next LD meeting on February 12th, we will select one representative each, for the Rules, Platform and Credentials committees, in preparation for our State Democratic Party Convention. She urged everyone to consider becoming a rep because this is an exciting and vital opportunity to shape the state party platform and agenda. These are the documents that were created by these committees two years ago. The task of each committee is to attend meetings to update the rules, platform and credential documents before the State Convention in Tacoma on June 12-14th: <a href="https://www.wa-democrats.org/about/documents" className="uri">https://www.wa-democrats.org/about/documents</a></p>
<p><strong>8:20 Resolutions</strong></p>
<p><strong>1) <Link to="/resolutions/2020/safeguards-to-guardianship-law-SB-5604/" className="uri">Resolution in favor of adding safeguards to the new guardianship law, SB 5604 (2019-2020)</Link></strong></p>
<p><em>Sponsored by Michael Brunson, PCO and member</em></p>
<p>In Spring, 2019, the WA State Legislature enacted Senate Bill 5604SA which is a new adult guardianship law for the state. This new law is based on the proposed Uniform Guardianship law that was formulated a few years ago by the National Commission on Uniform State Laws. So far, however, only Maine and Washington have passed it.</p>
<p>From a human rights standpoint, the adult guardianship law, prior to 2019, was very unfair because it gave for-profit businesses too much power over people's lives. This was corrected in the new guardianship law, SB 5604.</p>
<p>SB 5604, however, has too few safeguards for the rights of “alleged incapacitated persons” (AIP) and adjudged (AIP) persons and families. This resolution therefore argues that none of the safeguards should be lost in the transition to the new law.</p>
<p>REVISION: Reference added to <a href="https://app.leg.wa.gov/RCW/default.aspx?cite=11.92.190">RCW 11.92.190</a></p>
<p>RESOLVED: that the safeguard provision should be retained in the new law and this resolution should be sent to: the 32<sup>nd</sup> LD delegation; to the State Legislature; Governor Inslee; members of the Senate Law and Justice Committee; House Judiciary Committee; WA State Democrats; and King and Snohomish County Democrats.</p>
<p>Action: M/S/C (moved/seconded/carried) as REVISED on January 8, 2020 by the 32nd District Democrats.</p>
<p><strong><Link to="/resolutions/2020/protect-orcas-curtailing-suction-dredge-mining">2) Protect our orcas by curtailing suction dredge mining</Link></strong></p>
<p><em>Sponsored by Dean Fournier, PCO and member</em></p>
<p><em>Originated from: Jim Freeburg, passed by Jefferson County Democrats, 206-245-0059, jifreeburg@yahoo.com</em></p>
<p>RESOLVED: that we formally request that the Washington State Legislature, in its 2020 Supplemental Session, ensure that the Southern Resident Orca pods have a prey base sufficient to rebuild their seriously depleted numbers by promptly (1) requiring designation and protection of Critical Spawning Areas in specified Washington streams and rivers (as in Oregon), <em><em>or</em></em> (2) banning outright the practice of Suction Dredge Mining in all Washington streams and rivers (as in California).</p>
<p>Dean Fournier spoke in favor of this resolution.</p>
<p>Action: M/S/C (moved/seconded/carried) on January 8, 2020 by the 32nd District Democrats.</p>
<p><strong>3) <Link to="/resolutions/2020/aid-low-income-lynnwood-residents">Resolution in support of aid for low-income Lynnwood residents</Link></strong></p>
<p>Action: M/S/F (moved/seconded/failed) to Suspend the Rules for Discussion</p>
<p>Sponsored by Ted Hikel, PCO and member</p>
<p>RESOLVED: that the City of Lynnwood should utilize state programs to provide financial assistance to current Lynnwood low-income residents at Whispering Pines.</p>
<p>Whispering Pines is a low-cost apartment complex in Lynnwood, which is scheduled to be torn down and rebuilt with far fewer low-income units. There is a state program that can assist Whispering Pines' residents to move to other low-income housing.</p>
<p>Action: Postponed for action until the next LD meeting on February 12th.</p>
<p><strong>8:45 Good of the order</strong></p>
<p>• Resolution discussion</p>
<p>• Housing is a human right</p>
<p>• Environmental program - Phinney Neighborhood Association</p>
<p>• Warren speaker</p>
<p>• Bernie speaker</p>
<p><strong>9:00 M/S/C, Meeting Adjourned</strong></p>
<hr/>
<p><strong>Timeline—</strong></p>
<p><strong>2020 Democratic National Convention</strong></p>
<p><strong>• January 31 - February 2, 2020</strong></p>
<p>State Committee Meeting</p>
<p>301 W 6th St, Vancouver, WA 98660</p>
<p><strong>• February 19, 2020</strong></p>
<p>Washington State Presidential Primary Ballots mailed</p>
<p><strong>• March 10, 2020</strong></p>
<p>Washington State Presidential Primary</p>
<p><strong>• April 26, 2020</strong></p>
<p>Legislative District Caucuses</p>
<p><strong>• May 3, 2020</strong></p>
<p>County Conventions &amp; Legislative District Caucuses</p>
<p><strong>• May 30th, 2020</strong></p>
<p>Congressional District Caucuses</p>
<p><strong>• June 12-14, 2020</strong></p>
<p>State Convention </p>
<p>Tacoma Convention Center</p>
<p>1500 Commerce St, Tacoma, WA 98402</p>
<p><strong>• July 13-16, 2020</strong></p>
<p>Democratic National Convention, Milwaukee, WI</p>
<hr/>
<p><strong>Statement from State Rep. Cindy Ryu</strong></p>
<p>Priorities for 32nd LD and my committees</p>
<p>1. Transportation funding for Shoreline and beyond: With the passage of I-976, we will be working on alternatives such as delaying certain projects and other funding.</p>
<p>2. Shoreline Community College plans on demolishing a building that houses the Dental Hygiene program and clinic among other programs without a permanent solution to continue this stellar program. I hope to work with House and Senate members for a long-term solution.</p>
<p>3. Washington State offers many water recreation activities. We even have a very well-subscribed life vest loaner program for boaters. Unfortunately, with rise of new sports such as paddleboards combined with our cold waters, we lost 4 more lives in 2019 for a historical total of 6 deaths while paddle-boarding. I am introducing a bill to update the Use of Personal Flotation Devices to hopefully keep Washingtonians safe in our waters.</p>
<p>4. I have been serving on the Disaster Resiliency Work Group convened by the Office of the Insurance Commissioner. We will have a final report with some recommendations at the end of this year so that hopefully we cane become more prepared for certain natural disasters. As a former insurance agent and chair of the committee with jurisdiction over disaster preparedness and resiliency as well as veterans and military, I am also looking forward to supporting and moving bills that improve our disaster response out of Housing, Community Development and Veterans Committee.</p>
<p>5. Several bills to increase housing supply, access, and incentives are being introduced. I am planning on moving these and hold-over Housing bills from the previous Session.</p>
<p>6. 2020 will be a challenging year on Appropriations Committee since there are so many outstanding needs on top of finding new funds for our transportation needs.</p>
	</Layout>
)
