import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../../components/layout'
import {dates} from '../../../components/dates'
import {LinkResolution} from '../../../components/resolution'
import {MinutesHeader, minutes_type, minutes_status} from '../../../components/minutes'

import {r as ahmaud_arbery} from '../../resolutions/2020/ahmaud-arbery'
import {r as i_1776} from '../../resolutions/2020/I-1776'
const resolutions = [ahmaud_arbery,i_1776]

const date = '2020-06-10T19:00-07:00'
const remote = true
const status = minutes_status.approved
const type = minutes_type.general

export const m = {date, remote, status, type}
const {prev_path} = dates(date)
export default () => (
	<Layout>
		<MinutesHeader m={m}/>
		<dl className="agenda">
			<dt>7:00</dt><dd>Call to order</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land Acknowledgment</dd>
			<dt>7:03</dt><dd><Link to={prev_path} className="link">Approval of May 13, 2020, LD Draft Meeting Minutes</Link></dd>
			<dt>7:05</dt><dd>Thanks to everyone who filed to be a PCO.</dd>
			<dd>Moment of Silence for Racial Justice</dd>
			<dt>7:08</dt><dd>Endorsement considerations
			<p>
				Please review this slate as recommended by the Endorsement Committee and approved by the Executive Board.
				Also note, any member can pull a race to be focused on by itself.
				Endorsement considerations in the following races:
			</p>
			<ul>
				<li>Lieutenant Governor:&nbsp;<a href="https://dennyheck.com/"><strong>Denny Heck</strong></a></li>
				<li>State Representative, Position 1:&nbsp;<strong>Shirley Sutton</strong></li>
				<li>State Representative, Position 2:&nbsp;<a href="http://www.electgraypetersen.com/"><strong>Gray Petersen</strong></a></li>
				<li>State Supreme Court, Position 6:&nbsp;<a href="https://www.judgehelenwhitener.com/"><strong>G. Helen Whitener</strong></a></li>
				<li>King County Superior Court, Dept. 13:&nbsp;<a href="https://www.hillaryforjudge.com/"><strong>Hillary Madsen</strong></a></li>
				<li>Snohomish County Council, District 4:&nbsp;<a href="https://voteamberking.com/"><strong>Amber King</strong></a></li>
			</ul>
			</dd>
			<dt>8:30</dt><dd><Link to="/resolutions" className="link">Resolutions</Link>
				<ul className="menu">
					{resolutions.map(r=><li><LinkResolution r={r} status={true}/></li>)}
				</ul>
			</dd>
			<dt>8:50</dt><dd>Treasurer's report</dd>
			<dt>8:53</dt><dd>New Business</dd>
			<dt>8:55</dt><dd>Good of the order</dd>
			<dt>9:00</dt><dd>Adjourn
			<br/>
			Feel free to stay around and talk after the meeting.
			We’ve scheduled an extra 30-minutes of zoom time after we officially adjourn, for post-meeting conversation.
			</dd>
		</dl>
		<p>
			“If we don't like what the Republicans do, we need to get in there and change it.”
			<br/>– Medgar Evers
		</p>
<p><strong><u>MINUTES</u></strong></p>
<p><strong>32nd Legislative District Meeting</strong></p>
<p><strong>Wednesday, June 10, 2020, 7:00 PM</strong></p>
<p>Via Zoom &amp; Call-in</p>
<p>32democrats.org • chair@32democrats.org</p>
<p><strong>7:00 PM Credential Process</strong></p>
<p><strong>7:30 Call To Order by Alan Charnley, Chair</strong></p>
<p><strong>Chris Roberts, Parliamentarian</strong></p>
<p><strong>7:32 Pledge of Allegiance</strong></p>
<p><strong>7:35 Land Acknowledgment</strong></p>
<p>We in the 32nd LD acknowledge that we live and work on the ancestral lands of the Duwamish, Tulalip, and the Puyallup Tribes, and next to their ancestral waters, the Salish Sea. We honor with gratitude this land, water and the Puyallup, Tulalip, and Duwamish Peoples.</p>
<p><strong>7:37 Roberts Rules of Order, Re: Personal Attacks</strong></p>
<p>Alan Charnley: I'm reading a section on the subject of Personal Attacks during meetings: <u>Roberts Rules 10th Edition</u> (p. 380 1-14): </p>
<p><em>Debate issues not personalities. You may attack an idea or likely result of a proposal in strong terms but must avoid personalities. Under no circumstances can you attack or question the motives of other members. If you disagree with someone's statements you cannot say that “the statement is false”. You might instead say “I believe there is strong evidence that the member is mistaken”. Terms such as "fraud, liar, lie", etc. may not be used when speaking about another speaker or member in a debate.</em></p>
<p>It's important to keep these guidelines in mind as we move forward.</p>
<p><strong>7:40 ~ Moment of Silence for Racial Justice ~</strong></p>
<p><strong>7:42 Approval of Minutes - <a href="https://32democrats.org/download/2020-05-13/">May 13, 2020</a> - M/S/C - Approved</strong></p>
<p><strong>7:47 Candidate Endorsements</strong></p>
<p><strong>1. <a href="https://32democrats.org/bylaws-rules/">32nd LD Standing Rules 2015</a>:</strong></p>
<blockquote>
<p><em><strong>Standing Rule 2: Endorsement Policy and Procedures </strong></em></p>
<p><em><strong>6. Voting procedures shall be as follows: </strong>A. Endorsements shall require a sixty percent (60%) vote of the members present and voting. More than one candidate may be endorsed for the same office.</em></p>
</blockquote>
<p><strong>2. Slate Proposed to Membership by Endorsement Committee and Executive Board (see report at end of minutes)</strong></p>
<blockquote>
<p>• Lieutenant Governor: Denny Heck</p>
<p>• State Representative, Position 1: Shirley Sutton</p>
<p>• State Representative, Position 2: Gray Petersen</p>
<p>• State Supreme Court, Position 6: G. Helen Whitener</p>
<p>• King County Superior Court, Dept. 13: Hillary Madsen</p>
<p>• Snohomish County Council District 4: Amber King</p>
</blockquote>
<p><strong>3. Candidate Race Tabled at Endorsement Meeting on May 20, 2020</strong></p>
<blockquote>
<p>• King County Superior Court, Position 30, Douglass North vs. Carolyn Ladd</p>
</blockquote>
<p><strong>4. Slate Races Flagged by Members to Be Discussed Individually</strong></p>
<blockquote>
<p>• Amy Freedheim: King County Superior Court, Dept. 13, Madsen vs. Roberts</p>
<p>• Elizabeth Lunsford: Gray Petersen, State Rep, Position 2</p>
<p>• Krista Tenney: Shirley Sutton, State Rep, Position 1</p>
<p>• Charlie Lewis: Lt. Governor, Heck vs. Liias</p>
<p>• Ted Hikel: Snohomish County Council, District 4, King vs. Mead</p>
<p>• Ted Hikel: State Supreme Court, Position 6, G. Helen Whitener</p>
<p>• Stephanie Harris: King County Superior Court, Position 30, North vs. Ladd</p>
<p><strong>a. Lieutenant Governor: Denny Heck v. Marko Liias</strong></p>
<p>• Denny Heck, proposed in slate, therefore nominated and seconded</p>
<p>• Marko Liias, nominated by David Parshall, seconded by Liz Brown</p>
<p>• Recorded vote:</p>
<p>Heck = 86% yeas; 10% nays</p>
<p>Liias = 35% yeas; 65% nays</p>
<p><strong>b. King County Superior Court, Dept. 13: Hillary Madsen vs. Andrea Robertson</strong></p>
<p>• Hillary Madsen, proposed in slate, therefore nominated and seconded</p>
<p>• Andrea Robertson, nominated by Amy Freedheim, seconded by Lael White</p>
<p>• Recorded vote:</p>
<p>Madsen = 83% yeas; 17% nays</p>
<p>Robertson = 45% yeas; 55% nays</p>
<p><strong>c. State Representative, Position 2: Gray Petersen vs. Lauren Davis</strong></p>
<p>• Gray Petersen, proposed in slate, therefore nominated and seconded</p>
<p>• Lauren Davis, nominated by Chris Roberts, seconded by David Parshall</p>
<p>• Recorded vote:</p>
<p>Petersen = 49% yeas; 51% nays</p>
<p>Davis = 69% yeas; 31% nays</p>
<p><strong>d. State Representative, Position 1: Shirley Sutton vs. Cindy Ryu</strong></p>
<p>• Shirley Sutton, proposed in slate, therefore nominated and seconded</p>
<p>• Cindy Ryu, nominated by Deborah Kilgore, seconded by Liz Brown</p>
<p>• Recorded vote:</p>
<p>Sutton = 64% yeas; 36% nays</p>
<p>Ryu = 45% yeas; 55% nays</p>
<p><strong>e. State Supreme Court, Position 6: G. Helen Whitener</strong></p>
<p>• G. Helen Whitener, proposed in slate, therefore nominated and seconded</p>
<p>• Recorded vote:</p>
<p>Whitener = 97% yeas; 3% nays</p>
<p><strong>f. Snohomish County Council District 4: Amber King vs. Jared Mead</strong></p>
<p>• Amber King, proposed in slate, therefore nominated and seconded</p>
<p>• Jared Mead, nominated by Ted Hikel, seconded by David Parshall, Janet Way</p>
<p>• Recorded vote:</p>
<p>King = 71% yeas; 29% nays</p>
<p>Mead = 60% yeas; 40% nays</p>
<p><strong>g. King County Superior Court, Pos. 30: Douglass North vs. Carolyn Ladd</strong></p>
<p>• Douglass North, nominated by Stephanie Harris; seconded by Marylou Eckhart</p>
<p>• Carolyn Ladd, nominated by Jenna Nand; seconded by Liz Brown</p>
<p>• Recorded vote:</p>
<p>North = 61% yeas; 39% nays</p>
<p>Ladd = 43% yeas; 57% nays</p>
</blockquote>
<p><strong>5. Pre-Audit Endorsement Summary</strong></p>
<blockquote>
<p>• Lieutenant Governor: Denny Heck</p>
<p>• State Representative, Position 1: Shirley Sutton</p>
<p>• State Representative, Position 2: Lauren Davis</p>
<p>• State Supreme Court, Position 6: G. Helen Whitener</p>
<p>• King County Superior Court, Dept. 13: Hillary Madsen</p>
<p>• Snohomish County Council District 4: Amber King / Jared Mead</p>
<p>• King County Superior Court, Position 30: Douglass North</p>
</blockquote>
<p><strong>10:25 Resolutions</strong></p>
<p><strong>1. <a href="https://32democrats.org/2020/05/resolution-calling-for-justice-in-the-killing-of-unarmed-jogger-ahmaud-arbery/">Resolution Calling for Justice in the Killing of Unarmed Jogger Ahmaud Arbery</a></strong></p>
<p><strong>Submitted by Dean Fournier</strong>: I wrote this resolution out of anger and fury. An innocent young man, exercising in his own hometown, was gunned down by three would-be vigilantes. No crime was committed but Arbery was pursued, caught and killed. This was all on tape but it took three months to indict.</p>
<p><strong>Carin Chase:</strong> It's infuriating as this was just the start of hearing about even more killings of innocent Black men and women. Thanks for writing this Dean.</p>
<p><strong>M/S/C - Adopted</strong></p>
<p><strong>2.</strong> <strong><a href="https://32democrats.org/download/i-1776-resolution-washington-anti-discrimination-act-wada/">I-1776 Resolution - Washington Anti-Discrimination Act (WADA)</a></strong></p>
<p><strong>Submitted by Carin Chase:</strong> The former I-1000 has been reintroduced as I-1776, the Washington Anti-Discrimination Act. The only addition to I-1776 is a provision on the availability and accessibility of COVID-19 vaccines: "<em>prohibiting discrimination in the vaccine’s availability and accessibility based on age, race, gender, sexual orientation, disability, citizenship, county, income or employment."</em> This I-1776 petition signature drive will pioneer our state's first <u>on-line</u> signature gathering process — it could be groundbreaking.</p>
<p><strong>Gray Petersen:</strong> We definitely need this affirmative action ban to be overturned, as it is harmful for so many people.</p>
<p><strong>Lael White</strong>: I want to make sure the language in this resolution does not "mandate" that people must be vaccinated.</p>
<p><strong>Maralyn Chase:</strong> There was a discussion last week with I-1776 attorneys and it was made clear that the language only speaks to "availability and accessibility" thus leaving the individual with the option of being vaccinated or not.</p>
<p><strong>Dean Fournier</strong>: What is the earliest date that this law (WADA) could be implemented?</p>
<p><strong>Maralyn Chase:</strong> July, 2021</p>
<p><strong>M/S/C - Adopted</strong></p>
<p><strong>11:31 New Business</strong></p>
<p><strong>Michael Brunson:</strong> There is an immigrant state resident who is in the hospital in Tacoma with COVID-19. Because he doesn't have health insurance, the hospital is talking about taking him off the ventilator in a few days, according to KOMO News. <strong>I would like to suspend the rules</strong> to offer a resolution demanding that the hospital continue to give him treatment.</p>
<p><strong>Lael White</strong>: It's difficult to consider this resolution when it's not in writing.</p>
<p>M/S - Vote to Suspend Rules, requires a 2/3rds vote; 66.66%.</p>
<p>Vote is 15-8; 65.2%. Vote fails and rules are not therefore suspended to consider the resolution.</p>
<p>*This link may be helpful:</p>
<p>PBS, 3/25/20 - How Uninsured Patients Can Get Help During COVID-19 Pandemic</p>
<p><a href="https://www.pbs.org/newshour/health/how-uninsured-patients-can-get-help-during-covid-19-pandemic">https://www.pbs.org/newshour/health/how-uninsured-patients-can-get-help-during-covid-19-pandemic</a></p>
<p><em>“Hospitals are going to treat uninsured patients the same way they would treat any other patient. Their ability to pay is not going to be taken into account,” said Molly Smith, vice president of the coverage and state issues forum at the American Hospital Association."</em></p>
<p><strong>11:30 Treasurer's Report, Eric Valpey</strong></p>
<p>At the last LD meeting I showed people where the donation button was located on the website. Lots of people used it, thanks! So, I thought I would mention it again.</p>
<p><strong>11:35 Good of the Order</strong></p>
<p>• Mark Weiss: Pramila Jayapal's campaign dropped off two campaign t-shirts at my house because I nominated her!</p>
<p>• Gray Petersen: I want very much to thank all the members tonight for considering my nomination. I look forward to running a vigorous campaign.</p>
<p><strong>11:44 PM M/S/C - Meeting Adjourned</strong></p>
<p><strong>Submitted by,</strong></p>
<p>Sally Soriano, 32LD Secretary</p>
<p>sally.soriano@32democrats.org</p>
<hr/>
<p><strong>May 20, 2020</strong></p>
<p><strong>Endorsement Committee Report—</strong></p>
<p><strong>Recommendations to the Executive Board, 32nd LD Democratic Organization</strong></p>
<p>Since the Committee's last report, we have had three more interview events, speaking with 13 additional candidates for a variety of positions.</p>
<p>In total, since the 'endorsement season' began, we spent 30 hours in interviews and discussion. Additionally, the chair spent time scheduling interviews and arranging Zoom meetings with several hosts. We are very grateful to the SnoCo Dems for allowing us to use their Zoom platform for the majority of these events. </p>
<p>As the chair, I would like to acknowledge the great work done by committee members, and for the many extra hours they spent on: researching candidates’ claims made on their questionnaires, following the donation money on the PDC website, researching the 'back story' of the individual and group donations, and researching newspaper accounts, if there were any. This extraordinary work attempts to bring to the body the clearest rationale of why we, as a committee are recommending certain candidates and not others. I commend this year's committee as the hardest working and most fair group I have worked with since I began this task fifteen years ago! It is, after all, the Legislative District's mission to help elect the best and brightest to public service.</p>
<hr/>
<p><strong>Washington State Supreme Court, Position 6: G. Helen Whitener</strong></p>
<p><strong><u>Ms. Whitener is recommended to the Executive Board for endorsement.</u></strong></p>
<p>Rationale: G. Helen Whitener has served on the Supreme Court since being appointed to the position by Governor Inslee, and is now seeking re-election. She has served many communities in Washington as a Superior Court Judge, as well as on the WA State Board of Industrial Insurance Appeals, as a Pro-Tem Judge in Pierce County, and has worked for many legal firms in her role as attorney. Her favorite quote, "Justice Whitener believes in just treatment because just treatment is justice".</p>
<hr/>
<p><strong>Washington State Lt. Governor: Denny Heck</strong> <strong>vs.</strong> <strong>Marko Liias</strong></p>
<p><strong><u>Mr. Heck is recommended to the Executive Board for endorsement.</u></strong></p>
<p>Rationale: Denny Heck is about more than applying the rules of the Senate and the Constitution to legislative deliberations. He brings a legislative philosophy that honors both the people of our state and the constitution that provides the network for holding our public sector together.  He has demonstrated over the years a commitment to principled centered politics and to transparency and accountability that strengthens our democracy. That is why we support his candidacy for Lt. Governor.</p>
<hr/>
<br/>
<p><strong>King County Superior Court, Dept. 13: Hillary Madsen</strong> <strong>vs. Andrea Robertson</strong></p>
<p><strong><u>Ms. Madsen is recommended to the Executive Board for endorsement.</u></strong></p>
<p>Rationale: Committee members were extremely impressed with Ms. Madsen's demeanor; her references to research done on issues, her excellent work with Columbia Legal Services, and her work with many youth and youth groups.</p>
<hr/>
<p><strong>King County Superior Court, Dept. 30: Judge Douglass North</strong> <strong>vs. Carolyn Ladd</strong> </p>
<p><strong><u>Mr. North is recommended to the Executive Board for endorsement.</u></strong></p>
<p>Rationale: Douglass North presents extensive knowledge and experience, a long positive record on the bench, and displays the progressive values to which we aspire and expect from candidates.  He is an excellent role model for less experienced judges.</p>
<hr/>
<p><strong>Snohomish County Council District 4: Amber King vs. Jared Mead</strong> </p>
<p><strong><u>Ms. King is recommended to the Executive Board for endorsement.</u></strong></p>
<p>Rationale: Amber King lacks political experience, but has extensive life experience in many areas that would promote the kind of progressive decision making that we expect from candidates. She demonstrates tenacity and ability to quickly learn in a variety of situations, both of which would be conducive to performance on the council.</p>
<hr/>
<p><strong>32nd Legislative District Representative, Position 1: Gray Petersen</strong> <strong>vs. Lauren Davis</strong>  </p>
<p><strong><u>Mr. <em>Petersen</em> is recommended to the Executive Board for endorsement.</u></strong></p>
<p>Rationale: The Committee endorsed Gray Peterson due to his passionate convictions around the urgency for social, economic, and environmental justice. He has sufficient working knowledge of local and state political processes, is engaged and productive, and we believe he will serve our communities well as our 32<sup>nd</sup> LD Representative to the Legislature.</p>
<hr/>
<p><strong>32nd Legislative District Representative, Position 2: Cindy Ryu vs. Shirley Sutton vs. Keith Smith</strong></p>
<p><strong><u>Ms. Sutton is recommended to the Executive Board for endorsement.</u></strong></p>
<p>Rationale: Shirley Sutton will seek equitable solutions to financial burdens for working families and small businesses: fair taxation; close tax loopholes for big business; and establish a state bank to provide low interest loans. As Lynnwood City Council Member, she found alternative funding to protect Whispering Pines residents. Her management and labor experience at Burlington Northern informs her commitment to public works that reduce carbon pollution, and promotes ideals of fairness, diversity and inclusion. She will not accept corporate PAC or charter school donations.</p>
<hr/>
<p><strong>Standards Used for Evaluation of Candidates</strong></p>
<p>This report from your 32nd LD Endorsement Committee contains our recommendations for candidate endorsements for 2020. It is fair that we lay out the standards we used to evaluate the candidates.</p>
<br/>
<p><strong>Our overall position is that the endorsed candidates represent the values and policy positions of the 32<sup>nd</sup> Legislative District as identified in our platform. This platform has been crafted over many years and represents the principles by which we expect to be represented by our elected officials. It is a broad, sweeping document, encompassing local, state, national, and international policy positions. </strong></p>
<br/>
<p>The district platform is our guiding document. The articles of the platform set the policy standards we use when evaluating whether or not a candidate adheres to those policy positions or not. It is important to recognize that our recommendations are grounded in public policy positions, not in our personal likes or dislikes. Which policies do our elected representatives and candidates actually support or do not support? </p>
<br/>
<p>We ask, what public policy accomplishments or volunteer work related to specific public policy has the candidate supported or not supported?</p>
<br/>
<p>Other questions are: what are the candidate’s legislative priorities and do they fit with our district’s platform? Which significant issues in our platform, does the candidate <u>not</u> address?</p>
<br/>
<p>Additionally, Is the candidate electable; have name recognition; a good strategy; experience in the district? What funds have been raised? What are the sources of those funds, and what are the fundraising goals?</p>
<br/>
<p>We ask, has the candidate signed and adhered to our Civility Pledge?</p>
<p>What are other endorsing organizations? Are there constituent groups that present challenges and, if so, how will these challenges be addressed?</p>
<br/>
<br/>
<p>Respectfully Submitted,</p>
<p>Endorsement Committee, 32nd LD Democratic Organization: Steph Harris, Chair; Lael White; Sally Soriano; Brent McFarlane; Ted Hikel; Thom White; Pat Weber; Maralyn Chase; Kyle Burleigh; and Joe Cunningham (absent).  </p>
	</Layout>
)
