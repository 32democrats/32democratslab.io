import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../../components/layout'
import {MinutesHeader, minutes_type, minutes_status} from '../../../components/minutes'

const date = '2020-05-20T19:00-07:00'
const path = '/minutes/2020/05-20'
const remote = true
const status = minutes_status.approved
const type = minutes_type.endorsement

export const m = {date, path, remote, status, type}
export default () => (
	<Layout>
		<MinutesHeader m={m}/>
		<dl className="agenda">
			<dt>7:00</dt><dd>Call to order</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land Acknowledgment</dd>
			<dt>7:03</dt><dd>Thanks to everyone who filed for PCO.</dd>
			<dt>7:05</dt><dd>Endorsements
			<br/>Please review the Endorsement Committee Recommendations approved by the Executive Board as a slate.
			<br/>Also note, any member can pull a race to be focused on by itself.
			</dd>
			<dt>9:00</dt><dd>Adjourn</dd>
		</dl>
		<p>
			“Never bend your head. Always hold it high. Look the world straight in the eye.”
			<br/>– Helen Keller
		</p>
<pre className="article">{`
Via Zoom:
https://us02web.zoom.us/j/81156429919?pwd=SDJ1N2k2K2ZpMk5TbmZhemo1Sklodz09

Meeting ID: 811 5642 9919
Password: 654789

Call-in Phone # +1-699-900-6833

Standing rules governing candidate endorsements as specified in the Bylaws and Standing Rules

ENDORSEMENT RECOMMENDATIONS to Be Considered

STATE WIDE
Governor: Jay Inslee
Secretary of State: Gael Tarleton
State Treasurer: Mike Pellicciotti
Attorney General: Bob Ferguson
Commissioner of Public Lands: Hilary Franz
Superintendent of Public Instruction: Chris Reykdal
Insurance Commissioner: Mike Kreidler

FEDERAL REPRESENTATIVE
Congressional District 2: Jason Call
Congressional District 7: Pramila Jayapal

SNOHOMISH COUNTY
Court of Appeals, Div 1, Dist 2
Judge Position 2: Linda Coburn
Snohomish Superior Court
Judge Position 4: Edirin Okoloko
Judge Position 8: Robert Grant
Judge Position 13: Jennifer Langbehn
Judge Position 14: Paul Thompson

KING COUNTY
King County Superior Court
Judge Position 51: Cindi Port
Judge Position 19: Nelson Kao Hwa Lee
Judge Position 26: David Keenan


ENDORSEMENT AUDIT REPORT

After our May 20th Endorsement meeting, we received a copy of the POLL VOTING as well as registration. I checked the Membership list supplied by Eric and the PCO list supplied by Jeff for those members eligible and voting.  

AUDIT REPORT
82 Registrations
Correcting for 7 Double registrations, 17 candidates  (co-hosts and others signing in twice)
and guests we had 37 Eligible Voters (6 Members, 31 PCOs)

Three members that did not appear on the Membership or PCO list I believe to be eligible to vote but need documentation to verify before the next meeting - their votes did not impact any of the outcomes. 

Superior Court Judge, Position 8: Robert Grant (26 Yes, 5 No) Endorsed 84%
Superior Court Judge, Position 14: Cassandra Lopez-Shaw (11 Yes, 16 No) Not endorsed
Federal U.S. Representative District 2:
	Rick Larsen (22 Yes, 10 No) Endorsed 69%
	Jason Call (12 Yes, 20 No) Not Endorsed

State slate:  (30 Yes, 1 No)
Governor: Jay Inslee
Secretary of State: Gael Tarleton
State Treasurer: Mike Pellicciotti
Attorney General: Bob Ferguson
Commissioner of Public Lands: Hilary Franz
Superintendent of Public Instruction: Chris Reykdal
Insurance Commissioner: Mike Kreidler
Federal U.S. Representative
District 2: Rick Larsen
District 7: Pramila Jayapal

Snohomish County Court slate (Yes, unaminous)
Court of Appeals, Division 1, District 2
Judge Position 2: Linda Coburn
Superior Court
Judge Position 4: Edirin Okoloko
Judge Position 8: Robert Grant
Judge Position 13: Jennifer Langbehn
Judge Position 14: Paul Thompson

King County Court slate (Yes, unaminous)
King County Superior Court
19: Nelson Kao Hwa Lee
26: David Keenan
51: Cindi Port
`}</pre>
	</Layout>
)
