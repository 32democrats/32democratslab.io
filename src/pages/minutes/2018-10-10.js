import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>32LD Meeting Minutes – October 10, 2018</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Minutes of Regular Monthly Meeting, October 10, 2018
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
		</p>
		<p>
			Following a delicious, complete, tortilla-based dinner prepared by
			Rosamaria Graziani’s volunteer crew and made available to all comers (for
			free will contributions), and about 25 minutes of “open mic” time for any
			and all to express briefly their views, concerns, and announcements, the
			meeting was called to order at 7:09 pm by District Chair Carin Chase.
			After opening with the Pledge of Allegiance, M/S/C to adopt the agenda as
			proposed. M/S/C to approve the minutes of our last meeting, that of
			September 12, 2018.
		</p>
		<h2>Program: Two guest speakers</h2>
		<p>
			After recognition of various elected officials and a few candidates who
			were present, Congressmember Rick Larsen (CD2) spoke on several Washington
			state races and selected national issues, including the importance of
			taking allegations of sexual molestation seriously, and Pres. Trump’s
			execrable immigration policy. During the ensuing Q&A he spoke of the need
			to enforce the U.S. Constitution’s “emoluments” clause against this
			President, and endorsed the current security of voting and vote counting
			throughout the U.S.
		</p>
		<p>
			We next heard from Andrew Villeneuve, founder and executive director of
			the Northwest Progressive Institute, who discussed Washington’s initiative
			process in the historical context of (1) the nationwide “Progressive”
			movement’s long-standing promotion of holding public votes on specific
			major issues, as a key aspect of political reform, and (2) Washington’s
			very own, not-so-progressive initiative promoter, Tim Eyman. Andrew then
			gave a brief rundown of certain statewide measures that will appear on our
			upcoming November ballot; namely, Initiatives 1631, 1639, 940, and 1634,
			and “Advisory Vote” #19 (most of which our LD has already taken a formal
			position on). Further information on all such matters can be found on
			his/NWPI’s blog, “The Cascadia Advocate.”
		</p>
		<h2>Ballot Measures </h2>
		<p>
			Following a break, Shoreline City Councilmember Keith Scully spoke on
			behalf of Shoreline Proposition 1, a proposed “sales and use” tax that
			would supplement an existing car tab tax in order to provide for 4 miles
			of new sidewalks, and potentially up to 10-20 more miles later. Ridgecrest
			resident Dustin McIntyre spoke against the proposal. M/S that we endorse a
			“YES” vote thereon. Following remarks by Debbie Viertel in favor of the
			proposal, and by Janet Way against it, the motion failed by a 16-17 vote –
			60% being required for an endorsement of either position.
		</p>
		<p>
			M/S to endorse a “YES” vote on Initiative 1634. Following at least three
			speakers in favor and three opposed (in alternation), M/S/C to close
			debate – whereupon the motion to endorse I-1634 failed by a vote of 8-30.
			M/S/C (33-8) that we recommend a “NO” vote on I-1634.
		</p>
		<p>
			Sally Soriano explained Seattle Proposition 1, a “Families, Education and
			Preschool Promise” Levy, about which she had prepared and distributed a
			lengthy handout detailing its principal flaw(s). M/S that we take a
			position opposing the measure. Following speakers pro and con, it was
			voted (33-3) to oppose Seattle Prop 1.
		</p>
		<p>
			M/S/C that we support the “Maintain” position on (statewide) Advisory Vote
			#19.
		</p>
		<h2>Other Business and Announcements</h2>
		<p>
			Treasurer Eric Valpey reported a bank balance of $1444, slightly lower
			than we’d expected at this point – attributable at least in part to our
			2018 LD caucus having produced less revenue than expected.
		</p>
		<p>Our upcoming biennial reorganization will be held on December 12.</p>
		<p>
			<b>PCO appointments: </b>
			M/S/C to approve the appointment of Jacqueline Powers as the PCO for
			precinct SHL 32-0470. M/S/C to approve the appointment of Lael White as
			Acting PCO for MLT 10.
		</p>
		<p>
			Rebecca Wolfe thanked us for our help in her campaign for a position on
			the Snoco PUD.
		</p>
		<p>
			It was announced that Congressmember Pramila Jayapal (CD-7) will be
			appearing at an event with Senator Chase on Tuesday, October 23.
		</p>
		<p>M/S/C to adjourn, at 9:35 pm.</p>
		<p>Prepared and submitted by Dean Fournier, LD32 Secretary</p>
	</Layout>
)