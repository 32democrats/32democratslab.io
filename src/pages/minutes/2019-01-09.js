import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-01-09 Meeting</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Minutes of Regular Monthly Meeting, January 9, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<dl className="agenda">
		<dt>7:02</dt><dd><Link to="/minutes/2018-12-12" className="link">Approve of previous meeting's minutes</Link></dd>
		<dt>7:05</dt><dd>Announce city and legislative action civic engagement teams</dd>
		<dt>7:15</dt><dd>Member action on <Link to="/state-legislative-agenda">legislative priorities</Link></dd>
		<dt>7:45</dt><dd>Time with our elected officials</dd>
		<dt>8:15</dt><dd><Link to="/resolutions" className="link">Resolutions</Link>
			<ul className="links">
				<li><Link to="/resolutions/2019/Green-new-deal-and-committee">Supporting the plan for a green new deal & creation of the select committee for the green new deal</Link></li>
				<li><Link to="/resolutions/2019/I-1000">In support of I-1000</Link></li>
				<li><Link to="/resolutions/2019/Presidential-candidate-tax-returns">Supporting a legislative requirement that presidential and vice presidential candidates disclose eight years of tax returns in order to appear on Washington’s primary election ballot</Link></li>
			</ul>
		</dd>
		<dt>8:55</dt><dd>Treasurer's report</dd>
		</dl>
<div className="article">
<p>
	Email: <a href="mailto:info@32democrats.org">info@32democrats.org</a>
</p>
<p>
	Thanks to Rosamaria Graziani and the volunteer crew who prepared food and drink.
	There was good conversation for all. At 6:45 p.m. there were 15 minutes of
	"open mic" time for any and all to briefly their express views, concerns, and
	announcements. The meeting was called to order at 7:00 p.m. by District Chair
	Alan Charnley.
</p>
<p>
	After opening with the Pledge of Allegiance, M/S/C (Moved/Seconded/Carried) to
	adopt the agenda as proposed. M/S/C to approve the minutes of our last
	meeting, December 12, 2018. Alan then recognized various current and former
	elected officials in attendance.
</p>
<h2>1. Civic Engagement Teams</h2>
<p><strong>Jenna Nand, 2nd Vice-Chair</strong></p>
<p>
	The new executive board is interested in engaging in activism at all
	levels—public utility districts, school boards, city councils, county
	councils, state Legislature and Congress. "Civic Engagement Teams" can create
	'calls for action' when any of these bodies are facing important votes. We'd
	like one person to be a point-person. So far we have two volunteers: Gray
	Petersen, <a href="mailto:gray.petersen@32democrats.org">gray.petersen@32democrats.org</a> - Lynnwood
	and Janet Way, <a href="mailto:janetway@yahoo.com">janetway@yahoo.com</a> - Shoreline.
	If you would like to be a point-person, please contact Jenna: <a href="mailto:jenna.nand@32democrats.org">jenna.nand@32democrats.org</a>
</p>
<h2>2. 32nd LD State Legislative Priorities</h2>
<p>
	<strong>Alan Charnley, Chair,</strong> <a href="mailto:alan.charnley@32democrats.org"
		>alan.charnley@32democrats.org</a
	>
</p>
<p>
	The following legislative priorities were debated and voted on at our November
	LD meeting. As 2019 begins we're now ready to start the dialogue with elected
	officials, our neighbors and friends, so that these priorities will be
	legislated.
	Full version at: <a href="https://32democrats.gitlab.io/state-legislative-agenda/">https://32democrats.gitlab.io/state-legislative-agenda/</a>
	— edited version below:
</p>
<ol>
	<li>
		Repeal of sales tax on non-prescription medication &amp; hygiene products
	</li>
	<li>Protection of salmon and orcas by removal of barriers to fish passage</li>
	<li>Creation of a publicly-owned state bank</li>
	<li>Protection of good jobs and vote-approved Sound Transit 3 projects</li>
	<li>Lower the threshold for passage of school bonds to a simple majority</li>
	<li>Creation of a Single Payer Health Care system in WA State</li>
	<li>Find solutions to end the housing crisis</li>
	<li>Repeal the state sales tax and replace it with income tax</li>
	<li>Reduce green house gases in 12 years or less</li>
</ol>
<p>
	<strong>Our 32nd LD state elected officials</strong> were invited to discuss
	their legislative agendas at tonight's meeting but were unable to attend due
	to schedule conflicts. The committee assignments for 2019:
</p>
<ul>
	<li>
		<strong>Rep. Lauren Davis</strong>, <a href="mailto:Lauren.Davis@leg.wa.gov">Lauren.Davis@leg.wa.gov</a>
		<br />
		Public Safety (Vice-Chair); Capital Budget; Health Care and Wellness;
		Rules
	</li>
	<li>
		<strong>Rep. Cindy Ryu</strong>, <a href="mailto:Cindy.Ryu@leg.wa.gov">Cindy.Ryu@leg.wa.gov</a>
		<br />
		Housing, Community Development and Veterans (Chair); Appropriations;
		Consumer Protections and Business
	</li>
	<li>
		<strong>Sen. Jesse Salomon</strong>, <a href="mailto:Jesse.Salomon@leg.wa.gov">Jesse.Salomon@leg.wa.gov</a>
		<br />
		Agriculture, Water, Natural Resources and Parks (Vice Chair); Local
		Government (Vice Chair); Early Learning and K-12 Education; Law and Justice
	</li>
</ul>
<h3>
	Alan asked those in attendance: What legislation do you think is most
	important for 2019?
</h3>
<ul>
	<li>
		<strong>Mark Weiss:</strong> the State Bank legislation that Senator Hasegawa
		will be moving forward.
	</li>
	<li>
		<strong>Carin Chase, Edmonds School Board Member,</strong> <a href="mailto:chaseca@edmonds.wednet.edu">chaseca@edmonds.wednet.edu</a>:
		Education funding needs some fixes, particularly special ed, funding since
		McCleary did not fully fund K-12 education.
	</li>
	<li>
		<strong>Deborah Kilgore, Edmonds School Board,</strong> <a href="mailto:kilgored952@edmonds.wednet.edu">kilgored952@edmonds.wednet.edu</a>
		: OSPI (Office of Superintendent of Public Education)
		requests increased special ed funding. Edmonds spends $33.5 million for
		special ed whereas federal government sends only $1.4 million for special
		ed. So the state has to fix this funding gap. Please visit Madrona School, on January 15th at 5:45 p.m. (20420 68th Avenue West, Lynnwood, WA 98036).
		Madrona houses deaf and hard of hearing programs and you will be able to
		talk with special ed teachers.
	</li>
	<li>
		<strong>David Johnson:</strong> Get rid of the sales tax and implement a
		state income tax. This would help everybody and we must have more affordable
		housing—it's a crisis.
	</li>
	<li>
		<strong>Gray Petersen:</strong> We must achieve a Single Payer Health Care
		bill, not a public option bill which would contract with private providers.
		We support Single Payer as in HR 676 and won't accept substitutes.
	</li>
	<li>
		<strong>Brent McFarlane:</strong> We need more housing for renters below the
		poverty line, which is defined by below 50% of the area median income.
	</li>
	<li>
		<strong>Michael Bachety:</strong> rent stabilization, rights for tenants, energy self sufficiency
	</li>
	<li>
		<strong>Jenna Nand:</strong> Average people are being forced to move out of
		the way for development. The problem is gentrification. How do elected officials
		stand up to the developers and to Jeff Bezos? How can people live with
		dignity? Furthermore, we must have rest stops with showers, bathrooms, and
		washing machines available for un-housed persons.
	</li>
	<li>
		<strong>Rosamaria Graziani:</strong> I lived in a high-rise apartment with
		units that were affordable to very poor tenants. It worked well. It was a
		magnificent community!
	</li>
	<li>
		<strong>Jean Thomas:</strong> We need a State Bank because we're sending far
		too much money out of our state instead of using it for our needs here. Half
		the population rents and we need to figure out how to sustain a better mix
		for the viability of both renting and owning property.
	</li>
</ul>
<h2>3. Shoreline City Council Issues</h2>
<p>
	<strong>Shoreline Councilmember Keith Scully,</strong> <a href="mailto:kscully@shorelinewa.gov">kscully@shorelinewa.gov</a>
</p>
<h3>Several things going on in Shoreline:</h3>
<ol className="alpha">
	<li>
		<em>The swimming pool at the Spartan Center</em> is near the end of its
		life. I want to hear from your recommendations about our options. If we
		replace it, the cost will be $80 million, that's between $400-500 hundred
		dollars per household. Considerations are what the school district needs (a
		pool with lanes); and that the school district helps pay for a new pool.
		Should it stay at its current location? I'm not sure how I will vote on this
		issue.
	</li>
	<li>
		Speaker Chopp will introduce <em>legislation to close down Fircrest</em>.
		The Shoreline City Council would have to agree to up-zone this property for
		affordable housing. Shoreline needs more housing than Ronald Commons
		provides.
	</li>
	<li>
		Nothing is happening on the Council on the issue of <em>tree protection</em>.
		I couldn't even get a second to introduce legislation on this issue.
	</li>
</ol>
<h3>32nd LD member response to Council Member Scully's issues:</h3>
<ul>
	<li>
		We really need more housing for renters below the poverty line, which is
		defined as income below 50% of the area median.
	</li>
	<li>
		I'm concerned about where the proposed new housing would be located at
		Fircrest given the issue of toxicity from the lab located nearby.
		Potentially putting poor people in harms way.
	</li>
	<li>
		Who is looking out for the people living at Fircrest now? What voice will
		they have in the City Council and State's decisions?
	</li>
	<li>
		I suggested you a look at the tree protection legislation in Lynnwood
		(people are only allowed to cut down two trees per year), also checkout the
		Lynnwood pool as it is very popular and you might consider building the new
		pool on the Fircrest property.
	</li>
	<li>
		I'm shocked that no one would second your motion on tree protection. We'll
		be ready to help you next time it comes up. We've fought hard to save
		Fircrest and the Council needs to listen to Fircrest advocates.
	</li>
	<li>
		I'm concerned about how the Council spends money as it spent $20 million for
		62 units of affordable housing ($322,580.00 per unit) when backyard cottages
		would cost only $60,000 each. We also need rent stabilization, rights for
		tenants and energy self-sufficiency.
	</li>
	<li>
		In Vienna, Austria, 62% of the population is in public housing. We could
		finance it through city bonding.
	</li>
	<li>
		Most people I graduated from high school with have had to leave this area
		because they can't afford to live here.
	</li>
</ul>
<p>
	<strong>Council Member Scully:</strong> We need to stop subsidizing big
	business. People with high salaries are moving here and housing prices are now
	unaffordable for our own community. We must invest in those who make 30% below
	the median income.
</p>
<h2>4. Edmonds City Council Issues</h2>
<p>
	<strong>Adrienne Fraley-Monillas, Edmonds City Council Member,</strong> <a href="mailto:adrienne.monillas@edmondswa.gov"
		>adrienne.monillas@edmondswa.gov</a
	>
</p>
<p>What's going on in Edmonds:</p>
<ol className="alpha">
	<li>
		<em>Housing Affordability</em>: It's clear, you have to make $70,000 dollars
		a year to get into affordable housing in Edmonds.
	</li>
	<li>
		<em>Fircrest</em>: I spent 33 years working at Fircrest. The pool there was
		closed because it was built in the 1950s and cost $1 million a year to run.
		Fircrest is full of wonderful trees. The lab does handle dangerous materials
		such as anthrax and it's well known that it could be a target for terrorism.
		While there is a lot of extra space at Fircrest, but the current Shoreline
		master plan would deny living space to many clients.
	</li>
	<li>
		<em>Edmonds City Council Seats</em>: We have four council members up for
		election this year. The Council did give $1 million on a 3 to 4 vote to the
		Edmonds wetlands. That was good environmental work.
	</li>
	<li>
		<em>Recent Murders in Edmonds</em>: We are experiencing the same issues of
		gun violence as other cities. The murder and attempted murder/suicide
		involved people from outside of Edmonds — very tragic. I encourage you to
		talk to your city council members about gun ordinances. People brought guns
		into Edmonds. We may be having less violence than other cities but we've
		still had to put police officers into all of our high schools.
	</li>
	<li>
		<em>Edmonds will be 100% renewable by the end of 2019</em>: We are
		purchasing clean energy and are encouraging other cities to go 100%
		renewable as well. <br />- Sierra Club Press Release: <a
		href="https://www.sierraclub.org/ready-for-100/commitments"
			>https://www.sierraclub.org/ready-for-100/commitments</a
		>
	</li>
</ol>
<h3>Q&A:</h3>
<ul>
	<li>
		Q: When is Edmonds going to get a tree ordinance? <br />
		A: When we have more voices on the council.
	</li>
	<li>
		Q: What's the council's response to the neo-Nazi pamphlets all over Edmonds?
		<br />
		A: We rallied against it and we are not going to tolerate it. People were at
		the rally from surrounding cities. If you follow this issue, you will see
		that cities as far south as Texas have had similar pamphlets spread through
		their communities.
	</li>
	<li>
		Q: What is the state of the homeless population in Edmonds? What about rest
		stops (showers, bathrooms and laundry facilities)? <br />
		A: We hired a consultant who came in and interviewed people. Many homeless
		people hide in obscure places in Edmonds. We have to have enough affordable
		housing.
	</li>
</ul>
<h2>5. McCleary, Fircrest, Fair State Tax</h2>
<p>
	<strong>State Senator Maralyn Chase</strong>, <a href="mailto:maralynchase@gmail.com">maralynchase@gmail.com</a>
</p>
<p>
	<em>McCleary/Article 9</em>: The McCleary education lawsuit pertained only to
	Article 9 in the State Constitution, which requires fully funding K-12
	education for all children. The outcome of the McCleary lawsuit was that
	education received more funding, but not enough. Special education was not
	funded. The Legislature did a levy swap, and the state took over local
	funding. Now the state will probably allow local school boards to raise the
	cap on their levies. So, once again wealthy school districts will benefit, poor school districts will lose out. The systemic problem is that we don't
	have a large enough tax base.
</p>
<p>
	Fircrest/Article 13: There's a larger picture here related to Fircrest. In
	June, 2018, the Feds decertified Western State Hospital for failing to comply
	with standards and withdrew 20% of their operating budget. Then in December
	Inslee settled a lawsuit brought by Disability Washington against the state on
	behalf of incarcerated mentally ill people. Inslee offered a settlement and
	the judge agreed to build about a dozen community hospitals to provide
	services and beds for individuals referred by the courts (Article 13-safety
	net funding). The question for the future of Fircrest is if it is closed down, where is the state going to find inexpensive property to build? Fircrest
	serves vulnerable, fragile people, and there is currently a concentration of
	services around Fircrest. We can't just move the Fircrest residents out to
	Arlington or Smokey Point, for example, because there aren't the complex
	services there. It is one thing to say we want community-based services but
	another thing to <em>really</em> deliver these services.
</p>
<p>
	<em>Creating a Fair State Tax System/Article 7</em>: All this comes down to
	Article 7. We need a fair taxation system in this state, and every income
	group needs to pay its fair share. Keep in mind that there are currently $50.3
	billion in tax exemptions at the state level and $55.5 billion at the local
	level.Ask the legislature to remove state tax exemptions. This will give us
	the broad tax base we need to fairly pay for quality education and quality
	habilitation centers like Fircrest.
</p>
<h2>6. City of Lynnwood Issues</h2>
<p>
	<strong>Ted Hikel, former Lynnwood City Council Member,</strong> <a href="mailto:councilmantedhikel@hotmail.com"
		>councilmantedhikel@hotmail.com</a
	>
</p>
<p>What's going on in Lynnwood:</p>
<ol className="alpha">
	<li>
		<em>Lynnwood City Council has four seats up for election this year.</em> We
		have two Democrats on the Council now — Shirley Sutton and George Hurst.
	</li>
	<li>
		<em>Regional Fire Authority</em> will be voted on in the <em>April election</em>.
	</li>
	<li>
		<em
			>On repealing the state sales tax: I suggest we repeal of <strong>all</strong> sales taxes</em
		>, period. Lynnwood has the highest sales tax in the state. If you buy a
		$10,000 car in Lynnwood, you pay a sales tax of $1,000. Who would want to
		shop in Lynnwood?
	</li>
</ol>
<h2>7. Resolutions</h2>
<ol className="h h3 upper-alpha">
	<li>
		<h3>
			<a href="https://32democrats.gitlab.io/resolutions/2019/Green-new-deal-and-committee">
			Resolution supporting the Plan for a Green New Deal and creation of the select committee for the green new deal
			</a>
		</h3>
		<p>
			<strong>Introduced by Gray Petersen,</strong> <a href="mailto:gray.petersen@32democrats.org"
				>gray.petersen@32democrats.org</a
			>
		</p>
		<p>
			Green New Deal (GND) is a bold proposal to solve both the climate crisis
			and the economic crisis that we are facing. The GND is an umbrella term
			for a set of policies that will transition our country off of fossil fuels
			to a 100% renewable energy future. All of this will create of 10s of
			millions of good paying jobs that will help to eliminate poverty. This is
			a great socio-economic project which rivals some of the greatest economic
			projects this country has ever initiated. It will mitigate climate
			catastrophe and align with the latest UN climate science and recommended
			protocol.
		</p>
		<h3>Q&A:</h3>
		<p>
			Q: What will the funding structure look like? <br />
			A: It doesn't have a finance section yet. The committee would determine
			this. It also has to be voted on by the U.S. House and U.S. Senate.
			Somehow Congress finds a way to give tax cuts to the rich and fund a huge
			military budget. I'm sure, with far-thinking members of Congress we'll be
			able to fund such an important socio-economic project. Also, I think the
			carbon tax gets in the way, rather than taxing emissions, we need to get
			Exxon out of government.
		</p>
		<p>
			Comment: The whole purpose is to change the paradigm and to not put the
			burden on poor people. The transition away from fossil fuel currently
			scares people. However, a massive push for full employment and livable
			wages can move us away from climate catastrophe toward a 100% renewable
			energy future. This will inspire everyone to support the Green New Deal.
		</p>
		<p>
			Comment in Support: Working people are not going to buy into solving
			global pollution if their choice is between that and feeding their
			families. This resolution is about creating an economy that doesn't chew
			up the planet — no more consume, consume, consume.
		</p>
		<p>
			Comment in Opposition: I'm in support of the concept but it's over two
			pages long and will not be accepted at King County Democrats.
		</p>
		<p>
			Comment for clarification: Yes, length does matter with state resolutions
			but we're early in the process. Right now we want to give readers as much
			information as possible.
		</p>
		<p><strong>M/S/C - Passes unanimously</strong></p>
	</li>
	<li>
		<h3>
			<a href="https://32democrats.gitlab.io/resolutions/2019/I-1000">
			Resolution in Support of I-1000
			</a>
		</h3>
		<p>
			<strong>Introduced by Carin Chase,</strong> <a href="mailto:carin.chase@32democrats.org"
				>carin.chase@32democrats.org</a
			>
		</p>
		<p>
			In our September 12th, 2018 meeting we passed a resolution supporting
			signature gathering for I-1000. Senator Chase urged us to sign
			this"Initiative to the Legislature" intended to rescind Initiative 200.
			The measure was enacted several years ago to prohibit using affirmative
			action to redress pass racial injustice. The 32nd LD turned in over 30
			petitions for I-1000. On January 4th I-1000 organizers submitted over
			367,000 signatures to the Secretary of State. The Legislature now has 3
			options:
		</p>
		<ol>
			<li>
				Pass the Initiative, which makes I-1000 state law; <strong>OR</strong>
			</li>
			<li>
				Pass an alternative bill which would go on the November, 2019 ballot
				right next to I-1000, as I-1000 (B); **OR **
			</li>
			<li>
				Do nothing (meaning no vote) and simply allow I-1000 to go to the
				November, 2019 ballot by itself.
			</li>
		</ol>
		<p>
			<em
				>The 32nd LD supports #1, the Legislature passing Initiative 1000 to
				make it state law.</em
			>
		</p>
		<p><strong>M/S/C - Passes unanimously</strong></p>
	</li>
	<li>
		<h3>
			<a href="https://32democrats.gitlab.io/resolutions/2019/Presidential-candidate-tax-returns">
			Resolution Supporting a Legislative Requirement that Presidential and Vice
			Presidential Candidates Disclose Eight Years of Tax Returns in Order to
			Appear on Washington's Primary Election Ballot
			</a>
		</h3>
		<p>
			<strong>Introduced by Janet Way</strong>, <a href="mailto:janetway@yahoo.com">janetway@yahoo.com</a>
		</p>
		<p>
			<strong>Janet:</strong> Carl and Paula Larson are from the 1st LD and are
			part of the group working on this resolution.
		</p>
		<p>
			<strong>Carl:</strong> This resolution says presidential candidates and
			vice-presidential candidates must submit their tax returns before being
			included on our state's primary ballot. SB 5078 will be introduced by
			Patty Kuderer (48th-D) in the State Senate and Rep. Derek Stanford
			(1st-D) will introduce a similar bill in the State House. If this
			resolution is approved tonight, the State Democratic Central Committee can
			consider it at their meeting on January 28th &amp; 29th. Similar
			resolutions have passed in six other Legislative Districts.
		</p>
		<h3>Q&A:</h3>
		<ul>
			<li>
				Q: This resolution applies only to the primary, why was the general left
				out? <br />
				A: It was left out because of technical issues of our State
				Constitutional issue.
			</li>
			<li>
				Q: Has it been passed into law in other states? <br />
				A: Both California and New Jersey legislatures passed it but it was
				vetoed by both governors. It has been reintroduced in California and is
				expected to be signed by Gov. Newsom. It is being introduced in 20 other
				states. We have a local working group if you would like to join us.
				There are about 22 people involved and we meet once a month. Contact
				Janet: <a href="mailto:janetway@yahoo.com">janetway@yahoo.com</a>
			</li>
		</ul>
		<p><strong>M/S/C - Passes unanimously</strong></p>
	</li>
</ol>
<h2>8. Treasurer's Report - Eric Valpey, Treasurer</h2>
<p>
	We now have $2,960 in the bank and have no debt! The takeaways for the year
	are: we've had $9,500 in contributions; had healthy contributions from members
	— $6,000 came from membership and $3,000 from fundraisers (dinners, raffles).
	Most of our funds were spent on food. We did however have some legal expenses, similar to the other LDs. We had fewer printing expenses this year than last
	year. <a href="mailto:eric.valpey@32democrats.org">eric.valpey@32democrats.org</a>
</p>
<h2>9. New Business and Good of the Order</h2>
<ul>
	<li>
		Rosamaria Graziani: Next month the Masonic Hall kitchen construction will be
		finished and we're looking forward to cooking a wonderful Valentine's Day
		dinner for everyone — a big red evening! <a href="mailto:rosamaria.graziani@32democrats.org"
			>rosamaria.graziani@32democrats.org</a
		>
	</li>
	<li>
		Cathy Baylor, Vice-President, WSFDW: The Washington State Federation of
		Democratic Women will be meeting this Sunday, January 13th at the Mount Lake
		Terrace Library, 1:30 p.m. - 3 p.m., everyone is invited. We are Democratic women
		working on Democratic issues. Carol McMahon is President, <a href="mailto:carol.mcmahon@32democrats.org"
			>carol.mcmahon@32democrats.org</a
		>
	</li>
	<li>
		Michael Brunson: If you have a small appliance that is not working, there is
		a group of dedicated volunteers who are concerned about not filling up
		landfills with broken small appliances by offering to fix them. Contact the
		NE Seattle Tool Lending Library for more information: <a href="http://neseattletoollibrary.org"
			>http://neseattletoollibrary.org</a
		>
	</li>
</ul>
<p><strong>M/S/C Meeting adjourned at 9:10 p.m.</strong></p>
<p>
	Prepared and submitted by Sally Soriano, LD32 Secretary, <a href="mailto:sally.soriano@32democrats.org">sally.soriano@32democrats.org</a>
</p>
</div>
</Layout>
)
