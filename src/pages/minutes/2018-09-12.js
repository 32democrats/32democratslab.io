import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>32LD Meeting Minutes – September 12, 2018</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Minutes of Regular Monthly Meeting, September 12, 2018
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
		</p>
		<p>
			Following a delicious complete Mexican dinner prepared by Rosamaria
			Graziani’s volunteer crew and made available to all comers (for free will
			contributions), and about 30 minutes of “open mic” time for any and all to
			express briefly their views, concerns, and announcements, the meeting was
			called to order at 7 pm sharp by District Chair Carin Chase. After opening
			with the Pledge of Allegiance, M/S/C to adopt the agenda as proposed.
			M/S/C to approve the minutes of our last meeting, that of August 8.
		</p>
		<h2>Filling Vacancies</h2>
		<p>
			Following recognition of various elected officials and a few candidates
			who were present, Carin reminded the body that one of our two positions on
			the State Democratic Central Committee had been vacated by the recent
			death of Elaine Phelps. She accordingly opened the meeting to nominations
			to fill that office for the remainder of Elaine’s term. M/S to nominate
			Heather Fralick. There were no other nominations. Heather spoke, and – by
			a vote limited to our PCO members only – was elected to serve in that
			role.
		</p>
		<p>
			Carin then announced another newly arisen vacancy on our Executive Board,
			that of liaison to the Young Democrats – previous incumbent Victoria
			Valentine having moved to Oregon. This being a role that, under our
			Bylaws, is to be filled by the Chair through appointment, she announced
			Jenna Nand as her selection for that office. Jenna spoke, and it was M/S/C
			to ratify her selection for that post.
		</p>
		<h2>Nominating Convention</h2>
		<p>
			With the results of the “Top-Two” (winnowing) primary now behind us, Carin
			convened our meeting as a “Nominating Convention” of our PCOs, to
			determine the official Democratic Party nominees for LD32 elective public
			offices.
		</p>
		<p>
			For our State Senate position, M/S to nominate Maralyn Chase. There were
			no other nominations. Senator Chase spoke, and the PCOs voted to make her
			our nominee.
		</p>
		<p>
			For our State Representative Position No. 1, M/S to nominate Cindy Ryu.
			There were no other nominations. In her absence, our PCOs voted (13-7) to
			make her our nominee.
		</p>
		<p>
			For our State Representative Position No. 2, M/S (by Chris Roberts) to
			nominate Lauren Davis. There were no other nominations. Lauren spoke, and
			the PCOs voted (19-1) to make her our nominee.
		</p>
		<h2>New Business</h2>
		<p>
			With the specified business of the Nominating Convention thus concluded,
			Carin reconvened our general meeting to consider “New Business.”
		</p>
		<p>
			Audrey Smith, of this year’s Coordinated Campaign, described both their
			planned and ongoing efforts in our area, to get Democrats elected. She can
			be reached at audrey@wa-democrats.org, or 206-817-9007.
		</p>
		<p>
			A representative from the Initiative 1639 campaign (to reduce gun violence
			and related deaths) urged us all to vote “Yes” thereon. It was then
			suggested that at our next meeting we assess all the initiatives that have
			qualified for the November ballot, in order to establish our formal
			positions thereon.
		</p>
		<p>
			On a related matter, Senator Chase urged us to sign Initiative 1000, an
			“Initiative to the Legislature” intended to rescind Initiative 200, the
			measure enacted several years ago to prohibit “affirmative action” steps
			in redress of past racial injustice. Some 300,000 signatures will be
			required to qualify I-1000 for a public vote. M/S/C to support
			signature-gathering for I-1000.
		</p>
		<p>
			<b>
				<u>WSDCC</u>
			</b>
			: Chris Roberts reported on the upcoming WSDCC meeting, to be held in
			Spokane this very weekend, at which many proposed Party rules changes will
			be considered – including one providing for “automatic” delegates. M/S
			(Gray Petersen) to instruct our WSDCC representatives to “do their best”
			to establish a rule preventing disclosure of the names or other
			information about individual members of WSDCC “constituency caucuses,”
			without the consent of those individuals. After some discussion, Gray’s
			motion carried.
		</p>
		<p>
			<b>
				<u>Resolutions</u>
			</b>
			: M/S/C to suspend the rules to permit consideration of two tardily posted
			resolutions that had been made available in printed form since the opening
			of the meeting. M/S/C to adopt the first of them, the “ Resolution in
			Support of the Keep Washington Working Act ” – a bill that will be 2SSB
			5689 in the upcoming (2019-20) legislative session, and of which Senator
			Chase is a co-sponsor. Then, M/S to adopt the other printed resolution,
			titled “ Reject Corporate PAC Money from the Fossil Fuel Industry ”; after
			some discussion, this resolution too was adopted.
		</p>
		<p>
			<b>
				<u>Appointments</u>
			</b>
			: M/S to approve the appointment of Michael Bachety as PCO for precinct
			SHL 1030. Michael spoke, and – following Q&amp;A – it was voted to approve
			his appointment.
		</p>
		<p>
			<b>
				<u>Expenses at WSDCC meetings</u>
			</b>
			: M/S to authorize our reimbursing the hotel lodging expenses of each of
			our (three) officially designated representatives at each WSDCC meeting
			held east of the Cascade Mountains. M/S/C to limit any such reimbursement
			to $250 per meeting, for each such person. After some discussion, M/S/C to
			extend such authorization to include attendance at WSDCC meetings held
			over 100 miles from our LD, even if west of the Cascades. As thus amended,
			the motion carried.
		</p>
		<p>
			<b>
				<u>Good of the Order</u>
			</b>
			: Senator Chase suggested that we give attention to the question of how
			the provision of reproductive services and mental health services can be
			preserved and protected, if Trump nominee Brett Kavanaugh is confirmed for
			a seat on the U.S. Supreme Court.
		</p>
		<p>M/S/C to adjourn, at 8:49 pm.</p>
		<p>Prepared and submitted by Dean Fournier, LD32 Secretary</p>
	</Layout>
)