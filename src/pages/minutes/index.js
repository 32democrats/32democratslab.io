import React from 'react'
import { Link } from 'gatsby'
import { base } from '../../config'
import Layout from '../../components/layout'
import {LinkMinutes} from '../../components/minutes'

import {m as m2021_01} from './2021/01'
import {m as m2020_12} from './2020/12'
import {m as m2020_10} from './2020/10'
import {m as m2020_09} from './2020/09'
import {m as m2020_08} from './2020/08'
import {m as m2020_07} from './2020/07'
import {m as m2020_06} from './2020/06'
import {m as m2020_05_20} from './2020/05-20'
import {m as m2020_05} from './2020/05'
import {m as m2020_04} from './2020/04'
import {m as m2020_02} from './2020/02'
import {m as m2020_01} from './2020/01'
export const meetings = [
	m2021_01,
	m2020_12,
	m2020_10,
	m2020_09,
	m2020_08,
	m2020_07,
	m2020_06,
	m2020_05_20,
	m2020_05,
	m2020_04,
	m2020_02,
	m2020_01,
]

export default () => (
	<Layout>
		<h1 className="title">Minutes</h1>
		<ul className="menu">
			{meetings.map(m=>(
				<li><LinkMinutes m={m}/></li>
			))}
			<li>
				<Link to="/minutes/2019-12-11">2019-12-11</Link>
			</li>
			<li>
				<Link to="/minutes/2019-11-13">2019-11-13</Link>
			</li>
			<li>
				<Link to="/minutes/2019-10-09">2019-10-09</Link>
			</li>
			<li>
				<Link to="/minutes/2019-09-11">2019-09-11</Link>
			</li>
			<li>
				<Link to="/minutes/2019-08-14">2019-08-14</Link>
			</li>
			<li>
				<Link to="/minutes/2019-06-12">2019-06-12</Link>
			</li>
			<li>
				<Link to="/minutes/2019-05-08">2019-05-08</Link>
			</li>
			<li>
				<Link to="/minutes/2019-04-10">2019-04-10</Link>
			</li>
			<li>
				<Link to="/minutes/2019-03-13">2019-03-13</Link>
			</li>
			<li>
				<Link to="/minutes/2019-02-20">2019-02-20</Link>
			</li>
			<li>
				<Link to="/minutes/2019-01-09">2019-01-09</Link>
			</li>
			<li>
				<Link to="/minutes/2018-12-12">2018-12-12</Link>
			</li>
			<li>
				<Link to="/minutes/2018-10-10">2018-10-10</Link>
			</li>
			<li>
				<Link to="/minutes/2018-09-12">2018-09-12</Link>
			</li>
			<li>
				<Link to="/minutes/2018-08-08">2018-08-08</Link>
			</li>
			<li>
				<a href={base + "/meeting-minutes/"}>...more minutes</a>
			</li>
		</ul>
	</Layout>
)