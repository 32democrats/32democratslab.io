import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React from 'react'
import Layout from '../../components/layout'
export default () => (
	<Layout>
		<Helmet>
			<title>2019-12-11 Meeting & Party</title>
		</Helmet>
		<p>
			<b>
				32<sup>nd</sup> Legislative District Democratic Organization
			</b>
		</p>
		<h1>
			Meeting, Auction, and Party, December 11, 2019
		</h1>
		<p>
			<b>at the Richmond Masonic Center</b>
			<br/>753 N 185th St, Shoreline, WA 98133
		</p>
		<dl className="agenda">
			<dt>6:00</dt><dd>Dinner and desserts <span className="money">$10 suggested donation</span></dd>
			<dt>6:30</dt><dd>Silent auction open</dd>
			<dt>7:00</dt><dd>Call to order</dd>
			<dd>Chair statements</dd>
			<dt>7:01</dt><dd><Link to="/pledge-of-allegiance" className="link">Pledge of Allegiance</Link></dd>
			<dt>7:02</dt><dd>Land Acknowledgment</dd>
			<dt>7:03</dt><dd><Link to="/minutes/2019-11-13" className="link">Approve of previous meeting's minutes</Link></dd>
			<dt>7:05</dt><dd>Auction</dd>
			<dt>7:15</dt><dd>Raffle</dd>
			<dt>8:10</dt><dd>Silent auction closed</dd>
			<dt>8:30</dt>
				<dd>Other business</dd>
				<dd>Good of the order</dd>
			<dt>9:00</dt><dd>Adjourn</dd>
		</dl>
	</Layout>
)
