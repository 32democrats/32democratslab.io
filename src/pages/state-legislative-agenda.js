import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../components/layout'
export default () => (
<Layout>
<Helmet>
	<title>
		2019 State Legislative Agenda
	</title>
</Helmet>
<h1>
	2019 State Legislative Agenda
</h1>
<p>
The 32nd District Democrats advance this legislative agenda for 2019 and strongly encourage our legislators to sponsor or co-sponsor legislation, publicly speak in favor of these issues, and advance these issues in their respective caucuses.
</p>
<h2>
Issues we strongly support:
</h2>
<ol>
<li>
The repeal of sales taxes on non-prescription medication and hygiene products.
</li>
<li>
Protection of our salmon, orcas, and tribal fishing rights by speeding up removal of physical state and local barriers to fish passage.
</li>
<li>
The creation of a publicly-owned state bank.
</li>
<li>
The protection of good jobs and voter-approved Sound Transit 3 projects by fully replacing (on a 1:1 basis) any revenue lost due to vehicle fee formula changes.
</li>
<li>
Amending the State Constitution to lower the threshold for passage of school bonds to a simple majority.
</li>
<li>
The creation of a single-payer health care system in Washington State.
</li>
<li>
An aggressive state-led construction program of public, subsidized, low-income, and supportive housing to address the housing crisis as well as emergency provision of shelter and services for those currently experiencing houselessness.
</li>
<li>
Repeal the state sales tax and replace it with a progressive, graduated income tax.
</li>
<li>
Recognizing the urgent need to drastically reduce greenhouse gases in 12 years or less, and to rapidly implement Electric Vehicle, rail, and other alternative transportation programs.
</li>
</ol>
<h2>
Issues we strongly oppose:
</h2>
<ol>
<li>
Preempting local zoning authority.
</li>
<li>
Public funding of charter schools.
</li>
<li>
Any restrictions on a woman’s right to make her own health care decisions, including any regulation that would limit her practical ability to carry out those decisions.
</li>
</ol>
<p>
The ordering within these lists do not indicate relative priority.
</p>
</Layout>
)
