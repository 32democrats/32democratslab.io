import { Link } from 'gatsby'
import React from 'react'
import Helmet from 'react-helmet'
import Layout from '../../components/layout'

const title = 'Events'
// const gcal = <img src="https://www.gstatic.com/images/branding/product/2x/calendar_16dp.png" alt='GCal' title='Google Calendar'/>
export default () => (
	<Layout>
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<h1 className="title">{title}</h1>
		<dl className="agenda events">
			{/* <dt id="e2019-10-12">
				2019-10-12
				<br/><span>10:00 - 12:00</span>
			</dt>
			<dd>
				<h1>
					<b>PCO literature pick-up</b>, Saturday at Shoreline Library
				</h1>
				Get your canvass materials, ask questions about candidates, and start getting out the vote!
			</dd> */}
		</dl>
		<ul className='menu'>
			<li><a href="https://calendar.google.com/calendar/embed?src=calendar%4032democrats.org&ctz=America%2FLos_Angeles">Calendar</a></li>
			<li><Link to="/minutes">Monthly Meetings</Link></li>
			<li><a href='https://www.kcdems.org/events/'>King County Democrats events</a></li>
			<li><a href='http://snocodems.org/calendar/'>Snohomish County Democrats events</a></li>
		</ul>
	</Layout>
)