import Helmet from 'react-helmet'
import React from 'react'
import Layout from '../components/layout'
import Input from '../components/input'
import Print from '../components/print'

const demo = {
	// on: true,
}
const email = 'chair@32democrats.org'
const title = 'Civility Pledge'
const no = 'No'
const yes = 'Yes'

const data = {
	civility: yes,
	name: 'J. Doe',
	office: 'Mayor',
	position: 'Seattle',
	signature: 'X',
}
const data_blank = Object.fromEntries(
	Object.entries(data).map(([k, v])=>[k, ''])
)
const today = (new Date()).toISOString().substring(0,10)

const Page = () => (
	<Layout>
		<CivilityPledge data={demo.on && data}/>
	</Layout>
)

export class CivilityPledge extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			...data_blank,
			...props.data,
		}
		this.onChange = this.onChange.bind(this)
	}
	onChange(ev){
		this.setState({[ev.target.name]: ev.target.value})
	}
	onSubmit = (ev) => {
		ev.preventDefault()
	}
	render(){
		const {onChange, state} = this
		return (
	<div className="form32">
		<Helmet>
			<title>{title}</title>
		</Helmet>
		<p>
			All candidates seeking endorsement are asked to sign a civility pledge.
		</p>
		<form
			action={`mailto:${email}?subject=${title}`}
			onSubmit={this.onSubmit}
			method='get' enctype='text/plain'
		>
		<h1>{title}</h1>
		<p>
			As a candidate for elective office, I pledge to uphold the following principles in my campaign.
		</p>
		<ol>
			<li>
				<p>
					I will respect and listen to those who bring different points of view to the public arena, including those with whom I disagree.
				</p>
			</li>
			<li>
				<p>
					I will insist that my campaign message, literature, slogans, radio and TV ads, paid staff, and volunteers respect the dignity of all people – no matter what their background may be. I will demand corrective action if violations do occur.
				</p>
			</li>
			<li>
				<p>
					I will represent myself and my beliefs in an honest and forthright manner, and I will be clear with voters about the programs and policies that I plan to promote if elected. I will not use deceptive campaign tactics.
				</p>
			</li>
			<li>
				<p>
					I will challenge any attempts to stifle public debate and I will protect everyone's right to express their beliefs and views without the threat of harassment, intimidation, or violence.
				</p>
			</li>
			<li>
				<p>
					I will honor the spirit as well as the letter of all campaign finance laws and honestly report, as fully as possible, all expenditures made on my behalf.
				</p>
			</li>
		</ol>
			<Input name='name' label="Candidate's Name" required={true} value={state.name} onChange={onChange}/>
			<div className="flex wrap">
				<label className="label grow">
					<span>Elective Office</span>
					<input name='office' required={true} className="grow" value={state.office} onChange={onChange}/>
				</label>
				<label className="label grow">
					District/Position
					<input name='position' required={true} className="grow" value={state.position} onChange={onChange}/>
				</label>
			</div>
			<label className="label">
				<input value={yes} name="civility" type="radio" checked={state.civility===yes} onChange={onChange}/>
				Yes, I agree to uphold the Civility Pledge in my campaign.
			</label>
			<label className="label">
				<input value={no} name="civility" type="radio" checked={state.civility===no} onChange={onChange}/>
				No, I do not agree to uphold the Civility Pledge in my campaign.
			</label>
			<div className="flex wrap">
				<label className="label">
					Date{' '}
					<input
						name='date'
						pattern="[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])"
						placeholder="YYYY-MM-DD"
						required={true}
						size="11"
						defaultValue={today}
					 onChange={onChange}
					/>
				</label>
				<label className="label grow">
					Signature <input name='signature' required={true} className='grow' value={state.signature} onChange={onChange}/>
				</label>
			</div>
			<div className='flex wrap no-print'>
				{/* <button type='submit' className='grow'>
					Email Client
				</button> */}
				<Print className='grow block'/>
			</div>
		</form>
	</div>
		)
	}
}

export default Page
