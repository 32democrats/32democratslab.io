import {codename} from '../config'
import {createElement as h} from 'react'

export const wsdcc_action = {
	unknown: 0,
	not_passed: 1,
	passed: 2,
	referred_to_originator: 3,
	submitted: 4,
	tabled: 5,
}
export const wsdcc_action_names = {
	[wsdcc_action.unknown]: '______',
	[wsdcc_action.not_passed]: 'NOPASS',
	[wsdcc_action.passed]: 'PASS',
	[wsdcc_action.referred_to_originator]: 'REFORG',
	[wsdcc_action.submitted]: 'SUB',
	[wsdcc_action.tabled]: 'TABLE',
}
export const wsdcc_issue = {
	unknown: 0,
	agriculture: 1,
	arts: 2,
	civil_rights: 3,
	corporate: 4,
	economic: 5,
	education: 6,
	environment: 7,
	foreign: 8,
	government: 9,
	gun: 10,
	health: 11,
	housing: 12,
	human: 13,
	immigration: 14,
	labor: 15,
	law: 16,
	media: 17,
	military: 18,
	party: 19,
	transportation: 20,
	tribal: 21,
}
export const wsdcc_issue_doc = {
	[wsdcc_issue.unknown]: "Unknown",
	[wsdcc_issue.agriculture]: "Agriculture, Aquaculture, Fisheries, and Forestry",
	[wsdcc_issue.arts]: "Culture and Arts",
	[wsdcc_issue.civil_rights]: "Human Rights and Civil Rights",
	[wsdcc_issue.corporate]: "Corporate Power",
	[wsdcc_issue.economic]: "Economic Justice, Jobs, and Tax Fairness",
	[wsdcc_issue.education]: "Education",
	[wsdcc_issue.environment]: "Energy, Environment, and the Climate Crisis",
	[wsdcc_issue.foreign]: "Foreign Policy",
	[wsdcc_issue.government]: "Government and Political Reform",
	[wsdcc_issue.gun]: "Reducing Gun Violence",
	[wsdcc_issue.health]: "Health Care",
	[wsdcc_issue.housing]: "Housing Justice",
	[wsdcc_issue.human]: "Human Services",
	[wsdcc_issue.immigration]: "Immigration",
	[wsdcc_issue.labor]: "Labor",
	[wsdcc_issue.law]: "Law and the Justice System",
	[wsdcc_issue.media]: "Media Reform",
	[wsdcc_issue.military]: "Military and Veterans Affairs",
	[wsdcc_issue.party]: "Internal Party Business/Party Affairs",
	[wsdcc_issue.transportation]: "Transportation",
	[wsdcc_issue.tribal]: "Tribal Relations and Sovereignty",
}
export const wsdcc_issue_names = {
	[wsdcc_issue.unknown]: '____',
	[wsdcc_issue.agriculture]: 'AGR',
	[wsdcc_issue.arts]: 'ARTS',
	[wsdcc_issue.civil_rights]: 'CIV',
	[wsdcc_issue.corporate]: 'CORP',
	[wsdcc_issue.economic]: 'ECON',
	[wsdcc_issue.education]: 'EDU',
	[wsdcc_issue.environment]: 'ENV',
	[wsdcc_issue.foreign]: 'FOR',
	[wsdcc_issue.government]: 'GOV',
	[wsdcc_issue.gun]: 'GUN',
	[wsdcc_issue.health]: 'HEA',
	[wsdcc_issue.housing]: 'HOU',
	[wsdcc_issue.human]: 'HUM',
	[wsdcc_issue.immigration]: 'IMM',
	[wsdcc_issue.labor]: 'LAB',
	[wsdcc_issue.law]: 'LAW',
	[wsdcc_issue.media]: 'MED',
	[wsdcc_issue.military]: 'MIL',
	[wsdcc_issue.party]: 'PAR',
	[wsdcc_issue.transportation]: 'TRAN',
	[wsdcc_issue.tribal]: 'TRIB',
}


export function WsdccHeader({w}){
	return h('div',
		{style:{textAlign:'right'}},
		[
			`${w.organization || codename}RES`,
			w.number,
			new Date(w.action_date).toISOString().substring(2,10).split('-').join(''), // YYMMDD
			wsdcc_action_names[w.wsdcc_action],
			wsdcc_issue_names[w.wsdcc_issue],
			w.title_short,
		].join(' – ')
	)
}
