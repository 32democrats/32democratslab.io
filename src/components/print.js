import React from 'react'
const print = () => window.print()
export default function Print({
	children = 'Print',
	...attrs
}){
	return (
		<button
			className='grow'
			onClick={print}
			type='button'
			{...attrs}
		>
			{children}
		</button>
	)
}