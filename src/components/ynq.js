import React from 'react'
const values = {
	no: 0,
	yes: 1,
	other: 2,
	blank: 3,
}
export default class Answer extends React.Component {
	state = {
		note: '',
		value: values.blank,
	}
	label = ({text, v})=>{
		const checked = v === this.state.value
		const name = 'c-'+this.props.name
		const id = name+'-'+v
		return (<div className='flex'>
			<input value={v}
				checked={checked}
				id={id}
				name={name}
				onChange={this.choose}
				type='checkbox'
			/>
			<label htmlFor={id} className={checked?'checked':''}>
				{text}
			</label>
		</div>)
	}
	note = (ev) => {
		this.setState({note: ev.target.value})
	}
	choose = (ev) => {
		const v = parseInt(ev.target.value)
		this.setState({value: (v === this.state.value) ? values.blank : v})
	}
	render(){
		const {children, name, q} = this.props
		const question = children || q
		const note = this.state.note
		const v = this.state.value
		const required_not_blank = v!==1 && v!==values.blank
		const exp = <input placeholder={
				`Explanation${required_not_blank?' required':''}`
			}
			className='ynq-e grow'
			name={'e-'+name}
			onChange={this.note}
			required={required_not_blank}
			value={note}
		/>
		return (
			<div className={['ynq', required_not_blank?'required':''].join(' ')}>
				<div className='ynq-q'>{question}</div>
				<div className='ynq-c flex wrap'>
					{this.label({v:values.yes, text:'Yes'})}
					{this.label({v:values.no, text:'No'})}
					{0===v && exp}
					{this.label({v:values.other, text:'Qualified'})}
					{(v >= 2) && exp}
				</div>
			</div>
		)
	}
}
