import React from 'react'
const h = React.createElement
const d = '.'

export function createHeader({
	prefixes = [],
	suffixes = [d],
	separator = d,
	show = true,
}){
	const show_slice = show ? 0 : -1
	return function H({children, className, id}){
		const ids = id.split(separator)
		const level = Math.min(ids.length, 6) - 1
		const prefix = prefixes[level] || ''
		const suffix = suffixes[level] || ''
		return (
			h('h'+(level+1),
				{
					id:'S'+id,
					className,
				},
				<a href={'#S'+id}>
					{[
						prefix +
							ids.slice(show_slice).join(separator) +
							suffix,
						children && [' ', children],
					]}
				</a>
			)
		)
	}
}
