export function dates(date_string){
	const d = new Date(date_string)
	const day = d.getUTCDate()
	const month_name = d.toLocaleString('default', {month:'long'})
	const year = d.getUTCFullYear()
	const hour = d.getUTCHours()
	const time = `${(hour%12)||12}:${(d.getUTCMinutes()+'').padStart(2,'0')} ${hour>11?'p':'a'}.m.`
	const prev = new Date(year, d.getUTCMonth()-1).valueOf()
	const prev_path = minutes_path(prev)
	return {day, month_name,	prev_path, time, year}
}

export function yyyymmdd(date_string){
	return new Date(date_string.slice(0, 10)).toISOString().slice(0,10)
}

export function dates_offset(date_string){
	let d = new Date(date_string)
	let offset = (date_string.length === 22)
		? new Date(date_string.slice(0, 16)+'Z') - d
		: 0
	const d_offset = d.valueOf() + offset
	return dates(d_offset)
}

export function minutes_path(date_arg){
	const date = new Date(date_arg)
	const path = `/minutes/${date.getUTCFullYear()}/${(date.getUTCMonth()+1+'').padStart(2,'0')}`
	return path
}
