import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React, {createElement as h} from 'react'
import {minutes_path} from './dates'
import {Header} from './shared'

export const resolution_status = {
	proposed: 1,
	adopted: 2,
	rejected: 3,
}
export const resolution_status_names = {
	[resolution_status.proposed]: 'Proposed',
	[resolution_status.adopted]: 'Adopted',
	[resolution_status.rejected]: 'Rejected',
	[resolution_status.withdrawn]: 'Withdrawn',
}
export const resolution_status_icons = {
	[resolution_status.proposed]: '⏳',
	[resolution_status.adopted]: '✔️',
	[resolution_status.rejected]: '❌',
	[resolution_status.withdrawn]: '🧹',
}


export function resolution_status_icon(r){
	return resolution_status_icons[r.status]
}
export function resolution_status_name(r){
	return resolution_status_names[r.status]
}
function resolution_path(r){
	const year = (new Date(r.minutes_date)).getUTCFullYear()
	return `/resolutions/${year}/${r.file}`
}

export function ResolutionHeader({r}){
	return (<>
		<Helmet>
			<title>{r.title}</title>
		</Helmet>
		<Header/>
		<div>
			<LinkMinutes className='block' r={r}/>
		</div>
		<div className="h">
			<h1 className="inline">
				{r.title}
			</h1>
		</div>
	</>)
}

export function LinkMinutes({r, ...props}){
	const path = (r.minutes||{}).path || minutes_path(r.minutes_date)
	return h(Link,
		{
			to: path,
			...props,
		},
		<span className="status">{resolution_status_icon(r)} {resolution_status_name(r)}</span>,
		` ${r.minutes_date}`,
	)
}

export function LinkResolution({r, status=false, ...props}){
	return h(Link, {
		children: `${resolution_status_icon(r)} ${r.title}${status ? ` [${resolution_status_name(r)}]` : ''}`,
		to: resolution_path(r),
		...props,
	})
}

export function LinkResolutionDetail({r, ...props}){
	return h(Link, {
		children: `${r.minutes_date} - ${r.title}`,
		to: resolution_path(r),
		...props,
	})
}
