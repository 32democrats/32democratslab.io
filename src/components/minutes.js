import Helmet from 'react-helmet'
import { Link } from 'gatsby'
import React, {createElement as h} from 'react'
import {dates_offset, minutes_path, yyyymmdd} from './dates'
import {Header} from './shared'

const length = 4 * 60 * 60 * 1000
export const minutes_type = {
	general: 0,
	endorsement: 1,
	reorganization: 2,
}
export const minutes_type_names = {
	[minutes_type.general]: 'Meeting',
	[minutes_type.endorsement]: 'Endorsement Meeting',
	[minutes_type.reorg]: 'Reorganization Meeting',
}
export const minutes_status = {
	placeholder: 0,
	agenda: 1,
	draft: 2,
	approved: 3,
}
export const minutes_status_names = {
	[minutes_status.placeholder]: 'Placeholder Agenda',
	[minutes_status.agenda]: 'Agenda',
	[minutes_status.draft]: 'Draft',
	[minutes_status.approved]: 'Approved',
	[minutes_status.canceled]: 'Canceled',
	[minutes_status.not_submitted]: 'Not Submitted',
}

function concluded(date){
	return (Date.now() > (new Date(date)).valueOf()+length)
}
export function minutes_name(m){
	return `${m.remote ? 'Remote ' : ''}${minutes_type_names[m.type]}`
}
export function minutes_name_date(m){
	const {day, month_name, year} = dates_offset(m.date)
	return `${minutes_name(m)}, ${month_name} ${day}, ${year}`
}
export function minutes_date_name(m){
	const {day, month_name, time} = dates_offset(m.date)
	return `${month_name} ${day} ${minutes_name(m)}, ${time}`
}
export function minutes_type_name(m){
	return minutes_type_names[m.type]
}
export function minutes_status_name(m){
	return ((m.status === minutes_status.agenda) && concluded(m.date)) ? 'Drafting' : minutes_status_names[m.status]
}

export function LinkMinutes({m, ...props}){
	const path = minutes_path(m.date)
	return h(Link, {
		children: `${yyyymmdd(m.date)} ${(m.type === minutes_type.general) ? '' : minutes_name(m)} ${m.status === minutes_status.approved ? '' : `(${minutes_status_name(m)})`}`,
		to: m.path || path,
		...props,
	})
}
export function LinkMinutesDetail({m, ...props}){
	const path = minutes_path(m.date)
	return h(Link, {
		children: `${minutes_date_name(m)}`,
		to: m.path || path,
		...props,
	})
}
export function MinutesHeader({m}){
	return (<>
		<Helmet>
			<title>{yyyymmdd(m.date)} {minutes_type_name(m)}</title>
		</Helmet>
		<Header/>
		<div className="h">
			<h1 className="inline">
				{minutes_name_date(m)}
			</h1>
			<span className="status hstatus">
				{minutes_status_name(m)}
			</span>
		</div>
	</>)
}
