import Helmet from 'react-helmet'
import Menu from './menu'
import React, {useEffect, useRef} from 'react'
import './style.css'
import theme_dark from './dark'
import theme_light from './light'

const Page = ({ children, title }) => {
	let light = false
	if(typeof window !== 'undefined'){
		light = !!(window && window.matchMedia('(prefers-color-scheme: light)').matches)
	}
	const theme = light ? theme_light : theme_dark
	const main = useRef()
	useEffect(()=>{
		main.current.focus()
	}, [children])
	return (
		<div className='body'>
			<Helmet defaultTitle='32nd LD Democrats'>
				<meta name='theme-color' content={theme.color}/>
				<link rel='icon' href='/img/ico/96.png' sizes='96x96'/>
				<link rel='icon' href='/img/ico/192.png' sizes='192x192'/>
				<link rel='apple-touch-icon' href='/img/ico/180.png'/>
				<style media="(prefers-color-scheme: dark)">{theme_dark.style}</style>
				<style media="(prefers-color-scheme: no-preference), (prefers-color-scheme: light)">{theme_light.style}</style>
				<title>{title}</title>
			</Helmet>
			<div
				ref={main}
				className={['main', light?'light':'dark'].join(' ')}
				/* eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex */
				tabIndex='0'
			>
				{children}
			</div>
			<Menu theme={theme}/>
		</div>
	)
}

export default Page
