import { Link } from 'gatsby'
import React from 'react'
import {base} from '../../config'
import logo from '../logo.js'

const Menu = ({theme}) => (
<ul className='menu body-menu no-print'>
	<li><a href="/" className="logo-link">
		<span className="logo-box">
			<span className="logotype">
				<div className="logo-img" title="D">
					{logo({fill:theme.logo2})}
				</div>
				<span className="logotext">
					32
				</span>
			</span>
		</span>
	</a></li>
	<li><Link to="/bylaws">Bylaws</Link></li>
	<li><a href="https://calendar.google.com/calendar/embed?src=calendar%4032democrats.org&ctz=America%2FLos_Angeles">Calendar</a></li>
	<li><Link to="/contacts">Contacts</Link></li>
	<li><Link to="/documents">Documents</Link></li>
	<li><a className="donate" href="https://secure.actblue.com/donate/32nd-legislative-district-democrats-1"><span>Donate</span></a></li>
	<li><Link to="/elected-officials">Elected Officials</Link></li>
	<li><Link to="/endorsements">Endorsements</Link></li>
	<li><Link to="/events">Events</Link></li>
	<li><a href="https://www.facebook.com/32democrats.org/">Facebook</a></li>
	<li><a href="https://app.leg.wa.gov/DistrictFinder/">Map</a></li>
	<li><Link to="/membership">Membership</Link></li>
	<li><Link to="/minutes">Minutes</Link></li>
	<li><a href={base}>News</a></li>
	<li><Link to="/platform/2018">Platform</Link></li>
	<li><Link to="/pco" title="Precinct Committee Officers">PCO</Link></li>
	<li><Link to="/resolutions">Resolutions</Link></li>
	<li><Link to="/rules">Rules</Link></li>
	<li><a href="https://gitlab.com/32democrats/32democrats.gitlab.io">Source Code</a></li>
	<li><a href="https://twitter.com/32dems">Twitter</a></li>
</ul>
)

export default Menu
