const color = '#001020'
const logo2 = '#BF0D3E'
const style = `
@media screen {
	:root {
	--a: #FF6080;
	--a-active-back: #003060;
	--a-back: #001020;
	--a-back-hover: #002040;
	--back: #000810;
	--blue: #003399;
	--border: #304050;
	--color: #D0E0FF;
	--dd-border: #2F030F;
	--donate-back: rgba(255,96,128,.2);
	--event: #BF0D3E;
	--highlight: #2F030F;
	--hr: rgba(255,255,255,.1);
	--label: #111;
	--label-hover: #123;
	--logo2: ${logo2};
	--logo-back: #041E42;
	--menu-input-border: #808080;
	--money: #444;
	--placeholder: #C0C0C0;
	--scroll: rgba(0,128,255,.1);
	--scroll-back: #000810;
}
}
`
export default {color, logo2, style}
