const color = '#F0F8FF'
const logo2 = '#C00000'
const style = `
@media screen {
	:root {
	--a: #003399;
	--a-active-back: #C0D0FF;
	--a-back: #F0F8FF;
	--a-back-hover: #D0E0FF;
	--back: #FFF;
	--blue: #003399;
	--border: #000;
	--color: #000;
	--dd-border: #EEE;
	--donate-back: rgba(0,156,222,.2);
	--event: #800;
	--highlight: #FFA;
	--hr: #C0C0C0;
	--label: #EEE;
	--label-hover: #E0E0E0;
	--logo2: ${logo2};
	--logo-back: #003399;
	--menu-input-border: #808080;
	--money: #BFB;
	--placeholder: #C0C0C0;
	--scroll: #D0E0FF;
	--scroll-back: #F0F8FF;
}
}
`
export default {color, logo2, style}
