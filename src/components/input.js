import React from 'react'

export default function Input({label, ...attrs}){ return (
	<label className='label'>
		{label}
		<input className='grow' {...attrs}/>
	</label>
)}
