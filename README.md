[Gatsby](https://www.gatsbyjs.org/) website

# GitLab
[GitLab Pages](https://pages.gitlab.io).

Static pages are built by [GitLab CI](https://about.gitlab.com/gitlab-ci/)
according to [`.gitlab-ci.yml`](.gitlab-ci.yml).

You may edit content with the [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/).
The drawback is that no preview is available.

# Development
1. Install [NodeJS](https://nodejs.org/).
1. Fork, clone or download this project.
1. `npm install` Install package dependencies in the project directory.
1. `npm start` Generate the website with hot-reloading.
1. http://localhost:8000/ Preview the website.
1. Edit source files.

[Gatsby documentation](https://www.gatsbyjs.org/docs/)
