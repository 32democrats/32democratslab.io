module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: `32nd District Democrats`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.md`, `.mdx`],
      },
    },
  ],
}
